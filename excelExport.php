Generating Excel File..
<!-- Progress bar holder -->
<div id="progress" style="width:500px;border:1px solid #ccc;"></div>
<!-- Progress information -->
<div id="information" style="width"></div>


<?php
ob_start();


include 'config.php';
include 'assets/php/PHPExcel/Classes/PHPExcel.php';

//set memory limit
ini_set('memory_limit', '2048M');
					
					
//data query
$query = "
SELECT
first_name AS 'First Name',
last_name AS 'Last Name',
email AS 'E-mail Address',
contact_num1 AS 'Contact Number',
address AS 'Address',
address_city AS 'City'
FROM pos_client
";
		
		
//Excel Header
$headings1 = array();


//measure time of execution
$time_start = microtime(true);

if (($result = mysqli_query($link,$query) or die(mysqli_error())

)) { 

    // Create a new PHPExcel object 
    $objPHPExcel = new PHPExcel(); 
	
	//SET TITLE OF WORKSHEET
    $objPHPExcel->getActiveSheet()->setTitle('Data');

    $rowNumber = 1; 
    $col = 'A'; 
	
	
	$row_count = mysqli_num_rows($result);
	$j = 0;
	
	//GET THE QUERY TABLE HEADER TITLE AND SET TO EXCEL 
	while($row = mysqli_fetch_field($result)) {
			
		$headings[] =  $row->name;
		
	}
	
	
	
    foreach($headings as $heading) { 
       $objPHPExcel->getActiveSheet()->setCellValue($col.$rowNumber,$heading); 
	   
	   
	     //autosize column witdth
	   $objPHPExcel->getActiveSheet()
        ->getColumnDimension($col)
        ->setAutoSize(true);
		
		
	   //set cell color 
	   //cellColor('A'.$rowNumber.':'.$col.$rowNumber, 'F28A8C');
       $col++; 
    } 

    // Loop through the result set 
    $rowNumber = 2; 
    while ($row = mysqli_fetch_row($result)) { 
       $col = 'A'; 
       foreach($row as $cell) { 
          $objPHPExcel->getActiveSheet()->setCellValueExplicit($col.$rowNumber,$cell); 
          $col++; 
       } 
       $rowNumber++; 
	   
	   
	   
	   //==================================================================================
			 	$j++;
				$percent = intval($j/$row_count * 100)."%";
				
				// Javascript for updating the progress bar and information
    			echo '<script language="javascript">
    			document.getElementById("progress").innerHTML="<div style=\"width:'.$percent.';background-color:#ddd;\">&nbsp;</div>";
    			document.getElementById("information").innerHTML="' . number_format($j) . ' of ' . number_format($row_count) . ' row(s) processed . <br>' . $percent . ' completed.";
    			</script>';
				ob_flush();

				// Sleep one second so we can see the delay
    			//sleep(1);
	//==================================================================================
	   
   
	   
    } 
	

	// Set autofilter range
	$objPHPExcel->getActiveSheet()->setAutoFilter($objPHPExcel->getActiveSheet()->calculateWorksheetDimension());

	// Set active filters
	$autoFilter = $objPHPExcel->getActiveSheet()->getAutoFilter();

	 // Freeze pane so that the heading line won't scroll 
    $objPHPExcel->getActiveSheet()->freezePane('A2');
	
	//================= END OF WORKSHEET 1==============================
	
	
	//set active sheet to sheet1(first sheet)
	$objPHPExcel->setActiveSheetIndex(0);
	

    // Save as an Excel BIFF (xls) file 
	
	/*
	FILE TYPE
	
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');  ==> EXCEL
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');  ==> CSV	
	*/
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV'); 

   //header('Content-Type: application/vnd.ms-excel'); 
   //header('Content-Disposition: attachment;filename="rawdata_activations.xls"'); 
   //header('Cache-Control: max-age=0'); 

   //$objWriter->save('php://output');
   
   /*
   SAVE THE EXCEL FILE LOCATION and FILE NAME
   example:
   @location - data_excel/outright_price_bracket/
   @file name - opb_20150604.xlsx
   
   NOTE: change file extension accoring to selected file type
   
   */
   $objWriter->save('test.csv');
   
    $time_end = microtime(true);
	$execution_time = ($time_end - $time_start)/60;
	
	//Display to browser that the process completed
	echo '<script language="javascript">document.getElementById("information").innerHTML="Process completed. <br> Execution Time: ' . number_format($execution_time,2) . ' mins"</script>';
	ob_end_flush();
   
   
	echo '<b>done</b>';
	
	
	//SET THE NEXT RUN
	//echo '<meta http-equiv="Refresh" content="5; url=mail_outright_price_point.php">';

	
	
	exit;
} 
echo 'a problem has occurred... no data retrieved from the database! retrying'; 

//RETRY HEADER
//SHOUD BE THE NAME OF php file
//echo '<meta http-equiv="Refresh" content="5; url=mail_outright_price_point.php">';


?>