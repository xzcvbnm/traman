-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 23, 2015 at 03:24 AM
-- Server version: 5.6.24
-- PHP Version: 5.5.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `host_claygo`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-add_client_credit`(

IN id VARCHAR(255),
IN amount DECIMAL(10,2),
IN user_id VARCHAR(255)


)
BEGIN 

INSERT INTO pos_client_credit (credit_id, client_id, amount, user_id)
VALUES (CONCAT(NOW(), @random, UNIX_TIMESTAMP()), id, amount, user_id);

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-add_item`(

IN item_name VARCHAR(255)

)
BEGIN 

INSERT INTO pos_item (item_name) 
VALUES (item_name);

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-add_service_price`( 

IN code VARCHAR(50),
IN name VARCHAR(50),
IN price VARCHAR(50),
IN unit VARCHAR(50),
IN user_id VARCHAR(50)

)
BEGIN 


INSERT INTO pos_service_and_price (code, name, price, unit, add_by_user_id) 
VALUES (code, name, price, unit, user_id);

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-add_special`(



IN name  VARCHAR(100),
IN value  VARCHAR(100),
IN special_type  VARCHAR(100),
IN value_type  VARCHAR(100),
IN user_id  VARCHAR(100)

)
BEGIN 

INSERT INTO pos_special (name, value, special_type, value_type, add_by_user_id) 
VALUES (name, value, special_type, value_type, user_id);

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-add_store`(


IN store_code  VARCHAR(100),
IN store_name VARCHAR(100),
IN address VARCHAR(100),
IN address_city VARCHAR(100),
IN num_tel VARCHAR(100),
IN num_cell VARCHAR(100),

IN user_id VARCHAR(100),
IN surcharge VARCHAR(100)

)
BEGIN 



INSERT INTO pos_store (store_code, store_name, address, address_city, num_tel, num_cell, add_by_user_id, surcharge_ref) 
VALUES (store_code, store_name, address, address_city, num_tel, num_cell, user_id, surcharge);

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-add_user_admin`(

IN username  VARCHAR(100),
IN password VARCHAR(100),
IN first_name VARCHAR(100),
IN middle_name VARCHAR(100),
IN last_name VARCHAR(100)


)
BEGIN 


INSERT INTO pos_user (store_id, username, password, first_name, middle_name, last_name, user_type) 
VALUES ('0', username, password, first_name, middle_name, last_name, 'ADMIN')

;




END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-add_user_store`(

IN username  VARCHAR(100),
IN password VARCHAR(100),

IN first_name VARCHAR(100),
IN middle_name VARCHAR(100),
IN last_name VARCHAR(100),
IN store_id VARCHAR(100)


)
BEGIN 

INSERT INTO pos_user (store_id, username, password, first_name, middle_name, last_name, user_type) 
VALUES (store_id, username, password, first_name, middle_name, last_name, 'STORE');

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-approve_void_transaction`(

IN id VARCHAR(100),
IN user_id VARCHAR(100),
IN status VARCHAR(100)

)
BEGIN 

UPDATE pos_trans_master 
SET status = status 
WHERE trans_master_id = id; 


INSERT INTO pos_trans_trail (trans_master_id, status, user_store_id, user_admin_id) 
VALUES (id, status, 'NA', user_id);

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-daily_report`(

IN datefrom date,
IN dateto date



)
BEGIN


SET SESSION group_concat_max_len = 1000000;
SET @header = NULL;
SET @query = NULL;


SELECT 
GROUP_CONCAT(DISTINCT
CONCAT('SUM(CASE WHEN pos_trans_detail.service_id =\'', pos_trans_detail.service_id, '\' THEN TRIM(TRAILING \'.\' FROM TRIM(TRAILING \'0\' from pos_trans_detail.unit_count)) ELSE 0 END) AS \'', code, '\'')
) INTO @header
FROM pos_service_and_price 

INNER JOIN pos_trans_detail 
ON pos_service_and_price.service_id = pos_trans_detail.service_id

WHERE code != ''
;


SET @query = CONCAT(
'
SELECT 
pos_trans_master.trans_master_id as \'ID\',
CONCAT(pos_client.first_name, \' \', pos_client.middle_name, \' \', pos_client.last_name) AS \'Customer\',
pos_store.store_name AS \'Location\',
pos_trans_master.record_datetime AS \'Date Recieve\',

pos_trans_master.due_date AS \'Date Due\',

pos_trans_master.total AS \'Payment\',


CASE WHEN
pos_trans_master.status = \'complete - paid\' 
THEN \'paid\' ELSE \'unpaid\' 
END AS \'Paid/Unpaid\',



', @header, '




FROM pos_trans_master 

LEFT JOIN pos_store 
ON pos_trans_master.store_id = pos_store.store_id 


LEFT JOIN pos_client 
ON pos_trans_master.client_id = pos_client.client_id 

LEFT JOIN pos_trans_detail 
ON pos_trans_master.trans_master_id = pos_trans_detail.trans_master_id 

LEFT JOIN pos_service_and_price 
ON pos_trans_detail.service_id = pos_service_and_price.service_id

WHERE 
(pos_trans_master.record_date BETWEEN \'',  datefrom, '\' AND \'', dateto, '\') 
AND pos_trans_master.status NOT LIKE \'%cancelled%\' 
AND pos_trans_master.status NOT LIKE \'%void%\'


GROUP BY pos_trans_master.trans_master_id

'


); 


PREPARE stmt FROM @query;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

#SELECT @query;



END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-dashboard-display_customer_sales_analytics`(

IN monthNow VARCHAR(10),
IN yearNow VARCHAR(10)


)
BEGIN 

SELECT 


SUM(CASE WHEN MONTH(pos_client.add_datetime) = monthNow AND YEAR(pos_client.add_datetime) = yearNow THEN 1 ELSE 0 END) AS 'client_count_new',
SUM(CASE WHEN MONTH(pos_client.add_datetime) != monthNow AND YEAR(pos_client.add_datetime) != yearNow THEN 1 ELSE 0 END) AS 'client_count_existing',
COUNT(pos_client.client_id) AS 'client_count_total',


SUM(
CASE 
WHEN 
(MONTH(pos_client.add_datetime) = monthNow AND YEAR(pos_client.add_datetime) = yearNow)
AND pos_trans_master.status = 'complete - paid'
THEN pos_trans_master.total
ELSE 0 
END
) AS 'client_paid_sales_new',


SUM(
CASE 
WHEN 
(MONTH(pos_client.add_datetime) != monthNow AND YEAR(pos_client.add_datetime) != yearNow)
AND pos_trans_master.status = 'complete - paid'
THEN pos_trans_master.total
ELSE 0 
END
) AS 'client_paid_sales_existing',

SUM(
CASE 
WHEN 
pos_trans_master.status = 'complete - paid'
THEN pos_trans_master.total
ELSE 0 
END
) AS 'client_paid_sales_total',

###########
SUM(
CASE 
WHEN 
(MONTH(pos_client.add_datetime) = monthNow AND YEAR(pos_client.add_datetime) = yearNow)
AND pos_trans_master.status = 'complete - unpaid'
THEN pos_trans_master.total

ELSE 0 
END
) AS 'client_unpaid_sales_new',



SUM(
CASE 
WHEN 
(MONTH(pos_client.add_datetime) != monthNow AND YEAR(pos_client.add_datetime) != yearNow)
AND pos_trans_master.status = 'complete - unpaid'
THEN pos_trans_master.total
ELSE 0 
END
) AS 'client_unpaid_sales_existing',


SUM(
CASE 
WHEN 
pos_trans_master.status = 'complete - unpaid'
THEN pos_trans_master.total
ELSE 0 
END
) AS 'client_unpaid_sales_total'

FROM pos_client 

LEFT JOIN pos_trans_master 
ON pos_client.client_id = pos_trans_master.client_id;


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-dashboard-display_month_daily_sales_count`(

IN datefrom date,
IN dateto date

)
BEGIN 


SELECT 
DATE_FORMAT(pos_trans_master.record_date,'%b-%d') AS 'day',
SUM(pos_trans_master.total) AS 'total',
SUM(CASE WHEN pos_trans_master.status = 'complete - paid' THEN pos_trans_master.total ELSE 0 END) AS 'total_paid',
SUM(CASE WHEN pos_trans_master.status = 'complete - unpaid' THEN pos_trans_master.total ELSE 0 END) AS 'total_unpaid'

FROM pos_trans_master 

WHERE status LIKE '%complete%' 
AND (pos_trans_master.record_date BETWEEN datefrom AND dateto)
GROUP BY DATE_FORMAT(pos_trans_master.record_date,'%d');

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-dashboard-display_month_sales_count`(

IN datefrom date,
IN dateto date

)
BEGIN 

SELECT 
SUM(pos_trans_master.total) AS 'total',
SUM(CASE WHEN pos_trans_master.status = 'complete - paid' THEN pos_trans_master.total END) AS 'total_paid',
SUM(CASE WHEN pos_trans_master.status = 'complete - unpaid' THEN pos_trans_master.total END) AS 'total_unpaid'

FROM pos_trans_master 

WHERE pos_trans_master.status LIKE '%complete%'  
AND (pos_trans_master.record_date BETWEEN datefrom AND dateto);

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-dashboard-display_store_contribution`(

IN datefrom date,
IN dateto date

)
BEGIN

SELECT 
pos_store.store_code,
SUM(pos_trans_master.total) AS 'total',
SUM(CASE WHEN pos_trans_master.status = 'complete - paid' THEN pos_trans_master.total END) AS 'total_paid',
SUM(CASE WHEN pos_trans_master.status = 'complete - unpaid' THEN pos_trans_master.total 
END) AS 'total_unpaid'

FROM pos_trans_master 

LEFT JOIN pos_store 
ON pos_trans_master.store_id = pos_store.store_id

WHERE pos_trans_master.status LIKE '%complete%' 
AND (pos_trans_master.record_date BETWEEN datefrom AND dateto)
GROUP BY pos_store.store_id;


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-dashboard-hourly_sales`(

IN datefrom date,
IN dateto date

)
BEGIN 

SELECT 
DATE_FORMAT(record_datetime, '%l%p') AS 'hour_time',
SUM(pos_trans_master.total) AS 'total',

SUM(CASE WHEN pos_trans_master.status = 'complete - paid' THEN pos_trans_master.total END) AS 'total_paid',
SUM(CASE WHEN pos_trans_master.status = 'complete - unpaid' THEN pos_trans_master.total END) AS 'total_unpaid'
FROM pos_trans_master 

WHERE pos_trans_master.record_date = dateto

GROUP BY DATE_FORMAT(record_datetime, '%l%p');



END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-delete_item`(


IN id VARCHAR(50)

)
BEGIN


DELETE FROM pos_item 
WHERE item_id = id;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-delete_service_price`(

IN id VARCHAR(255)

)
BEGIN 

UPDATE pos_service_and_price 
SET status = 'inactive' 
WHERE service_id = id;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-delete_special`(

IN id  VARCHAR(100)

)
BEGIN

UPDATE pos_special 
SET status = 'inactive' 
WHERE special_id = id;


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-delete_store`(

IN id  VARCHAR(100),
IN user_id  VARCHAR(100)

)
BEGIN 


UPDATE pos_store 
SET status = 'inactive', 
status_change_by_user_id = user_id,
status_change_datetime = CURRENT_TIMESTAMP 
WHERE store_id = id;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-delete_user_admin`(




IN id  VARCHAR(100)

)
BEGIN 


UPDATE pos_user 
SET status = 'inactive',
status_change_datetime = CURRENT_TIMESTAMP 
WHERE user_id = id;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-delete_user_store`(

IN id  VARCHAR(100)

)
BEGIN 


UPDATE pos_user 
SET status = 'inactive',
status_change_datetime = CURRENT_TIMESTAMP 
WHERE user_id = id;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-display_client_all`()
BEGIN 

SELECT 
pos_client.client_id,
pos_client.address_state,
CONCAT(pos_client.first_name, ' ', pos_client.last_name) AS 'Customer Name',
pos_client.email AS 'Email',
pos_client.contact_num1 AS 'Contact No#',

CASE 
WHEN pos_client_credit.amount IS NULL THEN 0 
ELSE pos_client_credit.amount
END
AS 'Credit Balance'
FROM pos_client 

LEFT JOIN 
(

SELECT 
pos_client_credit.client_id,
SUM(pos_client_credit.amount) AS 'Amount'
FROM pos_client_credit 

GROUP BY pos_client_credit.client_id

) AS pos_client_credit 

ON  pos_client.client_id = pos_client_credit.client_id


WHERE status = 'active';


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-display_client_credit_memo_history`(

IN id VARCHAR(255)

)
BEGIN 

SELECT 
amount AS 'Amount',
record_datetime AS 'Record Datetime'
FROM pos_client_credit 
WHERE client_id = id;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-display_discount_all`()
BEGIN 

SELECT 
special_id,
name AS 'Name',
value AS 'Value',
value_type AS 'Value Type' 
FROM pos_special 
WHERE special_type = 'discount' 
AND status = 'active';


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-display_item_all`()
BEGIN 

SELECT 
item_id AS 'ID',
item_name AS 'Item Name' 
FROM pos_item;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-display_markup_all`()
BEGIN 


SELECT 
special_id,
name AS 'Name',

value AS 'Value',
value_type AS 'Value Type' 
FROM pos_special 
WHERE special_type = 'MARK-UP' 
AND status = 'active';

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-display_paid_transaction_all`(

IN start_date date,
IN end_date date

)
BEGIN 

SELECT 

pos_trans_master.trans_master_id,
pos_trans_master.central_code AS 'Central Code',
pos_trans_master.local_code AS 'Local Code',

pos_store.store_name AS 'Store',
CONCAT(pos_user.first_name, ' ', pos_user.last_name) AS 'Cashier',
CONCAT(pos_client.first_name, ' ', pos_client.last_name) AS 'Client',
DATE_FORMAT(pos_trans_master.record_datetime, '%Y-%m-%d') AS 'Date' 
FROM pos_trans_master 

LEFT JOIN pos_client 

ON pos_trans_master.client_id = pos_client.client_id 

LEFT JOIN pos_store 
ON pos_trans_master.store_id = pos_store.store_id 

LEFT JOIN pos_user 
ON pos_trans_master.user_id = pos_user.user_id

WHERE pos_trans_master.status = 'complete - paid'
AND (DATE_FORMAT(pos_trans_master.record_datetime, '%Y-%m-%d') BETWEEN start_date AND end_date)

;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-display_pending_void_transaction_all`()
BEGIN 

SELECT 
pos_trans_master.trans_master_id AS 'ID',

pos_store.store_name AS 'Store',
CONCAT(pos_user.first_name, ' ', pos_user.last_name) AS 'Cashier',

CONCAT(pos_client.first_name, ' ', pos_client.last_name) AS 'Client',
DATE_FORMAT(pos_trans_master.record_datetime, '%Y-%m-%d') AS 'Date',
pos_trans_master.status AS 'Status'

FROM pos_trans_master 

LEFT JOIN pos_client 
ON pos_trans_master.client_id = pos_client.client_id 

LEFT JOIN pos_store 
ON pos_trans_master.store_id = pos_store.store_id 

LEFT JOIN pos_user 
ON pos_trans_master.user_id = pos_user.user_id

WHERE pos_trans_master.status LIKE '%pending void%';

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-display_service_price_all`()
BEGIN 

SELECT 
service_id AS 'ID',
code AS 'Service Code',
name AS 'Name',
CONCAT(price, '/', unit) AS 'Price/Unit'
FROM pos_service_and_price 
WHERE status = 'active';

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-display_store_all`()
BEGIN

SELECT 
store_id,
address_city,
store_code AS 'Store Code',
store_name  AS 'Store Name',
surcharge_ref AS 'Surcharge Reference'
FROM pos_store 
WHERE store_id != 0 
AND status != 'inactive';

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-display_unpaid_transaction_all`(

IN start_date date,

IN end_date date

)
BEGIN  


SELECT 
pos_trans_master.trans_master_id,
pos_trans_master.central_code AS 'Central Code',
pos_trans_master.local_code AS 'Local Code',

pos_store.store_name AS 'Store',
CONCAT(pos_user.first_name, ' ', pos_user.last_name) AS 'Cashier',
CONCAT(pos_client.first_name, ' ', pos_client.last_name) AS 'Client',
DATE_FORMAT(pos_trans_master.record_datetime, '%Y-%m-%d') AS 'Date' 
FROM pos_trans_master 

cross join (select @rownum := 0) r

LEFT JOIN pos_client 
ON pos_trans_master.client_id = pos_client.client_id 

LEFT JOIN pos_store 
ON pos_trans_master.store_id = pos_store.store_id 

LEFT JOIN pos_user 
ON pos_trans_master.user_id = pos_user.user_id

WHERE pos_trans_master.status = 'complete - unpaid'
AND (DATE_FORMAT(pos_trans_master.record_datetime, '%Y-%m-%d') BETWEEN start_date AND end_date)

;



END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-display_user_admin_all`(

IN id  VARCHAR(100)

)
BEGIN 

SELECT 
user_id AS 'User ID',
CONCAT(first_name, ' ', last_name) AS 'Name'



FROM pos_user  
WHERE user_id != 0 
AND user_type = 'ADMIN' 
AND status = 'active' 
and user_id != id;


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-display_user_store_all`()
BEGIN 



SELECT 
pos_user.user_id,
pos_store.store_name,
CONCAT(first_name, ' ', last_name) AS 'Name & Store designation',
pos_user.username AS 'Username'



FROM pos_user 
LEFT JOIN pos_store 

ON pos_user.store_id = pos_store.store_id

WHERE user_type = 'STORE' 
AND pos_user.status = 'active';




END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-load_store_list_option`()
BEGIN


SELECT 
store_id,
store_code,
store_name 
FROM pos_store
WHERE store_id != 0 
AND status = 'active';


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-update_profile`(

IN id  VARCHAR(100),
IN username  VARCHAR(100),
IN first_name  VARCHAR(100),
IN middle_name  VARCHAR(100),
IN last_name  VARCHAR(100)

)
BEGIN 

UPDATE pos_user 
SET username = username,
first_name = first_name,
middle_name = middle_name,
last_name = last_name 
WHERE user_id = id;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-update_service_price`(

IN update_id VARCHAR(255),
IN code VARCHAR(255),
IN name VARCHAR(255),
IN price VARCHAR(255),
IN unit VARCHAR(255)

)
BEGIN 


UPDATE pos_service_and_price 
SET code = code,
name = name,
price = price,
unit = unit 
WHERE service_id = update_id;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-update_special`(

IN id  VARCHAR(100),
IN name  VARCHAR(100),
IN value  VARCHAR(100),
IN special_type  VARCHAR(100),
IN value_type  VARCHAR(100),
IN user_id VARCHAR(100)

)
BEGIN 

UPDATE pos_special 

SET status = 'inactive' 
WHERE special_id = id; 

INSERT INTO pos_special (name, value, special_type, value_type, add_by_user_id) 
VALUES (name, value, special_type, value_type, user_id);

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-update_store`(

IN id  VARCHAR(100),
IN store_code  VARCHAR(100),
IN store_name VARCHAR(100),
IN address VARCHAR(100),
IN address_city VARCHAR(100),

IN num_tel VARCHAR(100),
IN num_cell VARCHAR(100),
IN surcharge VARCHAR(100)

)
BEGIN 


UPDATE pos_store 
SET store_code = store_code, 
store_name = store_name,
address = address,
address_city = address_city,
num_tel = num_tel,
num_cell = num_cell ,
surcharge_ref = surcharge
WHERE store_id = id;



END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-update_user_admin`(

IN id  VARCHAR(100),
IN username  VARCHAR(100),
IN first_name VARCHAR(100),
IN middle_name VARCHAR(100),
IN last_name VARCHAR(100)

)
BEGIN 

UPDATE pos_user 
SET username = username,
first_name = first_name,
middle_name = middle_name,
last_name = last_name 
WHERE user_id = id;


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-update_user_admin_password`(

IN id  VARCHAR(100),
IN new_pw  VARCHAR(100)

)
BEGIN 

UPDATE pos_user 
SET password = new_pw,
require_new_pw = 'YES'
WHERE user_id = id;


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-update_user_store`(

IN id  VARCHAR(100),
IN username  VARCHAR(100),
IN first_name VARCHAR(100),
IN middle_name VARCHAR(100),
IN last_name VARCHAR(100),
IN store_id VARCHAR(100)


)
BEGIN 

UPDATE pos_user 
SET 
store_id = store_id,
username = username,
first_name = first_name,
middle_name = middle_name,
last_name = last_name

WHERE user_id = id;



END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-update_user_store_password`(

IN id  VARCHAR(100),
IN new_pw  VARCHAR(100)

)
BEGIN


UPDATE pos_user 
SET password = new_pw,
require_new_pw = 'YES'
WHERE user_id = id;


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-view_profile`(

IN id  VARCHAR(100)

)
BEGIN 


SELECT 
pos_user.username,
pos_user.first_name,
pos_user.middle_name,
pos_user.last_name,
pos_store.store_name 
FROM pos_user 


LEFT JOIN pos_store 

ON pos_user.store_id = pos_store.store_id 
WHERE pos_user.user_id = id;



END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-view_service_and_price_details`( 

IN id VARCHAR(255)

)
BEGIN 

SELECT 
pos_service_and_price.service_id,
pos_service_and_price.code,

pos_service_and_price.name,
pos_service_and_price.price,
pos_service_and_price.unit,
CONCAT(pos_user.first_name, ' ', pos_user.last_name) AS 'add_by', 
pos_service_and_price.add_datetime 
FROM pos_service_and_price 

LEFT JOIN pos_user 
ON pos_service_and_price.add_by_user_id = pos_user.user_id

WHERE pos_service_and_price.service_id = id;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-view_special_details`(

IN id  VARCHAR(100)

)
BEGIN 

SELECT 
pos_special.special_id,
pos_special.name,
pos_special.value,
pos_special.special_type,
pos_special.value_type,
CONCAT(pos_user.first_name, ' ', pos_user.last_name) AS 'add_by',
pos_special.add_datetime 

FROM pos_special 

LEFT JOIN pos_user 
ON pos_special.add_by_user_id = pos_user.user_id  


WHERE pos_special.special_id = id;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-view_store_details`(

IN id  VARCHAR(100)

)
BEGIN 


SELECT 

pos_store.store_id,
pos_store.store_code,
pos_store.store_name,
pos_store.address,
pos_store.address_city,
pos_store.num_tel,
pos_store.num_cell,
CONCAT(pos_user.first_name, ' ', pos_user.last_name) AS 'add_by',
pos_store.add_datetime,
pos_store.surcharge_ref

FROM pos_store

LEFT JOIN pos_user 
ON pos_store.add_by_user_id = pos_user.user_id 

WHERE pos_store.store_id = id;




END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-view_user_admin_details`(

IN id  VARCHAR(100)

)
BEGIN

SELECT 
user_id,
username,
first_name,
middle_name,
last_name 
FROM pos_user 
WHERE user_id = id;


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-view_user_store_details`(

IN id  VARCHAR(100)

)
BEGIN 

SELECT 
user_id,
store_id,
username,
first_name,
middle_name,
last_name 
FROM pos_user 
WHERE user_id = id;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-add_client`(


IN id VARCHAR(255),
IN first_name  VARCHAR(100),
IN middle_name  VARCHAR(100),
IN last_name  VARCHAR(100),
IN contact_num1  VARCHAR(100),
IN contact_num2  VARCHAR(100),
IN email  VARCHAR(100),
IN address  VARCHAR(100),
IN address_city  VARCHAR(100),
IN address_country  VARCHAR(100),
IN address_state  VARCHAR(100),
IN user_id  VARCHAR(100),
IN client_type  VARCHAR(100),
IN company_name  VARCHAR(100),
IN fax  VARCHAR(100)


)
BEGIN 


INSERT INTO pos_client (client_id, first_name, middle_name, last_name, contact_num1, contact_num2, email, address, address_city, address_country, address_state, add_by_user_id, client_type, company_name, fax) 
VALUES (id, first_name, middle_name, last_name, contact_num1, contact_num2, email, address, address_city, address_country, address_state, user_id, client_type, company_name, fax);

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-check_client_exist`(

IN fname VARCHAR(255),
IN mname VARCHAR(255),
IN lname VARCHAR(255)

)
BEGIN 

SELECT * FROM pos_client 
WHERE first_name LIKE CONCAT('%', fname, '%') 
AND middle_name LIKE CONCAT('%', mname, '%') 
AND last_name LIKE CONCAT('%', lname, '%');


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-daily_report`(

IN datefrom date,
IN dateto date,
IN id VARCHAR(10)

)
BEGIN 


SET SESSION group_concat_max_len = 1000000;
SET @header = NULL;
SET @query = NULL;

SELECT 
GROUP_CONCAT(DISTINCT
CONCAT('SUM(CASE WHEN pos_trans_detail.service_id =\'', pos_trans_detail.service_id, '\' THEN TRIM(TRAILING \'.\' FROM TRIM(TRAILING \'0\' from pos_trans_detail.unit_count)) ELSE 0 END) AS \'', code, '\'')
) INTO @header
FROM pos_service_and_price 

INNER JOIN pos_trans_detail 
ON pos_service_and_price.service_id = pos_trans_detail.service_id

WHERE code != ''
;


SET @query = CONCAT(
'
SELECT 
pos_trans_master.trans_master_id as \'ID\',
CONCAT(pos_client.first_name, \' \', pos_client.middle_name, \' \', pos_client.last_name) AS \'Customer\',
pos_store.store_name AS \'Location\',
pos_trans_master.record_datetime AS \'Date Recieve\',

pos_trans_master.due_date AS \'Date Due\',

pos_trans_master.total AS \'Payment\',


CASE WHEN
pos_trans_master.status = \'complete - paid\' 
THEN \'paid\' ELSE \'unpaid\' 
END AS \'Paid/Unpaid\',



', @header, '




FROM pos_trans_master 

LEFT JOIN pos_store 
ON pos_trans_master.store_id = pos_store.store_id 

LEFT JOIN pos_client 
ON pos_trans_master.client_id = pos_client.client_id 

LEFT JOIN pos_trans_detail 
ON pos_trans_master.trans_master_id = pos_trans_detail.trans_master_id 

LEFT JOIN pos_service_and_price 
ON pos_trans_detail.service_id = pos_service_and_price.service_id

WHERE 
(pos_trans_master.record_date BETWEEN \'',  datefrom, '\' AND \'', dateto, '\') 
AND pos_trans_master.status NOT LIKE \'%cancelled%\' 
AND pos_trans_master.status NOT LIKE \'%void%\' 
AND pos_trans_master.user_id = ', id, '


GROUP BY pos_trans_master.trans_master_id

'


); 


PREPARE stmt FROM @query;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-delete_client`(

IN id VARCHAR(255)

)
BEGIN 

UPDATE pos_client 
SET status = 'inactive' 

WHERE client_id = id;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-display_client_all`(

IN store VARCHAR(255)

)
BEGIN 



IF (store != '') THEN

	SELECT 
	pos_client.client_id,
	pos_client.address_state,
	CONCAT(pos_client.first_name, ' ', pos_client.last_name) AS 'Customer Name',
	pos_client.email AS 'Email',
	pos_client.contact_num1 AS 'Contact No#'
	FROM pos_client 

	LEFT JOIN pos_user 
	ON pos_client.add_by_user_id = pos_user.user_id

	LEFT JOIN pos_store
	ON pos_store.store_id = pos_user.store_id

	WHERE pos_client.status = 'active' 
	AND pos_store.store_id = store;

ELSE 

	SELECT 
	client_id,
	address_state,
	CONCAT(first_name, ' ', last_name) AS 'Customer Name',
	email AS 'Email',
	contact_num1 AS 'Contact No#'
	FROM pos_client 
	WHERE status = 'active';



END IF;


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-display_paid_transaction_all`(

IN user_id VARCHAR(100),
IN start_date date,
IN end_date date,
IN store VARCHAR(255)


)
BEGIN 

SELECT 
pos_trans_master.trans_master_id AS 'Transaction ID',
CONCAT(pos_client.first_name, ' ', pos_client.last_name) AS 'Client Name',
DATE_FORMAT(pos_trans_master.record_datetime, '%Y-%m-%d') AS 'Date' 
FROM pos_trans_master 

LEFT JOIN pos_client 
ON pos_trans_master.client_id = pos_client.client_id 


WHERE pos_trans_master.status = 'complete - paid'
AND (DATE_FORMAT(pos_trans_master.record_datetime, '%Y-%m-%d') BETWEEN start_date AND end_date)
AND pos_trans_master.store_id = store

;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-display_pending_void_transaction_all`(

IN user_id VARCHAR(100),
IN start_date date,
IN end_date date

)
BEGIN 

SELECT 
pos_trans_master.trans_master_id AS 'Transaction ID',
CONCAT(pos_client.first_name, ' ', pos_client.last_name) AS 'Client Name',
DATE_FORMAT(pos_trans_master.record_datetime, '%Y-%m-%d') AS 'Date' 

FROM pos_trans_master 

LEFT JOIN pos_client 
ON pos_trans_master.client_id = pos_client.client_id 

WHERE pos_trans_master.status LIKE '%pending void%'
AND (DATE_FORMAT(pos_trans_master.record_datetime, '%Y-%m-%d') BETWEEN start_date AND end_date)

;


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-display_suspend_transaction_all`(

IN user_id VARCHAR(100),
IN start_date date,
IN end_date date,
IN store VARCHAR(255)


)
BEGIN 

SELECT 
pos_trans_master.trans_master_id AS 'Transaction ID',

CONCAT(pos_client.first_name, ' ', pos_client.last_name) AS 'Client Name',
DATE_FORMAT(pos_trans_master.record_datetime, '%Y-%m-%d') AS 'Date' 
FROM pos_trans_master 

LEFT JOIN pos_client 
ON pos_trans_master.client_id = pos_client.client_id 

WHERE pos_trans_master.status = 'suspend' 
AND (DATE_FORMAT(pos_trans_master.record_datetime, '%Y-%m-%d') BETWEEN start_date AND end_date)
AND pos_trans_master.store_id = store
;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-display_unpaid_transaction_all`(

IN user_id VARCHAR(100),
IN start_date date,
IN end_date date,
IN store VARCHAR(255)

)
BEGIN

SELECT 
pos_trans_master.trans_master_id AS 'Transaction ID',
CONCAT(pos_client.first_name, ' ', pos_client.last_name) AS 'Client Name',
DATE_FORMAT(pos_trans_master.record_datetime, '%Y-%m-%d') AS 'Date' 
FROM pos_trans_master 

LEFT JOIN pos_client 
ON pos_trans_master.client_id = pos_client.client_id 

WHERE pos_trans_master.status = 'complete - unpaid' 
AND (DATE_FORMAT(pos_trans_master.record_datetime, '%Y-%m-%d') BETWEEN start_date AND end_date)
AND pos_trans_master.store_id = store
;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-load_service_name_list_option`()
BEGIN 

SELECT 

service_id,
code,
name,
price,
unit
FROM pos_service_and_price
WHERE status = 'active';


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-load_special_name_list_option`(

IN special_type VARCHAR(100)

)
BEGIN

SELECT 
special_id,
name
FROM pos_special 
WHERE status = 'active' 

AND pos_special.special_type = special_type;


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-transaction-add_special_detail`(

IN unique_id VARCHAR(255),
IN id  VARCHAR(100),
IN special_id  VARCHAR(100)

)
BEGIN 


INSERT INTO pos_trans_special(trans_special_id, trans_master_id, special_id) 
VALUES (unique_id, id, special_id);

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-transaction-add_transaction_detail`( 


IN unique_id VARCHAR(255),
IN id  VARCHAR(255),
IN service  VARCHAR(100),
IN unit VARCHAR(100),
IN price VARCHAR(100),
IN unit_count  VARCHAR(100),
IN unit_price  VARCHAR(100)



)
BEGIN 


INSERT INTO pos_trans_detail (trans_detail_id, trans_master_id, service_id, unit, price, unit_count, unit_price) 
VALUES (unique_id, id, service, unit, price, unit_count, unit_price);

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-transaction-add_transaction_item`(

IN id  VARCHAR(100),
IN item_name  VARCHAR(100),
IN qty VARCHAR(100),
IN unique_id VARCHAR(100)


)
BEGIN 

INSERT INTO pos_trans_item (trans_item_id, trans_master_id, item_name, qty)
VALUES (unique_id, id, item_name, qty);


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-transaction-create_master_table`(

IN client_id  VARCHAR(100),
IN store_id VARCHAR(100),
IN user_id  VARCHAR(100)

)
BEGIN 



INSERT INTO pos_trans_master (client_id, record_date, store_id, user_id) 
VALUES (client_id, store_id, user_id);


SELECT 


pos_trans_master.trans_master_id,
pos_store.store_name,
CONCAT(pos_store.address, ' ', pos_store.address_city) AS 'store_address',
CONCAT(pos_store.num_cell, ' | ', pos_store.num_tel) AS 'store_contact',
CONCAT(pos_client.first_name, ' ', pos_client.middle_name, ' ', pos_client.last_name) AS 'client_name',

CONCAT(pos_client.address, ' ', pos_client.address_city) AS 'client_address', 
pos_client.contact_num1 AS 'client_contact',
pos_trans_master.record_datetime 
FROM pos_trans_master 

LEFT JOIN pos_client
ON pos_trans_master.client_id = pos_client.client_id 

LEFT JOIN pos_store 
ON pos_trans_master.store_id = pos_store.store_id;


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-transaction-delete_transaction_detail`(

IN id  VARCHAR(100)

)
BEGIN 


UPDATE pos_trans_detail 
SET line_void = 'YES' 
WHERE trans_detail_id = id;


#DELETE FROM pos_trans_detail WHERE trans_detail_id = id;


#DELETE FROM pos_trans_item WHERE trans_detail_id = id;


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-transaction-delete_transaction_item`(

IN id  VARCHAR(100)

)
BEGIN 


UPDATE pos_trans_item 
SET item_void = 'YES' 

WHERE trans_item_id = id;


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-transaction-delete_transaction_special`(

IN id  VARCHAR(100)

)
BEGIN 

UPDATE pos_trans_special 
SET special_void = 'YES' 
WHERE trans_special_id = id;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-transaction-dispay_trans_item`( 


IN id VARCHAR(255)

)
BEGIN 

SELECT 
trans_item_id,
item_name AS 'Item Name',
qty AS 'Qty' 
FROM pos_trans_item 
WHERE trans_master_id = id 
AND item_void = 'NO';


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-transaction-finalize_transaction`(

IN id VARCHAR(255),
IN user_store_id VARCHAR(100),
IN status VARCHAR(100),
IN due_date DATE,
IN total_grand DECIMAL(10,2),
IN new_id VARCHAR(255),
IN local_code VARCHAR(255),
IN user_id VARCHAR(255)


)
BEGIN 


UPDATE pos_trans_master 
SET status = status,
due_date = due_date,
total = total_grand,
trans_master_id = new_id,
local_code = local_code
WHERE trans_master_id = id; 


##CREATE PRIMARY ID BASE ON LAST INSERT ID OF POS TRANS INC REF
INSERT INTO pos_trans_inc_ref (datetime) VALUES (NOW());
SET @last_id = LAST_INSERT_ID();
SELECT SUBSTRING(MD5(RAND()) FROM 1 FOR 10) INTO @random;

INSERT INTO pos_trans_trail (trans_trail_id , trans_master_id, status, user_store_id, user_admin_id, user_staff_id) 
VALUES (CONCAT(@random, @last_id,id), id, status, user_store_id, 'NA', user_id); 


UPDATE pos_trans_detail 
SET trans_master_id = new_id 
WHERE trans_master_id = id;

UPDATE pos_trans_item 
SET trans_master_id = new_id 
WHERE trans_master_id = id;

UPDATE pos_trans_special 
SET trans_master_id = new_id 
WHERE trans_master_id =id; 

UPDATE pos_trans_trail 
SET trans_master_id = new_id 
WHERE trans_master_id = id;








END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-transaction-finalize_transaction_paid`(

IN id VARCHAR(255),
IN user_store_id VARCHAR(100),
IN status VARCHAR(100),
IN due_date DATE,
IN total_grand DECIMAL(10,2),
IN claim_by VARCHAR(100),
IN payment_type VARCHAR(100),
IN user_id VARCHAR(255)

)
BEGIN 



UPDATE pos_trans_master 
SET status = status,
due_date = due_date,
total = total_grand,
claim_by = claim_by,
claim_datetime = NOW(),
payment_type = payment_type

WHERE trans_master_id = id; 


##CREATE PRIMARY ID BASE ON LAST INSERT ID OF POS TRANS INC REF
INSERT INTO pos_trans_inc_ref (datetime) VALUES (NOW());
SET @last_id = LAST_INSERT_ID();
SELECT SUBSTRING(MD5(RAND()) FROM 1 FOR 10) INTO @random;

INSERT INTO pos_trans_trail (trans_trail_id, trans_master_id, status, user_store_id, user_admin_id, user_staff_id) 
VALUES (CONCAT(@random, @last_id,id), id, status, user_store_id, 'NA', user_id);



IF (payment_type = 'CREDIT MEMO') THEN
  
SELECT client_id 
INTO @client_id 
FROM pos_trans_master 
WHERE trans_master_id = id; 


INSERT INTO pos_client_credit (credit_id ,client_id, amount, trans_master_id, user_id)
VALUES (CONCAT(NOW(), @random, UNIX_TIMESTAMP()), @client_id, abs(total_grand) * -1, id, user_store_id);
  

END IF;






END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-transaction-suspend_transaction`(

IN id VARCHAR(100),
IN user_store_id VARCHAR(100),
IN status VARCHAR(100),

IN due date

)
BEGIN


UPDATE pos_trans_master 
SET status = status,
due_date = due
WHERE trans_master_id = id; 

INSERT INTO pos_trans_trail (trans_master_id, status, user_store_id, user_admin_id) 
VALUES (id, status, user_store_id, 'NA');


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-transaction-void_transaction`(

IN id VARCHAR(100),
IN user_store_id VARCHAR(100),
IN status VARCHAR(100)

)
BEGIN 

UPDATE pos_trans_master 
SET status = status 
WHERE trans_master_id = id; 

INSERT INTO pos_trans_trail (trans_master_id, status, user_store_id, user_admin_id) 
VALUES (id, status, user_store_id, 'NA');

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-update_client`( 

IN id  VARCHAR(100),
IN first_name  VARCHAR(100),
IN middle_name  VARCHAR(100),
IN last_name  VARCHAR(100),
IN contact_num1  VARCHAR(100),
IN contact_num2  VARCHAR(100),
IN email  VARCHAR(100),
IN address  VARCHAR(100),
IN address_city  VARCHAR(100),
IN address_country  VARCHAR(100),
IN address_state  VARCHAR(100),

IN client_type  VARCHAR(100),
IN company_name  VARCHAR(100),
IN fax  VARCHAR(100)

)
BEGIN 

UPDATE pos_client 
SET first_name = first_name, 
middle_name = middle_name,
last_name = last_name,
contact_num1 = contact_num1,
contact_num2 = contact_num2, 
email = email,
address = address,
address_city = address_city,
address_country = address_country,
address_state = address_state,
client_type = client_type,
company_name = company_name,
fax = fax 
WHERE client_id = id;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-update_profile`(

IN id  VARCHAR(100),
IN first_name  VARCHAR(100),
IN middle_name  VARCHAR(100),

IN last_name  VARCHAR(100)

)
BEGIN 

UPDATE pos_user 
SET first_name = first_name,
middle_name = middle_name,
last_name = last_name 
WHERE user_id = id;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-view_client_details`(

IN id  VARCHAR(100)

)
BEGIN 

SELECT 
pos_client.client_id,
pos_client.first_name,


pos_client.middle_name,

pos_client.last_name,
pos_client.contact_num1,
pos_client.contact_num2,
pos_client.email,
pos_client.address,
pos_client.address_city,
pos_client.address_country,
pos_client.address_state,
CONCAT(pos_user.first_name, ' ', pos_user.last_name) AS 'add_by',
pos_client.add_datetime,

pos_client.client_type,
pos_client.company_name,
pos_client.fax

FROM pos_client 
LEFT JOIN pos_user 
ON pos_client.add_by_user_id = pos_user.user_id 

WHERE pos_client.client_id = id

;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-view_profile`(

IN id  VARCHAR(100)


)
BEGIN 

SELECT 
pos_user.username,
pos_user.first_name,
pos_user.middle_name,
pos_user.last_name,
pos_store.store_name 
FROM pos_user 

LEFT JOIN pos_store 
ON pos_user.store_id = pos_store.store_id 
WHERE pos_user.user_id = id;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-user_change_password`(

IN id VARCHAR(100),
IN given_password VARCHAR(100)

)
BEGIN 

UPDATE pos_user 
SET `password` = given_password,
`require_new_pw` = 'NO'
WHERE `user_id` = id;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-user_check_new`(

IN id VARCHAR(100)

)
BEGIN 


SELECT 
user_id,
require_new_pw 
FROM pos_user 
WHERE require_new_pw = 'YES' 
AND user_id = id;



END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-user_check_password`(

IN id VARCHAR(100),
IN given_password VARCHAR(100)

)
BEGIN 

SELECT * FROM pos_user 
WHERE user_id = id 
AND password = given_password;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-user_login`(

IN given_username  VARCHAR(100),
IN given_password VARCHAR(100)

)
BEGIN 


SELECT 
pos_store.store_code,
pos_user.user_id,
pos_store.store_id,
CONCAT(pos_user.first_name, ' ', pos_user.last_name) AS 'name',
pos_user.user_type,
pos_store.store_name,

pos_user.status,
pos_store.surcharge_ref


FROM pos_user 

LEFT JOIN pos_store 
ON pos_user.store_id = pos_store.store_id 

WHERE pos_user.username = given_username 
AND pos_user.password = given_password

;

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `pos_client`
--

CREATE TABLE IF NOT EXISTS `pos_client` (
  `client_id` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `contact_num1` text NOT NULL,
  `contact_num2` text NOT NULL,
  `email` text NOT NULL,
  `address` varchar(255) NOT NULL,
  `address_city` varchar(255) NOT NULL,
  `address_country` varchar(255) NOT NULL,
  `address_state` varchar(255) NOT NULL,
  `add_by_user_id` bigint(20) NOT NULL,
  `add_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(50) NOT NULL DEFAULT 'active',
  `client_type` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `fax` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pos_client_credit`
--

CREATE TABLE IF NOT EXISTS `pos_client_credit` (
  `credit_id` varchar(255) NOT NULL,
  `client_id` varchar(255) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `trans_master_id` varchar(255) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `record_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pos_client_inc_ref`
--

CREATE TABLE IF NOT EXISTS `pos_client_inc_ref` (
  `id` bigint(20) NOT NULL,
  `datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pos_item`
--

CREATE TABLE IF NOT EXISTS `pos_item` (
  `item_id` bigint(20) NOT NULL,
  `item_name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pos_service_and_price`
--

CREATE TABLE IF NOT EXISTS `pos_service_and_price` (
  `service_id` bigint(20) NOT NULL,
  `code` varchar(100) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `unit` varchar(10) NOT NULL,
  `add_by_user_id` bigint(20) NOT NULL,
  `add_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(50) NOT NULL DEFAULT 'active'
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pos_special`
--

CREATE TABLE IF NOT EXISTS `pos_special` (
  `special_id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` decimal(10,0) NOT NULL,
  `special_type` varchar(50) NOT NULL COMMENT 'VALUE: markup/discount',
  `value_type` varchar(50) NOT NULL COMMENT 'VALUE: REAL/PERCENTAGE',
  `add_by_user_id` bigint(20) NOT NULL,
  `add_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(50) NOT NULL DEFAULT 'active'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pos_store`
--

CREATE TABLE IF NOT EXISTS `pos_store` (
  `store_id` bigint(20) NOT NULL,
  `store_code` varchar(50) NOT NULL,
  `store_name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `address_city` varchar(255) NOT NULL,
  `address_country` varchar(100) NOT NULL DEFAULT 'Philippines',
  `num_tel` varchar(50) NOT NULL,
  `num_cell` varchar(255) NOT NULL,
  `add_by_user_id` bigint(20) NOT NULL,
  `add_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(50) NOT NULL DEFAULT 'active' COMMENT 'VALUE: active / inactive | Default: active',
  `status_change_by_user_id` bigint(20) NOT NULL,
  `status_change_datetime` datetime NOT NULL,
  `surcharge_ref` decimal(10,2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pos_trans_detail`
--

CREATE TABLE IF NOT EXISTS `pos_trans_detail` (
  `trans_detail_id` varchar(255) NOT NULL,
  `trans_master_id` varchar(50) NOT NULL,
  `service_id` bigint(20) NOT NULL,
  `unit` varchar(50) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `unit_count` decimal(10,2) NOT NULL,
  `unit_price` decimal(10,2) NOT NULL,
  `record_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `line_void` varchar(50) NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pos_trans_inc_ref`
--

CREATE TABLE IF NOT EXISTS `pos_trans_inc_ref` (
  `id` bigint(20) NOT NULL,
  `datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pos_trans_inc_ref_finalize`
--

CREATE TABLE IF NOT EXISTS `pos_trans_inc_ref_finalize` (
  `id` bigint(20) NOT NULL,
  `datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pos_trans_item`
--

CREATE TABLE IF NOT EXISTS `pos_trans_item` (
  `trans_item_id` varchar(255) NOT NULL,
  `trans_master_id` varchar(255) NOT NULL,
  `item_name` text NOT NULL,
  `qty` int(11) NOT NULL,
  `record_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `item_void` varchar(50) NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pos_trans_master`
--

CREATE TABLE IF NOT EXISTS `pos_trans_master` (
  `trans_master_id` varchar(255) NOT NULL,
  `local_code` varchar(255) NOT NULL,
  `client_id` varchar(255) NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'pending',
  `record_date` date NOT NULL,
  `record_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `store_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `due_date` date NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `claim_by` varchar(255) NOT NULL,
  `claim_datetime` datetime NOT NULL,
  `payment_type` varchar(255) NOT NULL,
  `check_no` varchar(255) NOT NULL,
  `central_code` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pos_trans_special`
--

CREATE TABLE IF NOT EXISTS `pos_trans_special` (
  `trans_special_id` varchar(255) NOT NULL,
  `trans_master_id` varchar(255) NOT NULL,
  `special_id` bigint(20) NOT NULL,
  `special_void` varchar(50) NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pos_trans_trail`
--

CREATE TABLE IF NOT EXISTS `pos_trans_trail` (
  `trans_trail_id` varchar(255) NOT NULL,
  `trans_master_id` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'pending',
  `user_store_id` varchar(100) NOT NULL,
  `user_staff_id` bigint(20) NOT NULL,
  `user_admin_id` varchar(100) NOT NULL,
  `record_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pos_user`
--

CREATE TABLE IF NOT EXISTS `pos_user` (
  `user_id` bigint(20) NOT NULL,
  `store_id` bigint(20) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(100) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `user_type` varchar(50) NOT NULL COMMENT 'DEFAULT VALUE: ADMIN / STORE',
  `require_new_pw` varchar(10) NOT NULL DEFAULT 'YES',
  `status` varchar(30) NOT NULL DEFAULT 'active' COMMENT 'VALUE: active / inactive | DEFAULT: active',
  `status_change_datetime` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pos_client`
--
ALTER TABLE `pos_client`
  ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `pos_client_credit`
--
ALTER TABLE `pos_client_credit`
  ADD PRIMARY KEY (`credit_id`);

--
-- Indexes for table `pos_client_inc_ref`
--
ALTER TABLE `pos_client_inc_ref`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pos_item`
--
ALTER TABLE `pos_item`
  ADD PRIMARY KEY (`item_id`);

--
-- Indexes for table `pos_service_and_price`
--
ALTER TABLE `pos_service_and_price`
  ADD PRIMARY KEY (`service_id`);

--
-- Indexes for table `pos_special`
--
ALTER TABLE `pos_special`
  ADD PRIMARY KEY (`special_id`);

--
-- Indexes for table `pos_store`
--
ALTER TABLE `pos_store`
  ADD PRIMARY KEY (`store_id`), ADD UNIQUE KEY `store_code` (`store_code`);

--
-- Indexes for table `pos_trans_detail`
--
ALTER TABLE `pos_trans_detail`
  ADD PRIMARY KEY (`trans_detail_id`);

--
-- Indexes for table `pos_trans_inc_ref`
--
ALTER TABLE `pos_trans_inc_ref`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pos_trans_inc_ref_finalize`
--
ALTER TABLE `pos_trans_inc_ref_finalize`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pos_trans_item`
--
ALTER TABLE `pos_trans_item`
  ADD PRIMARY KEY (`trans_item_id`);

--
-- Indexes for table `pos_trans_master`
--
ALTER TABLE `pos_trans_master`
  ADD PRIMARY KEY (`trans_master_id`);

--
-- Indexes for table `pos_trans_special`
--
ALTER TABLE `pos_trans_special`
  ADD PRIMARY KEY (`trans_special_id`);

--
-- Indexes for table `pos_trans_trail`
--
ALTER TABLE `pos_trans_trail`
  ADD PRIMARY KEY (`trans_trail_id`);

--
-- Indexes for table `pos_user`
--
ALTER TABLE `pos_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pos_client_inc_ref`
--
ALTER TABLE `pos_client_inc_ref`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pos_item`
--
ALTER TABLE `pos_item`
  MODIFY `item_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `pos_service_and_price`
--
ALTER TABLE `pos_service_and_price`
  MODIFY `service_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=84;
--
-- AUTO_INCREMENT for table `pos_special`
--
ALTER TABLE `pos_special`
  MODIFY `special_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `pos_store`
--
ALTER TABLE `pos_store`
  MODIFY `store_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `pos_trans_inc_ref`
--
ALTER TABLE `pos_trans_inc_ref`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pos_trans_inc_ref_finalize`
--
ALTER TABLE `pos_trans_inc_ref_finalize`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pos_user`
--
ALTER TABLE `pos_user`
  MODIFY `user_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
