<?php
session_start();
include '../config.php';



//pages menu setup
if(!isset($_SESSION['user_id'])) {


	die("Session Expired! Please Login to Traman System.");


}



?>



<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title>SMS Chat App</title>
    
    
    <link rel="stylesheet" href="css/reset.css">
	<link rel="stylesheet" href="css/style.css">
    <link rel="icon" type="image/png" href="../pages/favicon.ico">
    
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
	

    
    
    <script type="text/javascript">

		
		<!-- DOCUMENT READY FUNCTION -->
		jQuery(document).ready(function($) {
			
			
			
			
			var loading_logo = '<br> <center><img src="../assets/images/loading.gif"></center>';
			
			function show_loader(tag_id) {

				$('#' + tag_id).html(loading_logo);	
				
				
			}
			
			
			function sendsms1(number, message) {
			
					var sender = sessionStorage.name;
					
					
					//toastr.warning("Sending SMS message, please wait!", "SMS NOTIFICATION!", opts);
					
					$.ajax({
					url: "smschatapp.php",
					method: 'POST',
					dataType: 'HTML', //html or json
					data: {
						"sendsms": true,
						number: number,
						message: message,
						sender: sender
					},
						success: function(resp)
						{									
						
							//var myContent = resp;
							
							//alert($(myContent).text());
							
							//toastr.info($(myContent).text(), "SMS NOTIFICATION!", opts);
							
							var number = $('#smschat-phone').text();
							load_conversation_data(number);	
							
							
						
						}
					
					});
									
			}
			
			
			
			//LOAD UNKNOWN NUMBER INBOX
			function load_unknown_inbox(refresh = false) {


				var limit_start = $('li#link_sms_view_conversation.person').length;

				if(refresh == true){
					limit_start = 0;
				}

				if(limit_start ==0) {
					show_loader('smsinbox');
				}else{
					show_loader('li-load_more');
				}


				$.ajax({
				url: "smschatapp.php",
				method: 'POST',
				dataType: 'HTML', //html or json
				data: {
					"sms-display_inbox_unknown": true,
					"limit_start": limit_start
				},
					success: function(resp)
					{

						if(limit_start ==0){
							$('#smsinbox').html(resp);
						}else{
							$('#li-load_more').remove();
							$(resp).appendTo('#smsinbox');
						}
						var options = {
						  valueNames: [ 'name', 'born' ]
						};
						
						var userList = new List('users', options);
					
	
					}
	
				});
	
	
			}
			
			
			// LOAD CONVESATION DATA
			function load_conversation_data(phone) {
				
				show_loader('smschat-conversation');
	
				$.ajax({
				url: "smschatapp.php",
				method: 'POST',
				dataType: 'HTML', //html or json
				data: {
					"sms-display_conversation_data": true,
					phone: phone
				},
					success: function(resp)
					{
						
						
						
						//alert(resp);
						
						$('#smschat-conversation').html(resp);
						
						$('div#smschat-conversation').wrapInner($('<div id="dv_wrapper"></div>'));
						
						$("#dv_wrapper").scrollTop($("#dv_wrapper")[0].scrollHeight);
						
						
						//
						
						//$("#sms-conversation_box").scrollTop($("#sms-conversation_box")[0].scrollHeight);
	
					}
	
					
				});
				
				
				
			}
				
				
			
			
			
			<!-- ACTION BUTTONS -->
			
			
			
			
			<!-- INBOX LINK CLICK -->
			$("#smsinbox").on("click", "#link_sms_view_conversation", function() { 
			
				var contact = $(this).attr('contact');
				var phone = $(this).attr('phone');
				
				if (contact != phone) {
					
					$('#smschat-name').html(contact);
					
				} else {
					
					$('#smschat-name').html('Unknown');
					
				}
				
				
				
				$('#smschat-phone').html(phone);
				
				load_conversation_data(phone);
				
				
				
			
				
			
			});
			
			
			<!-- SEND BUTTON CLICK (CONVERSATION BOX)-->
			$("#btn-sms-replysend").click(function () {
			
				
				var number = $('#smschat-phone').text();
				var message = $('#txt-sms-reply_message').val();
				sendsms1(number,message);
				
				document.getElementById("txt-sms-reply_message").value = "";						
				
				
				
			});
			
			
			$("#btn-refresh").click(function() {
				
				load_unknown_inbox(true);
				
			});
			


			$("#smsinbox").on("click", "#btn-load_more", function() {
				console.log('more');
				load_unknown_inbox();
			});


				load_unknown_inbox();
			
					
			//$('div#smschat-conversation').wrap($('<div id="dv_wrapper">'));
			
		});
		
		
	</script>

    
    <style id="style-custom">

		.wrapper{
			width: auto;
		}

		.container {
			width: 100%;
			height: 100%;
		}

		body {
			position: absolute;
			width: 100%;
			height: 100%;
		}

		.wrapper {
			width: auto;
			height: 100%;
		}

		ul#smsinbox{
			height: calc(100% - 96px);
		}

		.container .right .write input{
			width: calc(100% - 80px);
		}

		.container .left input.search {
			width: calc(100% - 55px);
		}

		div#smschat-conversation {
			height: calc(100% - 50px);
		}

		div#smschat-conversation #dv_wrapper {
			height: auto;
			border-bottom: solid 1px #c1c1c1;
		}


		button#btn-load_more {
			background: #00b0ff;
			color: #FFF;
			line-height: 40px;
			padding: 0 20px;
			border-radius: 10px;
			border: none;
			cursor: pointer;
			border: solid 3px #FFF;
		}

		li#li-load_more {
			text-align: center;
		}
		li#li-load_more center img {
			float: none;
			margin: auto;
		}
		.bubble pre {
			line-height: normal;
		}
		.bubble {
			max-width: 70%;
		}
		.container .left .people .person:hover {
			width: 100% !important;
		}
		.container .left .people .person:after{
			background-color: #bbbbbb;
		}
	</style>
    
  </head>

  <body>
	
    <div class="wrapper">
    <div class="container">
        <div class="left" id="users">
            <div class="top" >
               <input class="search" placeholder="Search" />
               <button class="sort" data-sort="name" hidden>Sort by name</button>                
               <a href="javascript:;" class="search" id="btn-refresh"></a>                
            </div>
            <ul class="people list" id="smsinbox" style="overflow-y: scroll;">
            	
               
                
                
            	<!--
                <li class="person" data-chat="person1">
                   
                    <span class="name">Thomas Bangalter</span>
                    <span class="time">2:09 PM</span>
                    <span class="preview">I was wondering...</span>
                </li>
                <li class="person" data-chat="person2">
                   
                    <span class="name">Dog Woofson</span>
                    <span class="time">1:44 PM</span>
                    <span class="preview">I've forgotten how it felt before</span>
                </li>
                <li class="person" data-chat="person3">
                    
                    <span class="name">Louis CK</span>
                    <span class="time">2:09 PM</span>
                    <span class="preview">But we’re probably gonna need a new carpet.</span>
                </li>
                <li class="person" data-chat="person4">
                    
                    <span class="name">Bo Jackson</span>
                    <span class="time">2:09 PM</span>
                    <span class="preview">It’s not that bad...</span>
                </li>
                <li class="person" data-chat="person5">
                    
                    <span class="name">Michael Jordan</span>
                    <span class="time">2:09 PM</span>
                    <span class="preview">Wasup for the third time like is 
you bling bitch</span>
                </li>
                <li class="person" data-chat="person6">
                   
                    <span class="name">Drake</span>
                    <span class="time">2:09 PM</span>
                    <span class="preview">howdoyoudoaspace</span>
                </li>
                -->
            </ul>
        </div>
        
        
        <div class="right">
            <div class="top">
            
            <span class="name" id="smschat-name"></span> 
            
            <span id="smschat-phone"></span>
            
            </div>
            
            <div class="chat" data-chat="person2" id="smschat-conversation">
            	
                <div class="conversation-start">
                    <span>No message loaded...</span>
                </div>
              
               
            </div>
           
           
            <div class="write">
            	<a href="javascript:;" class="write-link attach"></a>
                <input type="text" id="txt-sms-reply_message" />
                <a href="javascript:;" class="write-link smiley"></a>
                <a href="#" id="btn-sms-replysend" class="write-link send"></a>
                
                <!--
                <textarea name="comment" form="usrform">Enter text here...</textarea>
                -->
            </div>
        </div>
        
        
       
    </div>
</div>


<script src="js/index.js"></script>
<script src="js/list.js"></script>



    
    
    
    
  </body>
</html>
