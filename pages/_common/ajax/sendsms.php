<?php
session_start();

include '../../../config.php';

//setup PHP UTF-8 stuff
setlocale(LC_CTYPE, 'en_US.UTF-8');
mb_internal_encoding("UTF-8");
mb_http_output('UTF-8'); 



function sendsms($ip, $port, $password, $phone, $text) {
	
	require 'central_config.php';

    $string = $phone;
    $message = $text;
    $string = preg_replace('/\.$/', '', $string); //Remove dot at end if exists
    $array = explode(', ', $string); //split string into array seperated by ', '


    $url = "https://semysms.net/api/3/sms_more.php"; //Url address for sending SMS
    $device = '69417';  // Phone number
    $token = 'd07a3d6f13d922029241f18fc43f65c7';  // Your token (secret)

    $params = array('token'  => $token);


    $id = 0;
    foreach($array as $phone) {
        $id++;

        $params['data'][] = array(
            'my_id' => $id,
            'device' => $device, 'phone' => $phone, 'msg' => $message);

    }



    $params = json_encode($params);

    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($params))
    );
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS,  $params);
    $result = curl_exec($curl);
    curl_close($curl);

    return $result;
	
	
	//INSERT TO SMS DATA LISTEN SYSTEM SMS REMARKS
	$random = md5(uniqid(rand(), true));
	$query = "INSERT INTO sms_data_listen (id, phone, text, sender, status) 
	VALUES ('$random', '$phone', '$text', 'SYSTEM', 'read');";
	mysqli_query($link,$query);
	
	
}



if(isset($_POST['sendsms_noticeOfClaim']) && $_POST['sendsms_noticeOfClaim'] == true) {
	
	$id = mysqli_real_escape_string($link,$_POST['id']);
	
	$query = "CALL `SP-all-display_claim_detail` ('$id');";
	
	$result = mysqli_query($link,$query);
	
	
	if ($row = mysqli_fetch_array($result)) {
		
		$store_code = $row['store_code'];
		$store_name = $row['store_name'];
		$local_code = $row['local_code'];
		$client_name = $row['client_name'];
		$client_contact = $row['client_contact'];
		$client_network = $row['client_network'];
		$claim_by = $row['claim_by'];
		$claim_datetime = $row['claim_datetime'];

$text = "Clean As You Go Laundry Shop 
NOTICE OF PAYMENT AND RELEASE
JO No#: $store_code - $local_code 
Client Name: $client_name 
Contact No#: $client_contact
Claim By: $claim_by
Date Claim: $claim_datetime

TERMS AND CONDITION:
1. Please bring the JOB ORDER when claiming your items to avoid inconvenience.

2. Items not claimed within 30 days will be subject to additional PHP 60.00 storage fee.

3. Items not claimed within 60 days will be disposed by ClayGo with appropriate notice to the given address or contact number of client to recover the cost of service provided.

4 ClayGo shall not be liable for any damages incurred due to natural effect of washer and dryer to the garments. To avoid such incidence, client should declare the fragility of item to upon signing the terms and condition.

5. ClayGo shall not be liable for any damagaes in case of fire, flood, and other unforeseen events or loss through force majeure.

6. Although ClayGo promised to provide outmost care on your item, ClayGo shall not be liable for any changes resulting from normal washing process, loss of buttons, shrinkage, facing and loss of item left in the pockets. 

7. ClayGo reserves the right to confirm accuracy of the item for laundry and inform the client for any discrepancy within 24 hours.

8. Liability of damage loss is limited to an amount not exceeding Two (2) times the laundry cost.

9. Complaints will be entertained within 24 hours from the date of delivery or pick-up.

10. By affixing your signature below, client agree with the above terms and conditions.

For Further Assistance please Call or
Text our Helpline:
Textline: 0998-952-3230 (Text Only)
Globe: 0917-840-9951 (Call Only)
Smart / Sun: 0998-955-5705 (Call Only)
Landline: 882-6235 / 757-2200.
Thank you!";
		
		
		sendsms('', '', '', $client_contact, $text);
		
		
	}



}



if(isset($_POST['sendsms_acknowledgement']) && $_POST['sendsms_acknowledgement'] == true) {
	
	
	$sub_total = 0;
	$total_amount_due = 0;
	
	//$ip = $_POST['ip'];
	//$port = $_POST['port'];
	//$password = $_POST['password'];	
	//$phone = $_POST['phone'];
	//$text = $_POST['text'];
	
	
	
	$id = mysqli_real_escape_string($link,$_POST['id']);
	
	$query = "
	SELECT 
	pos_trans_master.local_code,
	pos_store.store_code,
	CONCAT(pos_client.first_name, ' ', pos_client.last_name) AS 'client_name',
	pos_client.contact_num1,
	pos_client.network1,
	CONCAT(pos_user.first_name, ' ', pos_user.last_name) AS 'staff',
	DATE_FORMAT(pos_trans_master.record_datetime,'%W %b %d, %Y %h:%i %p') AS 'date',
	DATE_FORMAT(pos_trans_master.due_date,'%W %b %d, %Y') AS 'due_date'
	
	
	FROM
	pos_trans_master 
	
	LEFT JOIN pos_store 
	ON pos_trans_master.store_id = pos_store.store_id 
	
	LEFT JOIN pos_client 
	ON pos_trans_master.client_id = pos_client.client_id 
	
	LEFT JOIN pos_user 
	ON pos_trans_master.user_id = pos_user.user_id
	
	
	
	WHERE pos_trans_master.trans_master_id = '$id'
	";
	
	
	$result = mysqli_query($link,$query);
	
	
	if ($row = mysqli_fetch_array($result)) {
		
		$local_code = $row['local_code'];
		$store_code = $row['store_code'];
		$client_name = $row['client_name'];
		$client_contact = $row['contact_num1'];
		$network = $row['network1'];
		$staff = $row['staff'];
		$date = $row['date'];
		$due_date = $row['due_date'];
		
$header = "JO No#: $store_code - $local_code
Client Name: $client_name
Contact No#: $client_contact
Laundry Recieved by: $staff
Date: $date
Due: $due_date";
		
	}
	
	
	
	
	$query = "
	SELECT

CONCAT(pos_category_service.name, '-', pos_service_and_price.name) AS 'service',
CONCAT(TRIM(TRAILING '.' FROM TRIM(TRAILING '0' from pos_trans_detail.unit_count)), pos_trans_detail.unit) AS 'unit',
pos_trans_detail.unit_price AS 'amount'
FROM pos_trans_master

LEFT JOIN pos_trans_detail
ON pos_trans_master.trans_master_id = pos_trans_detail.trans_master_id

LEFT JOIN pos_service_and_price
ON pos_trans_detail.service_id = pos_service_and_price.service_id

LEFT JOIN pos_category_service 
ON pos_service_and_price.category_id = pos_category_service.category_id

WHERE pos_trans_master.trans_master_id = '$id'
AND pos_trans_detail.line_void = 'NO'
	
	";
	
	
	
$body = "
Services:
";
	
	
	$result = mysqli_query($link,$query);
	
	
	while ($row = mysqli_fetch_array($result)) {
		
		$service = $row['service'];
		$unit = $row['unit'];
		$amount = $row['amount'];
		
		$sub_total = $sub_total + $amount;
		
		$total_amount_due = $total_amount_due + $amount;
		
		
$body .= "$service : $unit / $amount PhP
";
		
		
	}
	
	
	
	
	
	$query = " SELECT
			pos_store.surcharge_store,
            pos_store.surcharge_pickup,
			pos_trans_special.trans_special_id,
			pos_special.value,
			pos_special.special_type,
			pos_special.value_type,
			pos_special.name AS 'special'
			FROM pos_trans_special

			LEFT JOIN pos_special
			ON pos_trans_special.special_id = pos_special.special_id 
                      
            LEFT JOIN pos_trans_master
            ON pos_trans_special.trans_master_id = pos_trans_master.trans_master_id
            
            LEFT JOIN pos_store 
            ON pos_trans_master.store_id = pos_store.store_id

			WHERE pos_trans_special.trans_master_id = '$id'
			AND pos_trans_special.special_void = 'NO' 
			
			ORDER BY pos_special.priority ASC
			";
			
			
	$result = mysqli_query($link,$query);
	
	
	$total_special_price = 0;
	$total_discount_price = 0;
	$total_service_fee_price = 0;
	$total_vat_price = 0;
	
	$body1 = "";
	
	
	while ($row = mysqli_fetch_array($result)) {
		
		
		$session_surcharge_store = $row['surcharge_store'];
		$session_surcharge_pickup = $row['surcharge_pickup'];
		$value = $row['value'];
		$special_type = $row['special_type'];
		$value_type = $row['value_type'];
		$special = $row['special'];		
		
		
		if ($special_type == "MARK-UP") {
			
			if ($value_type == "PERCENTAGE") {

				$percent_value = ($value / 100);

				$special_price = ($sub_total * $percent_value);


			}  else {

				$special_price = ($value);

			}
			
			
			//ADD ALL SPECIAL PRICE
			$total_special_price = $special_price + $total_special_price;
			
			
		} elseif ($special_type == "STORE SERVICE FEE") {
			
			
			if ($session_surcharge_store > $sub_total) {

				$special_price = $session_surcharge_store - ($sub_total + $total_special_price);


			} else {

				$special_price = 0;

			}
			
			//SERVICE FEE -> STORE
			$total_service_fee_price = $special_price;
						
		}  elseif ($special_type == "PICK-UP SERVICE FEE") {
			
			if ($session_surcharge_pickup > $sub_total) {

				$special_price = $session_surcharge_pickup - $sub_total;


			} else {

				$special_price = 0;


			}
			
			//SERVICE FEE -> PICK-UP
			$total_service_fee_price = $special_price;
			
			
		}  elseif ($special_type == "DISCOUNT") {
			
			
			if ($value_type == "PERCENTAGE") {

				$percent_value = ($value / 100);

				$special_price = (-1 * abs(( ($sub_total + $total_special_price + $total_service_fee_price) * $percent_value)));


			} else {

				$special_price = (-1 * $value);

			}
			
			
			//ADD ALL DISCOUNT PRICE
			$total_discount_price = $special_price + $total_discount_price;
			
			
			
			
		} elseif ($special_type == "VAT") {
			
			$special_price = ($sub_total + $total_special_price + $total_discount_price + $total_service_fee_price) * ($value / 100);
			
			$total_vat_price = $special_price;
			
			
		}
		
		$holder_price = number_format($special_price,2);
		
		$total_amount_due = $total_amount_due + $special_price;
		
		
$body1 .= "$special: $holder_price PhP
";
		
		
	}
	
//TOTAL AMOUNT DUE
$total_amount_due = number_format($total_amount_due,2);
$body1 .= "
Total Amount Due: $total_amount_due PhP
";
	
	
	
	$query = "SELECT * FROM pos_trans_item 
WHERE trans_master_id = '$id' 
AND item_void = 'NO'
;";

	$result = mysqli_query($link,$query);
	
	$total_item = 0;
	
$body2 = "Items:
";
	
	
	while ($row = mysqli_fetch_array($result)) {
		
		$item_name = $row['item_name'];
		$qty = $row['qty'];
		
		$total_item = $total_item + $qty;
		
		
$body2 .= "$item_name: $qty
";
		
		
	}
	
	
$body2 .= "Total No. of Items: $total_item
";
	
	
	
	
	
	
$text = "Clean As You Go Laundry Shop 
Laundry Acknowledgement Reciept 
$header
$body
$body1
$body2

For Further Assistance please Call or
Text our Helpline:
Textline: 0998-951-3230 (Text Only)
Globe: 0917-840-9951 (Call Only)
Smart / Sun: 0998-955-5705 (Call Only)
Landline: 882-6235 / 757-2200.
Thank you!";


//echo $text;

sendsms('', '', '', $client_contact, $text);
	
	
	
	
}





/*
----------------->>>>>>>>>>>>>>>>  SMS Script for HOME PAGE
*/


if(isset($_POST['sendsms']) && $_POST['sendsms'] == true) {
	
	
	$sender = $_POST['sender'];
	$number = $_POST['number'];
	$message = $_POST['message'];
	
	
	$qsender = mysqli_real_escape_string($link,$_POST['sender']);
	$qnumber =  mysqli_real_escape_string($link,$_POST['number']);
	$qmessage =  mysqli_real_escape_string($link,$_POST['message']);
	
	
	$random = md5(uniqid(rand(), true));

	$query = " CALL `SP-sms-save_send_sms`('$qnumber', '$qmessage', '$qsender');";
	
	
	mysqli_query($link,$query);
		
	sendsms('', '', '', $number, $message);

}




if(isset($_POST['sms-display_contact']) && $_POST['sms-display_contact'] == true) {


	$str = "";

	$str .= "<thead>";

	//$query = "CALL `SP-store-display_client_all`('$session_store_id');";
	$query = "CALL `SP-store-display_client_all`();";
	$result = mysqli_query($link,$query)or die(mysqli_error($link));

	$index = 0;

	while ($row = mysqli_fetch_field($result)) {

		if ($index >= 2) {

			$str .= "<th>";
			$str .= $row->name;
			$str .= "</th>";

		}

		$index++;


	}

	//add Action Header
	$str .= "<th>";
	$str .= "Actions";
	$str .= "</th>";


	//CLOSE THEAD
	$str .= "</thead>";


	//OPEN TBODY
	$str .= "<tbody>";


	$index = 0;

	while ($row = mysqli_fetch_array($result)) {
		
		
		$id = $row['client_id'];
		$address_city = $row['address_city'];
		$name = $row['Customer Name'];
		$email = $row['Email'];
		$contact_num = $row['Contact No#'];
		$credit_balance = $row['Credit Balance'];
		$branch = $row['Branch'];


		$str .= '

			<tr>

			<td class="user-name">
				<a href="#" class="name">' . $name . '</a>
				<span>' . $address_city . '</span>
			</td>

			<td class="hidden-xs hidden-sm">
				<span class="email">' . $email . '</span>
			</td>

			<td class="hidden-xs hidden-sm">
				<span class="email">' . $contact_num . '</span>
			</td>
			
			
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $branch . '</span>
			</td>
			
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $credit_balance . '</span>
			</td>


			<td class="action-links">
				
				<a href="#" class="edit" id="link_select" value="' . $id . '" contact_num="' . $contact_num . '" data-dismiss="modal">
					<i class="fa-folder-open-o"></i>
					Select Recipient
				</a>
				
			</td>


			</tr>


		';


	}


	//CLOSE TBODY
	$str .= "</tbody>";


	echo $str;


}




if(isset($_POST['sms-display_inbox_known']) && $_POST['sms-display_inbox_known'] == true) {
	
	
	$str = "<strong>Known</strong>";
	
	$query = '
	
	SELECT 
	
	CASE 
	WHEN pos_client.client_id IS NOT NULL 
	THEN CONCAT(pos_client.first_name, " ", pos_client.last_name) 
	ELSE REPLACE(sms_data_listen.phone, "+63", "0")
	END AS "contact",
	
	REPLACE(sms_data_listen.phone, "+63", "0") AS "phone",
	
	
	CASE 
	WHEN sms_data_status.status IS NULL THEN "read" 
	ELSE sms_data_status.status
	END AS "status"
	
	FROM sms_data_listen 
	
	LEFT JOIN pos_client 
	ON REPLACE(sms_data_listen.phone, "+63", "0") = pos_client.contact_num1 
	
	
	LEFT JOIN 
	(
	
	SELECT 
	REPLACE(sms_data_listen.phone, "+63", "0") AS "phone",
	sms_data_listen.status
	
	FROM sms_data_listen 
	
	WHERE sms_data_listen.status = "unread"
	
	) AS sms_data_status 
	ON REPLACE(sms_data_listen.phone, "+63", "0") = sms_data_status.phone
	
	
	WHERE pos_client.client_id IS NOT NULL 
	
	GROUP BY CASE 
	WHEN pos_client.client_id IS NOT NULL 
	THEN CONCAT(pos_client.first_name, " ", pos_client.last_name) 
	ELSE REPLACE(sms_data_listen.phone, "+63", "0")
	END 
	
	';
	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	
	while ($row = mysqli_fetch_array($result)) {
		
		$contact = $row['contact'];
		$phone = $row['phone'];
		$status = $row['status'];
		
		if ($status == "read") {
			
			$str .= '
			<a href="#" id="link_sms_view_conversation" contact="' . $contact . '" phone="' . $phone . '">
				<span class="user-status is-offline"></span> 
				<em>' . $contact . '</em>
			</a>';
			
			
		} else {
			
			$str .= '
			<a href="#" id="link_sms_view_conversation" contact="' . $contact . '" phone="' . $phone . '">
				<span class="user-status is-online"></span> 
				<em>' . $contact . '</em>
			</a>';
			
			
		}
		
	}
	
	
	echo $str;



}





if(isset($_POST['sms-display_inbox_unknown']) && $_POST['sms-display_inbox_unknown'] == true) {
	
	
	$str = "<strong>Unknown</strong>";
	
	$query = '
	
	SELECT 
	
	CASE 
	WHEN pos_client.client_id IS NOT NULL 
	THEN CONCAT(pos_client.first_name, " ", pos_client.last_name) 
	ELSE REPLACE(sms_data_listen.phone, "+63", "0")
	END AS "contact",
	
	REPLACE(sms_data_listen.phone, "+63", "0") AS "phone",
	
	
	CASE 
	WHEN sms_data_status.status IS NULL THEN "read" 
	ELSE sms_data_status.status
	END AS "status"
	
	FROM sms_data_listen 
	
	LEFT JOIN pos_client 
	ON REPLACE(sms_data_listen.phone, "+63", "0") = pos_client.contact_num1 
	
	
	LEFT JOIN 
	(
	
	SELECT 
	REPLACE(sms_data_listen.phone, "+63", "0") AS "phone",
	sms_data_listen.status
	
	FROM sms_data_listen 
	
	WHERE sms_data_listen.status = "unread"
	
	) AS sms_data_status 
	ON REPLACE(sms_data_listen.phone, "+63", "0") = sms_data_status.phone
	
	
	WHERE pos_client.client_id IS NULL 
	
	GROUP BY CASE 
	WHEN pos_client.client_id IS NOT NULL 
	THEN CONCAT(pos_client.first_name, " ", pos_client.last_name) 
	ELSE REPLACE(sms_data_listen.phone, "+63", "0")
	END 
	
	';
	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	
	while ($row = mysqli_fetch_array($result)) {
		
		$contact = $row['contact'];
		$phone = $row['phone'];
		$status = $row['status'];
		
		if ($status == "read") {
			
			$str .= '
			<a href="#" id="link_sms_view_conversation" contact="' . $contact . '" phone="' . $phone . '">
				<span class="user-status is-offline"></span> 
				<em>' . $contact . '</em>
			</a>';
			
			
		} else {
			
			$str .= '
			<a href="#" id="link_sms_view_conversation" contact="' . $contact . '" phone="' . $phone . '">
				<span class="user-status is-online"></span> 
				<em>' . $contact . '</em>
			</a>';
			
			
		}
		

		
	}
	
	
	echo $str;



}






if(isset($_POST['sms-display_conversation_data']) && $_POST['sms-display_conversation_data'] == true) {
	
	$phone = $_POST['phone'];
	
	$str = "";
	
	$query = "
	
	SELECT 
	CASE 
	WHEN pos_client.client_id IS NOT NULL 
	THEN CONCAT(pos_client.first_name, ' ', pos_client.last_name) 
	ELSE REPLACE(sms_data_listen.phone, '+63', '0')
	END AS 'contact',
	
	REPLACE(sms_data_listen.phone, '+63', '0') AS 'phone',
	
	CASE 
	WHEN sms_data_listen.sender = ''
	THEN 
	
	CASE 
	WHEN pos_client.client_id IS NOT NULL 
	THEN CONCAT(pos_client.first_name, ' ', pos_client.last_name) 
	ELSE REPLACE(sms_data_listen.phone, '+63', '0')
	END 
	
	ELSE 
	
	sms_data_listen.sender 
	
	END AS 'header', 
	
	
	sms_data_listen.sender,
	sms_data_listen.text,
	DATE_FORMAT(sms_data_listen.record_datetime, '%b. %d, %Y %h:%i %p') as 'record_datetime'
	
	
	FROM sms_data_listen 
	
	LEFT JOIN pos_client 
	ON REPLACE(sms_data_listen.phone, '+63', '0') = pos_client.contact_num1 
	
	WHERE REPLACE(sms_data_listen.phone, '+63', '0') = '$phone'
	
	
	ORDER BY sms_data_listen.record_datetime
	
	";
	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	
	while ($row = mysqli_fetch_array($result)) {
		
		/*
		
		
		*/
		
		$contact = $row['contact'];
		$phone = $row['phone'];
		$header = $row['header'];
		$text = $row['text'];
		$record_datetime = $row['record_datetime'];
		
		$str .= '
		
		 <li>
			<span class="user">' . $header  . '</span>
			<span class="time">' . $record_datetime . '</span>
			<p>' . $text . '</p>
		</li>
		
		';

		
	}
	
	
	echo $str;
	
	
	
	//SET UNREAD TO READ STATUS
	$query = "
	UPDATE sms_data_listen 
	SET status = 'read'
	WHERE REPLACE(sms_data_listen.phone, '+63', '0') = '$phone' 
	AND status = 'unread'
	";
	
	mysqli_query($link, $query);



}



if(isset($_POST['sms-display_unread_count']) && $_POST['sms-display_unread_count'] == true) {

	
	$sms_unread_count = '';
	
	$query = "
	
	SELECT 
	COUNT( sms_data_listen.status) AS 'sms_unread_count'
	FROM sms_data_listen 
	WHERE sms_data_listen.status = 'unread' 
	GROUP BY  sms_data_listen.status 
	
	";
	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	
	if ($row = mysqli_fetch_array($result)) {
		
		$sms_unread_count = $row['sms_unread_count'];
		
	} 
	
	
	echo $sms_unread_count;



}


?>