<?php
session_start();

include '../../../../config.php';



$session_user_id = $_SESSION['user_id'];


$date_from = date('Y') . '-' . date('m') . '-' . '01';
$date_to = date('Y-m-d');
$month_now = date('m');
$year_now = date('Y');



//DASHBOARD START

if(isset($_POST['pos-admin-dashboard-display_sales_count-widget']) && $_POST['pos-admin-dashboard-display_sales_count-widget'] == true) {
	
	$resp = array(
	'total' => '',
	'total_paid' => '',
	'total_unpaid' => ''
	);
	
	//$old_pw = mysqli_real_escape_string($link,$_POST['old_pw']);
	//$new_pw = mysqli_real_escape_string($link,$_POST['new_pw']);
	
	
	
	$query = "CALL `SP-admin-dashboard-display_month_sales_count`('$date_from', CURDATE());";
	
	
	
	
	$result = mysqli_query($link,$query);
	
	
	
	if ($row = mysqli_fetch_array($result)) {
		
		$resp['total'] = $row['total'];
		$resp['total_paid'] = $row['total_paid'];
		$resp['total_unpaid'] = $row['total_unpaid'];
		
	}
	
	echo json_encode($resp);	
	
}


if(isset($_POST['pos-admin-dashboard-display_hourly_sales-chart']) && $_POST['pos-admin-dashboard-display_hourly_sales-chart'] == true) {

	if (isset($_POST['start_date']) && isset($_POST['end_date'])) {
		
		$start_date = $_POST['start_date'];
		$end_date = $_POST['end_date'];
		
		$query = "CALL `SP-admin-dashboard-hourly_sales`('$start_date', '$end_date')";
		
	} else {
		
		$query = "CALL `SP-admin-dashboard-hourly_sales`('$date_from', CURDATE())";
		
	}

	
	
	
	$result = mysqli_query($link,$query);
	
	while ($row = mysqli_fetch_array($result)) {
		
		
		$hour_time = $row['hour_time'];
		$total = $row['total'];
		$total_paid = $row['total_paid'];
		$total_unpaid = $row['total_unpaid'];
		
		//add data
		$resp[] = array(
		'hour_time' => $hour_time,
		'total' => (float)$total,
		'total_paid' => (float)$total_paid,
		'total_unpaid' => (float)$total_unpaid
		);
		
		
		
	}
	
	echo json_encode($resp);


}



if(isset($_POST['pos-admin-dashboard-display_daily_sales-chart']) && $_POST['pos-admin-dashboard-display_daily_sales-chart'] == true) {
	
	if (isset($_POST['start_date']) && isset($_POST['end_date'])) {
		
		$start_date = $_POST['start_date'];
		$end_date = $_POST['end_date'];
		
		$query = "CALL `SP-admin-dashboard-display_month_daily_sales_count`('$start_date', '$end_date')";
		
	} else {
		
		$query = "CALL `SP-admin-dashboard-display_month_daily_sales_count`('$date_from', CURDATE())";
		
	}

	
	
	
	$result = mysqli_query($link,$query);
	
	while ($row = mysqli_fetch_array($result)) {
		
		
		$day_num = $row['day'];
		$total = $row['total'];
		$total_paid = $row['total_paid'];
		$total_unpaid = $row['total_unpaid'];
		
		
		
		//add data
		$resp[] = array(
		'day_num' => $day_num,
		'total' => (float)$total,
		'total_paid' => (float)$total_paid,
		'total_unpaid' => (float)$total_unpaid
		);
		
		
		
	}
	
	echo json_encode($resp);
	
	
	
}



if(isset($_POST['pos-admin-dashboard-display_store_contribution-chart']) && $_POST['pos-admin-dashboard-display_store_contribution-chart'] == true) {
	
	if (isset($_POST['start_date']) && isset($_POST['end_date'])) {
		
		$start_date = $_POST['start_date'];
		$end_date = $_POST['end_date'];
		
		$query = "CALL `SP-admin-dashboard-display_store_contribution`('$start_date', '$end_date');";
		
	} else {
		
		$query = "CALL `SP-admin-dashboard-display_store_contribution`('$date_from', CURDATE());";
		
	}
	

	
	

	$result = mysqli_query($link,$query);
	
	
	while ($row = mysqli_fetch_array($result)) {
		
		
		$store_code = $row['store_code'];
		$total = $row['total'];
		$total_paid = $row['total_paid'];
		$total_unpaid = $row['total_unpaid'];
		
		$resp[] = array(
		'store_code' => $store_code,
		'total' => (float)$total
		);
		
		
		
	}
	
	echo json_encode($resp);
	
	
	
}


if(isset($_POST['pos-admin-dashboard-display_customer_sales_analytics-table']) && 
$_POST['pos-admin-dashboard-display_customer_sales_analytics-table'] == true) {
	
	//$old_pw = mysqli_real_escape_string($link,$_POST['old_pw']);
	//$new_pw = mysqli_real_escape_string($link,$_POST['new_pw']);
	

	
	$query = "CALL `SP-admin-dashboard-display_customer_sales_analytics`('$month_now', '$year_now');";

	$result = mysqli_query($link,$query);
	
	$str = '';
	
	
	if ($row = mysqli_fetch_array($result)) {
		
		
		$client_count_new = $row['client_count_new'];
		$client_count_existing = $row['client_count_existing'];
		$client_count_total = $row['client_count_total'];
		
		$client_paid_sales_new = $row['client_paid_sales_new'];
		$client_paid_sales_existing = $row['client_paid_sales_existing'];
		$client_paid_sales_total = $row['client_paid_sales_total'];
		
		$client_unpaid_sales_new = $row['client_unpaid_sales_new'];
		$client_unpaid_sales_existing = $row['client_unpaid_sales_existing'];
		$client_unpaid_sales_total = $row['client_unpaid_sales_total'];
		
		$client_expected_sales_new = ($client_paid_sales_new + $client_unpaid_sales_new );
		$client_expected_sales_existing = ($client_paid_sales_existing + $client_unpaid_sales_existing);
		$client_expected_sales_total = ($client_paid_sales_new + $client_unpaid_sales_total);
		
		
		$str = '
		
		<thead>
                         
			<tr>
				<th>Customer</th>
				<th>New</th>
				<th>Existing</th>
				<th>Total</th>
			   
			</tr>
			
		</thead>
		
		<tbody>
		
			<tr>
				<td>Count</td>
				<td>' . $client_count_new .'</td>
				<td>' . $client_count_existing .'</td>
				<td>' . $client_count_total .'</td>				
			</tr>
			
			<tr>
				<td>Paid Sales</td>
				<td>' . $client_paid_sales_new .'</td>
				<td>' . $client_paid_sales_existing .'</td>
				<td>' . $client_paid_sales_total .'</td>		
			</tr>
			
			<tr>
				<td>Unpaid Sales</td>
				<td>' . $client_unpaid_sales_new .'</td>
				<td>' . $client_unpaid_sales_existing .'</td>
				<td>' . $client_unpaid_sales_total .'</td>		
			</tr>
			
			<tr>
				<td>Expected Sales</td>
				<td>' . $client_expected_sales_new .'</td>
				<td>' . $client_expected_sales_existing .'</td>
				<td>' . $client_expected_sales_total .'</td>		
			</tr>
	   
		</tbody>
		
		
		
		
		';
		
		
		
		
		
		
		
	}
	
	echo $str;
	
	
	
}

//END OF DASHBOARD



?>