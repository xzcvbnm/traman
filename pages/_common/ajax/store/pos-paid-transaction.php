<?php
session_start();

include '../../../../config.php';

$session_user_id = $_SESSION['user_id'];
$session_store_id = $_SESSION['store_id'];
//$session_store_code = $_SESSION['store_code'];

$date_from = date('Y') . '-' . date('m') . '-' . '01';
$date_to = date('Y-m-d');
$month_now = date('m');
$year_now = date('y');



if(isset($_POST['pos-store-display_paid_transaction-table']) && $_POST['pos-store-display_paid_transaction-table'] == true) {
	
	
	if(isset($_POST['start_date']) && isset($_POST['end_date'])) {
		
		
		$start_date = $_POST['start_date'];
		$end_date = $_POST['end_date'];
		
		
		$query = "CALL `SP-store-display_paid_transaction_all`('$session_user_id', '$start_date', '$end_date');";
		
	} else {
		
		$query = "CALL `SP-store-display_paid_transaction_all`('$session_user_id', '$date_from', '$date_to');";
		
	}
	
	
	$str = "";

	$str .= "<thead>";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	$index = 0;
	
	while ($row = mysqli_fetch_field($result)) {
		
		//if ($index >= 1) {
			
			$str .= "<th>";
			$str .= $row->name;
			$str .= "</th>";		
			
		//}
		
		//$index++;
	
		
	}
	
	//add Action Header
	$str .= "<th>";
	$str .= "Actions";
	$str .= "</th>";
			
	
	//CLOSE THEAD
	$str .= "</thead>";
	

	//OPEN TBODY
	$str .= "<tbody>";
		
		
	$index = 0;
	
	while ($row = mysqli_fetch_row($result)) {
		
		$str .= "<tr>";
		
		foreach ($row as $data) {
			
			switch ($index) {
				case 0:
					$id = $data;
					break;
				case 1:
					$client_name = $data;
					break;
				case 2:
					$trans_date = $data;
					break;
			}
										
			$index++;
			
		}
		
		
		//reset index
		$index = 0;
		
	
	
		/*
		<td class="user-name">
			<a href="#" class="name">' . $store_name . '</a>
			
			<span>' . $address_city . '</span>
			
		</td>
		
		*/
		
		$str .= '	
			
			<td class="hidden-xs hidden-sm">
				<span>' . $id . '</span>
			</td>
		
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $client_name . '</span>
			</td>
			
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $trans_date . '</span>
			</td>
		
			
			<td class="action-links">
				<a href="#invoice" class="edit" id="link_load_detail" value="' . $id . '">
					<i class="fa-folder-open-o"></i>
					Load Details
				</a>
			
				<!--
				<a href="#" class="delete" id="link_status_history" value="' . $id . '">
					<i class="fa-history"></i>
					View Status History
				</a>
				-->
				
			</td>
			
		
		';
		
		
		$str .= "</tr>";	
		
	}


	//CLOSE TBODY
	$str .= "</tbody>";
	
	
	echo $str;
	

} 




if(isset($_POST['pos-store-display_paid_transaction_detail']) && $_POST['pos-store-display_paid_transaction_detail'] == true) {
	
	$resp = array(
	'total_price' => '',
	'table_data' => '',
	'store_name' => '',
	'store_address' => '',
	'store_contact' => '',
	'client_name' => '',
	'client_address' => '',
	'client_contact' => '',
	'due_date' => '',
	'record_datetime' => ''
	);	
	
	$id = mysqli_real_escape_string($link,$_POST['id']);
	
	$str = "";

	$str .= "<thead>";
	
	$str .= '<tr class="no-borders">';

	//DATE_FORMAT(pos_trans_master.due_date, '%m/%d/%Y') AS 'due_date',
	
	$query = "SELECT
trans_detail_id,

pos_trans_master.due_date,
DATE_FORMAT(pos_trans_master.record_datetime , '%m/%d/%Y') AS 'record_datetime',
pos_store.store_name,
CONCAT(pos_store.address, ' ', pos_store.address_city) AS 'store_address',
CONCAT(pos_store.num_tel, '/', pos_store.num_cell) AS 'store_contact',
CONCAT(pos_client.first_name, ' ', pos_client.last_name) AS 'client_name',
CONCAT(pos_client.address_city) AS 'client_address',
pos_client.contact_num1 AS 'client_contact',
pos_service_category.name AS 'Category',
pos_service_sub_category.name AS 'Service',
CONCAT(pos_trans_detail.price, '/', pos_trans_detail.unit) AS 'Price/Unit',
TRIM(TRAILING '.' FROM TRIM(TRAILING '0' from pos_trans_detail.unit_count)) AS 'Weight/Pcs',
pos_trans_detail.unit_price AS 'Price',
'' AS 'Items'
FROM pos_trans_master 

LEFT JOIN pos_trans_detail
ON pos_trans_master.trans_master_id = pos_trans_detail.trans_master_id

LEFT JOIN pos_service_sub_category 
ON pos_trans_detail.service_sub_category = pos_service_sub_category.service_sub_category_id 

LEFT JOIN pos_service_category 
ON pos_service_sub_category.service_category_id = pos_service_category.service_category_id 




LEFT JOIN pos_store 
ON pos_trans_master.store_id = pos_store.store_id 

LEFT JOIN pos_client 
ON pos_trans_master.client_id = pos_client.client_id

WHERE pos_trans_master.trans_master_id = '$id' 
AND pos_trans_detail.line_void = 'NO'";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	
	
	//TOTAL ITEM QUANTITY COUNT
	$total_item_qty = 0;
	
	$index = 0;
	
	
	
	//add Action Header
	//$str .= '<th class="hidden-xs">';
	//$str .= "";
	//$str .= "</th>";
	
	while ($row = mysqli_fetch_field($result)) {
		
		if ($index >= 10) {
			
			$str .= "<th>";
			$str .= $row->name;
			$str .= "</th>";		
			
		}
		
		$index++;
	
		
	}
	
	
			
	//CLOSE THEAD
	$str .= "</tr>";
	
	//CLOSE THEAD
	$str .= "</thead>";
	

	//OPEN TBODY
	$str .= "<tbody>";
		
		
	$index = 0;
	
	$total_price = 0;
	
	while ($row = mysqli_fetch_row($result)) {
		
		$str .= "<tr>";
		
		foreach ($row as $data) {
			
			switch ($index) {		
				case 0:
					$id = $data;
					break;
				case 1:
					$due_date = $data;
					break;
				case 2:
					$record_datetime = $data;
					break;
				case 3:
					$store_name = $data;
					break;	
				case 4:
					$store_address = $data;
					break;
				case 5:
					$store_contact = $data;
					break;
				case 6:
					$client_name = $data;
					break;
				case 7:
					$client_address = $data;
					break;
				case 8:
					$client_contact = $data;
					break;
				case 9:
					$service_category = $data;
					break;
				case 10:
					$service_sub_category = $data;
					break;
				case 11:
					$unit_price = $data;
					break;
				case 12:
					$unit_count = $data;
					break;
				case 13:
					$price = $data;
					break;
			}
			
			
			$query1 = "SELECT 
					trans_item_id,
					trans_detail_id,
					item_name,
					qty 
					FROM pos_trans_item 
					WHERE trans_detail_id = '$id' 
					AND item_void = 'NO'
					";
				
			$result1 = mysqli_query($link,$query1);
			
			$item_str = "";
			
			while ($row = mysqli_fetch_array($result1)) {
				
				$item_name = $row['item_name'];
				$qty = $row['qty'];
				
				$item_str .= $qty . " " . $item_name . "<br>"; 
				
				
				$total_item_qty = ($total_item_qty + $qty);
				
				
			}
					
			$index++;
			
		}
		
		
		//reset index
		$index = 0;
		
		
		//add total price
		$total_price = ($total_price + $price);
		
		
		
		$str .= '
			
			<!-- COMMENT
			<td class="action-links hidden-xs">
		
				<a href="#" class="edit" id="link_add" value="' . $id . '" >
					<i class="linecons-t-shirt"></i>
					
				</a>
				
				
				<a href="#" class="edit" id="link_remove" value="' . $id . '" >
					<i class="fa-remove"></i>
					
				</a>
						
			</td>
			-->
			
				
			
			
			<td>
				<span class="email">' . $service_sub_category . '</span>
			</td>
			
			<td>
				<span class="email">' . $unit_price . '</span>
			</td>
			
			<td>
				<span class="email">' . $unit_count . '</span>
			</td>
			
			<td>
				<span class="email">' . $price . '</span>
			</td>
			
			
			<td>
				
				' . $item_str . '
				
			</td>
			
		
		';
		
		
		$str .= "</tr>";	
		
	}
	
	
	
	//INSERT LAST COLUMN TOTAL ITEM QUANTITY COUNT
	$str .= "<tr>";	
	
	$str .= '<td>
				<span class="email">' . 'Total Items' . '</span>
			</td>';
			
	$str .= '<td>
				<span class="email">' . '***' . '</span>
			</td>';
			
	$str .= '<td>
				<span class="email">' . '***' . '</span>
			</td>';
			
	$str .= '<td>
				<span class="email">' . '***' . '</span>
			</td>';
			
	$str .= '<td>
				<span class="email">' . ($total_item_qty / 15) . ' Item</span>
			</td>';
	
	
	$str .= "</tr>";


	//CLOSE TBODY
	$str .= "</tbody>";
	
	
	$resp['total_price'] = $total_price;
	$resp['table_data'] = $str;
	
	$resp['store_name'] = $store_name;
	$resp['store_address'] = $store_address;
	$resp['store_contact'] = $store_contact;
	$resp['client_name'] = $client_name;
	$resp['client_address'] = $client_address;
	$resp['client_contact'] = $client_contact;
	$resp['record_datetime'] = $record_datetime;
	$resp['due_date'] = $due_date;
	
	
	
	echo json_encode($resp);
	

}


if(isset($_POST['pos-store-transaction-display_trans_special-table']) && $_POST['pos-store-transaction-display_trans_special-table'] == true) {
	
	$resp = array(
	'special_total' => '',
	'table_data' => ''
	);
	
	
	$special_total = 0;
	
	$id = mysqli_real_escape_string($link,$_POST['id']);
	$sub_total = mysqli_real_escape_string($link,$_POST['sub_total']);
	
	$str = "";
	
	

	$str .= '<thead>';
	
	$str .= '<tr class="no-borders">';

	$query = "SELECT 
			pos_trans_special.trans_special_id,
			pos_special.value,
			pos_special.special_type,
			pos_special.value_type,
			pos_special.name AS 'Special',
			'' AS 'Price'
			
			FROM pos_trans_special 
			
			LEFT JOIN pos_special 
			ON pos_trans_special.special_id = pos_special.special_id
			
			WHERE pos_trans_special.trans_master_id = '$id' 
			AND pos_trans_special.special_void = 'NO'";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	$index = 0;
	
	
	
	//add Action Header
	//$str .= '<th class="hidden-xs">';
	//$str .= "";
	//$str .= "</th>";
	
	while ($row = mysqli_fetch_field($result)) {
		
		if ($index == 4) {
			
			$str .= '<th Style="text-transform: capitalize !important;">';
			$str .= $row->name;
			$str .= "</th>";		
			
		} elseif ($index == 5) {
			
			
			
			$str .= '<th Style="text-transform: capitalize !important;">';
			$str .= $row->name;
			$str .= "</th>";
			
			
			$str .= '<th Style="text-transform: capitalize !important;">';
			$str .= '';
			$str .= "</th>";	
			
			
			
			
		}
		
		$index++;
	
		
	}
	
	
			
	//CLOSE table row
	$str .= "</tr>";
	
	//CLOSE THEAD
	$str .= "</thead>";
	

	//OPEN TBODY
	$str .= "<tbody>";
		
		
	$index = 0;
	
	$total_price = 0;
	
	
	while ($row = mysqli_fetch_row($result)) {
		
		$str .= "<tr>";
		
		foreach ($row as $data) {
			
			switch ($index) {
				case 0:
					$id = $data;
					break;
				case 1:
					$value = $data;
					break;
				case 2:
					$special_type = $data;
					break;
				case 3:
					$value_type = $data;
					break;
				case 4:
					$name = $data;
					break;
				case 5:
					$price = $data;
					break;
			}
			
			$index++;
			
		}
		
		
		//reset index
		$index = 0;
		
		
		
		
		
		if ($special_type == "DISCOUNT") {
			
			if ($value_type == "PERCENTAGE") {
				
				$percent_value = ($value / 100);
				
				$special_price = (-1 * abs(($sub_total * $percent_value)));
				
				
			} else {
				
				$special_price = (-1 * $value);
				
			}
			
			
		} else {
			
			if ($value_type == "PERCENTAGE") {
				
				$percent_value = ($value / 100);
				
				$special_price = ($sub_total * $percent_value);
				
				
			} else {
				
				$special_price = ($value);
				
			}
						
		}
				
		$special_total = ($special_total + $special_price);
		
	
		$str .= '
			
			<td>
				<span class="email">' . $name . '</span>
			</td>
			
			
			
			
			<td>
				<span class="email">' . number_format($special_price,2) . '</span>
			</td>
		
		';
		
		
		$str .= "</tr>";	
		
	}
	
	$row_count = mysqli_num_rows($result);
	
	if ($row_count == 0) {
			
		$str .= '
		<tr>
			<td colspan="5">
				<span class="email">No Special Service/Discount</span>
			</td>
		</tr>
		
		';
		
	}
	
	


	//CLOSE TBODY
	$str .= "</tbody>";
	
	
	$resp['special_total'] = $special_total;
	$resp['table_data'] = $str;
	
	
	
	echo json_encode($resp);
	

}



if(isset($_POST['pos-store-transaction-void_transaction']) && $_POST['pos-store-transaction-void_transaction'] == true) {
	
	$resp = array(
	'status' => '',
	'message' => ''
	);
	
	$trans_master_id = mysqli_real_escape_string($link,$_POST['id']);
	$status = mysqli_real_escape_string($link,$_POST['status']);
	
	
	
	$query = "CALL `SP-store-transaction-void_transaction`('$trans_master_id', '$session_store_id', '$status');";

	$result = mysqli_query($link,$query);
	
	if (!$result) {
		
		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);
		
	
	} else {
		
		$resp['status'] = 'success';
	
	}
	
	
	echo json_encode($resp);


}



?>