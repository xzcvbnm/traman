<?php
session_start();

include '../../../../config.php';

$session_user_id = $_SESSION['user_id'];
$session_store_id = $_SESSION['store_id'];


$date_from = date('Y') . '-' . date('m') . '-' . '01';
$date_to = date('Y-m-d');


if(isset($_POST['pos-admin-display_transaction_report-table']) && $_POST['pos-admin-display_transaction_report-table'] == true) {
	
	if (isset($_POST['start_date']) && isset($_POST['end_date'])) {
		
		$start_date = $_POST['start_date'] . ' 01:00:00';
		$end_date = $_POST['end_date'] . ' 23:59:59';
		
		$query = "CALL `SP-admin-report_transaction`('$start_date', '$end_date', '$session_store_id');";
		
	} else {
		
		$query = "CALL `SP-admin-report_transaction`('$date_from', '$date_to', '$session_store_id');";
		
	}
	
	$str = "";

	$str .= "<thead>";


	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	$index = 0;
	
	while ($row = mysqli_fetch_field($result)) {
		
		//if ($index >= 2) {
			
			$str .= "<th>";
			$str .= $row->name;
			$str .= "</th>";		
			
		//}
		
		//$index++;
	
		
	}
			
	
	//CLOSE THEAD
	$str .= "</thead>";
	

	//OPEN TBODY
	$str .= "<tbody>";
		
		
	$index = 0;
	
	$total = 0;
	
	while ($row = mysqli_fetch_array($result)) {

        $store = $row['Store-JO#'];
		$status = $row['Status'];
		$customer = $row['Customer'];
		$cashier = $row['Cashier'];
		$date = $row['Date'];
		$amount = $row['Amount'];
		
		$total = $total + $amount;
		
		$str .= '
		
			<tr>
			
			<td>
				<span>' . $store . '</span>
			</td>
			
			<td>
				<span>' . $status . '</span>
			</td>
			
			<td>
				<span>' . $customer . '</span>
			</td>
			
			<td>
				<span>' . $cashier . '</span>
			</td>
			
			<td>
				<span>' . $date . '</span>
			</td>
			
			<td>
				<span>' . number_format($amount,2) . '</span>
			</td>
			
			
			</tr>
			
		';
		
	}
	
	
	
	$str .= '
		
			<tr>
			
			<td>
				<span>' . 'TOTAL' . '</span>
			</td>
			
			<td>
				<span>' . '**' . '</span>
			</td>
			
			<td>
				<span>' . '**' . '</span>
			</td>
			
			<td>
				<span>' . '**' . '</span>
			</td>
			
			<td>
				<span>' . '**' . '</span>
			</td>
			
			<td>
				<span>' . number_format($total,2) . '</span>
			</td>
			
			
			</tr>
			
		';
		
	

	//CLOSE TBODY
	$str .= "</tbody>";
	
	
	echo $str;

	
}




?>