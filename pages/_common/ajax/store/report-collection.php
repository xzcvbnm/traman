<?php
session_start();

include '../../../../config.php';

$session_user_id = $_SESSION['user_id'];
$session_store_id = $_SESSION['store_id'];

$date_from = date('Y') . '-' . date('m') . '-' . '01';
$date_to = date('Y-m-d');


if(isset($_POST['pos-admin-dashboard-display_daily_sales-chart']) && $_POST['pos-admin-dashboard-display_daily_sales-chart'] == true) {
	
	if (isset($_POST['start_date']) && isset($_POST['end_date'])) {
		
		$start_date = $_POST['start_date'];
		$end_date = $_POST['end_date'];
		
		$query = "CALL `SP-admin-dashboard-display_month_daily_sales_count`('$start_date', '$end_date')";
		
	} else {
		
		$query = "CALL `SP-admin-dashboard-display_month_daily_sales_count`('$date_from', '$date_to')";
		
	}

	
	
	
	$result = mysqli_query($link,$query);
	
	while ($row = mysqli_fetch_array($result)) {
		
		
		$day_num = $row['day'];
		$total = $row['total'];
		$total_paid = $row['total_paid'];
		$total_unpaid = $row['total_unpaid'];
		
		
		
		//add data
		$resp[] = array(
		'day_num' => $day_num,
		'total' => (float)$total,
		'total_paid' => (float)$total_paid,
		'total_unpaid' => (float)$total_unpaid
		);
		
		
		
	}
	
	echo json_encode($resp);
	
	
	
}



if(isset($_POST['pos-admin-dashboard-display_store_contribution-chart']) && $_POST['pos-admin-dashboard-display_store_contribution-chart'] == true) {
	
	if (isset($_POST['start_date']) && isset($_POST['end_date'])) {
		
		$start_date = $_POST['start_date'];
		$end_date = $_POST['end_date'];
		
		$query = "CALL `SP-admin-dashboard-display_store_contribution`('$start_date', '$end_date');";
		
	} else {
		
		$query = "CALL `SP-admin-dashboard-display_store_contribution`('$date_from', '$date_to');";
		
	}
	

	
	

	$result = mysqli_query($link,$query);
	
	
	while ($row = mysqli_fetch_array($result)) {
		
		
		$store_code = $row['store_code'];
		$total = $row['total'];
		$total_paid = $row['total_paid'];
		$total_unpaid = $row['total_unpaid'];
		
		$resp[] = array(
		'store_code' => $store_code,
		'total' => (float)$total
		);
		
		
		
	}
	
	echo json_encode($resp);
	
	
	
}


if(isset($_POST['pos-admin-dashboard-display_customer_sales_analytics-table']) && 
$_POST['pos-admin-dashboard-display_customer_sales_analytics-table'] == true) {
	
	//$old_pw = mysqli_real_escape_string($link,$_POST['old_pw']);
	//$new_pw = mysqli_real_escape_string($link,$_POST['new_pw']);
	

	
	$query = "CALL `SP-admin-dashboard-display_customer_sales_analytics`('$month_now', '$year_now');";

	$result = mysqli_query($link,$query);
	
	$str = '';
	
	
	if ($row = mysqli_fetch_array($result)) {
		
		
		$client_count_new = $row['client_count_new'];
		$client_count_existing = $row['client_count_existing'];
		$client_count_total = $row['client_count_total'];
		
		$client_paid_sales_new = $row['client_paid_sales_new'];
		$client_paid_sales_existing = $row['client_paid_sales_existing'];
		$client_paid_sales_total = $row['client_paid_sales_total'];
		
		$client_unpaid_sales_new = $row['client_unpaid_sales_new'];
		$client_unpaid_sales_existing = $row['client_unpaid_sales_existing'];
		$client_unpaid_sales_total = $row['client_unpaid_sales_total'];
		
		$client_expected_sales_new = ($client_paid_sales_new + $client_unpaid_sales_new );
		$client_expected_sales_existing = ($client_paid_sales_existing + $client_unpaid_sales_existing);
		$client_expected_sales_total = ($client_paid_sales_new + $client_unpaid_sales_total);
		
		
		$str = '
		
		<thead>
                         
			<tr>
				<th>Customer</th>
				<th>New</th>
				<th>Existing</th>
				<th>Total</th>
			   
			</tr>
			
		</thead>
		
		<tbody>
		
			<tr>
				<td>Count</td>
				<td>' . $client_count_new .'</td>
				<td>' . $client_count_existing .'</td>
				<td>' . $client_count_total .'</td>				
			</tr>
			
			<tr>
				<td>Paid Sales</td>
				<td>' . $client_paid_sales_new .'</td>
				<td>' . $client_paid_sales_existing .'</td>
				<td>' . $client_paid_sales_total .'</td>		
			</tr>
			
			<tr>
				<td>Unpaid Sales</td>
				<td>' . $client_unpaid_sales_new .'</td>
				<td>' . $client_unpaid_sales_existing .'</td>
				<td>' . $client_unpaid_sales_total .'</td>		
			</tr>
			
			<tr>
				<td>Expected Sales</td>
				<td>' . $client_expected_sales_new .'</td>
				<td>' . $client_expected_sales_existing .'</td>
				<td>' . $client_expected_sales_total .'</td>		
			</tr>
	   
		</tbody>
		
		
		
		
		';
		
		
		
		
		
		
		
	}
	
	echo $str;
	
	
	
}

//END OF DASHBOARD



if(isset($_POST['pos-admin-display_collection_report-table']) && $_POST['pos-admin-display_collection_report-table'] == true) {
	
	if (isset($_POST['start_date']) && isset($_POST['end_date'])) {
		
		$start_date = $_POST['start_date'] . ' 01:00:00';
		$end_date = $_POST['end_date'] . ' 23:59:59';
		
		$query = "CALL `SP-admin-report_collection`('$start_date', '$end_date', '$session_store_id');";
		
	} else {
		
		$query = "CALL `SP-admin-report_collection`('$date_from', '$date_to', '$session_store_id');";
		
	}
	
	$str = "";

	$str .= "<thead>";


	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	$index = 0;
	
	while ($row = mysqli_fetch_field($result)) {
		
		//if ($index >= 2) {
			
			$str .= "<th>";
			$str .= $row->name;
			$str .= "</th>";		
			
		//}
		
		//$index++;
	
		
	}
			
	
	//CLOSE THEAD
	$str .= "</thead>";
	

	//OPEN TBODY
	$str .= "<tbody>";
		
		
	$index = 0;
	
	$total = 0;
	
	while ($row = mysqli_fetch_array($result)) {
		
		$store = $row['Store'];
		$customer = $row['Customer'];
		$jo = $row['JO#'];
		$received_by = $row['Received By'];
		$date_collected = $row['Date Collected'];
		$payment_type = $row['Payment Type'];
		$amount = $row['Amount'];
		
		$total = $amount + $total;
		
		
		$str .= '
		
			<tr>
			
			<td>
				<span>' . $store . '</span>
			</td>
			
			<td>
				<span>' . $customer . '</span>
			</td>
			
			<td>
				<span>' . $jo . '</span>
			</td>
			
			<td>
				<span>' . $received_by . '</span>
			</td>
			
			<td>
				<span>' . $date_collected . '</span>
			</td>
			
			<td>
				<span>' . $payment_type . '</span>
			</td>
			
			<td>
				<span>' . number_format($amount,2) . '</span>
			</td>
			
			
			</tr>
			
		';
		
	}
	
	
	
	//TOTAL
	
	
	$str .= '
		
			<tr>
			
			<td>
				<span>' . 'TOTAL' . '</span>
			</td>
			
			<td>
				<span>' . '**' . '</span>
			</td>
			
			<td>
				<span>' . '**' . '</span>
			</td>
			
			<td>
				<span>' . '**' . '</span>
			</td>
			
			<td>
				<span>' . '**' . '</span>
			</td>
			
			<td>
				<span>' . '**' . '</span>
			</td>
			
			<td>
				<span>' . number_format($total,2) . '</span>
			</td>
			
			
			</tr>
			
		';
	

	//CLOSE TBODY
	$str .= "</tbody>";
	
	
	echo $str;

	
}




?>