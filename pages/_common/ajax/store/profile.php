<?php
session_start();

include '../../../../config.php';

$session_user_id = $_SESSION['user_id'];
$session_store_id = $_SESSION['store_id'];
//$session_store_code = $_SESSION['store_code'];

$date_from = date('Y') . '-' . date('m') . '-' . '01';
$date_to = date('Y-m-d');
$month_now = date('m');
$year_now = date('y');



if(isset($_POST['pos-store-load_user_profile']) && $_POST['pos-store-load_user_profile'] == true) {
	
	$resp = array(
	'username' => '',
	'first_name' => '',
	'middle_name' => '',
	'last_name' => '',
	'store_designation' => ''
	);
	
	$query = "CALL `SP-store-view_profile`('$session_user_id')";

	$result = mysqli_query($link,$query);
	
	if ($row = mysqli_fetch_array($result)) {
		
		$resp['username'] = $row['username'];
		$resp['first_name'] = $row['first_name'];
		$resp['middle_name'] = $row['middle_name'];
		$resp['last_name'] = $row['last_name'];
		$resp['store_designation'] = $row['store_name'];	
		
	}
	
	
	
	echo json_encode($resp);
	
	
	
}


if(isset($_POST['pos-store-update_user_profile']) && $_POST['pos-store-update_user_profile'] == true) {
	
	$resp = array(
	'status' => '',
	'message' => ''
	);
	

	$first_name = mysqli_real_escape_string($link,$_POST['first_name']);
	$middle_name = mysqli_real_escape_string($link,$_POST['middle_name']);
	$last_name = mysqli_real_escape_string($link,$_POST['last_name']);
	
	
	$query = "CALL `SP-store-update_profile`('$session_user_id', '$first_name', '$middle_name', '$last_name');";

	$result = mysqli_query($link,$query);
	
	if (!$result) {
		
		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);
		
	
	} else {
		
		$resp['status'] = 'success';
	
		
	}
	
	
	echo json_encode($resp);


}



if(isset($_POST['pos-store-change_pw']) && $_POST['pos-store-change_pw'] == true) {
	
	$resp = array(
	'status' => '',
	'message' => ''
	);
	
	$old_pw = mysqli_real_escape_string($link,$_POST['old_pw']);
	$new_pw = mysqli_real_escape_string($link,$_POST['new_pw']);
	
	$query = "SELECT * FROM pos_user 
		WHERE user_id = '$session_user_id' 
		AND password = '$old_pw';
		";

	$result = mysqli_query($link,$query);
	
	if ($row = mysqli_fetch_array($result)) {
			
			
			$result1 = mysqli_query($link,"
			UPDATE pos_user 
			SET `password` = '$new_pw',
			`require_new_pw` = 'NO'
			WHERE `user_id` = '$session_user_id';
			");
			
			
			if (!$result1) {
		
				$resp['status'] = 'failed';
				$resp['message'] = mysqli_error($link);
				
			
			} else {
				
				$resp['status'] = 'success';
			
				
			}
			
		
	} else {
		
		$resp['status'] = 'failed';
    	$resp['message'] = "Old password incorrect!";
		
		
	}
	
	
	echo json_encode($resp);
	
	
}



?>