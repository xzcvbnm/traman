<?php
session_start();

include '../../../../config.php';

$session_user_id = $_SESSION['user_id'];
$session_store_id = $_SESSION['store_id'];
$session_store_code = $_SESSION['store_code'];


$session_surcharge_store = $_SESSION['surcharge_store'];
$session_surcharge_pickup = $_SESSION['surcharge_pickup'];


$date_from = date('Y') . '-' . date('m') . '-' . '01';
$date_to = date('Y-m-d');
$month_now = date('m');
$year_now = date('y');


$trans_space_count = 26;



//load trans special details without link buttons
if(isset($_POST['pos-store-transaction-display_trans_special-table']) && $_POST['pos-store-transaction-display_trans_special-table'] == true) {
	
	$resp = array(
	'special_total' => '',
	'table_data' => ''
	);
	
	
	$special_total = 0;
	$less_total = 0;
	
	$id = mysqli_real_escape_string($link,$_POST['id']);
	$sub_total = mysqli_real_escape_string($link,$_POST['sub_total']);
	
	$str = "";
	
	

	$str .= '<thead>';
	
	$str .= '<tr class="no-borders">';

	$query = "SELECT 
			pos_trans_special.trans_special_id,
			pos_special.value,
			pos_special.special_type,
			pos_special.value_type,
			pos_special.name AS 'Special',
			'' AS 'Price'
			
			FROM pos_trans_special 
			
			LEFT JOIN pos_special 
			ON pos_trans_special.special_id = pos_special.special_id
			
			WHERE pos_trans_special.trans_master_id = '$id' 
			AND pos_trans_special.special_void = 'NO'";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	$index = 0;
	
	
	
	//add Action Header
	//$str .= '<th class="hidden-xs">';
	//$str .= "";
	//$str .= "</th>";
	
	while ($row = mysqli_fetch_field($result)) {
		
		if ($index == 4) {
			
			$str .= '<th Style="text-transform: capitalize !important;">';
			$str .= $row->name;
			$str .= "</th>";		
			
		} elseif ($index == 5) {
			
			
			
			$str .= '<th Style="text-transform: capitalize !important;">';
			$str .= $row->name;
			$str .= "</th>";
			
			
			$str .= '<th Style="text-transform: capitalize !important;">';
			$str .= '';
			$str .= "</th>";	
			
			
			
			
		}
		
		$index++;
	
		
	}
	
	
			
	//CLOSE table row
	$str .= "</tr>";
	
	//CLOSE THEAD
	$str .= "</thead>";
	

	//OPEN TBODY
	$str .= "<tbody>";
		
		
	$index = 0;
	
	$total_price = 0;
	
	
	while ($row = mysqli_fetch_row($result)) {
		
		$str .= "<tr>";
		
		foreach ($row as $data) {
			
			switch ($index) {
				case 0:
					$id = $data;
					break;
				case 1:
					$value = $data;
					break;
				case 2:
					$special_type = $data;
					break;
				case 3:
					$value_type = $data;
					break;
				case 4:
					$name = $data;
					break;
				case 5:
					$price = $data;
					break;
			}
			
			$index++;
			
		}
		
		
		//reset index
		$index = 0;
		
		
		
		
		
		if ($special_type == "DISCOUNT") {

			if ($value_type == "PERCENTAGE") {

				$percent_value = ($value / 100);

				$special_price = (-1 * abs(($sub_total * $percent_value)));


			} else {

				$special_price = (-1 * $value);

			}
			
			
			$less_total = $less_total + $special_price;


		} else {

			if ($value_type == "PERCENTAGE") {

				$percent_value = ($value / 100);

				$special_price = ($sub_total * $percent_value);


			} elseif ($value_type == "SURCHARGE") {

				if ($special == "STORE SURCHARGE") {
					
					
					if ($session_surcharge_store > $sub_total) {

						$special_price = $session_surcharge_store - $sub_total;
	
	
					} else {
	
						$special_price = 0;
	
					}
					
					
					
				} else {
					
					
					if ($session_surcharge_pickup > $sub_total) {

						$special_price = $session_surcharge_pickup - $sub_total;
	
	
					} else {
	
						$special_price = 0;
	
	
					}
					
					
				}

			} else {

				$special_price = ($value);

			}
			
			
			$special_total = ($special_total + $special_price);
			

		}
		
	
		$str .= '
			
			<td>
				<span class="email">' . $name . '</span>
			</td>
			
			
			
			
			<td>
				<span class="email">' . number_format($special_price,2) . '</span>
			</td>
		
		';
		
		
		$str .= "</tr>";	
		
	}
	
	$row_count = mysqli_num_rows($result);
	
	if ($row_count == 0) {
			
		$str .= '
		<tr>
			<td colspan="5">
				<span class="email">No Special Service/Discount</span>
			</td>
		</tr>
		
		';
		
	}
	
	


	//CLOSE TBODY
	$str .= "</tbody>";
	
	
	$resp['special_total'] = $special_total;
	$resp['table_data'] = $str;
	
	
	
	echo json_encode($resp);
	

}


if(isset($_POST['pos-store-display_paid_transaction_detail']) && $_POST['pos-store-display_paid_transaction_detail'] == true) {
	
	
	$resp = array(
	'total_price' => '',
	'table_data' => '',
	'store_code' => '',
	'store_name' => '',
	'store_address' => '',
	'store_num_tel' => '',
	'store_num_cell' => '',
	'client_name' => '',
	'client_address' => '',
	'client_contact1' => '',
	'client_contact2' => '',
	'due_date' => '',
	'record_datetime' => '',
	'email' => '',
	'claim_by' => '',
	'claim_datetime' => '',
	'store_code' => '',
	'due_date1' => '',
	'client_credit' => '',
	'local_code' => '',
	'total_special_price' => '',
	'total_discount_price' => '',
	'total_service_fee_price' => '',
	'total_overall' => ''
	);

	$id = mysqli_real_escape_string($link,$_POST['id']);
	
	$trans_master_id = mysqli_real_escape_string($link,$_POST['id']);

	$str = "";

	$str .= "<thead>";

	$str .= '<tr class="no-borders">';

	//DATE_FORMAT(pos_trans_master.due_date, '%m/%d/%Y') AS 'due_date',

	$query = "SELECT
trans_detail_id,

DATE_FORMAT(pos_trans_master.due_date, '%Y-%m-%d'),
DATE_FORMAT(pos_trans_master.record_datetime , '%m/%d/%Y') AS 'record_datetime',
pos_store.store_name,
CONCAT(pos_store.address, ' ', pos_store.address_city) AS 'store_address',
pos_store.num_tel,
pos_store.num_cell,
CONCAT(pos_client.first_name, ' ', pos_client.last_name) AS 'client_name',
CONCAT(pos_client.address_city) AS 'client_address',
pos_client.contact_num1,
pos_client.contact_num2,
pos_client.email,
pos_trans_master.claim_by,
DATE_FORMAT(pos_trans_master.claim_datetime,'%b %d %Y %h:%i %p') AS 'claim_datetime',
pos_store.store_code,
DATE_FORMAT(pos_trans_master.due_date, '%b %d %Y') AS 'due_date1', 
CASE WHEN credit.credit IS NULL THEN 0 ELSE credit.credit END AS 'credit',
pos_trans_master.local_code,
pos_service_and_price.name AS 'Service Instruction',
TRIM(TRAILING '.' FROM TRIM(TRAILING '0' from pos_trans_detail.unit_count)) AS '# of kl(s)',
pos_trans_detail.unit_price AS 'Amount'
FROM pos_trans_master

LEFT JOIN pos_trans_detail
ON pos_trans_master.trans_master_id = pos_trans_detail.trans_master_id

LEFT JOIN pos_service_and_price
ON pos_trans_detail.service_id = pos_service_and_price.service_id




LEFT JOIN pos_store
ON pos_trans_master.store_id = pos_store.store_id

LEFT JOIN pos_client
ON pos_trans_master.client_id = pos_client.client_id 

LEFT JOIN 
(

SELECT 
client_id,
SUM(amount) AS 'credit'
FROM pos_client_credit 
GROUP BY client_id

) AS credit
ON pos_trans_master.client_id = credit.client_id



WHERE pos_trans_master.trans_master_id = '$id'
AND pos_trans_detail.line_void = 'NO'";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));



	//TOTAL ITEM QUANTITY COUNT
	$total_item_qty = 0;

	$index = 0;



	//add Action Header
	//$str .= '<th class="hidden-xs">';
	//$str .= "";
	//$str .= "</th>";

	while ($row = mysqli_fetch_field($result)) {

		if ($index >= 18) {

			$str .= "<th>";
			$str .= $row->name;
			$str .= "</th>";

		}

		$index++;


	}



	//CLOSE THEAD
	$str .= "</tr>";

	//CLOSE THEAD
	$str .= "</thead>";


	//OPEN TBODY
	$str .= "<tbody>";


	$index = 0;

	$total_price = 0;


	//for inserting blank rows
	$service_line_count = 0;
	

	while ($row = mysqli_fetch_row($result)) {

		foreach ($row as $data) {

			switch ($index) {
				case 0:
					$id = $data;
					break;
				case 1:
					$due_date = $data;
					break;
				case 2:
					$record_datetime = $data;
					break;
				case 3:
					$store_name = $data;
					break;
				case 4:
					$store_address = $data;
					break;
				case 5:
					$store_num_tel = $data;
					break;
				case 6:
					$store_num_cell = $data;
					break;
				case 7:
					$client_name = $data;
					break;
				case 8:
					$client_address = $data;
					break;
				case 9:
					$client_contact1 = $data;
					break;
				case 10:
					$client_contact2 = $data;
					break;
				case 11:
					$email = $data;
					break;
				case 12:
					$claim_by = $data;
					break;
				case 13:
					$claim_datetime = $data;
					break;
				case 14:
					$store_code = $data;
					break;
				case 15:
					$due_date1 = $data;
					break;
				case 16:
					$credit = $data;
					break;
				case 17:
					$local_code = $data;
					break;
				case 18:
					$service = $data;
					break;
				case 19:
					$unit_count = $data;
					break;
				case 20:
					$price = $data;
					break;
			}


			$index++;


		}


		//reset index
		$index = 0;


		//add total price
		$total_price = ($total_price + $price);


		//increament service line counter
		$service_line_count++;


		$str .= "<tr>";

		$str .= '

			<td>
				<span class="email">' . $service . '</span>
			</td>

			<td>
				<span class="email">' . $unit_count . '</span>
			</td>

			<td>
				<span class="email">' . $price . '</span>
			</td>


		';


		$str .= "</tr>";

	}
	
	
	
	//SPECIAL SERVICES
	//========================================================================================================
	
	$sub_total = $total_price;
	
	$query = "SELECT
			pos_trans_special.trans_special_id,
			pos_special.value,
			pos_special.special_type,
			pos_special.value_type,
			pos_special.name AS 'special'
			FROM pos_trans_special

			LEFT JOIN pos_special
			ON pos_trans_special.special_id = pos_special.special_id

			WHERE pos_trans_special.trans_master_id = '$trans_master_id'
			AND pos_trans_special.special_void = 'NO' 
			
			ORDER BY pos_special.priority ASC
			
			";
	
	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	$total_special_price = 0;
	$total_discount_price = 0;
	$total_service_fee_price = 0;
	$total_vat_price = 0;
	
	while ($row = mysqli_fetch_array($result)) {
		
		//increament service line counter
		$service_line_count++;
		
		$value = $row['value'];
		$special_type = $row['special_type'];
		$value_type = $row['value_type'];
		$special = $row['special'];	
		
		
		if ($special_type == "MARK-UP") {
			
			if ($value_type == "PERCENTAGE") {

				$percent_value = ($value / 100);

				$special_price = ($sub_total * $percent_value);


			}  else {

				$special_price = ($value);

			}
			
			
			//ADD ALL SPECIAL PRICE
			$total_special_price = $special_price + $total_special_price;
			
			
		} elseif ($special_type == "STORE SERVICE FEE") {
			
			
			if ($session_surcharge_store > $sub_total) {

				$special_price = $session_surcharge_store - ($sub_total + $total_special_price);


			} else {

				$special_price = 0;

			}
			
			//SERVICE FEE -> STORE
			$total_service_fee_price = $special_price;
						
		}  elseif ($special_type == "PICK-UP SERVICE FEE") {
			
			if ($session_surcharge_pickup > $sub_total) {

				$special_price = $session_surcharge_pickup - $sub_total;


			} else {

				$special_price = 0;


			}
			
			//SERVICE FEE -> PICK-UP
			$total_service_fee_price = $special_price;
			
			
		}  elseif ($special_type == "DISCOUNT") {
			
			
			if ($value_type == "PERCENTAGE") {

				$percent_value = ($value / 100);

				$special_price = (-1 * abs(( ($sub_total + $total_special_price + $total_service_fee_price) * $percent_value)));


			} else {

				$special_price = (-1 * $value);

			}
			
			
			//ADD ALL DISCOUNT PRICE
			$total_discount_price = $special_price + $total_discount_price;
			
			
			
			
		} elseif ($special_type == "VAT") {
			
	
		
			
		}
		
	
		$str .= "<tr>";	
				
		$str .= '
	
			<td>
				<span class="email">' . $special . '</span>
			</td>
			
			<td>
				<span class="email">' . '**' . '</span>
			</td>
	
	
			<td>
				<span class="email">' . number_format($special_price,2) . '</span>
			</td>
	
		';
	
		$str .= "</tr>";	
		
	}
	
	
	
	//========================================================================================================
	
	
	
	
	
	$blank_row = $trans_space_count - $service_line_count;

	while ($blank_row > 0) {


		$str .= "<tr>";

		$str .= '



			<td>
				<span class="email">' . '&nbsp;' . '</span>
			</td>



			<td>
				<span class="email">' . '' . '</span>
			</td>

			<td>
				<span class="email">' . '' . '</span>
			</td>


		';


		$str .= "</tr>";


		$blank_row--;


	}
	
	
	


	//CLOSE TBODY
	$str .= "</tbody>";


	$resp['total_price'] = $total_price;
	$resp['table_data'] = $str;

	$resp['store_name'] = $store_name;
	$resp['store_code'] = $store_code;
	$resp['store_address'] = $store_address;
	$resp['store_num_tel'] = $store_num_tel;
	$resp['store_num_cell'] = $store_num_cell;
	$resp['client_name'] = $client_name;
	$resp['client_address'] = $client_address;
	$resp['client_contact1'] = $client_contact1;
	$resp['client_contact2'] = $client_contact2;
	$resp['record_datetime'] = $record_datetime;
	$resp['due_date'] = $due_date;
	$resp['due_date1'] = $due_date1;
	
	$resp['email'] = $email;

	$resp['claim_by'] = $claim_by;
	$resp['claim_datetime'] = $claim_datetime;
	
	$resp['store_code'] = $store_code;
	
	$resp['client_credit'] = $credit;
	
	$resp['local_code'] = $local_code;
	
	
	$resp['total_special_price'] = $total_special_price;
	$resp['total_discount_price'] = $total_discount_price;
	$resp['total_service_fee_price'] = $total_service_fee_price;
	
	$total_overall = $total_special_price + $total_discount_price + $total_service_fee_price + $sub_total;
	
	$resp['total_overall'] = $total_overall;
	

	echo json_encode($resp);
	
	
	
	
	//===================================================================================================
	exit;
	
	$resp = array(
	'total_price' => '',
	'table_data' => ''
	);	
	
	$id = mysqli_real_escape_string($link,$_POST['id']);
	
	$trans_master_id = mysqli_real_escape_string($link,$_POST['id']);
	
	$str = "";

	$str .= "<thead>";
	
	$str .= '<tr class="no-borders">';

	$query = "SELECT 
			trans_detail_id,
			pos_service_and_price.name AS 'Service Instruction',
			TRIM(TRAILING '.' FROM TRIM(TRAILING '0' from pos_trans_detail.unit_count)) AS '# of kl(s)',
			pos_trans_detail.unit_price AS 'Amount'
			FROM pos_trans_detail 
			
			LEFT JOIN pos_service_and_price 
			ON pos_trans_detail.service_id = pos_service_and_price.service_id 
			
			WHERE pos_trans_detail.trans_master_id = '$id' 
			AND pos_trans_detail.line_void = 'NO' 
			
			";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	
	$total_item_qty = 0;
	
	$index = 0;
	
	while ($row = mysqli_fetch_field($result)) {
		
		if ($index >= 1) {
			
			$str .= "<th>";
			$str .= $row->name;
			$str .= "</th>";		
			
		}
		
		$index++;
	
		
	}
	
	
			
	//CLOSE THEAD
	$str .= "</tr>";
	
	//CLOSE THEAD
	$str .= "</thead>";
	

	//OPEN TBODY
	$str .= "<tbody>";
		
		
	$index = 0;
	
	$total_price = 0;
	
	$special_total = 0;
	$less_total = 0;
	
	$service_line_count = 0;
	
	while ($row = mysqli_fetch_row($result)) {
		
		
		
		foreach ($row as $data) {
			
			switch ($index) {
				case 0:
					$id = $data;
					break;
				case 1:
					$service = $data;
					break;
				case 2:
					$unit_count = $data;
					break;
				case 3:
					$price = $data;
					break;
			}
						
					
			$index++;
			
		}
		
		
		//reset index
		$index = 0;
		
		$service_line_count++;
		
		//add total price
		$total_price = ($total_price + $price);
		
		$str .= "<tr>";
		
		$str .= '
		
						
			<td>
				<span class="email">' . $service . '</span>
			</td>
			
			
			<td>
				<span class="email">' . $unit_count . '</span>
			</td>
			
			<td>
				<span class="email">' . $price . '</span>
			</td>
			
			
		
		';
		
		
		$str .= "</tr>";	
		
	}
	
	
	
	//========================================================================================================
	
	$sub_total = $total_price;
	
	$total_special_price = 0;
	$total_discount_price = 0;
	$total_service_fee_price = 0;
	$total_vat_price = 0;
	
	$query = "SELECT
			pos_trans_special.trans_special_id,
			pos_special.value,
			pos_special.special_type,
			pos_special.value_type,
			pos_special.name AS 'special'
			FROM pos_trans_special

			LEFT JOIN pos_special
			ON pos_trans_special.special_id = pos_special.special_id

			WHERE pos_trans_special.trans_master_id = '$trans_master_id'
			AND pos_trans_special.special_void = 'NO' 
			
			ORDER BY pos_special.priority ASC
			
			";
	
	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	
	while ($row = mysqli_fetch_array($result)) {
		
		
		//increament service line counter
		$service_line_count++;
		
		$trans_special_id = $row['trans_special_id'];
		
		$value = $row['value'];
		$special_type = $row['special_type'];
		$value_type = $row['value_type'];
		$special = $row['special'];		
		
		
		$total_special_price = 0;
		$total_discount_price = 0;
		$total_service_fee_price = 0;
		$total_vat_price = 0;
		
	
		if ($special_type == "MARK-UP") {
			
			if ($value_type == "PERCENTAGE") {

				$percent_value = ($value / 100);

				$special_price = ($sub_total * $percent_value);


			}  else {

				$special_price = ($value);

			}
			
			
			//ADD ALL SPECIAL PRICE
			$total_special_price = $special_price + $total_special_price;
			
			
		} elseif ($special_type == "STORE SERVICE FEE") {
			
			
			if ($session_surcharge_store > $sub_total) {

				$special_price = $session_surcharge_store - ($sub_total + $total_special_price);


			} else {

				$special_price = 0;

			}
			
			//SERVICE FEE -> STORE
			$total_service_fee_price = $special_price;
						
		}  elseif ($special_type == "PICK-UP SERVICE FEE") {
			
			if ($session_surcharge_pickup > $sub_total) {

				$special_price = $session_surcharge_pickup - $sub_total;


			} else {

				$special_price = 0;


			}
			
			//SERVICE FEE -> PICK-UP
			$total_service_fee_price = $special_price;
			
			
		}  elseif ($special_type == "DISCOUNT") {
			
			
			if ($value_type == "PERCENTAGE") {

				$percent_value = ($value / 100);

				$special_price = (-1 * abs(( ($sub_total + $total_special_price + $total_service_fee_price) * $percent_value)));


			} else {

				$special_price = (-1 * $value);

			}
			
			
			//ADD ALL DISCOUNT PRICE
			$total_discount_price = $special_price + $total_discount_price;
			
			
			
			
		} elseif ($special_type == "VAT") {
			
	
		
			
		}
		
	
		$str .= "<tr>";	
				
		$str .= '
	
			<td>
				<span class="email">' . $special . '</span>
			</td>
			
			<td>
				<span class="email">' . '**' . '</span>
			</td>
	
	
			<td>
				<span class="email">' . number_format($special_price,2) . '</span>
			</td>
	
		';
	
		$str .= "</tr>";	
		
	}
	
	
	
	//========================================================================================================
	
	
	
	
	//BLANK ROW INSERT
	$blank_row = $trans_space_count - $service_line_count;

	while ($blank_row > 0) {


		$str .= "<tr>";

		$str .= '



			<td>
				<span class="email">' . '&nbsp;' . '</span>
			</td>



			<td>
				<span class="email">' . '' . '</span>
			</td>

			<td>
				<span class="email">' . '' . '</span>
			</td>


		';


		$str .= "</tr>";


		$blank_row--;


	}
	
	
	


	//CLOSE TBODY
	$str .= "</tbody>";
	
	
	$resp['total_price'] = $total_price;
	$resp['table_data'] = $str;
	
	
	
	echo json_encode($resp);

}




if(isset($_POST['pos-store-transaction-display_client_all-table']) && $_POST['pos-store-transaction-display_client_all-table'] == true) {
	
	
	$str = "";

	$str .= "<thead>";

	$query = "CALL `SP-store-display_client_all`();";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	$index = 0;
	
	while ($row = mysqli_fetch_field($result)) {
		
		if ($index >= 2) {
			
			$str .= "<th>";
			$str .= $row->name;
			$str .= "</th>";		
			
		}
		
		$index++;
	
		
	}
	
	//add Action Header
	$str .= "<th>";
	$str .= "Actions";
	$str .= "</th>";
			
	
	//CLOSE THEAD
	$str .= "</thead>";
	

	//OPEN TBODY
	$str .= "<tbody>";
		
		
	$index = 0;
	
	while ($row = mysqli_fetch_row($result)) {
		
		$str .= "<tr>";
		
		foreach ($row as $data) {
			
			switch ($index) {
				case 0:
					$id = $data;
					break;
				case 1:
					$address_state = $data;
					break;
				case 2:
					$name = $data;
					break;
				case 3:
					$email = $data;
					break;
				case 4:
					$contact_num = $data;
					break;
				case 5:
					$branch = $data;
					break;
			}
										
			$index++;
			
		}
		
		
		//reset index
		$index = 0;
		
	
	
		/*
		<td class="user-name">
			<a href="#" class="name">' . $store_name . '</a>
			
			<span>' . $address_city . '</span>
			
		</td>
		
		*/
		
		$str .= '	
		
			<td class="user-name">
				<a href="#" class="name">' . $name . '</a>
				<span>' . $address_state . '</span>
			</td>
		
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $email . '</span>
			</td>
			
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $contact_num . '</span>
			</td>
			
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $branch . '</span>
			</td>
		
			
			<td class="action-links">
		
				<button type="button" class="btn btn-info" id="btn-select_customer" value="' . $id . '" customer_name="' . $name . '" contact_num="' . $contact_num . '">Select Customer</button>
				
			</td>
			
		
		';
		
		
		$str .= "</tr>";	
		
	}


	//CLOSE TBODY
	$str .= "</tbody>";
	
	
	echo $str;
	

}



//TRANSACTION COMMAND
if(isset($_POST['pos-store-transaction_create_master']) && $_POST['pos-store-transaction_create_master'] == true) {
	
	$resp = array(
	'status' => '',
	'message' => '',
	
	'trans_master_id' => '',
	'store_name' => '',
	'store_address' => '',
	'store_num_cell' => '',
	'store_num_tel' => '',
	'client_name' => '',
	'client_address' => '',
	'client_num1' => '',
	'client_num2' => '',
	'client_email' => '',
	'record_datetime' => '',
	'store_code' => ''
	
	);
	

	$id = mysqli_real_escape_string($link,$_POST['id']);
	
	
	$query = "INSERT INTO pos_trans_inc_ref (datetime) VALUES (NOW());";
	
	mysqli_query($link,$query);
	
	$inserted_id1 = mysqli_insert_id($link);
	
	$inserted_id = 'TEMP' . $session_store_code . $inserted_id1;
	
	$random = substr(md5(rand()), 0, 10);
	$new_id = $random . $inserted_id;
	
	
	//insert trans master
	$master_id = generate_random_str();
	$query = "INSERT INTO pos_trans_master (id, trans_master_id ,client_id, record_date, store_id, user_id) 
			VALUES ('$master_id','$inserted_id', '$id', CURDATE(), '$session_store_id', '$session_user_id');";

	mysqli_query($link,$query);
	
	//insert trans trail details	
	$trail_id = generate_random_str();
	$query = "INSERT INTO pos_trans_trail (trans_trail_id, trans_master_id, user_store_id, user_admin_id, user_staff_id) 
			VALUES ('$trail_id', '$inserted_id', '$session_store_id', 'NA', '$session_user_id')";
	
	mysqli_query($link,$query);
	
	
	/*
	INSERT SURCHARGE LINE
	1 - STORE SURCHARGE
	2- PICK-UP SURCHARGE
	*/
	
	$random_special_id = generate_random_str();
	$query = "INSERT INTO pos_trans_special (trans_special_id ,trans_master_id, special_id) VALUES ('$random_special_id', '$inserted_id', '2')";
	mysqli_query($link,$query);
	

	$query = "SELECT 
			pos_trans_master.trans_master_id,
			pos_store.store_code,
			pos_store.store_name,
			pos_store.num_tel,
			pos_store.num_cell,
			CONCAT(pos_store.address, ' ', pos_store.address_city) AS 'store_address',
			CONCAT(pos_store.num_cell, ' | ', pos_store.num_tel) AS 'store_contact',
			CONCAT(pos_client.first_name, ' ', pos_client.last_name) AS 'client_name',
			CONCAT(pos_client.address, ' ', pos_client.address_city) AS 'client_address', 
			pos_client.contact_num1 AS 'client_num1',
			pos_client.contact_num2 AS 'client_num2',
			pos_client.email AS 'client_email',
			DATE_FORMAT(pos_trans_master.record_datetime , '%b %d, %Y %h:%i %p') AS 'record_datetime'
			
			FROM pos_trans_master 
			
			LEFT JOIN pos_client
			ON pos_trans_master.client_id = pos_client.client_id 
			
			LEFT JOIN pos_store 
			ON pos_trans_master.store_id = pos_store.store_id
			WHERE pos_trans_master.trans_master_id = '$inserted_id';";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	
	if ($row = mysqli_fetch_array($result)) {
		
	$resp['trans_master_id'] = $row['trans_master_id'];
	$resp['store_code'] = $row['store_code'];
	$resp['store_name'] = $row['store_name'];
	$resp['store_address'] = $row['store_address'];
	
	$resp['store_num_cell'] = $row['num_cell'];
	$resp['store_num_tel'] = $row['num_tel'];
	
	$resp['client_name'] = $row['client_name'];
	$resp['client_address'] = $row['client_address'];
	
	$resp['client_num1'] = $row['client_num1'];
	$resp['client_num2'] = $row['client_num2'];
	
	$resp['client_email'] = $row['client_email'];
	
	$resp['record_datetime'] = $row['record_datetime'];	
	
	
		
		
	}
	
		
	$resp['status'] = 'success';
	
		
	
	
	
	echo json_encode($resp);


} 



if(isset($_POST['pos-store-transaction-display_trans_detail-table']) && $_POST['pos-store-transaction-display_trans_detail-table'] == true) {
	
	$resp = array(
	'total_price' => '',
	'table_data' => '',
	'total_special_price' => '',
	'total_discount_price' => '',
	'total_service_fee_price' => '',
	'total_overall' => ''
	);	
	
	
	
	
	$id = mysqli_real_escape_string($link,$_POST['id']);
	
	$trans_master_id = mysqli_real_escape_string($link,$_POST['id']);
	
	$str = "";

	$str .= "<thead>";
	
	$str .= '<tr class="no-borders">';

	$query = "SELECT 
			trans_detail_id,
			pos_service_and_price.name AS 'Service Instruction',
			TRIM(TRAILING '.' FROM TRIM(TRAILING '0' from pos_trans_detail.unit_count)) AS '# of kl(s)',
			pos_trans_detail.unit_price AS 'Amount'
			FROM pos_trans_detail 
			
			LEFT JOIN pos_service_and_price 
			ON pos_trans_detail.service_id = pos_service_and_price.service_id 
			
			WHERE pos_trans_detail.trans_master_id = '$id' 
			AND pos_trans_detail.line_void = 'NO'";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	
	$total_item_qty = 0;
	
	$index = 0;
	
	
	
	while ($row = mysqli_fetch_field($result)) {
		
		if ($index == 1) {
			
			
			$str .= "<th>";
			
			
			$str .= '<a href="#link_add" class="edit" id="btn-add_line">
						<i class="fa-plus-circle"></i>
					</a>	';				
			
			$str .= $row->name;

			$str .= "</th>";	
			
			
		} elseif ($index >= 2) {
			
			$str .= "<th>";
			$str .= $row->name;
			$str .= "</th>";		
			
		}
		
		$index++;
	
		
	}
	
	
			
	//CLOSE THEAD
	$str .= "</tr>";
	
	//CLOSE THEAD
	$str .= "</thead>";
	

	//OPEN TBODY
	$str .= "<tbody>";
		
		
	$index = 0;
	
	$total_price = 0;
	
	$service_line_count = 0;
	
	while ($row = mysqli_fetch_row($result)) {
		
		$str .= "<tr>";
		
		foreach ($row as $data) {
			
			switch ($index) {
				case 0:
					$id = $data;
					break;
				case 1:
					$service = $data;
					break;
				case 2:
					$unit_count = $data;
					break;
				case 3:
					$price = $data;
					break;
			}
						
					
			$index++;
			
		}
		
		
		//reset index
		$index = 0;
		
		//count service line
		$service_line_count++;
		
		//add total price
		$total_price = ($total_price + $price);
		
		
		
		$str .= '
						
			<td>
				<span class="email">
				
				
				<a href="#" class="edit" id="link_remove" value="' . $id . '" >
					<i class="fa-remove"></i>
				</a>
				
				
				' . $service . '
				
				
				</span>
			</td>
			
			
			<td>
				<span class="email">' . $unit_count . '</span>
			</td>
			
			<td>
				<span class="email">' . $price . '</span>
			</td>
			
			
		
		';
		
		
		$str .= "</tr>";	
		
	}
	
	
	
	//========================================================================================================
	
	$sub_total = $total_price;
	
	$query = "SELECT
			pos_trans_special.trans_special_id,
			pos_special.value,
			pos_special.special_type,
			pos_special.value_type,
			pos_special.name AS 'special'
			FROM pos_trans_special

			LEFT JOIN pos_special
			ON pos_trans_special.special_id = pos_special.special_id

			WHERE pos_trans_special.trans_master_id = '$trans_master_id'
			AND pos_trans_special.special_void = 'NO'
			
			ORDER BY pos_special.priority ASC
			
			";
	
	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	
	$total_special_price = 0;
	$total_discount_price = 0;
	$total_service_fee_price = 0;
	$total_vat_price = 0;
	
	
	while ($row = mysqli_fetch_array($result)) {
		
		
		//increament service line counter
		$service_line_count++;
		
		$trans_special_id = $row['trans_special_id'];
		
		$value = $row['value'];
		$special_type = $row['special_type'];
		$value_type = $row['value_type'];
		$special = $row['special'];		
	
	
		$link_remove = '
		
		<a href="#" class="edit" id="link_remove_special" value="' . $trans_special_id . '" >
			<i class="fa-remove"></i>
		</a>
		
		';
	
		
		
		if ($special_type == "MARK-UP") {
			
			if ($value_type == "PERCENTAGE") {

				$percent_value = ($value / 100);

				$special_price = ($sub_total * $percent_value);


			}  else {

				$special_price = ($value);

			}
			
			
			//ADD ALL SPECIAL PRICE
			$total_special_price = $special_price + $total_special_price;
			
			
		} elseif ($special_type == "STORE SERVICE FEE") {
			
			$link_remove = '';
			
			if ($session_surcharge_store > $sub_total) {

				$special_price = $session_surcharge_store - ($sub_total + $total_special_price);


			} else {

				$special_price = 0;

			}
			
			//SERVICE FEE -> STORE
			$total_service_fee_price = $special_price;
						
		}  elseif ($special_type == "PICK-UP SERVICE FEE") {
			
			
			$link_remove = '';
			
			if ($session_surcharge_pickup > $sub_total) {

				$special_price = $session_surcharge_pickup - $sub_total;


			} else {

				$special_price = 0;


			}
			
			//SERVICE FEE -> PICK-UP
			$total_service_fee_price = $special_price;
			
			
		}  elseif ($special_type == "DISCOUNT") {
			
			
			if ($value_type == "PERCENTAGE") {

				$percent_value = ($value / 100);

				$special_price = (-1 * abs(( ($sub_total + $total_special_price + $total_service_fee_price) * $percent_value)));


			} else {

				$special_price = (-1 * $value);

			}
			
			
			//ADD ALL DISCOUNT PRICE
			$total_discount_price = $special_price + $total_discount_price;
			
			
			
			
		} elseif ($special_type == "VAT") {
			
	
		
			
		}
		
		
	
		$str .= "<tr>";	
				
		$str .= '
	
			<td>
				<span class="email">
				
				
				' . $link_remove .  $special . '
				
				
				</span>
			</td>
			
			<td>
				<span class="email">' . '**' . '</span>
			</td>
	
	
			<td>
				<span class="email">' . number_format($special_price,2) . '</span>
			</td>
	
		';
	
	
		$str .= "</tr>";	
		
	}
	
	
	
	//========================================================================================================
	
	
	
	
	//INSERT BLANK ROWS
	//MAX ROW COUNT = 36
	//&nbsp;

	$blank_row = $trans_space_count - $service_line_count;

	while ($blank_row > 0) {


		$str .= "<tr>";

		$str .= '



			<td>
				<span class="email">' . '&nbsp;' . '</span>
			</td>



			<td>
				<span class="email">' . '' . '</span>
			</td>

			<td>
				<span class="email">' . '' . '</span>
			</td>


		';


		$str .= "</tr>";


		$blank_row--;


	}
	


	//CLOSE TBODY
	$str .= "</tbody>";
	
	
	$resp['total_price'] = $total_price;
	$resp['table_data'] = $str;
	
	
	$resp['total_special_price'] = $total_special_price;
	$resp['total_discount_price'] = $total_discount_price;
	$resp['total_service_fee_price'] = $total_service_fee_price;
	
	$total_overall = $total_special_price + $total_discount_price + $total_service_fee_price + $total_price;
	
	$resp['total_overall'] = $total_overall;
	
	
	
	echo json_encode($resp);
	

}



//load trans items
if(isset($_POST['pos-store-transaction-display_trans_item-table']) && $_POST['pos-store-transaction-display_trans_item-table'] == true) {
	
	
	$resp = array(
	'item_table' => '',
	'total_item_qty' => ''
	);
	
	
	$id = mysqli_real_escape_string($link,$_POST['id']);
	
	$str = "";

	$str .= "<thead>";
	
	$str .= '<tr class="no-borders">';

	$query = "CALL `SP-store-transaction-dispay_trans_item`('$id')";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	
	
	$index = 0;
	
	$str .= "</th>";
	
	while ($row = mysqli_fetch_field($result)) {
		
		if ($index == 1) {
			
			
			
			
			
			$str .= "<th>";
			$str .= '<a href="#link_add" class="edit" id="link_add_item">
						<i class="fa-plus-circle"></i>
					</a>';
			$str .= $row->name;
			$str .= "</th>";		
			
		} elseif ($index >= 2) {
			
			$str .= "<th>";
			$str .= $row->name;
			$str .= "</th>";	
			
		}
		
		$index++;
	
		
	}
	
	
			
	//CLOSE THEAD
	$str .= "</tr>";
	
	//CLOSE THEAD
	$str .= "</thead>";
	

	//OPEN TBODY
	$str .= "<tbody>";
		
		
	$index = 0;
	
	$total_price = 0;
	
	
	$total_item_qty = 0;
	
	while ($row = mysqli_fetch_row($result)) {
		
		$str .= "<tr>";
		
		foreach ($row as $data) {
			
			switch ($index) {
				case 0:
					$id = $data;
					break;
				case 1:
					$item_name = $data;
					break;
				case 2:
					$qty = $data;
					break;
			}
						
					
			$index++;
			
		}
		
		
		//reset index
		$index = 0;
		
		$total_item_qty = $total_item_qty + $qty;
		
		
		$str .= '
			
			
			<td>
				<span class="email">
				
				<a href="#link_view" class="edit" id="link_view" value="' . $id . '" item_name ="' . $item_name . '" qty="' . $qty . '">
					<i class="fa-pencil"></i>
					
				</a>
				
				<a href="#link_remove" class="edit" id="link_remove" value="' . $id . '" >
					<i class="fa-remove"></i>
					
				</a>
				
				
				' . $item_name . '
				
				
				</span>
			</td>
			
			<td>
				<span class="email">' . $qty . '</span>
			</td>
			
			
		
		';
		
		
		$str .= "</tr>";	
		
	}
	
	
	
	

	//CLOSE TBODY
	$str .= "</tbody>";
	
	
	$resp['item_table'] = $str;
	$resp['total_item_qty'] = $total_item_qty;

	
	echo json_encode($resp);

	
	
	

}
//END OF load trans item



//LOAD TRANS ITEM WITHOUT CLICK BUTTON 

if(isset($_POST['pos-store-transaction-display_trans_item_wo_button-table']) && $_POST['pos-store-transaction-display_trans_item_wo_button-table'] == true) {
		
	
	$resp = array(
	'item_table' => '',
	'total_item_qty' => ''
	);	
		
		
	$id = mysqli_real_escape_string($link,$_POST['id']);
	
	$str = "";

	$str .= "<thead>";
	
	$str .= '<tr class="no-borders">';

	$query = "CALL `SP-store-transaction-dispay_trans_item`('$id')";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	
	
	$index = 0;
	
	
	
	$str .= "</th>";
	
	while ($row = mysqli_fetch_field($result)) {
		
		if ($index >= 1) {
			
			$str .= "<th>";
			$str .= $row->name;
			$str .= "</th>";		
			
		}
		
		$index++;
	
		
	}
	
	
			
	//CLOSE THEAD
	$str .= "</tr>";
	
	//CLOSE THEAD
	$str .= "</thead>";
	

	//OPEN TBODY
	$str .= "<tbody>";
		
		
	$index = 0;
	
	$total_price = 0;
	
	$total_item_qty = 0;
	
	while ($row = mysqli_fetch_row($result)) {
		
		$str .= "<tr>";
		
		foreach ($row as $data) {
			
			switch ($index) {
				case 0:
					$id = $data;
					break;
				case 1:
					$item_name = $data;
					break;
				case 2:
					$qty = $data;
					break;
			}
						
					
			$index++;
			
		}
		
		
		//reset index
		$index = 0;
		
		$total_item_qty = $total_item_qty + $qty;
		
		
		$str .= '
			
			
			<td>
				<span class="email">' . $item_name . '</span>
			</td>
			
			<td>
				<span class="email">' . $qty . '</span>
			</td>
			
			
		
		';
		
		
		$str .= "</tr>";	
		
	}
	

	//CLOSE TBODY
	$str .= "</tbody>";
	
	
	$resp['item_table'] = $str;
	$resp['total_item_qty']	= $total_item_qty;

	
	echo json_encode($resp);
	

}

//END OF LOAD TRANS ITEM WITHOU CLICK BUTTON



if(isset($_POST['pos-store-transaction-load_main_service-option']) && $_POST['pos-store-transaction-load_main_service-option'] == true) { 

	$query = "CALL `SP-store-load_main_service_option` ();";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	$index = 0;
	
	$str = '<option value="" disabled selected>...</option>';
	
	while ($row = mysqli_fetch_array($result)) {
	
		$service_main_id = $row['service_main_id'];
		$name = $row['name'];
		
		$str .= '<option value="' . $service_main_id . '">' . $name . '</option>';
			
	}
	
	
	echo $str;


}



if(isset($_POST['pos-store-transaction-load_category_service-option']) && $_POST['pos-store-transaction-load_category_service-option'] == true) { 


	$id = mysqli_real_escape_string($link,$_POST['service_id']);

	$query = "CALL `SP-store-load_service_category_id` ('$id');";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	
	$str = '<option value="" disabled selected>...</option>';
	
	
	while ($row = mysqli_fetch_array($result)) {
	
		$category_id = $row['category_id'];
		$name = $row['name'];
		
		$str .= '<option value="' . $category_id . '">' . $name . '</option>';
			
	}
	
	
	echo $str;




}



if(isset($_POST['pos-store-transaction-load_service_name_list-option']) && $_POST['pos-store-transaction-load_service_name_list-option'] == true) { 


	$id = mysqli_real_escape_string($link,$_POST['category_id']);

	$query = "CALL `SP-store-load_service_name_list_option`('$id');";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	$index = 0;
	
	$str = '<option value="" disabled selected>...</option>';
	
	
	while ($row = mysqli_fetch_array($result)) {
	
		$service_id = $row['service_id'];
		$code = $row['code'];
		$name = $row['name'];
		$price = $row['price'];
		$unit = $row['unit'];
		
		$str .= '<option value="' . $service_id . '" data-price="' . $price . '" data-unit="' . $unit . '">' . $code . ' - ' . $name . ' - ₱' . $price . '/' . $unit . '</option>';
			
	}
	
	
	echo $str;

}


if(isset($_POST['pos-store-transaction-add_transaction_detail']) && $_POST['pos-store-transaction-add_transaction_detail'] == true) {
	
	$resp = array(
	'status' => '',
	'message' => ''
	);
	
	$id = mysqli_real_escape_string($link,$_POST['id']);
	
	//service_id
	$service = mysqli_real_escape_string($link,$_POST['service']);

	$unit = mysqli_real_escape_string($link,$_POST['unit']);
	$price = mysqli_real_escape_string($link,$_POST['price']);
	$unit_count = mysqli_real_escape_string($link,$_POST['unit_count']);
	$unit_price = mysqli_real_escape_string($link,$_POST['unit_price']);
	
	
	
	//GENERATE UNIQUE VARCHAR PRIMARY KEY
	$query = "INSERT INTO pos_trans_inc_ref (datetime) VALUES (NOW());";
	mysqli_query($link,$query);
	$unique_id = $session_store_code . mysqli_insert_id($link);
	
	
	
	$query = "CALL `SP-store-transaction-add_transaction_detail`('$unique_id', '$id', '$service', '$unit', '$price', '$unit_count', '$unit_price');";

	$result = mysqli_query($link,$query);
	
	if (!$result) {
		
		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);
		
	
	} else {
		
		$resp['status'] = 'success';
	
		
	}
	
	
	echo json_encode($resp);


}



if(isset($_POST['pos-store-transaction-delete_transaction_detail']) && $_POST['pos-store-transaction-delete_transaction_detail'] == true) {
	
	$resp = array(
	'status' => '',
	'message' => ''
	);
	
	
	$id = mysqli_real_escape_string($link,$_POST['id']);
	
	
	
	$query = "CALL `SP-store-transaction-delete_transaction_detail`('$id' );";

	$result = mysqli_query($link,$query);
	
	if (!$result) {
		
		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);
		
	
	} else {
		
		$resp['status'] = 'success';
	
		
	}
	
	
	echo json_encode($resp);


} 






if(isset($_POST['pos-store-transaction-delete_transaction_item']) && $_POST['pos-store-transaction-delete_transaction_item'] == true) {
	
	$resp = array(
	'status' => '',
	'message' => ''
	);
	
	
	$id = mysqli_real_escape_string($link,$_POST['id']);
	
	
	
	$query = "CALL `SP-store-transaction-delete_transaction_item`('$id' );";

	$result = mysqli_query($link,$query);
	
	if (!$result) {
		
		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);
		
	
	} else {
		
		$resp['status'] = 'success';
	
		
	}
	
	
	echo json_encode($resp);


} 




if(isset($_POST['pos-store-transaction-add_transaction_item']) && $_POST['pos-store-transaction-add_transaction_item'] == true) {
	
	$resp = array(
	'status' => '',
	'message' => ''
	);
	
	
	//GENERATE UNIQUE VARCHAR PRIMARY KEY
	$query = "INSERT INTO pos_trans_inc_ref (datetime) VALUES (NOW());";
	mysqli_query($link,$query);
	$unique_id = $session_store_code . mysqli_insert_id($link);
	
	
	$id = mysqli_real_escape_string($link,$_POST['id']);
	
	$item_name = mysqli_real_escape_string($link,$_POST['item_name']);
	$qty = mysqli_real_escape_string($link,$_POST['item_qty']);
	
	
	
	$query = "CALL `SP-store-transaction-add_transaction_item`('$id', '$item_name', '$qty', '$unique_id');";

	$result = mysqli_query($link,$query);
	
	if (!$result) {
		
		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);
		
	
	} else {
		
		$resp['status'] = 'success';
	
		
	}
	
	
	echo json_encode($resp);


}



//load trans special details WITH link buttons
if(isset($_POST['pos-store-transaction-display_trans_special_with_link_buttons-table']) && $_POST['pos-store-transaction-display_trans_special_with_link_buttons-table'] == true) {
	
	$resp = array(
	'special_total' => '',
	'less_total' => '',
	'table_data' => ''
	);
	
	
	$special_total = 0;
	$less_total = 0;

	
	$id = mysqli_real_escape_string($link,$_POST['id']);
	$sub_total = mysqli_real_escape_string($link,$_POST['sub_total']);
	
	$str = "";
	
	

	$str .= "<thead>";
	
	$str .= '<tr class="no-borders">';

	$query = "SELECT 
			pos_trans_special.trans_special_id,
			pos_special.value,
			pos_special.special_type,
			pos_special.value_type,
			pos_special.name AS 'Special',
			'' AS 'Price'
			
			FROM pos_trans_special 
			
			LEFT JOIN pos_special 
			ON pos_trans_special.special_id = pos_special.special_id
			
			WHERE pos_trans_special.trans_master_id = '$id' 
			AND pos_trans_special.special_void = 'NO'";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	$index = 0;
	
	
	
	$str .= "</th>";
	
	while ($row = mysqli_fetch_field($result)) {
		
		if ($index >= 4) {
			
			
			
			$str .= "<th>";
			$str .= '<a href="#link_add" class="edit" id="btn-add_special">
						<i class="fa-plus-circle"></i>
					</a>';
			$str .= $row->name;
			$str .= "</th>";		
			
		} elseif ($index >= 5) {
			
			$str .= "<th>";
			$str .= $row->name;
			$str .= "</th>";	
			
		}
		
		$index++;
	
		
	}
	
	
			
	//CLOSE table row
	$str .= "</tr>";
	
	//CLOSE THEAD
	$str .= "</thead>";
	

	//OPEN TBODY
	$str .= "<tbody>";
		
		
	$index = 0;
	
	$total_price = 0;
	
	
	while ($row = mysqli_fetch_array($result)) {
		
		$str .= "<tr>";
		
		
		$id = $row['trans_special_id'];
		$value = $row['value'];
		$special_type = $row['special_type'];
		$value_type = $row['value_type'];
		$name = $row['Special'];
		$special = $row['Special'];
		
		
		if ($special_type == "DISCOUNT") {

			if ($value_type == "PERCENTAGE") {

				$percent_value = ($value / 100);

				$special_price = (-1 * abs(($sub_total * $percent_value)));


			} else {

				$special_price = (-1 * $value);

			}
			
			
			$less_total = $less_total + $special_price;


		} else {

			if ($value_type == "PERCENTAGE") {

				$percent_value = ($value / 100);

				$special_price = ($sub_total * $percent_value);


			} elseif ($value_type == "SURCHARGE") {

				if ($special == "STORE SURCHARGE") {
					
					
					if ($session_surcharge_store > $sub_total) {

						$special_price = $session_surcharge_store - $sub_total;
	
	
					} else {
	
						$special_price = 0;
	
					}
					
					
					
				} else {
					
					
					if ($session_surcharge_pickup > $sub_total) {

						$special_price = $session_surcharge_pickup - $sub_total;
	
	
					} else {
	
						$special_price = 0;
	
	
					}
					
					
				}

			} else {

				$special_price = ($value);

			}
			
			
			$special_total = ($special_total + $special_price);
			

		}
		
		
		$str .= '
			
			
			<td>
				<span class="email">
				
				<a href="#" class="edit" id="link_remove" value="' . $id . '" >
					<i class="fa-remove"></i>
					
				</a>
				
				' . $name . '
				
				</span>
			</td>
			
			<td>
				<span class="email">' . number_format($special_price,2) . '</span>
			</td>
		
		
		';
		
		
		
		$str .= "</tr>";
		
	}
	
	/*
	while ($row = mysqli_fetch_row($result)) {
		
		$str .= "<tr>";
		
		foreach ($row as $data) {
			
			switch ($index) {
				case 0:
					$id = $data;
					break;
				case 1:
					$value = $data;
					break;
				case 2:
					$special_type = $data;
					break;
				case 3:
					$value_type = $data;
					break;
				case 4:
					$name = $data;
					break;
				case 5:
					$price = $data;
					break;
			}
			
			$index++;
			
		}
		
		
		//reset index
		$index = 0;
		
		
		if ($special_type == "DISCOUNT") {
			
			if ($value_type == "PERCENTAGE") {
				
				$percent_value = ($value / 100);
				
				$special_price = (-1 * abs(($sub_total * $percent_value)));
				
				
			} else {
				
				$special_price = ($sub_total - $value);
				
			}
			
			
		} else {
			
			if ($value_type == "PERCENTAGE") {
				
				$percent_value = ($value / 100);
				
				$special_price = ($sub_total * $percent_value);
				
				
			} else {
				
				$special_price = ($sub_total + $value);
				
			}
						
		}
				
		$special_total = ($special_total + $special_price);
		
		
		
		
		
		$str .= '
			
			
			<td class="action-links hidden-xs">
				
				<a href="#" class="edit" id="link_remove" value="' . $id . '" >
					<i class="fa-remove"></i>
					
				</a>
						
			</td>
			
			
			<td>
				<span class="email">' . $name . '</span>
			</td>
			
			<td>
				<span class="email">' . number_format($special_price,2) . '</span>
			</td>
		
		
		';
		
		
		$str .= "</tr>";	
		
	}
	*/
	
	$row_count = mysqli_num_rows($result);
	
	if ($row_count == 0) {
			
		$str .= '
		<tr>
			<td colspan="5">
				<span class="email">No Special Service/Discount</span>
			</td>
		</tr>
		
		';
		
	}
	
	


	//CLOSE TBODY
	$str .= "</tbody>";
	
	
	$resp['special_total'] = $special_total;
	$resp['less_total'] = $less_total;
	$resp['table_data'] = $str;
	
	
	
	echo json_encode($resp);
	
	
	
}




if(isset($_POST['pos-store-transaction-load_special_name_list-option']) && $_POST['pos-store-transaction-load_special_name_list-option'] == true) { 
	
	
	$special_type = mysqli_real_escape_string($link,$_POST['special_type']);

	$query = "CALL `SP-store-load_special_name_list_option`('$special_type');";
	
	

	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	$index = 0;
	
	//$str = '<option value="" disabled selected>Select Service..</option>';
	
	$str = '<option value="" disabled selected>Select ' . $special_type . '..</option>';
	
	while ($row = mysqli_fetch_array($result)) {
	
		$special_id = $row['special_id'];
		$name = $row['name'];
		
		
		$str .= '<option value="' . $special_id . '" >' . $special_id . ' - ' . $name . '</option>';
			
	}
	
	
	echo $str;

}



if(isset($_POST['pos-store-transaction-add_transaction_special']) && $_POST['pos-store-transaction-add_transaction_special'] == true) {
	
	$resp = array(
	'status' => '',
	'message' => ''
	);
	
	$trans_master_id = mysqli_real_escape_string($link,$_POST['id']);
	$special_id = mysqli_real_escape_string($link,$_POST['special_id']);
	
	//GENERATE UNIQUE VARCHAR PRIMARY KEY
	$query = "INSERT INTO pos_trans_inc_ref (datetime) VALUES (NOW());";
	mysqli_query($link,$query);
	$unique_id = $session_store_code . mysqli_insert_id($link);
	
	
	
	$query = "CALL `SP-store-transaction-add_special_detail`('$unique_id', '$trans_master_id', '$special_id');";

	$result = mysqli_query($link,$query);
	
	if (!$result) {
		
		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);
		
	
	} else {
		
		$resp['status'] = 'success';
	
	}
	
	
	echo json_encode($resp);


}



if(isset($_POST['pos-store-transaction-delete_transaction_special']) && $_POST['pos-store-transaction-delete_transaction_special'] == true) {
	
	$resp = array(
	'status' => '',
	'message' => ''
	);
	
	
	$id = mysqli_real_escape_string($link,$_POST['id']);
	
	
	
	$query = "CALL `SP-store-transaction-delete_transaction_special`('$id' );";

	$result = mysqli_query($link,$query);
	
	if (!$result) {
		
		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);
		
	
	} else {
		
		$resp['status'] = 'success';
	
		
	}
	
	
	echo json_encode($resp);


} 




if(isset($_POST['pos-store-transaction-void_transaction']) && $_POST['pos-store-transaction-void_transaction'] == true) {
	
	$resp = array(
	'status' => '',
	'message' => ''
	);
	
	$trans_master_id = mysqli_real_escape_string($link,$_POST['id']);
	$status = mysqli_real_escape_string($link,$_POST['status']);
	
	
	
	$query = "CALL `SP-store-transaction-void_transaction`('$trans_master_id', '$session_store_id', '$status');";

	$result = mysqli_query($link,$query);
	
	if (!$result) {
		
		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);
		
	
	} else {
		
		$resp['status'] = 'success';
	
	}
	
	
	echo json_encode($resp);


}



if(isset($_POST['pos-store-transaction-finalize_transaction']) && $_POST['pos-store-transaction-finalize_transaction'] == true) {
	
	$resp = array(
	'status' => '',
	'message' => '',
	'new_id' => '',
	'due_date' => '',
	'local_code' => ''
	);
	
	
	$query = "INSERT INTO pos_trans_inc_ref_finalize (datetime) VALUES (NOW());";
	
	mysqli_query($link,$query);
	
	$inserted_id1 = mysqli_insert_id($link);
	
	
	
	$trans_master_id = mysqli_real_escape_string($link,$_POST['id']);
	$status = mysqli_real_escape_string($link,$_POST['status']);
	
	$due_date = mysqli_real_escape_string($link,$_POST['due_date']);
	
	$total_grand = mysqli_real_escape_string($link,$_POST['total_grand']);
	
	
	$time = strtotime($due_date);

	$newformat_date = date('M d Y',$time);
	$resp['due_date'] = $newformat_date;
	
	//NEW ASSIGNED ID
	$new_id = $session_store_code . '-' . $inserted_id1;
	
	
	$local_code = '0000' . $inserted_id1;
	
	
	$resp['new_id'] = $new_id;
	$resp['local_code'] = $local_code;
	
	
	$query = "CALL `SP-store-transaction-finalize_transaction`('$trans_master_id', '$session_store_id', '$status', '$due_date', '$total_grand', '$new_id', '$local_code', '$session_user_id');";

	$result = mysqli_query($link,$query);
	
	if (!$result) {
		
		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);
		
		
	
	} else {
		
		$resp['status'] = 'success';
	
	}
	
	
	echo json_encode($resp);


}



if(isset($_POST['pos-store-transaction-suspend_transaction']) && $_POST['pos-store-transaction-suspend_transaction'] == true) {


	$trans_master_id = mysqli_real_escape_string($link,$_POST['id']);
	$status = mysqli_real_escape_string($link,$_POST['status']);
	$due_date = mysqli_real_escape_string($link,$_POST['due_date']);
	
	$query = "CALL `SP-store-transaction-suspend_transaction`('$trans_master_id', '$session_store_id', '$status', '$due_date');";

	$result = mysqli_query($link,$query);
	
	if (!$result) {
		
		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);
		
	
	} else {
		
		$resp['status'] = 'success';
	
	}
	
	
	echo json_encode($resp);



}



//END OF TRANSACTION COMMAND




//check due date
if(isset($_POST['pos-store-check_date_date']) && $_POST['pos-store-check_date_date'] == true) {
	
	
	$resp = array(
	'due_date' => '',
	'message' => ''
	);
	

	$time_now = date('H:i:s');
	$date_now = date('Y-m-d');
	
	//cut-off time 3pm - 6am
	$cut_start = '15:00:00';
	$cut_end = '06:00:00';
	
	$type = $_POST['type'];
	$trans_id = $_POST['trans_id'];
	
	
	//execute query
	$query = "CALL `SP-store-add_special_due_date`('$trans_id', '$type');";
	mysqli_query($link,$query)or die(mysqli_error($link));
	
	if ( ($cut_start > $time_now) && ($cut_end < $time_now) ) {
		
		
		if ($type == "regular") {
			
			$due_date = date('Y-m-d ', strtotime($date_now . ' +2 day'));
			
			
		} elseif ($type == "rush") {
			
			$due_date = date('Y-m-d ', strtotime($date_now . ' +0 day'));
			
		} elseif ($type == "dryclean") {
			
			$due_date = date('Y-m-d ', strtotime($date_now . ' +7 day'));
			
		}
		
		
		
		
	} else {
		
		if ($type == "regular") {
			
			$due_date = date('Y-m-d ', strtotime($date_now . ' +3 day'));
			
		} elseif ($type == "rush") {
			
			$due_date = date('Y-m-d ', strtotime($date_now . ' +1 day'));
			
		} elseif ($type == "dryclean") {
			
			$due_date = date('Y-m-d ', strtotime($date_now . ' +7 day'));
						
			
		}
		
		
	}
	
	
	
	echo $due_date;



}





?>