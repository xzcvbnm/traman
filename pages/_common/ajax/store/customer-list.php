<?php
session_start();

include '../../../../config.php';

$session_user_id = $_SESSION['user_id'];
$session_store_id = $_SESSION['store_id'];
$session_store_code = $_SESSION['store_code'];

$date_from = date('Y') . '-' . date('m') . '-' . '01';
$date_to = date('Y-m-d');
$month_now = date('m');
$year_now = date('y');




if(isset($_POST['pos-admin-load_branch-option']) && $_POST['pos-admin-load_branch-option'] == true) {



	$query = "CALL `SP-store-load_store_option`();";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));

	$index = 0;

	$str = '<option selected="true" disabled="disabled">...</option>';


	while ($row = mysqli_fetch_array($result)) {

		$store_id = $row['store_id'];
		$store_code = $row['store_code'];
		$store_name = $row['store_name'];

		$str .= '<option value="' . $store_id . '">' . $store_code . ' - ' . $store_name . '</option>';

	}


	echo $str;



}



if(isset($_POST['pos-store-display_client_all-table']) && $_POST['pos-store-display_client_all-table'] == true) {


	$str = "";

	$str .= "<thead>";

	//$query = "CALL `SP-store-display_client_all`('$session_store_id');";
	$query = "CALL `SP-store-display_client_all`();";
	$result = mysqli_query($link,$query)or die(mysqli_error($link));

	$index = 0;

	while ($row = mysqli_fetch_field($result)) {

		if ($index >= 2 && $row->name != "Email") {

			$str .= "<th>";
			$str .= $row->name;
			$str .= "</th>";

		}

		$index++;


	}

	//add Action Header
	$str .= "<th>";
	$str .= "Actions";
	$str .= "</th>";


	//CLOSE THEAD
	$str .= "</thead>";


	//OPEN TBODY
	$str .= "<tbody>";


	$index = 0;

	while ($row = mysqli_fetch_array($result)) {


		$id = $row['client_id'];
		$address_city = $row['address_city'];
		$name = $row['Customer Name'];
		$email = $row['Email'];
		$contact_num = $row['Contact No#'];
		$credit_balance = $row['Credit Balance'];
		$branch = $row['Branch'];


		$str .= '

			<tr>

			<td class="user-name">
				<a href="#" class="name">' . $name . '</a>
				<span>' . $address_city . '</span>
			</td>
	
			<!--<td class="hidden-xs hidden-sm">
				<span class="email">' . $email . '</span>
			</td>-->

			<td class="hidden-xs hidden-sm">
				<span class="email">' . $contact_num . '</span>
			</td>
			
			
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $branch . '</span>
			</td>
			
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $credit_balance . '</span>
			</td>


			<td class="action-links">
				<a href="#" class="edit" id="link_view" value="' . $id . '">
					<i class="fa-folder-open-o"></i>
					View Details
				</a>



				<!--

				<a href="#" id="link_history" value="' . $id . '">
					<i class="fa-history"></i>
					History
				</a>

				<a href="#" class="delete" id="link_delete" value="' . $id . '">
					<i class="linecons-trash"></i>
					Delete
				</a>
				-->
			</td>


			</tr>


		';






	}


	//CLOSE TBODY
	$str .= "</tbody>";


	echo $str;


}



if(isset($_POST['pos-store-add_client']) && $_POST['pos-store-add_client'] == true) {




	$resp = array(
	'status' => '',
	'message' => ''
	);

	$first_name = mysqli_real_escape_string($link,$_POST['first_name']);
	$middle_name = mysqli_real_escape_string($link,$_POST['middle_name']);
	$last_name = mysqli_real_escape_string($link,$_POST['last_name']);
	$contact_num1 = mysqli_real_escape_string($link,$_POST['contact_num1']);
	$contact_num2 = mysqli_real_escape_string($link,$_POST['contact_num2']);
	$email = mysqli_real_escape_string($link,$_POST['email']);
	$address = mysqli_real_escape_string($link,$_POST['address']);
	$address_city = mysqli_real_escape_string($link,$_POST['address_city']);
	//$address_country = mysqli_real_escape_string($link,$_POST['address_country']);
	//$address_state = mysqli_real_escape_string($link,$_POST['address_state']);
	$client_type = mysqli_real_escape_string($link,$_POST['client_type']);
	$retail_type = mysqli_real_escape_string($link,$_POST['retail_type']);
	$company_name = mysqli_real_escape_string($link,$_POST['company_name']);
	$fax_num = mysqli_real_escape_string($link,$_POST['fax_num']);
	$network1 = mysqli_real_escape_string($link,$_POST['network1']);
	$network2 = mysqli_real_escape_string($link,$_POST['network2']);
	$birthdate = mysqli_real_escape_string($link,$_POST['birthdate']);
	$postal_code = mysqli_real_escape_string($link,$_POST['postal_code']);
	
	$branch = mysqli_real_escape_string($link,$_POST['branch']);


	$query = "CALL `SP-store-add_client`('$first_name', '$middle_name', '$last_name', '$contact_num1', '$contact_num2', '$email', '$address', '$address_city','$session_user_id', '$client_type', '$company_name', '$fax_num', '$retail_type', '$network1', '$network2', '$birthdate', '$postal_code', '$branch');";

	$result = mysqli_query($link,$query);

	if (!$result) {

		$resp['status'] = 'failed';
    $resp['message'] = mysqli_error($link);


	} else {

		$resp['status'] = 'success';


	}


	echo json_encode($resp);



}


if(isset($_POST['pos-store-load_client_details']) && $_POST['pos-store-load_client_details'] == true) {

	$resp = array(
	'id' => '',
	'first_name' => '',
	'middle_name' => '',
	'last_name' => '',
	'contact_num1' => '',
	'contact_num2' => '',
	'email' => '',
	'address' => '',
	'address_city' => '',
	//'address_country' => '',
//	'address_state' => '',
	'add_by' => '',
	'add_datetime' => '',
	'client_type' => '',
	'company_name' => '',
	'fax' => '',
	'branch' => ''
	);


	$id = mysqli_real_escape_string($link,$_POST['id']);

	$query = "CALL `SP-store-view_client_details`('$id');";

	$result = mysqli_query($link,$query);

	if ($row = mysqli_fetch_array($result)) {

		$resp['id'] = $row['client_id'];
		$resp['first_name'] = $row['first_name'];
		$resp['middle_name'] = $row['middle_name'];
		$resp['last_name'] = $row['last_name'];
		$resp['contact_num1'] = $row['contact_num1'];
		$resp['contact_num2'] = $row['contact_num2'];
		$resp['email'] = $row['email'];
		$resp['address'] = $row['address'];
		$resp['address_city'] = $row['address_city'];
		//$resp['address_country'] = $row['address_country'];
		//$resp['address_state'] = $row['address_state'];
		$resp['add_by'] = $row['add_by'];
		$resp['add_datetime'] = $row['add_datetime'];
		$resp['client_type'] = $row['client_type'];
		$resp['company_name'] = $row['company_name'];
		$resp['fax'] = $row['fax'];
		$resp['network1'] = $row['network1'];
		$resp['network2'] = $row['network2'];
		$resp['postal_code'] = $row['postal_code'];
		$resp['birth_date'] = $row['birth_date'];
		$resp['retail_type'] = $row['retail_type'];
		$resp['branch'] = $row['store_id'];

	}



	echo json_encode($resp);



}



if(isset($_POST['pos-store-update_client']) && $_POST['pos-store-update_client'] == true) {

	$resp = array(
	'status' => '',
	'message' => ''
	);


	$update_id = mysqli_real_escape_string($link,$_POST['id']);
	$first_name = mysqli_real_escape_string($link,$_POST['first_name']);
	$middle_name = mysqli_real_escape_string($link,$_POST['middle_name']);
	$last_name = mysqli_real_escape_string($link,$_POST['last_name']);
	$contact_num1 = mysqli_real_escape_string($link,$_POST['contact_num1']);
	$contact_num2 = mysqli_real_escape_string($link,$_POST['contact_num2']);
	$email = mysqli_real_escape_string($link,$_POST['email']);
	$address = mysqli_real_escape_string($link,$_POST['address']);
	$address_city = mysqli_real_escape_string($link,$_POST['address_city']);
	$client_type = mysqli_real_escape_string($link,$_POST['client_type']);
	$company_name = mysqli_real_escape_string($link,$_POST['company_name']);
	$fax = mysqli_real_escape_string($link,$_POST['fax']);
	$retail_type = mysqli_real_escape_string($link,$_POST['retail_type']);
	$network1 = mysqli_real_escape_string($link,$_POST['network1']);
	$network2 = mysqli_real_escape_string($link,$_POST['network2']);
	$birth_date = mysqli_real_escape_string($link,$_POST['birth_date']);
	$postal_code = mysqli_real_escape_string($link,$_POST['postal_code']);
	
	$branch = mysqli_real_escape_string($link,$_POST['branch']);

	$query = "CALL `SP-store-update_client`('$update_id', '$first_name', '$middle_name', '$last_name', '$contact_num1', '$contact_num2', '$email', '$address', '$address_city','$client_type', '$company_name', '$fax', '$retail_type','$network1', '$network2','$birth_date', '$postal_code', '$branch');";

	$result = mysqli_query($link,$query);

	if (!$result) {

		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);


	} else {

		$resp['status'] = 'success';


	}


	echo json_encode($resp);


}



?>
