<?php
include '../../../config.php';


if(isset($_POST['user_access-display_user_access_level-table']) && $_POST['user_access-display_user_access_level-table'] == true) {
	
	
	$str = "";

	$str .= "<thead>";

		$query = "CALL `SP-base-display_user_access_level`();";
	
		$result = mysqli_query($link,$query)or die(mysqli_error($link));
		
		
		while ($row = mysqli_fetch_field($result)) {
		
			$str .= "<th>";
			$str .= $row->name;
			$str .= "</th>";
		
		}
		
		//add Action Header
		/*
		$str .= "<th>";
		$str .= "Actions";
		$str .= "</th>";
		*/
		
			
	
	//CLOSE THEAD
	$str .= "</thead>";

	//OPEN TBODY
	
	$str .= "<tbody>";
		
		
		$index = 0;
		$id = '';
		
		while ($row = mysqli_fetch_row($result)) {
			
			$str .= "<tr>";
			
			foreach ($row as $data) {
				
				if ($index == 0) {
				
					$id = $data;	
					
				}
	
				$str .= "<td>" . $data . "</td>";
				
				$index++;
				
				
			}
			
			//reset index
			$index = 0;
			
		
			/*
			$str .= 
			"<td>" . 
			
			'<button id="btn_job_view-details" class="btn btn-success btn-sm" value="' . $id . '" status=" ' .  $data . '">
		  	<i class="fa-refresh"></i>
			<span>View Details</span>
		 	</button>' . 
			
			"</td>";
			*/
			
			$str .= "</tr>";	
			
		}
	
	
		//CLOSE TBODY
		$str .= "</tbody>";
		
		
		echo $str;
	
} 



if(isset($_POST['user_access-display_user_role-table']) && $_POST['user_access-display_user_role-table'] == true) {
	
	
	$str = "";

	$str .= "<thead>";

	$query = "CALL `SP-base-display_user_role`();";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	
	while ($row = mysqli_fetch_field($result)) {
	
		$str .= "<th>";
		$str .= $row->name;
		$str .= "</th>";
	
	}
	
	//add Action Header
	$str .= "<th>";
	$str .= "Actions";
	$str .= "</th>";
			
	
	//CLOSE THEAD
	$str .= "</thead>";

	//OPEN TBODY
	
	$str .= "<tbody>";
		
		
	$index = 0;
	$id = '';
	$role_name = '';
	
	while ($row = mysqli_fetch_row($result)) {
		
		$str .= "<tr>";
		
		foreach ($row as $data) {
			
			switch ($index) {
				case 0:
					$role_code = $data;
					break;
				case 1:
					$role_name = $data;
			}
										
			$index++;
			
		}
		
		
		//reset index
		$index = 0;
		
		
		
		/*
		
		<td class="user-name">
			<a href="#" class="name">' . $name . '</a>
			<span>' . $address_state . '</span>
		</td>
		
		*/
		
	
		
		$str .= '
		
		<td class="hidden-xs hidden-sm">
			<span class="email">' . $role_code . '</span>
		</td>
			
			
		<td class="hidden-xs hidden-sm">
			<span class="email">' . $role_name . '</span>
		</td>
		
		
		
		<td class="action-links">
			<a href="#link_view" class="edit" id="link_view" value="' . $role_code . '" role_name="' . $role_name . '">
				<i class="fa-folder-open-o"></i>
				View
			</a>
			
			<a href="#link_delete" class="delete" id="link_delete" value="' . $role_code . '" role_name="' . $role_name . '">
				<i class="linecons-trash"></i>
				Delete
			</a>
			
		</td>
		
		
		
		';
		
		
		$str .= "</tr>";	
		
	}


	//CLOSE TBODY
	$str .= "</tbody>";
	
	
	echo $str;
	
	
}




if(isset($_POST['user_access-add_user_role']) && $_POST['user_access-add_user_role'] == true) {
	
	$role_code = mysqli_real_escape_string($link, $_POST['role_code']);
	$role_name = mysqli_real_escape_string($link, $_POST['role_name']);
		
	$query = "CALL `SP-base-add_user_role`('$role_code', '$role_name');";

	$result = mysqli_query($link,$query);
	
	if (!$result) {
		
		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);
		
	
	} else {
		
		$resp['status'] = 'success';
	
		
	}
	
	
	
	echo json_encode($resp);
	
	
	
}




if(isset($_POST['user_access-delete_user_role']) && $_POST['user_access-delete_user_role'] == true) {
	
	$resp = array(
	'status' => '',
	'message' => ''
	);
	
	
	$role_code = mysqli_real_escape_string($link, $_POST['role_code']);
		
	$query = "CALL `SP-base-delete_user_role`('$role_code');";

	$result = mysqli_query($link,$query);
	
	if (!$result) {
		
		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);
		
	
	} else {
		
		$resp['status'] = 'success';
	
		
	}
	
	
	
	echo json_encode($resp);
	
	
	
}




if(isset($_POST['user_access-update_user_role']) && $_POST['user_access-update_user_role'] == true) {
	
	$resp = array(
	'status' => '',
	'message' => ''
	);
	
	
	$role_code_ref = mysqli_real_escape_string($link, $_POST['role_code_ref']);
	$role_code = mysqli_real_escape_string($link, $_POST['role_code']);
	$role_name = mysqli_real_escape_string($link, $_POST['role_name']);
	
		
	$query = "CALL `SP-base-update_user_role`('$role_code_ref', '$role_code', '$role_name');";

	$result = mysqli_query($link,$query);
	
	if (!$result) {
		
		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);
		
	
	} else {
		
		$resp['status'] = 'success';
	
		
	}
	
	
	
	echo json_encode($resp);
	
	
	
}


?>