<?php
include '../../../config.php';

if(isset($_POST['user_base-display_user_base_all-table']) && $_POST['user_base-display_user_base_all-table'] == true) {
	
	
	$str = "";

	$str .= "<thead>";

	$query = "CALL `SP-base-display_user_base`();";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	$index = 0;
	
	while ($row = mysqli_fetch_field($result)) {
		
		if ($index >= 2) {
			
			$str .= "<th>";
			$str .= $row->name;
			$str .= "</th>";		
			
		}
		
		$index++;
	
		
	}
	
	//add Action Header
	$str .= "<th>";
	$str .= "Actions";
	$str .= "</th>";
			
	
	//CLOSE THEAD
	$str .= "</thead>";

	//OPEN TBODY
	
	$str .= "<tbody>";
		
		
	$index = 0;
	$id = '';
	$role_name = '';
	
	while ($row = mysqli_fetch_row($result)) {
		
		$str .= "<tr>";
		
		foreach ($row as $data) {
			
			switch ($index) {
				case 0:
					$user_id = $data;
					break;
				case 1:
					$role_description = $data;
					break;
				case 2:
					$name = $data;
					break;
				case 3:
					$access_code = $data;
					break;
				case 4:
					$access_name = $data;
					break;
			}
										
			$index++;
			
		}
		
		
		/*
		
			<td class="user-name">
				<a href="#" class="name">Jack Gates</a>
				<span>Administrator</span>
			</td>
		
		*/
		
		//reset index
		$index = 0;
		
	
		
		$str .= '	
		
			<td class="user-name">
				<a href="#" class="name">' . $name . '</a>
				<span>' . $role_description . '</span>
			</td>
			
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $access_code . '</span>
			</td>
			
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $access_name  . '</span>
			</td>
			
			
			<td class="action-links">
				<a href="#link_view" class="edit" id="link_view" value="' . $user_id . '" username=" ' . $name . '">
					<i class="fa-folder-open-o"></i>
					View
				</a>
	
				<a href="#link_delete" class="delete" id="link_delete" value="' . $user_id . '" username=" ' . $name . '">
					<i class="linecons-trash"></i>
					Delete
				</a>
			</td>
			
		
		';
		
		
		$str .= "</tr>";	
		
	}


	//CLOSE TBODY
	$str .= "</tbody>";
	
	
	echo $str;
		

	
}


if(isset($_POST['user_base-access_level_and_role-option']) && $_POST['user_base-access_level_and_role-option'] == true) {
	
	$resp = array(
	'user_access_level_option' => '',
	'user_role_option' => ''
	);
	
	
	$query = "SELECT * FROM ats_user_access_level";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	$index = 0;
	
	
	
	
	$resp['user_access_level_option'] .= '<option value="" disabled selected>Select access level..</option>';
	
	$resp['user_role_option'] .= '<option value="" disabled selected>Select user role..</option>';
	
	while ($row = mysqli_fetch_array($result)) {
	
		$access_level_id = $row['access_level_id'];
		$short_description = $row['short_description'];
		
		$resp['user_access_level_option'] .= '<option value="' . $access_level_id . '">' . $access_level_id . ' - ' . $short_description . '</option>';
			
	}
	
	
	mysqli_free_result($result);
	
	
	
	$query = "SELECT * FROM ats_user_role WHERE is_deleted = 'NO'";

	$result1 = mysqli_query($link,$query)or die(mysqli_error($link));
	
	
	while ($row = mysqli_fetch_array($result1)) {
		
		$role_id = $row['role_id'];
		$role_description = $row['role_description'];
		
			
		$resp['user_role_option'] .= '<option value="' . $role_id . '">' . $role_id . ' - ' . $role_description . '</option>';
			
		
		
	}
	
	mysqli_free_result($result1);
	
	echo json_encode($resp);
	
	
	
}





if(isset($_POST['user_base-add_new_base_user']) && $_POST['user_base-add_new_base_user'] == true) {
	

	
	$first_name = mysqli_real_escape_string($link,$_POST['first_name']);
	$middle_name = mysqli_real_escape_string($link,$_POST['middle_name']);
	$last_name = mysqli_real_escape_string($link,$_POST['last_name']);
	$address = mysqli_real_escape_string($link,$_POST['address']);
	$username = mysqli_real_escape_string($link,$_POST['username']);
	$email = mysqli_real_escape_string($link,$_POST['email']);
	
	$phone_work_num = mysqli_real_escape_string($link,$_POST['phone_work_num']);
	$phone_cell_num = mysqli_real_escape_string($link,$_POST['phone_cell_num']);
	$password = 'default';
	
	$user_access_level_id = mysqli_real_escape_string($link,$_POST['user_access_level']);
	$user_role_id = mysqli_real_escape_string($link,$_POST['user_role']);
	$user_type_id = '1'; //TYPE = BASE (FIX)
	
	
	$query = "CALL `SP-base-add_user_base`('$username', '$email', '$password', '$first_name', '$middle_name', '$last_name', '$phone_work_num', '$phone_cell_num', '$address', '$user_type_id', '$user_access_level_id', '$user_role_id')";
	
	$result = mysqli_query($link,$query);
	
	
	if (!$result) {
		
		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);
	
		
	} else {
		
		$resp['status'] = 'success';

		
	}
	
	
	echo json_encode($resp);
	
	
}



if(isset($_POST['user_base-delete_base_user']) && $_POST['user_base-delete_base_user'] == true) {
	
	$resp = array(
	'status' => '',
	'message' => ''
	
	);
		
	$user_id = mysqli_real_escape_string($link,$_POST['id']);
	
	
	
	$query = "CALL `SP-base-delete_user_base`('$user_id');";
	
	$result = mysqli_query($link,$query);
	
	
	if (!$result) {
		
		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);
	
		
	} else {
		
		$resp['status'] = 'success';

	}
	
	
	echo json_encode($resp);


}



if(isset($_POST['user_base-load_base_user']) && $_POST['user_base-load_base_user'] == true) {
	
	$resp = array(
	'id' => '',
	'first_name' => '',
	'middle_name' => '',
	'last_name' => '',
	'address' => '',
	'username' => '',
	'email' => '',
	'access_level' => '',
	'role' => '',
	'phone_work' => '',
	'phone_cell' => ''
	);
	
	$user_id = mysqli_real_escape_string($link,$_POST['id']);
	
	
	$query = "CALL `SP-base-load_user_base_detail`('$user_id')";
	
	$result = mysqli_query($link,$query);
	
	
	if($row = mysqli_fetch_array($result)) {
	
		$resp['id'] = $row['user_id'];
		$resp['first_name'] = $row['first_name'];
		$resp['middle_name'] = $row['middle_name'];
		$resp['last_name'] = $row['last_name'];
		$resp['address'] = $row['address'];
		$resp['username'] = $row['username'];
		$resp['email'] = $row['email'];
		$resp['access_level'] = $row['access_level_id'];
		$resp['role'] = $row['role_id'];
		$resp['phone_work'] = $row['phone_work'];
		$resp['phone_cell'] = $row['phone_cell'];	
		
	}
	
	
	echo json_encode($resp);
	
	
}



if(isset($_POST['user_base-update_base_user']) && $_POST['user_base-update_base_user'] == true) {
	
	$resp = array(
	'status' => '',
	'message' => ''
	);
	
	
	$id = mysqli_real_escape_string($link,$_POST['id']);
	$first_name = mysqli_real_escape_string($link,$_POST['first_name']);
	$middle_name = mysqli_real_escape_string($link,$_POST['middle_name']);
	$last_name = mysqli_real_escape_string($link,$_POST['last_name']);
	$address = mysqli_real_escape_string($link,$_POST['address']);
	$username = mysqli_real_escape_string($link,$_POST['username']);
	$email = mysqli_real_escape_string($link,$_POST['email']);
	$access_level = mysqli_real_escape_string($link,$_POST['access_level']);
	$role = mysqli_real_escape_string($link,$_POST['role']);
	$phone_work = mysqli_real_escape_string($link,$_POST['phone_work']);
	$phone_cell = mysqli_real_escape_string($link,$_POST['phone_cell']);
	
	$query = "CALL `SP-base-update_base_user`('$id', '$username', '$email', '$first_name', '$middle_name', '$last_name', '$phone_work', '$phone_cell', '$address', '$access_level', '$role')";
	
	$result = mysqli_query($link,$query);
	
	
	if (!$result) {
		
		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);
	
		
	} else {
		
		$resp['status'] = 'success';

	}
	
	
	echo json_encode($resp);
	
	
	
}



?>