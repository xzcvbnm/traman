<?php

include 'central_config.php';


error_reporting(0);

session_start();

$user_type = $_SESSION['user_type'];
$account_name = $_SESSION['account_name'];
$store_name = $_SESSION['store_name'];
$limit_count = 30;



//setup PHP UTF-8 stuff
//setlocale(LC_CTYPE, 'en_US.UTF-8');
//mb_internal_encoding("UTF-8");
//mb_http_output('UTF-8'); 



function sendsms($ip, $port, $password, $phone, $text) {


    $string = $phone;
    $message = $text;
    $string = preg_replace('/\.$/', '', $string); //Remove dot at end if exists
    $array = explode(', ', $string); //split string into array seperated by ', '


    $url = "https://semysms.net/api/3/sms_more.php"; //Url address for sending SMS
    $device = '69417';  // Phone number
    $token = 'd07a3d6f13d922029241f18fc43f65c7';  // Your token (secret)

    $params = array('token'  => $token);


    $id = 0;
    foreach($array as $phone) {
        $id++;

        $params['data'][] = array(
            'my_id' => $id,
            'device' => $device, 'phone' => $phone, 'msg' => $message);

    }



    $params = json_encode($params);

    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($params))
    );
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS,  $params);
    $result = curl_exec($curl);
    curl_close($curl);

    return $result;


	
}



/*
----------------->>>>>>>>>>>>>>>>  SMS Script for HOME PAGE
*/


if(isset($_POST['sendsms']) && $_POST['sendsms'] == true) {
	
	
	$sender = $_POST['sender'];
	$number = $_POST['number'];
	$message = $_POST['message'];
	
	
$sms_footer = "For Further Assistance please Call or
Text our Helpline:
Textline: 0998-952-3230 (Text Only)
Globe: 0917-840-9951 (Call Only)
Smart / Sun: 0998-955-5705 (Call Only)
Landline: 882-6235 / 757-2200.
Thank you!";

	
	
$message = $message
.
"

$account_name
$sms_footer
";


	$array_number = explode(',', $number);


	foreach($array_number as $val) {

		$qsender = mysqli_real_escape_string($link,$_POST['sender']);
		$qnumber =  mysqli_real_escape_string($link,$_POST['number']);
		$qmessage =  mysqli_real_escape_string($link,$message);

		if ($val != "") {

			$random = md5(uniqid(rand(), true));

			$query = "CALL `SP-sms-save_send_sms`('$val', '$qmessage', '$qsender');";

			mysqli_query($link,$query);



		}


	}


    echo (sendsms('', '', '', $number, $message));

}





if(isset($_POST['sms-display_inbox_known']) && $_POST['sms-display_inbox_known'] == true) {
	
	
	$str = "<strong>New</strong>";
	
	$query = '
	
	SELECT
	
	CASE 
	WHEN pos_client.client_id IS NOT NULL 
	THEN CONCAT(pos_client.first_name, " ", pos_client.last_name) 
	ELSE sms_data_datetime.phone
	END AS "contact",
	
	sms_data_datetime.*
	FROM
	(
	SELECT 
	TRIM(REPLACE(sms_data_listen.phone, "+63", "0")) AS "phone",
	record_datetime,
	status
	FROM sms_data_listen 
	ORDER BY record_datetime DESC
	) AS sms_data_datetime
	
	LEFT JOIN pos_client 
	ON sms_data_datetime.phone = pos_client.contact_num1
	
	WHERE sms_data_datetime.status = "unread"
	
	
	GROUP BY phone
	ORDER BY record_datetime DESC
	
	';
	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	
	while ($row = mysqli_fetch_array($result)) {
		
		$contact = $row['contact'];
		$phone = $row['phone'];
		$status = $row['status'];
		
		if ($status == "read") {
			
			$str .= '
			<a href="#" id="link_sms_view_conversation" contact="' . $contact . '" phone="' . $phone . '">
				<span class="user-status is-offline"></span> 
				<em>' . $contact . '</em>
			</a>';
			
			
		} else {
			
			$str .= '
			<a href="#" id="link_sms_view_conversation" contact="' . $contact . '" phone="' . $phone . '">
				<span class="user-status is-online"></span> 
				<em>' . $contact . '</em>
			</a>';
			
			
		}
		
	}
	
	
	echo $str;



}





if(isset($_POST['sms-display_inbox_unknown']) && $_POST['sms-display_inbox_unknown'] == true) {
	
	//set memory limit
	//ini_set('memory_limit', '2048M');
	
	//$str = "<strong>Done</strong>";

	$limit_start = $_POST['limit_start'];

	if($limit_start == 0){
		$limit_ = $limit_count;
	}else{
		$limit_ = $limit_start + $limit_count;
	}

	$str = "";
	
	$query = '
	
	SELECT
	
	CASE 
	WHEN pos_client.client_id IS NOT NULL 
	THEN CONCAT(pos_client.first_name, " ", pos_client.last_name) 
	ELSE `views-sms_data_datetime`.phone
	END AS "contact",
	`views-sms_data_datetime`.phone,
	`views-sms_data_datetime`.record_datetime,
    `views-sms_data_datetime`.status
	FROM `views-sms_data_datetime`
	
	LEFT JOIN pos_client 
	ON `views-sms_data_datetime`.phone = pos_client.contact_num1
    
    #WHERE sms_data_datetime.sender != "SYSTEM"
	
	
	GROUP BY phone
	ORDER BY record_datetime DESC
	
	LIMIT '.$limit_start.','.$limit_count.'
	
	';
	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	
	while ($row = mysqli_fetch_array($result)) {
		
		$contact = $row['contact'];
		$phone = $row['phone'];
		$status = $row['status'];
		
		if ($status == "read") {
			
			$str .= '
			<a href="#" id="link_sms_view_conversation" contact="' . $contact . '" phone="' . $phone . '">
				<span class="user-status is-offline"></span> 
				<em>' . $contact . '</em>
			</a>';
			
			
		} else {
			
			$str .= '
			<a href="#" id="link_sms_view_conversation" contact="' . $contact . '" phone="' . $phone . '">
				<span class="user-status is-online"></span> 
				<em>' . $contact . '</em>
			</a>';
			
			
		}
		
		//sleep(1);
		
		//echo $str;
		
		
		//flush();
		//ob_flush();
		
		//ob_clean();


		
	}

	$str .='
	<a id="a-load_more" class="person">
		<button id="btn-load_more">Load more...</button>
	</a>
	
	';
	echo $str;
	
	
	//ob_end_flush();
	
	
	



}






if(isset($_POST['sms-display_conversation_data']) && $_POST['sms-display_conversation_data'] == true) {
	
	$phone = $_POST['phone'];
	
	$str = "";
	
	$query = "
	
	SELECT 
	sms_data_listen.id,
	CASE 
	WHEN pos_client.client_id IS NOT NULL 
	THEN CONCAT(pos_client.first_name, ' ', pos_client.last_name) 
	ELSE REPLACE(sms_data_listen.phone, '+63', '0')
	END AS 'contact',
	
	REPLACE(sms_data_listen.phone, '+63', '0') AS 'phone',
	
	CASE 
	WHEN sms_data_listen.sender = ''
	THEN 
	
	CASE 
	WHEN pos_client.client_id IS NOT NULL 
	THEN CONCAT(pos_client.first_name, ' ', pos_client.last_name) 
	ELSE REPLACE(sms_data_listen.phone, '+63', '0')
	END 
	
	ELSE 
	
	sms_data_listen.sender 
	
	END AS 'header', 
	
	
	sms_data_listen.sender,
	sms_data_listen.text,
	DATE_FORMAT(sms_data_listen.record_datetime, '%b. %d, %Y %h:%i %p') as 'record_datetime'
	
	
	FROM sms_data_listen 
	
	LEFT JOIN pos_client 
	ON REPLACE(sms_data_listen.phone, '+63', '0') = pos_client.contact_num1 
	
	WHERE REPLACE(sms_data_listen.phone, '+63', '0') = '$phone'
	
	GROUP BY sms_data_listen.id
	
	ORDER BY sms_data_listen.record_datetime 
	
	";
	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	
	while ($row = mysqli_fetch_array($result)) {		
		
		$id = $row['id'];
		$contact = $row['contact'];
		$phone = $row['phone'];
		$header = $row['header'];
		$text = $row['text'];
		$record_datetime = $row['record_datetime'];
		
		
		if ($user_type == "ADMIN") {
			
			
			$str .= '
		
			 <li>
			 
				<span class="user">' . ' <a href="#" id="sms-link_delete" value="' . $id . '" class="conversation-close">×</a> ' . $header  . '</span>
				<span class="time">' . $record_datetime . '</span>
				<p><pre style="overflow:hidden">' . $text . '</pre></p>
			</li>
			
			';
				
			
		} else {
			
			
			$str .= '
		
			 <li>
				<span class="user">' . $header  . '</span>
				<span class="time">' . $record_datetime . '</span>
				<p><pre style="overflow:hidden">' . $text . '</pre></p>
			</li>
		
		';
			
			
			
		}
		
		
		

		
	}
	
	
	echo $str;
	
	
	
	//SET UNREAD TO READ STATUS
	$query = "
	UPDATE sms_data_listen 
	SET status = 'read'
	WHERE REPLACE(sms_data_listen.phone, '+63', '0') = '$phone' 
	AND status = 'unread'
	";
	
	mysqli_query($link, $query);



}



if(isset($_POST['sms-display_unread_count']) && $_POST['sms-display_unread_count'] == true) {

	
	$counter = 0;
	
	$str = "";
	
	$arr_phones = array();
	
	$resp = array(
	'sms_count' => '',
	'sms_data' => '',
	'sms_phones' => ''
	);
	
	$sms_unread_count = '';
	
	$query = '
	
	SELECT
	
	CASE 
	WHEN pos_client.client_id IS NOT NULL 
	THEN CONCAT(pos_client.first_name, " ", pos_client.last_name) 
	ELSE sms_data_datetime.phone
	END AS "contact",
	
	sms_data_datetime.*
	FROM
	(
	SELECT 
	TRIM(REPLACE(sms_data_listen.phone, "+63", "0")) AS "phone",
	record_datetime,
	status
	FROM sms_data_listen 
	ORDER BY record_datetime DESC
	) AS sms_data_datetime
	
	LEFT JOIN pos_client 
	ON sms_data_datetime.phone = pos_client.contact_num1
    
    
    WHERE sms_data_datetime.status = "unread" 
	
	
	GROUP BY phone
	ORDER BY record_datetime DESC
	
	';
	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	
	while ($row = mysqli_fetch_array($result)) {
		
		
		$counter++;
		
		$contact = $row['contact'];
		$phone = $row['phone'];
		$record_datetime = $row['record_datetime'];
		$status = $row['status'];
		
		
		array_push($arr_phones, $phone);
		
		$str .= '
		<a href="#" id="link_sms_view_conversation" contact="' . $contact . '" phone="' . $phone . '">
			<span class="user-status is-online"></span> 
			<em>' . $contact . '</em>
		</a>';
		
		
		//$sms_unread_count = $row['sms_unread_count'];
		
	} 
	
	
	$resp['sms_count'] = $counter;
	$resp['sms_data'] = $str;
	$resp['sms_phones'] = json_encode($arr_phones);
	
	
	echo json_encode($resp);



}



//delete sms data
if(isset($_POST['sms-delete_sms_data']) && $_POST['sms-delete_sms_data'] == true) {
	
	$id = mysqli_real_escape_string($link,$_POST['id']);
	
	
	$resp = array(
	'status' => '',
	'message' => ''
	);


	$query = "DELETE FROM sms_data_listen WHERE id = '$id'";

	$result = mysqli_query($link,$query);

	if (!$result) {

		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);


	} else {

		$resp['status'] = 'success';


	}


	echo json_encode($resp);
	
	


}


?>