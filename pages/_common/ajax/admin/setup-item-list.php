<?php
session_start();

include '../../../../config.php';

$session_user_id = $_SESSION['user_id'];


if(isset($_POST['pos-admin-display_item-table']) && $_POST['pos-admin-display_item-table'] == true) {

	$str = "";

	$str .= "<thead>";

	$query = "CALL `SP-admin-display_item_all`();";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));

	$index = 0;

	while ($row = mysqli_fetch_field($result)) {

		//if ($index >= 1) {

			$str .= "<th>";
			$str .= $row->name;
			$str .= "</th>";

		//}

		$index++;


	}

	//add Action Header
	$str .= "<th>";
	$str .= "Actions";
	$str .= "</th>";


	//CLOSE THEAD
	$str .= "</thead>";


	//OPEN TBODY
	$str .= "<tbody>";


	$index = 0;

	while ($row = mysqli_fetch_row($result)) {

		$str .= "<tr>";

		foreach ($row as $data) {

			switch ($index) {
				case 0:
					$id = $data;
					break;
				case 1:
					$item_name = $data;
					break;
			}

			$index++;

		}


		//reset index
		$index = 0;



		/*
		<td class="user-name">
			<a href="#" class="name">' . $store_name . '</a>

			<span>' . $address_city . '</span>

		</td>

		*/

		$str .= '

			<td class="hidden-xs hidden-sm">
				<span class="email">' . $id . '</span>
			</td>

			<td class="hidden-xs hidden-sm">
				<span class="email">' . $item_name . '</span>
			</td>


			<td class="action-links">

				<a href="#" class="edit" id="link_view" value="' . $id . '" name=" ' . $item_name . '">
					<i class="fa-folder-open-o"></i>
					View
				</a>


				<a href="#" class="delete" id="link_delete" value="' . $id . '" name=" ' . $item_name . '">
					<i class="linecons-trash"></i>
					Delete
				</a>
			</td>


		';


		$str .= "</tr>";

	}

	//CLOSE TBODY
	$str .= "</tbody>";

	echo $str;

}




if(isset($_POST['pos-admin-add_item']) && $_POST['pos-admin-add_item'] == true) {

	$resp = array(
	'status' => '',
	'message' => ''
	);


	$item_name = mysqli_real_escape_string($link,$_POST['item_name']);

	$query = "CALL `SP-admin-add_item`('$item_name');";

	$result = mysqli_query($link,$query);

	if (!$result) {

		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);


	} else {

		$resp['status'] = 'success';


	}


	echo json_encode($resp);


}




if(isset($_POST['pos-admin-delete_item']) && $_POST['pos-admin-delete_item'] == true) {

	$resp = array(
	'status' => '',
	'message' => ''
	);

	$id = mysqli_real_escape_string($link,$_POST['id']);

	$query = "CALL `SP-admin-delete_item`('$id');";

	$result = mysqli_query($link,$query);

	if (!$result) {

		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);


	} else {

		$resp['status'] = 'success';


	}


	echo json_encode($resp);



}



if(isset($_POST['pos-admin-load_service_price_details']) && $_POST['pos-admin-load_service_price_details'] == true) {

	$resp = array(
	'id' => '',
	'code' => '',
	'name' => '',
	'price' => '',
	'unit' => '',
	'add_by' => '',
	'add_datetime' => ''
	);

	$id = mysqli_real_escape_string($link,$_POST['id']);

	$query = "CALL `SP-admin-view_service_and_price_details`('$id');";

	$result = mysqli_query($link,$query);

	if ($row = mysqli_fetch_array($result)) {

		$resp['id'] = $row['service_id'];
		$resp['code'] = $row['code'];
		$resp['name'] = $row['name'];
		$resp['price'] = $row['price'];
		$resp['unit'] = $row['unit'];
		$resp['add_by'] = $row['add_by'];
		$resp['add_datetime'] = $row['add_datetime'];


	}

	echo json_encode($resp);

}



if(isset($_POST['pos-setup-update_item']) && $_POST['pos-setup-update_item'] == true) {

	$resp = array(
	'status' => '',
	'message' => ''
	);

	$update_id = mysqli_real_escape_string($link,$_POST['id']);
	$item_name = mysqli_real_escape_string($link,$_POST['item_name']);



	$query = "CALL `SP-setup-update_item`('$update_id', '$item_name');";
	$result = mysqli_query($link,$query);

	if (!$result) {

		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);


	} else {

		$resp['status'] = 'success';


	}


	echo json_encode($resp);


}

?>
