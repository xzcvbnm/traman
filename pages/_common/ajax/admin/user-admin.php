<?php
session_start();

include '../../../../config.php';



$session_user_id = $_SESSION['user_id'];


if(isset($_POST['pos-admin-display_user_admin_all-table']) && $_POST['pos-admin-display_user_admin_all-table'] == true) {
	
	
	$str = "";

	$str .= "<thead>";

	$query = "CALL `SP-admin-display_user_admin_all`('$session_user_id');";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	$index = 0;
	
	while ($row = mysqli_fetch_field($result)) {
		
		//if ($index >= 2) {
			
			$str .= "<th>";
			$str .= $row->name;
			$str .= "</th>";		
			
		//}
		
		//$index++;
	
		
	}
	
	//add Action Header
	$str .= "<th>";
	$str .= "Actions";
	$str .= "</th>";
			
	
	//CLOSE THEAD
	$str .= "</thead>";
	

	//OPEN TBODY
	$str .= "<tbody>";
		
		
	$index = 0;
	
	while ($row = mysqli_fetch_row($result)) {
		
		$str .= "<tr>";
		
		foreach ($row as $data) {
			
			switch ($index) {
				case 0:
					$id = $data;
					break;
				case 1:
					$name = $data;
					break;
			}
										
			$index++;
			
		}
		
		
		//reset index
		$index = 0;
		
	
	
		/*
		<td class="user-name">
			<a href="#" class="name">' . $store_name . '</a>
			
			<span>' . $address_city . '</span>
			
		</td>
		
		*/
		
		$str .= '	
		
			
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $id . '</span>
			</td>
		
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $name . '</span>
			</td>
		
			
			<td class="action-links">
				<a href="#" class="edit" id="link_view" value="' . $id . '" admin_name=" ' . $name . '">
					<i class="fa-folder-open-o"></i>
					View
				</a>
	
				<a href="#" class="delete" id="link_delete" value="' . $id . '" admin_name=" ' . $name . '">
					<i class="linecons-trash"></i>
					Delete
				</a>
			</td>
			
		
		';
		
		
		$str .= "</tr>";	
		
	}


	//CLOSE TBODY
	$str .= "</tbody>";
	
	
	echo $str;

	
}




if(isset($_POST['pos-admin-add_user_admin']) && $_POST['pos-admin-add_user_admin'] == true) {
	
	$resp = array(
	'status' => '',
	'message' => ''
	);
	
	$username = mysqli_real_escape_string($link,$_POST['username']);
	$password = mysqli_real_escape_string($link,$_POST['password']);
	$first_name = mysqli_real_escape_string($link,$_POST['first_name']);
	$middle_name = mysqli_real_escape_string($link,$_POST['middle_name']);
	$last_name = mysqli_real_escape_string($link,$_POST['last_name']);
	
	
	$query = "CALL `SP-admin-add_user_admin`('$username', '$password', '$first_name', '$middle_name', '$last_name');";
	

	$result = mysqli_query($link,$query);
	
	if (!$result) {
		
		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);
		
	
	} else {
		
		$resp['status'] = 'success';
	
		
	}
	
	
	echo json_encode($resp);
	
	
	
}




if(isset($_POST['pos-admin-delete_user_admin']) && $_POST['pos-admin-delete_user_admin'] == true) {
	
	$resp = array(
	'status' => '',
	'message' => ''
	);

	$id = mysqli_real_escape_string($link,$_POST['id']);
	
	$query = "CALL `SP-admin-delete_user_admin`('$id');";

	$result = mysqli_query($link,$query);
	
	if (!$result) {
		
		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);
		
	
	} else {
		
		$resp['status'] = 'success';
	
		
	}
	
	
	echo json_encode($resp);
	
	
	
}



if(isset($_POST['pos-admin-load_user_admin_details']) && $_POST['pos-admin-load_user_admin_details'] == true) {
	
	$resp = array(
	'id' => '',
	'username' => '',
	'first_name' => '',
	'middle_name' => '',
	'last_name' => ''
	);

	$id = mysqli_real_escape_string($link,$_POST['id']);
	
	$query = "CALL `SP-admin-view_user_admin_details`('$id');";

	$result = mysqli_query($link,$query);
	
	if ($row = mysqli_fetch_array($result)) {
		
		$resp['id'] = $row['user_id'];
		$resp['username'] = $row['username'];
		$resp['first_name'] = $row['first_name'];
		$resp['middle_name'] = $row['middle_name'];
		$resp['last_name'] = $row['last_name'];
			
	}
	
	
	
	echo json_encode($resp);
	
	
	
}



if(isset($_POST['pos-admin-update_user_admin']) && $_POST['pos-admin-update_user_admin'] == true) {
	
	$resp = array(
	'status' => '',
	'message' => ''
	);
	
	
	$update_id = mysqli_real_escape_string($link,$_POST['id']);
	$username = mysqli_real_escape_string($link,$_POST['username']);
	$first_name = mysqli_real_escape_string($link,$_POST['first_name']);
	$middle_name = mysqli_real_escape_string($link,$_POST['middle_name']);
	$last_name = mysqli_real_escape_string($link,$_POST['last_name']);
	
	
	$query = "CALL `SP-admin-update_user_admin`('$update_id', '$username', '$first_name', '$middle_name', '$last_name');";

	$result = mysqli_query($link,$query);
	
	if (!$result) {
		
		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);
		
	
	} else {
		
		$resp['status'] = 'success';
	
		
	}
	
	
	echo json_encode($resp);


}





if(isset($_POST['pos-admin-reset_pw_user_admin']) && $_POST['pos-admin-reset_pw_user_admin'] == true) {
	
	$resp = array(
	'status' => '',
	'message' => ''
	);
	
	
	$update_id = mysqli_real_escape_string($link,$_POST['id']);
	$new_pw = mysqli_real_escape_string($link,$_POST['str_random']);
	
	
	$query = "CALL `SP-admin-update_user_admin_password`('$update_id', '$new_pw');";

	$result = mysqli_query($link,$query);
	
	if (!$result) {
		
		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);
		
	
	} else {
		
		$resp['status'] = 'success';
	
		
	}
	
	
	echo json_encode($resp);


}



?>