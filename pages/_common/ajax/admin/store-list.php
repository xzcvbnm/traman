<?php
session_start();

include '../../../../config.php';



$session_user_id = $_SESSION['user_id'];


if(isset($_POST['pos-admin-display_store_all-table']) && $_POST['pos-admin-display_store_all-table'] == true) {


	$str = "";

	$str .= "<thead>";

	$query = "CALL `SP-admin-display_store_all`();";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));

	$index = 0;

	while ($row = mysqli_fetch_field($result)) {

		if ($index >= 2) {

			$str .= "<th>";
			$str .= $row->name;
			$str .= "</th>";

		}

		$index++;


	}

	//add Action Header
	$str .= "<th>";
	$str .= "Actions";
	$str .= "</th>";


	//CLOSE THEAD
	$str .= "</thead>";


	//OPEN TBODY
	$str .= "<tbody>";


	$index = 0;

	while ($row = mysqli_fetch_row($result)) {

		$str .= "<tr>";

		foreach ($row as $data) {

			switch ($index) {
				case 0:
					$id = $data;
					break;
				case 1:
					$address_city = $data;
					break;
				case 2:
					$store_code = $data;
					break;
				case 3:
					$store_name = $data;
					break;
				case 4:
					$surcharge = $data;
					break;
			}

			$index++;

		}


		//reset index
		$index = 0;



		/*
		<td class="user-name">
			<a href="#" class="name">' . $store_name . '</a>

			<span>' . $address_city . '</span>

		</td>

		*/

		$str .= '

			<td class="hidden-xs hidden-sm">
				<span class="email">' . $store_code . '</span>
			</td>

			<td class="hidden-xs hidden-sm">
				<span class="email">' . $store_name . '</span>
			</td>

			<td class="hidden-xs hidden-sm">
				<span class="email">' . $surcharge . '</span>
			</td>


			<td class="action-links">
				<a href="#link_view" class="edit" id="link_view" value="' . $id . '" store_name=" ' . $store_name . '">
					<i class="fa-folder-open-o"></i>
					View
				</a>

				<a href="#link_delete" class="delete" id="link_delete" value="' . $id . '" store_name=" ' . $store_name . '">
					<i class="linecons-trash"></i>
					Delete
				</a>
			</td>


		';


		$str .= "</tr>";

	}


	//CLOSE TBODY
	$str .= "</tbody>";


	echo $str;

}



if(isset($_POST['pos-admin-load_store_details']) && $_POST['pos-admin-load_store_details'] == true) {

	$resp = array(
	'id' => '',
	'store_code' => '',
	'store_name' => '',
	'address' => '',
	'address_city' => '',
	'num_tel' => '',
	'num_cell' => '',
	'add_by' => '',
	'add_datetime' => '',
	'surcharge_store' => '',
	'surcharge_pickup' => ''
	);

	$id = mysqli_real_escape_string($link,$_POST['id']);

	$query = "CALL `SP-admin-view_store_details`('$id');";

	$result = mysqli_query($link,$query);

	if ($row = mysqli_fetch_array($result)) {

		$resp['id'] = $row['store_id'];
		$resp['store_code'] = $row['store_code'];
		$resp['store_name'] = $row['store_name'];
		$resp['address'] = $row['address'];
		$resp['address_city'] = $row['address_city'];
		$resp['num_tel'] = $row['num_tel'];
		$resp['num_cell'] = $row['num_cell'];
		$resp['add_by'] = $row['add_by'];
		$resp['add_datetime'] = $row['add_datetime'];
		$resp['surcharge_store'] = $row['surcharge_store'];
		$resp['surcharge_pickup'] = $row['surcharge_pickup'];


	}



	echo json_encode($resp);



}


if(isset($_POST['pos-admin-delete_store']) && $_POST['pos-admin-delete_store'] == true) {

	$resp = array(
	'status' => '',
	'message' => ''
	);

	$store_id = mysqli_real_escape_string($link,$_POST['id']);

	$query = "CALL `SP-admin-delete_store`('$store_id', '$session_user_id');";

	$result = mysqli_query($link,$query);

	if (!$result) {

		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);


	} else {

		$resp['status'] = 'success';


	}


	echo json_encode($resp);


}


if(isset($_POST['pos-admin-update_store']) && $_POST['pos-admin-update_store'] == true) {

	$resp = array(
	'status' => '',
	'message' => ''
	);


	$update_id = mysqli_real_escape_string($link,$_POST['id']);
	$store_code = mysqli_real_escape_string($link,$_POST['store_code']);
	$store_name = mysqli_real_escape_string($link,$_POST['store_name']);
	$num_cell = mysqli_real_escape_string($link,$_POST['num_cell']);
	$num_tel = mysqli_real_escape_string($link,$_POST['num_tel']);
	$address = mysqli_real_escape_string($link,$_POST['address']);
	$address_city = mysqli_real_escape_string($link,$_POST['city']);
	$surcharge_store = mysqli_real_escape_string($link,$_POST['surcharge_store']);
	$surcharge_pickup = mysqli_real_escape_string($link,$_POST['surcharge_pickup']);

	$query = "CALL `SP-admin-update_store`('$update_id', '$store_code', '$store_name', '$address', '$address_city', '$num_tel', '$num_cell', '$surcharge_store', '$surcharge_pickup');";

	$result = mysqli_query($link,$query);

	if (!$result) {

			$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);


	} else {

		$resp['status'] = 'success';


	}


	echo json_encode($resp);


}




if(isset($_POST['pos-admin-add_new_store']) && $_POST['pos-admin-add_new_store'] == true) {

	$resp = array(
	'status' => '',
	'message' => ''
	);

	$store_code = mysqli_real_escape_string($link,$_POST['store_code']);
	$store_name = mysqli_real_escape_string($link,$_POST['store_name']);
	$num_cell = mysqli_real_escape_string($link,$_POST['num_cell']);
	$num_tel = mysqli_real_escape_string($link,$_POST['num_tel']);
	$address = mysqli_real_escape_string($link,$_POST['address']);
	$address_city = mysqli_real_escape_string($link,$_POST['city']);
	$surcharge_store = mysqli_real_escape_string($link,$_POST['surcharge_store']);
	$surcharge_pickup= mysqli_real_escape_string($link,$_POST['surcharge_pickup']);

	$query = "CALL `SP-admin-add_store`('$store_code', '$store_name', '$address', '$address_city', '$num_tel', '$num_cell', '$session_user_id', '$surcharge_store', '$surcharge_pickup');";

	$result = mysqli_query($link,$query);

	if (!$result) {

		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);


	} else {

		$resp['status'] = 'success';


	}


	echo json_encode($resp);



}



?>
