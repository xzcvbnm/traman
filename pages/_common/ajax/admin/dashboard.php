<?php
session_start();

include '../../../../config.php';



$session_user_id = $_SESSION['user_id'];


$date_from = date('Y') . '-' . date('m') . '-' . '01';
$date_to = date('Y-m-d');
$month_now = date('m');
$year_now = date('Y');



//DASHBOARD START

if(isset($_POST['pos-admin-dashboard-display_sales_count-widget']) && $_POST['pos-admin-dashboard-display_sales_count-widget'] == true) {
	
	$resp = array(
	'total' => '',
	'total_paid' => '',
	'total_unpaid' => ''
	);
	
	//$old_pw = mysqli_real_escape_string($link,$_POST['old_pw']);
	//$new_pw = mysqli_real_escape_string($link,$_POST['new_pw']);
	
	
	
	$query = "CALL `SP-admin-dashboard-display_month_sales_count`('$date_from', CURDATE());";
	
	
	
	
	$result = mysqli_query($link,$query);
	
	
	
	if ($row = mysqli_fetch_array($result)) {
		
		$resp['total'] = $row['total'];
		$resp['total_paid'] = $row['total_paid'];
		$resp['total_unpaid'] = $row['total_unpaid'];
		
	}
	
	echo json_encode($resp);	
	
}


if(isset($_POST['pos-admin-dashboard-display_hourly_sales-chart']) && $_POST['pos-admin-dashboard-display_hourly_sales-chart'] == true) {

	if (isset($_POST['start_date']) && isset($_POST['end_date'])) {
		
		$start_date = $_POST['start_date'];
		$end_date = $_POST['end_date'];
		
		$query = "CALL `SP-admin-dashboard-hourly_sales`('$start_date', '$end_date')";
		
	} else {
		
		$query = "CALL `SP-admin-dashboard-hourly_sales`('$date_from', CURDATE())";
		
	}

	
	
	
	$result = mysqli_query($link,$query);
	
	while ($row = mysqli_fetch_array($result)) {
		
		
		$hour_time = $row['hour_time'];
		$total = $row['total'];
		$total_paid = $row['total_paid'];
		$total_unpaid = $row['total_unpaid'];
		
		//add data
		$resp[] = array(
		'hour_time' => $hour_time,
		'total' => (float)$total,
		'total_paid' => (float)$total_paid,
		'total_unpaid' => (float)$total_unpaid
		);
		
		
		
	}
	
	echo json_encode($resp);


}



if(isset($_POST['pos-admin-dashboard-display_daily_sales-chart']) && $_POST['pos-admin-dashboard-display_daily_sales-chart'] == true) {
	
	if (isset($_POST['start_date']) && isset($_POST['end_date'])) {
		
		$start_date = $_POST['start_date'];
		$end_date = $_POST['end_date'];
		
		$query = "CALL `SP-admin-dashboard-display_month_daily_sales_count`('$start_date', '$end_date')";
		
	} else {
		
		$query = "CALL `SP-admin-dashboard-display_month_daily_sales_count`('$date_from', CURDATE())";
		
	}

	
	
	
	$result = mysqli_query($link,$query);
	
	while ($row = mysqli_fetch_array($result)) {
		
		
		$day_num = $row['day'];
		$total = $row['total'];
		$total_paid = $row['total_paid'];
		$total_unpaid = $row['total_unpaid'];
		
		
		
		//add data
		$resp[] = array(
		'day_num' => $day_num,
		'total' => (float)$total,
		'total_paid' => (float)$total_paid,
		'total_unpaid' => (float)$total_unpaid
		);
		
		
		
	}
	
	echo json_encode($resp);
	
	
	
}



if(isset($_POST['pos-admin-dashboard-display_store_contribution-chart']) && $_POST['pos-admin-dashboard-display_store_contribution-chart'] == true) {
	
	if (isset($_POST['start_date']) && isset($_POST['end_date'])) {
		
		$start_date = $_POST['start_date'];
		$end_date = $_POST['end_date'];
		
		$query = "CALL `SP-admin-dashboard-display_store_contribution`('$start_date', '$end_date');";
		
	} else {
		
		$query = "CALL `SP-admin-dashboard-display_store_contribution`('$date_from', CURDATE());";
		
	}
	

	
	

	$result = mysqli_query($link,$query);
	
	
	while ($row = mysqli_fetch_array($result)) {
		
		
		$store_code = $row['store_code'];
		$total = $row['total'];
		$total_paid = $row['total_paid'];
		$total_unpaid = $row['total_unpaid'];
		
		$resp[] = array(
		'store_code' => $store_code,
		'total' => (float)$total
		);
		
		
		
	}
	
	echo json_encode($resp);
	
	
	
}


if(isset($_POST['pos-admin-dashboard-display_customer_sales_analytics-table']) && 
$_POST['pos-admin-dashboard-display_customer_sales_analytics-table'] == true) {
	
	//$old_pw = mysqli_real_escape_string($link,$_POST['old_pw']);
	//$new_pw = mysqli_real_escape_string($link,$_POST['new_pw']);
	

	
	$query = "CALL `SP-admin-dashboard-display_customer_sales_analytics`('$month_now', '$year_now');";

	$result = mysqli_query($link,$query);
	
	$str = '';
	
	
	if ($row = mysqli_fetch_array($result)) {
		
		
		$client_count_new = $row['client_count_new'];
		$client_count_existing = $row['client_count_existing'];
		$client_count_total = $row['client_count_total'];
		
		$client_paid_sales_new = $row['client_paid_sales_new'];
		$client_paid_sales_existing = $row['client_paid_sales_existing'];
		$client_paid_sales_total = $row['client_paid_sales_total'];
		
		$client_unpaid_sales_new = $row['client_unpaid_sales_new'];
		$client_unpaid_sales_existing = $row['client_unpaid_sales_existing'];
		$client_unpaid_sales_total = $row['client_unpaid_sales_total'];
		
		$client_expected_sales_new = ($client_paid_sales_new + $client_unpaid_sales_new );
		$client_expected_sales_existing = ($client_paid_sales_existing + $client_unpaid_sales_existing);
		$client_expected_sales_total = ($client_paid_sales_new + $client_unpaid_sales_total);
		
		
		$str = '
		
		<thead>
                         
			<tr>
				<th>Customer</th>
				<th>New</th>
				<th>Existing</th>
				<th>Total</th>
			   
			</tr>
			
		</thead>
		
		<tbody>
		
			<tr>
				<td>Count</td>
				<td>' . $client_count_new .'</td>
				<td>' . $client_count_existing .'</td>
				<td>' . $client_count_total .'</td>				
			</tr>
			
			<tr>
				<td>Paid Sales</td>
				<td>' . $client_paid_sales_new .'</td>
				<td>' . $client_paid_sales_existing .'</td>
				<td>' . $client_paid_sales_total .'</td>		
			</tr>
			
			<tr>
				<td>Unpaid Sales</td>
				<td>' . $client_unpaid_sales_new .'</td>
				<td>' . $client_unpaid_sales_existing .'</td>
				<td>' . $client_unpaid_sales_total .'</td>		
			</tr>
			
			<tr>
				<td>Expected Sales</td>
				<td>' . $client_expected_sales_new .'</td>
				<td>' . $client_expected_sales_existing .'</td>
				<td>' . $client_expected_sales_total .'</td>		
			</tr>
	   
		</tbody>
		
		
		
		
		';
		
		
		
		
		
		
		
	}
	
	echo $str;
	
	
	
}



//END OF DASHBOARD



//*******************************************************************************************
//*******************************************************************************************
//DASHBOARD -----> SMS
//*******************************************************************************************
//*******************************************************************************************

if(isset($_POST['pos-admin-dashboard-display_store_sms_contribution']) && 
$_POST['pos-admin-dashboard-display_store_sms_contribution'] == true) {
	
	
	if (isset($_POST['start_date']) && isset($_POST['end_date'])) {
		
		$start_date = $_POST['start_date'];
		$end_date = $_POST['end_date'];
		
		$query = "CALL `SP-admin-dashboard-display_store_sms_contribution`('$start_date', '$end_date');";
		
	} else {
		
		$query = "CALL `SP-admin-dashboard-display_store_sms_contribution`('01-01-0001', CURDATE());";
		
	}
	
	
	$query = "
	
	SELECT 
CASE 
WHEN pos_store.store_name IS NULL
THEN 'unsaved' 
ELSE pos_store.store_code
END AS 'store_code',

COUNT(*) AS 'count'

#REPLACE(sms_data_listen.phone, '+63', '0') AS 'phone' 

FROM sms_data_listen 

LEFT JOIN 
(
SELECT * FROM pos_client 
GROUP BY contact_num1
) AS pos_client 
ON REPLACE(sms_data_listen.phone, '+63', '0') = pos_client.contact_num1 

LEFT JOIN pos_store 
ON pos_client.store_id = pos_store.store_id 

#WHERE sms_data_listen.record_datetime BETWEEN '01-01-0001' AND 'CURDATE()'

GROUP BY CASE 
WHEN pos_store.store_name IS NULL
THEN 'unsaved' 
ELSE pos_store.store_name
END;
	
	
	";
	

	
	

	$result = mysqli_query($link,$query);
	
	
	while ($row = mysqli_fetch_array($result)) {
		
		
		$store_code = $row['store_code'];
		$count = $row['count'];
		
		$resp[] = array(
		'store_code' => $store_code,
		'count' => (int)$count
		);
		
		
		
	}
	
	echo json_encode($resp);
	
	
}




if(isset($_POST['pos-admin-dashboard-display_sms_count-widget']) && $_POST['pos-admin-dashboard-display_sms_count-widget'] == true) {
	
	$resp = array(
	'total_count' => '',
	'inbound_count' => '',
	'outbound_count' => '',
	'system_count' => ''
	);
	
	
	if (isset($_POST['start_date']) && isset($_POST['end_date'])) {
		
		$start_date = $_POST['start_date'];
		$end_date = $_POST['end_date'];
		
		$query = "CALL `SP-admin-dashboard-display_sms_count`('$start_date', '$end_date');";
		
	} else {
		
		$query = "CALL `SP-admin-dashboard-display_sms_count`('01-01-0001', CURDATE());";
		
	}	
	
	
	
	$result = mysqli_query($link,$query);
	
	
	
	if ($row = mysqli_fetch_array($result)) {
		
		$resp['total_count'] = number_format($row['total_count']);
		$resp['inbound_count'] = number_format($row['inbound_count']);
		$resp['outbound_count'] = number_format($row['outbound_count']);
		$resp['system_count'] = number_format($row['system_count']);
		
	}
	
	echo json_encode($resp);	
	
}




if(isset($_POST['pos-admin-dashboard-display_month_sms_count']) && $_POST['pos-admin-dashboard-display_month_sms_count'] == true) {
	
	
	if (isset($_POST['start_date']) && isset($_POST['end_date'])) {
		
		$start_date = $_POST['start_date'];
		$end_date = $_POST['end_date'];
		
		$query = "CALL `SP-admin-dashboard-display_month_sms_count`('$start_date', '$end_date');";
		
	} else {
		
		$query = "CALL `SP-admin-dashboard-display_month_sms_count`('01-01-0001', CURDATE());";
		
	}	
	
	
	$result = mysqli_query($link,$query);
	
	
	while ($row = mysqli_fetch_array($result)) {
		
		
		$month = $row['month'];
		$count = $row['count'];
		
		$resp[] = array(
		'month' => $month,
		'count' => (int)$count
		);
		
		
		
	}
	
	echo json_encode($resp);
	
}



if(isset($_POST['pos-admin-dashboard-display_client_network_contribution']) && $_POST['pos-admin-dashboard-display_client_network_contribution'] == true) {

	
	$query = "CALL `SP-admin-dashboard-display_client_sms_network`();";
	
	
	$result = mysqli_query($link,$query);
	
	
	while ($row = mysqli_fetch_array($result)) {
		
		
		$network = $row['network'];
		$count = $row['count'];
		
		$resp[] = array(
		'network' => $network,
		'count' => (int)$count
		);
		
		
		
	}
	
	echo json_encode($resp);
	
}


/*
DISPLAY SMS MONTHLY AND DAILY RATE VALUE
*/
if(isset($_POST['pos-admin-dashboard-display_sms_rate-widget']) && $_POST['pos-admin-dashboard-display_sms_rate-widget'] == true) {
	
	$resp = array(
	'rate_day' => '',
	'rate_month' => ''
	);
	
	/*
	if (isset($_POST['start_date']) && isset($_POST['end_date'])) {
		
		$start_date = $_POST['start_date'];
		$end_date = $_POST['end_date'];
		
		$query = "CALL `SP-admin-dashboard-display_sms_count`('$start_date', '$end_date');";
		
	} else {
		
		$query = "CALL `SP-admin-dashboard-display_sms_count`('01-01-0001', CURDATE());";
		
	}	
	
	*/
	
	
	$query = "CALL `SP-admin-dashboard-display_sms_rate`();";
	
	
	
	$result = mysqli_query($link,$query);
	
	
	
	if ($row = mysqli_fetch_array($result)) {
		
		$resp['rate_day'] = number_format($row['rate_day']);
		$resp['rate_month'] = number_format($row['rate_month']);
		
	}
	
	echo json_encode($resp);	
	
}



?>