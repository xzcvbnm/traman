<?php
session_start();

include '../../../../config.php';

$session_user_id = $_SESSION['user_id'];
$session_store_id = $_SESSION['store_id'];
//$session_store_surcharge = $_SESSION['surcharge'];
//$session_store_code = $_SESSION['store_code'];

$date_from = date('Y') . '-' . date('m') . '-' . '01';
$date_to = date('Y-m-d');
$month_now = date('m');
$year_now = date('y');


$trans_space_count = 22;
//$trans_space_count2 = 4;



//LOAD SPECIAL SERVICE IN OPTION ELEMENT
if(isset($_POST['pos-store-transaction-load_special_name_list-option']) && $_POST['pos-store-transaction-load_special_name_list-option'] == true) { 
	
	
	$special_type = mysqli_real_escape_string($link,$_POST['special_type']);

	$query = "CALL `SP-store-load_special_name_list_option`('$special_type');";
	
	

	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	$index = 0;
	
	//$str = '<option value="" disabled selected>Select Service..</option>';
	
	$str = '<option value="" disabled selected>Select ' . $special_type . '..</option>';
	
	while ($row = mysqli_fetch_array($result)) {
	
		$special_id = $row['special_id'];
		$name = $row['name'];
		
		
		$str .= '<option value="' . $special_id . '" >' . $special_id . ' - ' . $name . '</option>';
			
	}
	
	
	echo $str;

}


//LOAD SERVICE LIST IN OPTION ELEMENT
if(isset($_POST['pos-store-transaction-load_service_name_list-option']) && $_POST['pos-store-transaction-load_service_name_list-option'] == true) { 


	$query = "CALL `SP-store-load_service_name_list_option`();";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	$index = 0;
	
	$str = '<option value="" disabled selected>Select Service..</option>';
	
	
	while ($row = mysqli_fetch_array($result)) {
	
		$service_id = $row['service_id'];
		$code = $row['code'];
		$name = $row['name'];
		$price = $row['price'];
		$unit = $row['unit'];
		
		$str .= '<option value="' . $service_id . '" data-price="' . $price . '" data-unit="' . $unit . '">' . $code . ' - ' . $name . ' - ₱' . $price . '/' . $unit . '</option>';
			
	}
	
	
	echo $str;

}



//LOAD TRANS ITEM WITH LINK BUTTON
if(isset($_POST['pos-store-transaction-display_trans_item-table']) && $_POST['pos-store-transaction-display_trans_item-table'] == true) {
		
	$id = mysqli_real_escape_string($link,$_POST['id']);
	
	$str = "";

	$str .= "<thead>";
	
	$str .= '<tr class="no-borders">';

	$query = "CALL `SP-store-transaction-dispay_trans_item`('$id')";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	
	
	$index = 0;
	
	//add Action Header
	$str .= '<th class="hidden-xs">';
	
	$str .= '
	
	<a href="#link_add" class="edit" id="link_add_item">
		<i class="fa-plus-circle"></i>
	</a>
	
	';
	
	$str .= "</th>";
	
	while ($row = mysqli_fetch_field($result)) {
		
		if ($index >= 1) {
			
			$str .= "<th>";
			$str .= $row->name;
			$str .= "</th>";		
			
		}
		
		$index++;
	
		
	}
	
	
			
	//CLOSE THEAD
	$str .= "</tr>";
	
	//CLOSE THEAD
	$str .= "</thead>";
	

	//OPEN TBODY
	$str .= "<tbody>";
		
		
	$index = 0;
	
	$total_price = 0;
	
	while ($row = mysqli_fetch_row($result)) {
		
		$str .= "<tr>";
		
		foreach ($row as $data) {
			
			switch ($index) {
				case 0:
					$id = $data;
					break;
				case 1:
					$item_name = $data;
					break;
				case 2:
					$qty = $data;
					break;
			}
						
					
			$index++;
			
		}
		
		
		//reset index
		$index = 0;
		
		
		$str .= '
		
			<td class="action-links hidden-xs">
				
				
				<a href="#link_view" class="edit" id="link_view_item" value="' . $id . '" item_name ="' . $item_name . '" qty="' . $qty . '">
					<i class="fa-pencil"></i>
					
				</a>
				
				<a href="#link_remove" class="edit" id="link_remove_item" value="' . $id . '" >
					<i class="fa-remove"></i>
					
				</a>
				
				
						
			</td>
			
				
			
			
			<td>
				<span class="email">' . $item_name . '</span>
			</td>
			
			<td>
				<span class="email">' . $qty . '</span>
			</td>
			
			
		
		';
		
		
		$str .= "</tr>";	
		
	}
	
	
	
	
	


	//CLOSE TBODY
	$str .= "</tbody>";
	
	

	
	echo $str;
	

}



//LOAD TRANS ITEM WITH LINK BUTTONS
if(isset($_POST['pos-store-display_paid_transaction_detail_with_link_button']) && $_POST['pos-store-display_paid_transaction_detail_with_link_button'] == true) {
	
	$resp = array(
	'total_price' => '',
	'table_data' => '',
	'table_data2' => '',
	'store_name' => '',
	'store_address' => '',
	'store_contact' => '',
	'client_name' => '',
	'client_address' => '',
	'client_contact' => '',
	'due_date' => '',
	'record_datetime' => ''
	);	
	
	$id = mysqli_real_escape_string($link,$_POST['id']);
	
	$str = "";

	$str .= "<thead>";
	
	$str .= '<tr class="no-borders">';
	
	//add Action Header
	$str .= '<th class="hidden-xs">';
	
	$str .= '
	
	<a href="#link_add" class="edit" id="btn-add_line">
		<i class="fa-plus-circle"></i>
	</a>
	
	';
	
	$str .= "</th>";
	
	

	//DATE_FORMAT(pos_trans_master.due_date, '%m/%d/%Y') AS 'due_date',
	
	$query = "SELECT
trans_detail_id,

DATE_FORMAT(pos_trans_master.due_date, '%Y-%m-%d'),
DATE_FORMAT(pos_trans_master.record_datetime , '%b %d, %Y') AS 'record_datetime',
pos_store.store_name,
CONCAT(pos_store.address, ' ', pos_store.address_city) AS 'store_address',
CONCAT(pos_store.num_tel, '/', pos_store.num_cell) AS 'store_contact',
CONCAT(pos_client.first_name, ' ', pos_client.last_name) AS 'client_name',
CONCAT(pos_client.address_city) AS 'client_address',
pos_client.contact_num1 AS 'client_contact',
pos_service_and_price.name AS 'Service',
CONCAT(pos_trans_detail.price, '/', pos_trans_detail.unit) AS 'Price/Unit',
TRIM(TRAILING '.' FROM TRIM(TRAILING '0' from pos_trans_detail.unit_count)) AS 'Weight/Pcs',
pos_trans_detail.unit_price AS 'Price'
FROM pos_trans_master 

LEFT JOIN pos_trans_detail
ON pos_trans_master.trans_master_id = pos_trans_detail.trans_master_id

LEFT JOIN pos_service_and_price 
ON pos_trans_detail.service_id = pos_service_and_price.service_id 




LEFT JOIN pos_store 
ON pos_trans_master.store_id = pos_store.store_id 

LEFT JOIN pos_client 
ON pos_trans_master.client_id = pos_client.client_id

WHERE pos_trans_master.trans_master_id = '$id' 
AND pos_trans_detail.line_void = 'NO'";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	
	
	//TOTAL ITEM QUANTITY COUNT
	$total_item_qty = 0;
	
	$index = 0;
	
	
	
	//add Action Header
	//$str .= '<th class="hidden-xs">';
	//$str .= "";
	//$str .= "</th>";
	
	while ($row = mysqli_fetch_field($result)) {
		
		if ($index >= 9) {
			
			$str .= "<th>";
			$str .= $row->name;
			$str .= "</th>";		
			
		}
		
		$index++;
	
		
	}
	
	
			
	//CLOSE THEAD
	$str .= "</tr>";
	
	//CLOSE THEAD
	$str .= "</thead>";
	

	//OPEN TBODY
	$str .= "<tbody>";
		
		
	$index = 0;
	
	$total_price = 0;
	
	$service_line_count = 0;
	
	while ($row = mysqli_fetch_row($result)) {
		
		$str .= "<tr>";
		
		foreach ($row as $data) {
			
			switch ($index) {		
				case 0:
					$id = $data;
					break;
				case 1:
					$due_date = $data;
					break;
				case 2:
					$record_datetime = $data;
					break;
				case 3:
					$store_name = $data;
					break;	
				case 4:
					$store_address = $data;
					break;
				case 5:
					$store_contact = $data;
					break;
				case 6:
					$client_name = $data;
					break;
				case 7:
					$client_address = $data;
					break;
				case 8:
					$client_contact = $data;
					break;
				case 9:
					$service = $data;
					break;
				case 10:
					$unit_price = $data;
					break;
				case 11:
					$unit_count = $data;
					break;
				case 12:
					$price = $data;
					break;
			}
			
			
			$index++;
			
			
		}
		
		
		//reset index
		$index = 0;
		
		
		$service_line_count++;
		
		//add total price
		$total_price = ($total_price + $price);
		
		
		
		$str .= '
			
			<td class="action-links hidden-xs">
				
				<a href="#trans_invoice" class="edit" id="link_remove" value="' . $id . '" >
					<i class="fa-remove"></i>
					
				</a>
						
			</td>
			
			<td>
				<span class="email">' . $service . '</span>
			</td>
			
			<td>
				<span class="email">' . $unit_price . '</span>
			</td>
			
			<td>
				<span class="email">' . $unit_count . '</span>
			</td>
			
			<td>
				<span class="email">' . $price . '</span>
			</td>
			
		
		';
		
		
		$str .= "</tr>";	
		
	}
	
	
	
	
	$blank_row = $trans_space_count - $service_line_count;

	while ($blank_row > 0) {


		$str .= "<tr class='tr_blank_space'>";

		$str .= '



			<td>
				<span class="email">' . '&nbsp;' . '</span>
			</td>



			<td>
				<span class="email">' . '' . '</span>
			</td>

			<td>
				<span class="email">' . '' . '</span>
			</td>


		';


		$str .= "</tr>";


		$blank_row--;


	}
	

	//CLOSE TBODY
	$str .= "</tbody>";
	
	
	$resp['total_price'] = $total_price;
	$resp['table_data'] = $str;
	$resp['table_data2'] = $str;
	
	$resp['store_name'] = $store_name;
	$resp['store_address'] = $store_address;
	$resp['store_contact'] = $store_contact;
	$resp['client_name'] = $client_name;
	$resp['client_address'] = $client_address;
	$resp['client_contact'] = $client_contact;
	$resp['record_datetime'] = $record_datetime;
	$resp['due_date'] = $due_date;
	
	
	
	echo json_encode($resp);
	

}



if(isset($_POST['pos-store-transaction-display_trans_detail-table']) && $_POST['pos-store-transaction-display_trans_detail-table'] == true) {
	
	$resp = array(
	'total_price' => '',
	'table_data' => '',
	'table_data2' => ''
	);	
	
	$id = mysqli_real_escape_string($link,$_POST['id']);
	
	$str = "";

	$str .= "<thead>";
	
	$str .= '<tr class="no-borders">';

	$query = "SELECT 
			trans_detail_id,
			pos_service_and_price.name AS 'Service',
			CONCAT(pos_trans_detail.price, '/', pos_trans_detail.unit) AS 'Price/Unit',
			TRIM(TRAILING '.' FROM TRIM(TRAILING '0' from pos_trans_detail.unit_count)) AS 'Weight/Pcs',
			pos_trans_detail.unit_price AS 'Price'
			FROM pos_trans_detail 
			
			LEFT JOIN pos_service_and_price 
			ON pos_trans_detail.service_id = pos_service_and_price.service_id 
			
			WHERE pos_trans_detail.trans_master_id = '$id' 
			AND pos_trans_detail.line_void = 'NO'";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	
	$total_item_qty = 0;
	
	$index = 0;
	
	//add Action Header
	$str .= '<th class="hidden-xs">';
	
	$str .= '
	
	<a href="#link_add" class="edit" id="btn-add_line">
		<i class="fa-plus-circle"></i>
	</a>
	
	';
	
	$str .= "</th>";
	
	while ($row = mysqli_fetch_field($result)) {
		
		if ($index >= 1) {
			
			$str .= "<th>";
			$str .= $row->name;
			$str .= "</th>";		
			
		}
		
		$index++;
	
		
	}
	
	
			
	//CLOSE THEAD
	$str .= "</tr>";
	
	//CLOSE THEAD
	$str .= "</thead>";
	

	//OPEN TBODY
	$str .= "<tbody>";
		
		
	$index = 0;
	
	$total_price = 0;
	
	while ($row = mysqli_fetch_row($result)) {
		
		$str .= "<tr>";
		
		foreach ($row as $data) {
			
			switch ($index) {
				case 0:
					$id = $data;
					break;
				case 1:
					$service = $data;
					break;
				case 2:
					$unit_price = $data;
					break;
				case 3:
					$unit_count = $data;
					break;
				case 4:
					$price = $data;
					break;
			}
						
					
			$index++;
			
		}
		
		
		//reset index
		$index = 0;
		
		
		//add total price
		$total_price = ($total_price + $price);
		
		
		
		$str .= '
		
			<td class="action-links hidden-xs">
				
				<a href="#" class="edit" id="link_remove" value="' . $id . '" >
					<i class="fa-remove"></i>
					
				</a>
						
			</td>
			
				
			
			
			<td>
				<span class="email">' . $service . '</span>
			</td>
			
			<td>
				<span class="email">' . $unit_price . '</span>
			</td>
			
			<td>
				<span class="email">' . $unit_count . '</span>
			</td>
			
			<td>
				<span class="email">' . $price . '</span>
			</td>
			
			
		
		';
		
		
		$str .= "</tr>";	
		
	}
	
	
	
	
	


	//CLOSE TBODY
	$str .= "</tbody>";
	
	
	$resp['total_price'] = $total_price;
	$resp['table_data'] = $str;
	$resp['table_data2'] = $str;
	
	
	
	echo json_encode($resp);
	

}


//load trans special details WITH link buttons
if(isset($_POST['pos-store-transaction-display_trans_special_with_link_buttons-table']) && $_POST['pos-store-transaction-display_trans_special_with_link_buttons-table'] == true) {
	
	$resp = array(
	'special_total' => '',
	'table_data' => '',
	'table_data2' => ''
	);
	
	
	$special_total = 0;
	
	$id = mysqli_real_escape_string($link,$_POST['id']);
	$sub_total = mysqli_real_escape_string($link,$_POST['sub_total']);
	
	$str = "";
	
	

	$str .= "<thead>";
	
	$str .= '<tr class="no-borders">';

	$query = "SELECT 
			
            
            pos_store.surcharge_store,
            pos_store.surcharge_pickup,
			pos_trans_special.trans_special_id,
			pos_special.value,
			pos_special.special_type,
			pos_special.value_type,
			pos_special.name AS 'Special',
			'' AS 'Price'
			
			FROM pos_trans_special 
            
            
            
			
			LEFT JOIN pos_special 
			ON pos_trans_special.special_id = pos_special.special_id 
            
            LEFT JOIN pos_trans_master
            ON pos_trans_special.trans_master_id = pos_trans_special.trans_master_id
            
            
            LEFT JOIN pos_store 
            ON pos_trans_master.store_id = pos_store.store_id
			
			WHERE pos_trans_special.trans_master_id = '$id' 
			AND pos_trans_special.special_void = 'NO' 
            
            GROUP BY pos_trans_special.trans_special_id;";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	$index = 0;
	
	
	
	//add Action Header
	$str .= '<th class="hidden-xs">';
	
	$str .= '
	
	<a href="#link_add" class="edit" id="btn-add_special">
		<i class="fa-plus-circle"></i>
	</a>
	
	';
	
	$str .= "</th>";
	
	while ($row = mysqli_fetch_field($result)) {
		
		if ($index >= 7) {
			
			$str .= "<th>";
			$str .= $row->name;
			$str .= "</th>";		
			
		}
		
		$index++;
	
		
	}
	
	
			
	//CLOSE table row
	$str .= "</tr>";
	
	//CLOSE THEAD
	$str .= "</thead>";
	

	//OPEN TBODY
	$str .= "<tbody>";
		
		
	$index = 0;
	
	$total_price = 0;
	
	
	while ($row = mysqli_fetch_array($result)) {
		
		$str .= "<tr>";
		
		
		$surcharge_store = $row['surcharge_store'];
		$surcharge_pickup = $row['surcharge_pickup'];
		
		$id = $row['trans_special_id'];
		$value = $row['value'];
		$special_type = $row['special_type'];
		$value_type = $row['value_type'];
		$name = $row['Special'];
		$special = $row['Special'];
		
		
		if ($special_type == "DISCOUNT") {
			
			if ($value_type == "PERCENTAGE") {
				
				$percent_value = ($value / 100);
				
				$special_price = (-1 * abs(($sub_total * $percent_value)));
				
				
			} else {
				
				$special_price = (-1 * $value);
				
			}
			
			
		} else {
			
			if ($value_type == "PERCENTAGE") {
				
				$percent_value = ($value / 100);
				
				$special_price = ($sub_total * $percent_value);
				
			
			} elseif ($value_type == "SURCHARGE") {
				
				if ($special == "STORE SURCHARGE") {
					
					
					if ($session_surcharge_store > $sub_total) {

						$special_price = $session_surcharge_store - $sub_total;
	
	
					} else {
	
						$special_price = 0;
	
					}
					
					
					
				} else {
					
					
					if ($session_surcharge_pickup > $sub_total) {

						$special_price = $session_surcharge_pickup - $sub_total;
	
	
					} else {
	
						$special_price = 0;
	
	
					}
					
					
					
				}
				
				
			} else {
				
				$special_price = ($value);
				
			}
						
		}
				
		$special_total = ($special_total + $special_price);
		
		
		$str .= '
			
			
			<td class="action-links hidden-xs">
				
				<a href="#trans_invoice" class="edit" id="link_remove_special" value="' . $id . '" >
					<i class="fa-remove"></i>
					
				</a>
						
			</td>
			
			
			<td>
				<span class="email">' . $name . '</span>
			</td>
			
			<td>
				<span class="email">' . number_format($special_price,2) . '</span>
			</td>
		
		
		';
		
		
		
		$str .= "</tr>";
		
	}
	
	
	$row_count = mysqli_num_rows($result);
	
	if ($row_count == 0) {
			
		$str .= '
		<tr>
			<td colspan="5">
				<span class="email">No Special Service/Discount</span>
			</td>
		</tr>
		
		';
		
	}
	
	


	//CLOSE TBODY
	$str .= "</tbody>";
	
	
	$resp['special_total'] = $special_total;
	$resp['table_data'] = $str;
	$resp['table_data2'] = $str;
	
	
	
	echo json_encode($resp);
	
	
	
}



/*
DISPLAY ALL SUSPEND TRANSACTION ON TABLE
*/
if(isset($_POST['pos-store-display_suspend_transaction-table']) && $_POST['pos-store-display_suspend_transaction-table'] == true) {
	
	
	
	if(isset($_POST['start_date']) && isset($_POST['end_date'])) {
		
		
		$start_date = $_POST['start_date'];
		$end_date = $_POST['end_date'];
		
		
		$query = "CALL `SP-store-display_suspend_transaction_all`('$session_user_id', '$start_date', '$end_date');";
		
	} else {
		
		$query = "CALL `SP-store-display_suspend_transaction_all`('$session_user_id', '$date_from', '$date_to');";
		
	}
	
	
	
	
	
	$str = "";

	$str .= "<thead>";

	

	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	$index = 0;
	
	while ($row = mysqli_fetch_field($result)) {
		
		//if ($index >= 1) {
			
			$str .= "<th>";
			$str .= $row->name;
			$str .= "</th>";		
			
		//}
		
		//$index++;
	
		
	}
	
	//add Action Header
	$str .= "<th>";
	$str .= "Actions";
	$str .= "</th>";
			
	
	//CLOSE THEAD
	$str .= "</thead>";
	

	//OPEN TBODY
	$str .= "<tbody>";
		
		
	$index = 0;
	
	while ($row = mysqli_fetch_row($result)) {
		
		$str .= "<tr>";
		
		foreach ($row as $data) {
			
			switch ($index) {
				case 0:
					$id = $data;
					break;
				case 1:
					$client_name = $data;
					break;
				case 2:
					$trans_date = $data;
					break;
			}
										
			$index++;
			
		}
		
		
		//reset index
		$index = 0;
		
	
	
		/*
		<td class="user-name">
			<a href="#" class="name">' . $store_name . '</a>
			
			<span>' . $address_city . '</span>
			
		</td>
		
		*/
		
		$str .= '	
		
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $id . '</span>
			</td>
		
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $client_name . '</span>
			</td>
			
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $trans_date . '</span>
			</td>
		
			
			<td class="action-links">
				<a href="#invoice" class="edit" id="link_load_detail" value="' . $id . '">
					<i class="fa-folder-open-o"></i>
					Load Details
				</a>
			
				<!--
				<a href="#" class="delete" id="link_status_history" value="' . $id . '">
					<i class="fa-history"></i>
					View Status History
				</a>
				-->
			</td>
			
		
		';
		
		
		$str .= "</tr>";	
		
	}


	//CLOSE TBODY
	$str .= "</tbody>";
	
	
	echo $str;
	

}




/*
DISPLAY ALL PENDING VOID TRANSACTION ON TABLE
*/

if(isset($_POST['pos-store-display_pending_void_transaction-table']) && $_POST['pos-store-display_pending_void_transaction-table'] == true) {
	
	
	if(isset($_POST['start_date']) && isset($_POST['end_date'])) {
		
		
		$start_date = $_POST['start_date'];
		$end_date = $_POST['end_date'];
		
		
		$query = "CALL `SP-store-display_pending_void_transaction_all`('$session_user_id', '$start_date', '$end_date');";
		
	} else {
		
		$query = "CALL `SP-store-display_pending_void_transaction_all`('$session_user_id', '$date_from', '$date_to');";
		
	}
	
	$str = "";

	$str .= "<thead>";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	$index = 0;
	
	while ($row = mysqli_fetch_field($result)) {
		
		//if ($index >= 1) {
			
			$str .= "<th>";
			$str .= $row->name;
			$str .= "</th>";		
			
		//}
		
		//$index++;
	
		
	}
	
	//add Action Header
	$str .= "<th>";
	$str .= "Actions";
	$str .= "</th>";
			
	
	//CLOSE THEAD
	$str .= "</thead>";
	

	//OPEN TBODY
	$str .= "<tbody>";
		
		
	$index = 0;
	
	while ($row = mysqli_fetch_row($result)) {
		
		$str .= "<tr>";
		
		foreach ($row as $data) {
			
			switch ($index) {
				case 0:
					$id = $data;
					break;
				case 1:
					$client_name = $data;
					break;
				case 2:
					$trans_date = $data;
					break;
			}
										
			$index++;
			
		}
		
		
		//reset index
		$index = 0;
		
	
	
		/*
		<td class="user-name">
			<a href="#" class="name">' . $store_name . '</a>
			
			<span>' . $address_city . '</span>
			
		</td>
		
		*/
		
		$str .= '	
		
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $id . '</span>
			</td>
		
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $client_name . '</span>
			</td>
			
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $trans_date . '</span>
			</td>
		
			
			<td class="action-links">
				<a href="#trans_invoice" class="edit" id="link_load_detail" value="' . $id . '">
					<i class="fa-folder-open-o"></i>
					Load Details
				</a>
			
				
				<!--
				<a href="#" class="delete" id="link_status_history" value="' . $id . '">
					<i class="fa-history"></i>
					View Status History
				</a>
				-->
				
			</td>
			
		
		';
		
		
		$str .= "</tr>";	
		
	}


	//CLOSE TBODY
	$str .= "</tbody>";
	
	
	echo $str;
	

} 



/*
BUTTON FINALIZE - PAID
- CHANGE TRANSACTION STATUS FROM 'Unpaid' to 'Paid' Status
*/
if(isset($_POST['pos-store-transaction-finalize_transaction']) && $_POST['pos-store-transaction-finalize_transaction'] == true) {
	
	$resp = array(
	'status' => '',
	'message' => ''
	);
	
	$trans_master_id = mysqli_real_escape_string($link,$_POST['id']);
	$status = mysqli_real_escape_string($link,$_POST['status']);
	
	$due_date = mysqli_real_escape_string($link,$_POST['due_date']);
	
	$total_grand = mysqli_real_escape_string($link,$_POST['total_grand']);
	
	
	$query = "CALL `SP-store-transaction-finalize_transaction`('$trans_master_id', '$session_store_id', '$status', '$due_date', '$total_grand');";

	$result = mysqli_query($link,$query);
	
	if (!$result) {
		
		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);
		
	
	} else {
		
		$resp['status'] = 'success';
	
	}
	
	
	echo json_encode($resp);


}



if(isset($_POST['pos-admin-display_unpaid_transaction-table']) && $_POST['pos-admin-display_unpaid_transaction-table'] == true) {
	
	
	
	if(isset($_POST['start_date']) && isset($_POST['end_date'])) {
		
		
		$start_date = $_POST['start_date'];
		$end_date = $_POST['end_date'];
		
		
		$query = "CALL `SP-admin-display_unpaid_transaction_all`('$start_date', '$end_date');";
		
	} else {
		
		$query = "CALL `SP-admin-display_unpaid_transaction_all`('$date_from', '$date_to');";
		
	}
	
	
	
	
	
	$str = "";

	$str .= "<thead>";

	

	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	$index = 0;
	
	while ($row = mysqli_fetch_field($result)) {
		
		if ($index >= 1) {
			
			$str .= "<th>";
			$str .= $row->name;
			$str .= "</th>";		
			
		}
		
		$index++;
	
		
	}
	
	//add Action Header
	$str .= "<th>";
	$str .= "Actions";
	$str .= "</th>";
			
	
	//CLOSE THEAD
	$str .= "</thead>";
	

	//OPEN TBODY
	$str .= "<tbody>";
		
		
	$index = 0;
	
	while ($row = mysqli_fetch_row($result)) {
		
		$str .= "<tr>";
		
		foreach ($row as $data) {
			
			switch ($index) {
				case 0:
					$id = $data;
					break;
				case 1:
					$local_code = $data;
					break;
				case 2:
					$store_name = $data;
					break;
				case 3:
					$client_name = $data;
					break;
				case 4:
					$client_address = $data;
					break;
				case 5:
					$contact = $data;
					break;
				case 6:
					$trans_date = $data;
					break;
			}
										
			$index++;
			
		}
		
		
		//reset index
		$index = 0;
		
	
	
		/*
		<td class="user-name">
			<a href="#" class="name">' . $store_name . '</a>
			
			<span>' . $address_city . '</span>
			
		</td>
		
		*/
		
		$str .= '	
		
		
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $local_code . '</span>
			</td>
			
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $store_name . '</span>
			</td>
			
		
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $client_name . '</span>
			</td>
			
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $client_address . '</span>
			</td>
			
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $contact . '</span>
			</td>
			
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $trans_date . '</span>
			</td>
		
			
			<td class="action-links">
				<a href="#invoice" class="edit" id="link_load_detail" value="' . $id . '" local_code="' . $local_code . '" central_code="' . '' . '">
					<i class="fa-folder-open-o"></i>
					Load Details
				</a>
			
				<!--
				<a href="#invoice" class="delete" id="link_status_history" value="' . $id . '">
					<i class="fa-history"></i>
					View Status History
				</a>
				-->
			</td>
			
		
		';
		
		
		$str .= "</tr>";	
		
	}


	//CLOSE TBODY
	$str .= "</tbody>";
	
	
	echo $str;
	

} 





if(isset($_POST['pos-admin-display_paid_transaction-table']) && $_POST['pos-admin-display_paid_transaction-table'] == true) {
	
	
	if(isset($_POST['start_date']) && isset($_POST['end_date'])) {
		
		
		$start_date = $_POST['start_date'];
		$end_date = $_POST['end_date'];
		
		
		$query = "CALL `SP-admin-display_paid_transaction_all`('$start_date', '$end_date');";
		
	} else {
		
		$query = "CALL `SP-admin-display_paid_transaction_all`('$date_from', '$date_to');";
		
	}
	
	
	$str = "";

	$str .= "<thead>";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	$index = 0;
	
	while ($row = mysqli_fetch_field($result)) {
		
		if ($index >= 1) {
			
			$str .= "<th>";
			$str .= $row->name;
			$str .= "</th>";		
			
		}
		
		$index++;
	
		
	}
	
	//add Action Header
	$str .= "<th>";
	$str .= "Actions";
	$str .= "</th>";
			
	
	//CLOSE THEAD
	$str .= "</thead>";
	

	//OPEN TBODY
	$str .= "<tbody>";
		
		
	$index = 0;
	
	while ($row = mysqli_fetch_row($result)) {
		
		$str .= "<tr>";
		
		foreach ($row as $data) {
			
			switch ($index) {
				case 0:
					$id = $data;
					break;
				case 1:
					$central_code = $data;
					break;
				case 2:
					$local_code = $data;
					break;
				case 3:
					$store_name = $data;
					break;
				case 4:
					$cashier_name = $data;
					break;
				case 5:
					$client_name = $data;
					break;
				case 6:
					$trans_date = $data;
					break;
			}
										
			$index++;
			
		}
		
		
		//reset index
		$index = 0;
		
	
	
		/*
		<td class="user-name">
			<a href="#" class="name">' . $store_name . '</a>
			
			<span>' . $address_city . '</span>
			
		</td>
		
		*/
		
		$str .= '	
		
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $central_code . '</span>
			</td>
		
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $local_code . '</span>
			</td>
			
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $store_name . '</span>
			</td>
			
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $cashier_name . '</span>
			</td>
		
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $client_name . '</span>
			</td>
			
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $trans_date . '</span>
			</td>
		
			
			<td class="action-links">
				<a href="#invoice" class="edit" id="link_load_detail" value="' . $id . '" central_code="' . $central_code . '" local_code="' . $local_code . '">
					<i class="fa-folder-open-o"></i>
					Load Details
				</a>
			
				<!--
				<a href="#invoice" class="delete" id="link_status_history" value="' . $id . '">
					<i class="fa-history"></i>
					View Status History
				</a>
				-->
			</td>
			
		
		';
		
		
		$str .= "</tr>";	
		
	}


	//CLOSE TBODY
	$str .= "</tbody>";
	
	
	echo $str;
	

} 




//QR DETAIL
if(isset($_POST['pos-qr-detail']) && $_POST['pos-qr-detail'] == true) {

    $resp = array(
        'total_price' => '',
        'table_data' => '',
        'table_data2' => '',
        'store_code' => '',
        'store_name' => '',
        'store_address' => '',
        'store_num_tel' => '',
        'store_num_cell' => '',
        'client_name' => '',
        'client_address' => '',
        'client_contact1' => '',
        'client_contact2' => '',
        'due_date' => '',
        'record_datetime' => '',
        'email' => '',
        'claim_by' => '',
        'claim_datetime' => '',
        'store_code' => '',
        'due_date1' => '',
        'client_credit' => '',
        'local_code' => '',
        'total_special_price' => '',
        'total_discount_price' => '',
        'total_service_fee_price' => '',
        'total_vat_price' => '',
        'total_overall' => ''
    );

    $id = mysqli_real_escape_string($link,$_POST['id']);

    $trans_master_id = mysqli_real_escape_string($link,$_POST['id']);

    $str = "";

    //DATE_FORMAT(pos_trans_master.due_date, '%m/%d/%Y') AS 'due_date',

    $query = "SELECT
trans_detail_id,

DATE_FORMAT(pos_trans_master.due_date, '%Y-%m-%d'),
DATE_FORMAT(pos_trans_master.record_datetime , '%b %d, %Y') AS 'record_datetime',
pos_store.store_name,
CONCAT(pos_store.address, ' ', pos_store.address_city) AS 'store_address',
pos_store.num_tel,
pos_store.num_cell,
CONCAT(pos_client.first_name, ' ', pos_client.last_name) AS 'client_name',
CONCAT(pos_client.address, ' ', pos_client.address_city) AS 'client_address',
pos_client.contact_num1,
pos_client.contact_num2,
pos_client.email,
pos_trans_master.claim_by,
DATE_FORMAT(pos_trans_master.claim_datetime,'%b %d %Y %h:%i %p') AS 'claim_datetime',
pos_store.store_code,
DATE_FORMAT(pos_trans_master.due_date, '%b %d %Y') AS 'due_date1', 
CASE WHEN credit.credit IS NULL THEN 0 ELSE credit.credit END AS 'credit',
pos_trans_master.local_code,
CONCAT(pos_main_service.name, ':', pos_category_service.name, ' - ', pos_service_and_price.name) AS 'Service Instruction',
TRIM(TRAILING '.' FROM TRIM(TRAILING '0' from pos_trans_detail.unit_count)) AS '# of kl(s)',
pos_trans_detail.unit_price AS 'Amount'
FROM pos_trans_master

LEFT JOIN pos_trans_detail
ON pos_trans_master.trans_master_id = pos_trans_detail.trans_master_id

LEFT JOIN pos_service_and_price
ON pos_trans_detail.service_id = pos_service_and_price.service_id

LEFT JOIN pos_category_service 
ON pos_service_and_price.category_id = pos_category_service.category_id

LEFT JOIN pos_main_service 
ON pos_category_service.service_main_id = pos_main_service.service_main_id


LEFT JOIN pos_store
ON pos_trans_master.store_id = pos_store.store_id

LEFT JOIN pos_client
ON pos_trans_master.client_id = pos_client.client_id 

LEFT JOIN 
(

SELECT 
client_id,
SUM(amount) AS 'credit'
FROM pos_client_credit 
GROUP BY client_id

) AS credit
ON pos_trans_master.client_id = credit.client_id



WHERE pos_trans_master.trans_master_id = '$id'
AND pos_trans_detail.line_void = 'NO'";

    $result = mysqli_query($link,$query)or die(mysqli_error($link));



    //TOTAL ITEM QUANTITY COUNT
    $total_item_qty = 0;

    $index = 0;



    //add Action Header
    //$str .= '<th class="hidden-xs">';
    //$str .= "";
    //$str .= "</th>";

    while ($row = mysqli_fetch_field($result)) {

        if ($index >= 18) {
            $str .= $row->name;
            $str .= "\t";

        }

        $index++;


    }


    $str .= "\n";


    $index = 0;

    $total_price = 0;


    //for inserting blank rows
    $service_line_count = 0;


    while ($row = mysqli_fetch_row($result)) {

        foreach ($row as $data) {

            switch ($index) {
                case 0:
                    $id = $data;
                    break;
                case 1:
                    $due_date = $data;
                    break;
                case 2:
                    $record_datetime = $data;
                    break;
                case 3:
                    $store_name = $data;
                    break;
                case 4:
                    $store_address = $data;
                    break;
                case 5:
                    $store_num_tel = $data;
                    break;
                case 6:
                    $store_num_cell = $data;
                    break;
                case 7:
                    $client_name = $data;
                    break;
                case 8:
                    $client_address = $data;
                    break;
                case 9:
                    $client_contact1 = $data;
                    break;
                case 10:
                    $client_contact2 = $data;
                    break;
                case 11:
                    $email = $data;
                    break;
                case 12:
                    $claim_by = $data;
                    break;
                case 13:
                    $claim_datetime = $data;
                    break;
                case 14:
                    $store_code = $data;
                    break;
                case 15:
                    $due_date1 = $data;
                    break;
                case 16:
                    $credit = $data;
                    break;
                case 17:
                    $local_code = $data;
                    break;
                case 18:
                    $service = $data;
                    break;
                case 19:
                    $unit_count = $data;
                    break;
                case 20:
                    $price = $data;
                    break;
            }


            $index++;


        }


        //reset index
        $index = 0;


        //add total price
        $total_price = ($total_price + $price);


        //increament service line counter
        //$service_line_count++;



$str .=" 
" . $service . "\t" . $unit_count . "kl\t" . $price . "phP\t";

        $str .= "\n\n";

    }




    //========================================================================================================

    $sub_total = $total_price;

    $query = " 
			           
            SELECT
			pos_store.surcharge_store,
            pos_store.surcharge_pickup,
			pos_trans_special.trans_special_id,
			pos_special.value,
			pos_special.special_type,
			pos_special.value_type,
			pos_special.name AS 'special'
			FROM pos_trans_special

			LEFT JOIN pos_special
			ON pos_trans_special.special_id = pos_special.special_id 
                      
            LEFT JOIN pos_trans_master
            ON pos_trans_special.trans_master_id = pos_trans_master.trans_master_id
            
            LEFT JOIN pos_store 
            ON pos_trans_master.store_id = pos_store.store_id

			WHERE pos_trans_special.trans_master_id = '$trans_master_id'
			AND pos_trans_special.special_void = 'NO' 
			
			ORDER BY pos_special.priority ASC
			
			;";

    $result = mysqli_query($link,$query)or die(mysqli_error($link));


    $total_special_price = 0;
    $total_discount_price = 0;
    $total_service_fee_price = 0;
    $total_vat_price = 0;

    while ($row = mysqli_fetch_array($result)) {


        //increament service line counter
        $service_line_count++;

        $session_surcharge_store = $row['surcharge_store'];
        $session_surcharge_pickup = $row['surcharge_pickup'];
        $value = $row['value'];
        $special_type = $row['special_type'];
        $value_type = $row['value_type'];
        $special = $row['special'];


        if ($special_type == "MARK-UP") {

            if ($value_type == "PERCENTAGE") {

                $percent_value = ($value / 100);

                $special_price = ($sub_total * $percent_value);


            }  else {

                $special_price = ($value);

            }


            //ADD ALL SPECIAL PRICE
            $total_special_price = $special_price + $total_special_price;


        } elseif ($special_type == "STORE SERVICE FEE") {


            if ($session_surcharge_store > $sub_total) {

                $special_price = $session_surcharge_store - ($sub_total + $total_special_price);


            } else {

                $special_price = 0;

            }

            //SERVICE FEE -> STORE
            $total_service_fee_price = $special_price;

        }  elseif ($special_type == "PICK-UP SERVICE FEE") {

            if ($session_surcharge_pickup > $sub_total) {

                $special_price = $session_surcharge_pickup - $sub_total;


            } else {

                $special_price = 0;


            }

            //SERVICE FEE -> PICK-UP
            $total_service_fee_price = $special_price;


        }  elseif ($special_type == "DISCOUNT") {


            if ($value_type == "PERCENTAGE") {

                $percent_value = ($value / 100);

                $special_price = (-1 * abs(( ($sub_total + $total_special_price + $total_service_fee_price) * $percent_value)));


            } else {

                $special_price = (-1 * $value);

            }


            //ADD ALL DISCOUNT PRICE
            $total_discount_price = $special_price + $total_discount_price;




        } elseif ($special_type == "VAT") {

            $special_price = ($total_price + $total_special_price + $total_discount_price + $total_service_fee_price) * ($value / 100);

            $total_vat_price = $special_price;

        }





$str .= "" . $special . "\t" . '**' . "\t" . number_format($special_price,2) . "\t";

        $str .= "\n";

    }



    //========================================================================================================

    //CLOSE TBODY


    $resp['total_price'] = $total_price;


    $resp['store_name'] = $store_name;
    $resp['store_code'] = $store_code;
    $resp['store_address'] = $store_address;
    $resp['store_num_tel'] = $store_num_tel;
    $resp['store_num_cell'] = $store_num_cell;
    $resp['client_name'] = $client_name;
    $resp['client_address'] = $client_address;
    $resp['client_contact1'] = $client_contact1;
    $resp['client_contact2'] = $client_contact2;
    $resp['record_datetime'] = $record_datetime;
    $resp['due_date'] = $due_date;
    $resp['due_date1'] = $due_date1;

    $resp['email'] = $email;

    $resp['claim_by'] = $claim_by;
    $resp['claim_datetime'] = $claim_datetime;

    $resp['store_code'] = $store_code;

    $resp['client_credit'] = $credit;

    $resp['local_code'] = $local_code;


    $resp['total_special_price'] = $total_special_price;
    $resp['total_discount_price'] = $total_discount_price;
    $resp['total_service_fee_price'] = $total_service_fee_price;

    $resp['total_vat_price'] = $total_vat_price;

    $total_overall = $total_special_price + $total_discount_price + $total_service_fee_price + $sub_total + $total_vat_price;

    $resp['total_overall'] = $total_overall;

    $str .= "--------------------\n";
    $str .= "TOTAL: " . "" . "" . '**' . "" . number_format($total_overall,2) . "phP";
    $str .= "\n--------------------\n";

    $resp['table_data'] = $str;
    $resp['table_data2'] = $str;



    echo json_encode($resp);


}



if(isset($_POST['pos-store-display_paid_transaction_detail']) && $_POST['pos-store-display_paid_transaction_detail'] == true) {

	$resp = array(
	'total_price' => '',
	'table_data' => '',
	'table_data2' => '',
	'store_code' => '',
	'store_name' => '',
	'store_address' => '',
	'store_num_tel' => '',
	'store_num_cell' => '',
	'client_name' => '',
	'client_address' => '',
	'client_contact1' => '',
	'client_contact2' => '',
	'due_date' => '',
	'record_datetime' => '',
	'email' => '',
	'claim_by' => '',
	'claim_datetime' => '',
	'store_code' => '',
	'due_date1' => '',
	'client_credit' => '',
	'local_code' => '',
	'total_special_price' => '',
	'total_discount_price' => '',
	'total_service_fee_price' => '',
	'total_vat_price' => '',
	'total_overall' => ''
	);

	$id = mysqli_real_escape_string($link,$_POST['id']);
	
	$trans_master_id = mysqli_real_escape_string($link,$_POST['id']);

	$str = "";

	$str .= "<thead>";

	$str .= '<tr class="no-borders">';

	//DATE_FORMAT(pos_trans_master.due_date, '%m/%d/%Y') AS 'due_date',

	$query = "SELECT
trans_detail_id,

DATE_FORMAT(pos_trans_master.due_date, '%Y-%m-%d'),
DATE_FORMAT(pos_trans_master.record_datetime , '%b %d, %Y') AS 'record_datetime',
pos_store.store_name,
CONCAT(pos_store.address, ' ', pos_store.address_city) AS 'store_address',
pos_store.num_tel,
pos_store.num_cell,
CONCAT(pos_client.first_name, ' ', pos_client.last_name) AS 'client_name',
CONCAT(pos_client.address, ' ', pos_client.address_city) AS 'client_address',
pos_client.contact_num1,
pos_client.contact_num2,
pos_client.email,
pos_trans_master.claim_by,
DATE_FORMAT(pos_trans_master.claim_datetime,'%b %d %Y %h:%i %p') AS 'claim_datetime',
pos_store.store_code,
DATE_FORMAT(pos_trans_master.due_date, '%b %d %Y') AS 'due_date1', 
CASE WHEN credit.credit IS NULL THEN 0 ELSE credit.credit END AS 'credit',
pos_trans_master.local_code,
CONCAT(pos_main_service.name, ':', pos_category_service.name, ' - ', pos_service_and_price.name) AS 'Service Instruction',
TRIM(TRAILING '.' FROM TRIM(TRAILING '0' from pos_trans_detail.unit_count)) AS '# of kl(s)',
pos_trans_detail.unit_price AS 'Amount'
FROM pos_trans_master

LEFT JOIN pos_trans_detail
ON pos_trans_master.trans_master_id = pos_trans_detail.trans_master_id

LEFT JOIN pos_service_and_price
ON pos_trans_detail.service_id = pos_service_and_price.service_id

LEFT JOIN pos_category_service 
ON pos_service_and_price.category_id = pos_category_service.category_id

LEFT JOIN pos_main_service 
ON pos_category_service.service_main_id = pos_main_service.service_main_id


LEFT JOIN pos_store
ON pos_trans_master.store_id = pos_store.store_id

LEFT JOIN pos_client
ON pos_trans_master.client_id = pos_client.client_id 

LEFT JOIN 
(

SELECT 
client_id,
SUM(amount) AS 'credit'
FROM pos_client_credit 
GROUP BY client_id

) AS credit
ON pos_trans_master.client_id = credit.client_id



WHERE pos_trans_master.trans_master_id = '$id'
AND pos_trans_detail.line_void = 'NO'";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));



	//TOTAL ITEM QUANTITY COUNT
	$total_item_qty = 0;

	$index = 0;



	//add Action Header
	//$str .= '<th class="hidden-xs">';
	//$str .= "";
	//$str .= "</th>";

	while ($row = mysqli_fetch_field($result)) {

		if ($index >= 18) {

			$str .= "<th>";
			$str .= $row->name;
			$str .= "</th>";

		}

		$index++;


	}



	//CLOSE THEAD
	$str .= "</tr>";

	//CLOSE THEAD
	$str .= "</thead>";


	//OPEN TBODY
	$str .= "<tbody>";


	$index = 0;

	$total_price = 0;


	//for inserting blank rows
	$service_line_count = 0;
	

	while ($row = mysqli_fetch_row($result)) {

		foreach ($row as $data) {

			switch ($index) {
				case 0:
					$id = $data;
					break;
				case 1:
					$due_date = $data;
					break;
				case 2:
					$record_datetime = $data;
					break;
				case 3:
					$store_name = $data;
					break;
				case 4:
					$store_address = $data;
					break;
				case 5:
					$store_num_tel = $data;
					break;
				case 6:
					$store_num_cell = $data;
					break;
				case 7:
					$client_name = $data;
					break;
				case 8:
					$client_address = $data;
					break;
				case 9:
					$client_contact1 = $data;
					break;
				case 10:
					$client_contact2 = $data;
					break;
				case 11:
					$email = $data;
					break;
				case 12:
					$claim_by = $data;
					break;
				case 13:
					$claim_datetime = $data;
					break;
				case 14:
					$store_code = $data;
					break;
				case 15:
					$due_date1 = $data;
					break;
				case 16:
					$credit = $data;
					break;
				case 17:
					$local_code = $data;
					break;
				case 18:
					$service = $data;
					break;
				case 19:
					$unit_count = $data;
					break;
				case 20:
					$price = $data;
					break;
			}


			$index++;


		}


		//reset index
		$index = 0;


		//add total price
		$total_price = ($total_price + $price);


		//increament service line counter
		$service_line_count++;


		$str .= "<tr>";

		$str .= '

			<td>
				<span class="email">' . $service . '</span>
			</td>

			<td>
				<span class="email">' . $unit_count . '</span>
			</td>

			<td>
				<span class="email">' . $price . '</span>
			</td>


		';


		$str .= "</tr>";

	}
	
	
	
	
	//========================================================================================================
	
	$sub_total = $total_price;
	
	$query = " 
			           
            SELECT
			pos_store.surcharge_store,
            pos_store.surcharge_pickup,
			pos_trans_special.trans_special_id,
			pos_special.value,
			pos_special.special_type,
			pos_special.value_type,
			pos_special.name AS 'special'
			FROM pos_trans_special

			LEFT JOIN pos_special
			ON pos_trans_special.special_id = pos_special.special_id 
                      
            LEFT JOIN pos_trans_master
            ON pos_trans_special.trans_master_id = pos_trans_master.trans_master_id
            
            LEFT JOIN pos_store 
            ON pos_trans_master.store_id = pos_store.store_id

			WHERE pos_trans_special.trans_master_id = '$trans_master_id'
			AND pos_trans_special.special_void = 'NO' 
			
			ORDER BY pos_special.priority ASC
			
			;";
	
	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	
	$total_special_price = 0;
	$total_discount_price = 0;
	$total_service_fee_price = 0;
	$total_vat_price = 0;
	
	while ($row = mysqli_fetch_array($result)) {
		
		
		//increament service line counter
		$service_line_count++;
		
		$session_surcharge_store = $row['surcharge_store'];
		$session_surcharge_pickup = $row['surcharge_pickup'];
		$value = $row['value'];
		$special_type = $row['special_type'];
		$value_type = $row['value_type'];
		$special = $row['special'];		
	
	
		if ($special_type == "MARK-UP") {
			
			if ($value_type == "PERCENTAGE") {

				$percent_value = ($value / 100);

				$special_price = ($sub_total * $percent_value);


			}  else {

				$special_price = ($value);

			}
			
			
			//ADD ALL SPECIAL PRICE
			$total_special_price = $special_price + $total_special_price;
			
			
		} elseif ($special_type == "STORE SERVICE FEE") {
			
			
			if ($session_surcharge_store > $sub_total) {

				$special_price = $session_surcharge_store - ($sub_total + $total_special_price);


			} else {

				$special_price = 0;

			}
			
			//SERVICE FEE -> STORE
			$total_service_fee_price = $special_price;
						
		}  elseif ($special_type == "PICK-UP SERVICE FEE") {
			
			if ($session_surcharge_pickup > $sub_total) {

				$special_price = $session_surcharge_pickup - $sub_total;


			} else {

				$special_price = 0;


			}
			
			//SERVICE FEE -> PICK-UP
			$total_service_fee_price = $special_price;
			
			
		}  elseif ($special_type == "DISCOUNT") {
			
			
			if ($value_type == "PERCENTAGE") {

				$percent_value = ($value / 100);

				$special_price = (-1 * abs(( ($sub_total + $total_special_price + $total_service_fee_price) * $percent_value)));


			} else {

				$special_price = (-1 * $value);

			}
			
			
			//ADD ALL DISCOUNT PRICE
			$total_discount_price = $special_price + $total_discount_price;
			
			
			
			
		} elseif ($special_type == "VAT") {
			
			$special_price = ($total_price + $total_special_price + $total_discount_price + $total_service_fee_price) * ($value / 100);
			
			$total_vat_price = $special_price;

		}
		
		
	
		$str .= "<tr>";	
				
		$str .= '
	
			<td>
				<span class="email">' . $special . '</span>
			</td>
			
			<td>
				<span class="email">' . '**' . '</span>
			</td>
	
	
			<td>
				<span class="email">' . number_format($special_price,2) . '</span>
			</td>
	
		';
	
	
		$str .= "</tr>";	
		
	}
	
	
	
	//========================================================================================================
	
	
	
	
	
	$blank_row = $trans_space_count - $service_line_count;

	while ($blank_row > 0) {


		$str .= "<tr class='tr_blank_space'>";

		$str .= '



			<td>
				<span class="email">' . '&nbsp;' . '</span>
			</td>



			<td>
				<span class="email">' . '' . '</span>
			</td>

			<td>
				<span class="email">' . '' . '</span>
			</td>


		';


		$str .= "</tr>";


		$blank_row--;


	}
	
	//CLOSE TBODY
	$str .= "</tbody>";


	$resp['total_price'] = $total_price;
	$resp['table_data'] = $str;
	$resp['table_data2'] = $str;

	$resp['store_name'] = $store_name;
	$resp['store_code'] = $store_code;
	$resp['store_address'] = $store_address;
	$resp['store_num_tel'] = $store_num_tel;
	$resp['store_num_cell'] = $store_num_cell;
	$resp['client_name'] = $client_name;
	$resp['client_address'] = $client_address;
	$resp['client_contact1'] = $client_contact1;
	$resp['client_contact2'] = $client_contact2;
	$resp['record_datetime'] = $record_datetime;
	$resp['due_date'] = $due_date;
	$resp['due_date1'] = $due_date1;
	
	$resp['email'] = $email;

	$resp['claim_by'] = $claim_by;
	$resp['claim_datetime'] = $claim_datetime;
	
	$resp['store_code'] = $store_code;
	
	$resp['client_credit'] = $credit;
	
	$resp['local_code'] = $local_code;
	
	
	$resp['total_special_price'] = $total_special_price;
	$resp['total_discount_price'] = $total_discount_price;
	$resp['total_service_fee_price'] = $total_service_fee_price;
	
	$resp['total_vat_price'] = $total_vat_price;
	
	$total_overall = $total_special_price + $total_discount_price + $total_service_fee_price + $sub_total + $total_vat_price;
	
	$resp['total_overall'] = $total_overall;


	echo json_encode($resp);


}



//DISPLAY TRANS ITEM WITHOU BUTTON TABLE
if(isset($_POST['pos-store-transaction-display_trans_item_wo_button-table']) && $_POST['pos-store-transaction-display_trans_item_wo_button-table'] == true) {
	
	$resp = array(
	'total_item_qty' => '',
	'table_item' => ''
	);


	$id = mysqli_real_escape_string($link,$_POST['id']);

	$str = "";

	$str .= "<thead>";

	$str .= '<tr class="no-borders">';

	$query = "CALL `SP-store-transaction-dispay_trans_item`('$id')";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));



	$index = 0;
	$total_item_qty = 0;

	$str .= "</th>";

	while ($row = mysqli_fetch_field($result)) {

		if ($index >= 1) {

			$str .= "<th>";
			$str .= $row->name;
			$str .= "</th>";

		}

		$index++;


	}



	//CLOSE THEAD
	$str .= "</tr>";

	//CLOSE THEAD
	$str .= "</thead>";


	//OPEN TBODY
	$str .= "<tbody>";


	$index = 0;

	$total_price = 0;

	while ($row = mysqli_fetch_row($result)) {

		$str .= "<tr>";

		foreach ($row as $data) {

			switch ($index) {
				case 0:
					$id = $data;
					break;
				case 1:
					$item_name = $data;
					break;
				case 2:
					$qty = $data;
					break;
			}


			$index++;

		}


		//reset index
		$index = 0;
		
		$total_item_qty = $total_item_qty + $qty;


		$str .= '


			<td>
				<span class="email">' . $item_name . '</span>
			</td>

			<td>
				<span class="email">' . $qty . '</span>
			</td>



		';


		$str .= "</tr>";

	}


	//CLOSE TBODY
	$str .= "</tbody>";


	$resp['total_item_qty'] = $total_item_qty;
	$resp['table_item'] = $str;
	

	echo json_encode($resp);

	//echo $str;
	

}





if(isset($_POST['pos-store-transaction-display_trans_special-table']) && $_POST['pos-store-transaction-display_trans_special-table'] == true) {
	
	$resp = array(
	'special_total' => '',
	'less_total' => '',
	'table_data' => '',
	'table_data2' => ''
	);
	
	
	$special_total = 0;
	$less_total = 0;
	
	$id = mysqli_real_escape_string($link,$_POST['id']);
	$sub_total = mysqli_real_escape_string($link,$_POST['sub_total']);
	
	$str = "";
	
	

	$str .= '<thead>';
	
	$str .= '<tr class="no-borders">';

	$query = "SELECT 
			pos_trans_special.trans_special_id,
			pos_special.value,
			pos_special.special_type,
			pos_special.value_type,
			pos_special.name AS 'Special',
			'' AS 'Price'
			
			FROM pos_trans_special 
			
			LEFT JOIN pos_special 
			ON pos_trans_special.special_id = pos_special.special_id
			
			WHERE pos_trans_special.trans_master_id = '$id' 
			AND pos_trans_special.special_void = 'NO'";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	$index = 0;
	
	
	
	//add Action Header
	//$str .= '<th class="hidden-xs">';
	//$str .= "";
	//$str .= "</th>";
	
	while ($row = mysqli_fetch_field($result)) {
		
		if ($index == 4) {
			
			$str .= '<th Style="text-transform: capitalize !important;">';
			$str .= $row->name;
			$str .= "</th>";		
			
		} elseif ($index == 5) {
			
			
			
			$str .= '<th Style="text-transform: capitalize !important;">';
			$str .= $row->name;
			$str .= "</th>";
			
			
			$str .= '<th Style="text-transform: capitalize !important;">';
			$str .= '';
			$str .= "</th>";	
			
			
			
			
		}
		
		$index++;
	
		
	}
	
	
			
	//CLOSE table row
	$str .= "</tr>";
	
	//CLOSE THEAD
	$str .= "</thead>";
	

	//OPEN TBODY
	$str .= "<tbody>";
		
		
	$index = 0;
	
	$total_price = 0;
	
	
	while ($row = mysqli_fetch_row($result)) {
		
		$str .= "<tr>";
		
		foreach ($row as $data) {
			
			switch ($index) {
				case 0:
					$id = $data;
					break;
				case 1:
					$value = $data;
					break;
				case 2:
					$special_type = $data;
					break;
				case 3:
					$value_type = $data;
					break;
				case 4:
					$name = $data;
					break;
				case 5:
					$price = $data;
					break;
			}
			
			$index++;
			
		}
		
		
		//reset index
		$index = 0;
		
				
		if ($special_type == "DISCOUNT") {
			
			if ($value_type == "PERCENTAGE") {
				
				$percent_value = ($value / 100);
				
				$special_price = (-1 * abs(($sub_total * $percent_value)));
				
				
			} else {
				
				$special_price = (-1 * $value);
				
			}
			
			$less_total = ($less_total + $special_price);
			
			
		} else {
			
			if ($value_type == "PERCENTAGE") {
				
				$percent_value = ($value / 100);
				
				$special_price = ($sub_total * $percent_value);
				
				
			} elseif ($value_type == "SURCHARGE") {
				
				if ($session_store_surcharge > $sub_total) {
					
					$special_price = $session_store_surcharge - $sub_total;	
					
					
				} else {
					
					$special_price = 0;
					
					
				}
				
			} else {
				
				$special_price = ($value);
				
			}
			
			
			$special_total = ($special_total + $special_price);
						
		}
				
		
		
	
		$str .= '
			
			<td>
				<span class="email">' . $name . '</span>
			</td>
			
			
			
			
			<td>
				<span class="email">' . number_format($special_price,2) . '</span>
			</td>
		
		';
		
		
		$str .= "</tr>";	
		
	}
	
	$row_count = mysqli_num_rows($result);
	
	if ($row_count == 0) {
			
		$str .= '
		<tr>
			<td colspan="5">
				<span class="email">No Special Service/Discount</span>
			</td>
		</tr>
		
		';
		
	}
	
	


	//CLOSE TBODY
	$str .= "</tbody>";
	
	
	$resp['special_total'] = $special_total;
	$resp['less_total'] = $less_total;
	$resp['table_data'] = $str;
	$resp['table_data2'] = $str;
	
	
	
	echo json_encode($resp);
	

}



if(isset($_POST['pos-store-transaction-void_transaction']) && $_POST['pos-store-transaction-void_transaction'] == true) {
	
	$resp = array(
	'status' => '',
	'message' => ''
	);
	
	$trans_master_id = mysqli_real_escape_string($link,$_POST['id']);
	$status = mysqli_real_escape_string($link,$_POST['status']);
	
	
	
	$query = "CALL `SP-store-transaction-void_transaction`('$trans_master_id', '$session_store_id', '$status');";

	$result = mysqli_query($link,$query);
	
	if (!$result) {
		
		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);
		
	
	} else {
		
		$resp['status'] = 'success';
	
	}
	
	
	echo json_encode($resp);


}



?>