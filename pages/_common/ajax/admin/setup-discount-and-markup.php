<?php
session_start();

include '../../../../config.php';

$session_user_id = $_SESSION['user_id'];


if(isset($_POST['pos-admin-display_markup-table']) && $_POST['pos-admin-display_markup-table'] == true) {
	
	
	$str = "";

	$str .= "<thead>";

	$query = "CALL `SP-admin-display_markup_all`();";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	$index = 0;
	
	while ($row = mysqli_fetch_field($result)) {
		
		if ($index >= 1) {
			
			$str .= "<th>";
			$str .= $row->name;
			$str .= "</th>";		
			
		}
		
		$index++;
	
		
	}
	
	//add Action Header
	$str .= "<th>";
	$str .= "Actions";
	$str .= "</th>";
			
	
	//CLOSE THEAD
	$str .= "</thead>";
	

	//OPEN TBODY
	$str .= "<tbody>";
		
		
	$index = 0;
	
	while ($row = mysqli_fetch_row($result)) {
		
		$str .= "<tr>";
		
		foreach ($row as $data) {
			
			switch ($index) {
				case 0:
					$id = $data;
					break;
				case 1:
					$name = $data;
					break;
				case 2:
					$value = $data;
					break;
				case 3:
					$special_type = $data;
					break;
			}
										
			$index++;
			
		}
		
		
		//reset index
		$index = 0;
		
	
	
		/*
		<td class="user-name">
			<a href="#" class="name">' . $store_name . '</a>
			
			<span>' . $address_city . '</span>
			
		</td>
		
		*/
		
		$str .= '	
		
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $name . '</span>
			</td>
		
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $value . '</span>
			</td>
			
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $special_type . '</span>
			</td>
		
			
			<td class="action-links">
				<a href="#link_view" class="edit" id="link_view" value="' . $id . '" name=" ' . $name . '">
					<i class="fa-folder-open-o"></i>
					View
				</a>
	
				<a href="#link_delete" class="delete" id="link_delete" value="' . $id . '" name=" ' . $name . '">
					<i class="linecons-trash"></i>
					Delete
				</a>
			</td>
			
		
		';
		
		
		$str .= "</tr>";	
		
	}


	//CLOSE TBODY
	$str .= "</tbody>";
	
	
	echo $str;
	

	
}


if(isset($_POST['pos-admin-display_discount-table']) && $_POST['pos-admin-display_discount-table'] == true) {
	
	
	$str = "";

	$str .= "<thead>";

	$query = "CALL `SP-admin-display_discount_all`();";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	$index = 0;
	
	while ($row = mysqli_fetch_field($result)) {
		
		if ($index >= 1) {
			
			$str .= "<th>";
			$str .= $row->name;
			$str .= "</th>";		
			
		}
		
		$index++;
	
		
	}
	
	//add Action Header
	$str .= "<th>";
	$str .= "Actions";
	$str .= "</th>";
			
	
	//CLOSE THEAD
	$str .= "</thead>";
	

	//OPEN TBODY
	$str .= "<tbody>";
		
		
	$index = 0;
	
	while ($row = mysqli_fetch_row($result)) {
		
		$str .= "<tr>";
		
		foreach ($row as $data) {
			
			switch ($index) {
				case 0:
					$id = $data;
					break;
				case 1:
					$name = $data;
					break;
				case 2:
					$value = $data;
					break;
				case 3:
					$special_type = $data;
					break;
			}
										
			$index++;
			
		}
		
		
		//reset index
		$index = 0;
		
	
	
		/*
		<td class="user-name">
			<a href="#" class="name">' . $store_name . '</a>
			
			<span>' . $address_city . '</span>
			
		</td>
		
		*/
		
		$str .= '	
		
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $name . '</span>
			</td>
		
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $value . '</span>
			</td>
			
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $special_type . '</span>
			</td>
		
			
			<td class="action-links">
				<a href="#link_view" class="edit" id="link_view" value="' . $id . '" name=" ' . $name . '">
					<i class="fa-folder-open-o"></i>
					View
				</a>
	
				<a href="#link_delete" class="delete" id="link_delete" value="' . $id . '" name=" ' . $name . '">
					<i class="linecons-trash"></i>
					Delete
				</a>
			</td>
			
		
		';
		
		
		$str .= "</tr>";	
		
	}


	//CLOSE TBODY
	$str .= "</tbody>";
	
	
	echo $str;
	

	
}



if(isset($_POST['pos-admin-add_special']) && $_POST['pos-admin-add_special'] == true) {
	
	$resp = array(
	'status' => '',
	'message' => ''
	);
	
	$name = mysqli_real_escape_string($link,$_POST['special_name']);
	$value = mysqli_real_escape_string($link,$_POST['special_value']);
	$special_type = mysqli_real_escape_string($link,$_POST['special_type']);
	$value_type = mysqli_real_escape_string($link,$_POST['special_value_type']);
	
	
	$query = "CALL `SP-admin-add_special`('$name', '$value', '$special_type', '$value_type', '$session_user_id');";

	$result = mysqli_query($link,$query);
	
	if (!$result) {
		
		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);
		
	
	} else {
		
		$resp['status'] = 'success';
	
		
	}
	
	
	echo json_encode($resp);
	
	
	
}


if(isset($_POST['pos-admin-delete_special']) && $_POST['pos-admin-delete_special'] == true) {
	
	$resp = array(
	'status' => '',
	'message' => ''
	);

	$id = mysqli_real_escape_string($link,$_POST['id']);
	
	$query = "CALL `SP-admin-delete_special`('$id');";

	$result = mysqli_query($link,$query);
	
	if (!$result) {
		
		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);
		
	
	} else {
		
		$resp['status'] = 'success';
	
		
	}
	
	
	echo json_encode($resp);
	
	
}




if(isset($_POST['pos-admin-load_special_details']) && $_POST['pos-admin-load_special_details'] == true) {
	
	$resp = array(
	'id' => '',
	'special_type' => '',
	'special_name' => '',
	'special_value' => '',
	'special_value_type' => '',
	'add_by' => '',
	'add_datetime' => ''
	);

	$id = mysqli_real_escape_string($link,$_POST['id']);
	
	$query = "CALL `SP-admin-view_special_details`('$id');";

	$result = mysqli_query($link,$query);
	
	if ($row = mysqli_fetch_array($result)) {
		
		$resp['id'] = $row['special_id'];
		$resp['special_type'] = $row['special_type'];
		$resp['special_name'] = $row['name'];
		$resp['special_value'] = $row['value'];
		$resp['special_value_type'] = $row['value_type'];
		$resp['add_by'] = $row['add_by'];
		$resp['add_datetime'] = $row['add_datetime'];
		
		
	}
	
	
	
	echo json_encode($resp);
	
	
	
}


if(isset($_POST['pos-admin-update_special']) && $_POST['pos-admin-update_special'] == true) {
	
	$resp = array(
	'status' => '',
	'message' => ''
	);
	
	
	$update_id = mysqli_real_escape_string($link,$_POST['id']);
	$special_type = mysqli_real_escape_string($link,$_POST['special_type']);
	$name = mysqli_real_escape_string($link,$_POST['special_name']);
	$value = mysqli_real_escape_string($link,$_POST['special_value']);
	$value_type = mysqli_real_escape_string($link,$_POST['special_value_type']);
	
	
	$query = "CALL `SP-admin-update_special`('$update_id', '$name', '$value', '$special_type', '$value_type', '$session_user_id');";

	$result = mysqli_query($link,$query);
	
	if (!$result) {
		
		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);
		
	
	} else {
		
		$resp['status'] = 'success';
	
		
	}
	
	
	echo json_encode($resp);


}



?>