<?php
session_start();

include '../../../../config.php';



$session_user_id = $_SESSION['user_id'];


if(isset($_POST['pos-admin-display_user_store_all-table']) && $_POST['pos-admin-display_user_store_all-table'] == true) {
	
	
	$str = "";

	$str .= "<thead>";

	$query = "CALL `SP-admin-display_user_store_all`();";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	$index = 0;
	
	while ($row = mysqli_fetch_field($result)) {
		
		if ($index >= 2) {
			
			$str .= "<th>";
			$str .= $row->name;
			$str .= "</th>";		
			
		}
		
		$index++;
	
		
	}
	
	//add Action Header
	$str .= "<th>";
	$str .= "Actions";
	$str .= "</th>";
			
	
	//CLOSE THEAD
	$str .= "</thead>";
	

	//OPEN TBODY
	$str .= "<tbody>";
		
		
	$index = 0;
	
	while ($row = mysqli_fetch_row($result)) {
		
		$str .= "<tr>";
		
		foreach ($row as $data) {
			
			switch ($index) {
				case 0:
					$id = $data;
					break;
				case 1:
					$store_name = $data;
					break;
				case 2:
					$name = $data;
					break;
				case 3:
					$username = $data;
					break;
			}
										
			$index++;
			
		}
		
		
		//reset index
		$index = 0;
		
	
	
		/*
		<td class="user-name">
			<a href="#" class="name">' . $store_name . '</a>
			
			<span>' . $address_city . '</span>
			
		</td>
		
		*/
		
		$str .= '	
		
			
			<td class="user-name">
				<a href="#" class="name">' . $name . '</a>
				<span>' . $store_name . '</span>
			</td>
			
			
			<td class="user-name">
				<span>' . $username . '</span>
			</td>
		
			
			<td class="action-links">
				<a href="#" class="edit" id="link_view" value="' . $id . '" name=" ' . $name . '">
					<i class="fa-folder-open-o"></i>
					View
				</a>
	
				<a href="#" class="delete" id="link_delete" value="' . $id . '" name=" ' . $name . '">
					<i class="linecons-trash"></i>
					Delete
				</a>
			</td>
			
		
		';
		
		
		$str .= "</tr>";	
		
	}


	//CLOSE TBODY
	$str .= "</tbody>";
	
	
	echo $str;

	
}



if(isset($_POST['pos-admin-load_store_list-option']) && $_POST['pos-admin-load_store_list-option'] == true) { 


	$query = "CALL `SP-admin-load_store_list_option`();";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	$index = 0;
	
	$str = '<option selected="true" disabled="disabled">..</option>';
	
	while ($row = mysqli_fetch_array($result)) {
	
		$store_id = $row['store_id'];
		$store_code = $row['store_code'];
		$store_name = $row['store_name'];
		
		$str .= '<option value="' . $store_id . '">' . $store_code . ' - ' . $store_name . '</option>';
			
	}
	
	
	echo $str;



}



if(isset($_POST['pos-admin-add_user_store']) && $_POST['pos-admin-add_user_store'] == true) {
	
	$resp = array(
	'status' => '',
	'message' => ''
	);
	
	$username = mysqli_real_escape_string($link,$_POST['username']);
	$password = mysqli_real_escape_string($link,$_POST['password']);
	$first_name = mysqli_real_escape_string($link,$_POST['first_name']);
	$middle_name = mysqli_real_escape_string($link,$_POST['middle_name']);
	$last_name = mysqli_real_escape_string($link,$_POST['last_name']);
	$store_id = mysqli_real_escape_string($link,$_POST['store_code']);
	
	
	$query = "CALL `SP-admin-add_user_store`('$username', '$password', '$first_name', '$middle_name', '$last_name', '$store_id');";
	

	$result = mysqli_query($link,$query);
	
	if (!$result) {
		
		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);
		
	
	} else {
		
		$resp['status'] = 'success';
	
		
	}
	
	
	echo json_encode($resp);
	
	
	
}


if(isset($_POST['pos-admin-delete_user_store']) && $_POST['pos-admin-delete_user_store'] == true) {
	
	$resp = array(
	'status' => '',
	'message' => ''
	);

	$id = mysqli_real_escape_string($link,$_POST['id']);
	
	$query = "CALL `SP-admin-delete_user_store`('$id');";

	$result = mysqli_query($link,$query);
	
	if (!$result) {
		
		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);
		
	
	} else {
		
		$resp['status'] = 'success';
	
		
	}
	
	
	echo json_encode($resp);
	
	
	
}



if(isset($_POST['pos-admin-load_user_store_details']) && $_POST['pos-admin-load_user_store_details'] == true) {
	
	$resp = array(
	'id' => '',
	'username' => '',
	'first_name' => '',
	'middle_name' => '',
	'last_name' => '',
	'store_designation' => ''
	);

	$id = mysqli_real_escape_string($link,$_POST['id']);
	
	$query = "CALL `SP-admin-view_user_store_details`('$id');";

	$result = mysqli_query($link,$query);
	
	if ($row = mysqli_fetch_array($result)) {
		
		$resp['id'] = $row['user_id'];
		$resp['username'] = $row['username'];
		$resp['first_name'] = $row['first_name'];
		$resp['middle_name'] = $row['middle_name'];
		$resp['last_name'] = $row['last_name'];
		$resp['store_designation'] = $row['store_id'];
			
	}
	
	
	
	echo json_encode($resp);
	
	
	
}



if(isset($_POST['pos-admin-update_user_store']) && $_POST['pos-admin-update_user_store'] == true) {
	
	$resp = array(
	'status' => '',
	'message' => ''
	);
	
	
	$update_id = mysqli_real_escape_string($link,$_POST['id']);
	$username = mysqli_real_escape_string($link,$_POST['username']);
	$first_name = mysqli_real_escape_string($link,$_POST['first_name']);
	$middle_name = mysqli_real_escape_string($link,$_POST['middle_name']);
	$last_name = mysqli_real_escape_string($link,$_POST['last_name']);
	$store_id = mysqli_real_escape_string($link,$_POST['store_code']);
	
	
	$query = "CALL `SP-admin-update_user_store`('$update_id', '$username', '$first_name', '$middle_name', '$last_name', '$store_id');";

	$result = mysqli_query($link,$query);
	
	if (!$result) {
		
		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);
		
	
	} else {
		
		$resp['status'] = 'success';
	
		
	}
	
	
	echo json_encode($resp);


}


if(isset($_POST['pos-admin-reset_pw_user_store']) && $_POST['pos-admin-reset_pw_user_store'] == true) {
	
	$resp = array(
	'status' => '',
	'message' => ''
	);
	
	
	$update_id = mysqli_real_escape_string($link,$_POST['id']);
	$new_pw = mysqli_real_escape_string($link,$_POST['str_random']);
	
	
	$query = "CALL `SP-admin-update_user_store_password`('$update_id', '$new_pw');";

	$result = mysqli_query($link,$query);
	
	if (!$result) {
		
		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);
		
	
	} else {
		
		$resp['status'] = 'success';
	
		
	}
	
	
	echo json_encode($resp);


}



?>