<?php
session_start();

include '../../../../config.php';

$session_user_id = $_SESSION['user_id'];

////////////// load table ///////

if(isset($_POST['pos-admin-display_main_service-table']) && $_POST['pos-admin-display_main_service-table'] == true) {
	
	
	$str = "";

	$str .= "<thead>";

	$query = "CALL `SP-admin-display_main_service_all`();";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	$index = 0;
	
	while ($row = mysqli_fetch_field($result)) {
		
		//if ($index >= 1) {
			
			$str .= "<th>";
			$str .= $row->name;
			$str .= "</th>";		
			
		//}
		
		$index++;
	
		
	}
	
	//add Action Header
	$str .= "<th>";
	$str .= "Actions";
	$str .= "</th>";
			
	
	//CLOSE THEAD
	$str .= "</thead>";
	

	//OPEN TBODY
	$str .= "<tbody>";
		
		
	$index = 0;
	
	
	while ($row = mysqli_fetch_array($result)) {
		
		//////// same row in wrkbench
		$service_id = $row['Service ID'];
		$name = $row['Service Name'];
		
		
		$str .= "<tr>";
		
		$str .= '	
		
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $service_id . '</span>
			</td>
		
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $name . '</span>
			</td>
			
	
			
			<td class="action-links">
				<a href="#link_view" class="edit" id="link_view" value="' . $service_id . '" name=" ' . $name . '">
					<i class="fa-folder-open-o"></i>
					View
				</a>
	
				<a href="#link_delete" class="delete" id="link_delete" value="' . $service_id . '" name=" ' . $name . '">
					<i class="linecons-trash"></i>
					Delete
				</a>
			</td>
			
		
		';
		
		
		$str .= "</tr>";	
		
		
	}
	
	
	//CLOSE TBODY
	$str .= "</tbody>";
	
	
	echo $str;
	

} 


/////////////// ADMIN ADD /////////////////

if(isset($_POST['pos-admin-add_main_service']) && $_POST['pos-admin-add_main_service'] == true) {
	
	$resp = array(
	'status' => '',
	'message' => ''
	);
	
	
	$service_name = mysqli_real_escape_string($link,$_POST['service_name']);
	
	
	$query = "CALL `SP-admin-add_main_service`('$service_name');";

	$result = mysqli_query($link,$query);
	
	if (!$result) {
		
		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);
		
	
	} else {
		
		$resp['status'] = 'success';
	
		
	}
	
	
	echo json_encode($resp);
	

} 


/////////////  ADMIN DELETE ///////////

if(isset($_POST['pos-admin-delete_main_service']) && $_POST['pos-admin-delete_main_service'] == true) {
	
	$resp = array(
	'status' => '',
	'message' => ''
	);

	$id = mysqli_real_escape_string($link,$_POST['id']);
	//$name = mysqli_real_escape_string($link,$_POST['name']);
	
	$query = "CALL `SP-admin-delete_main_service`('$id');";

	$result = mysqli_query($link,$query);
	
	if (!$result) {
		
		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);
		
	
	} else {
		
		$resp['status'] = 'success';
	
		
	}
	
	
	echo json_encode($resp);
	
	
	
}

/////////// ADMIN LOAD DETAIL /////////////////////


if(isset($_POST['pos-admin-load_main_service_detail']) && $_POST['pos-admin-load_main_service_detail'] == true) {
	
	$resp = array(
	'service_main_id' => '',
	'name' => '',
	);
	

	$id = mysqli_real_escape_string($link,$_POST['id']);
	
	$query = "CALL `SP-admin-load_main_service`('$id')";

	$result = mysqli_query($link,$query);
	
	if ($row = mysqli_fetch_array($result)) {
		
		$resp['service_main_id'] = $row['service_main_id'];
		$resp['name'] = $row['name'];
		
	}
		
	echo json_encode($resp);
	
}


//////////// ADMIN UPDATE /////////////////

if(isset($_POST['pos-admin-update_main_service']) && $_POST['pos-admin-update_main_service'] == true) {
	
	$resp = array(
	'status' => '',
	'message' => ''
	);
	
	$id = mysqli_real_escape_string($link,$_POST['id']);
	$name = mysqli_real_escape_string($link,$_POST['name']);
	
	
	$query = "CALL `SP-admin-update_main_service`('$id', '$name');";

	$result = mysqli_query($link,$query);
	
	if (!$result) {
		
		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);
		
	
	} else {
		
		$resp['status'] = 'success';
	
		
	}
	
	
	echo json_encode($resp);


}





//=-======================================================================================================================



if(isset($_POST['pos-admin-display_service_sub_category_all-table']) && $_POST['pos-admin-display_service_sub_category_all-table'] == true) {
	
	
	$str = "";

	$str .= "<thead>";

	$query = "CALL `SP-admin-display_service_sub_category_all`();";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	$index = 0;
	
	while ($row = mysqli_fetch_field($result)) {
		
		if ($index >= 1) {
			
			$str .= "<th>";
			$str .= $row->name;
			$str .= "</th>";		
			
		}
		
		$index++;
	
		
	}
	
	//add Action Header
	$str .= "<th>";
	$str .= "Actions";
	$str .= "</th>";
			
	
	//CLOSE THEAD
	$str .= "</thead>";
	

	//OPEN TBODY
	$str .= "<tbody>";
		
		
	$index = 0;
	
	while ($row = mysqli_fetch_row($result)) {
		
		$str .= "<tr>";
		
		foreach ($row as $data) {
			
			switch ($index) {
				case 0:
					$id = $data;
					break;
				case 1:
					$service_name = $data;
					break;
				case 2:
					$category_name = $data;
					break;
				case 3:
					$sub_category_code = $data;
					break;
				case 4:
					$sub_category_name = $data;
					break;
				case 5:
					$unit_price = $data;
					break;
			}
										
			$index++;
			
		}
		
		
		//reset index
		$index = 0;
		
	
	
		/*
		<td class="user-name">
			<a href="#" class="name">' . $store_name . '</a>
			
			<span>' . $address_city . '</span>
			
		</td>
		
		*/
		
		$str .= '	
		
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $service_name . '</span>
			</td>
		
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $category_name . '</span>
			</td>
			
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $sub_category_code . '</span>
			</td>
			
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $sub_category_name . '</span>
			</td>
			
			<td class="hidden-xs hidden-sm">
				<span class="email">' . $unit_price . '</span>
			</td>
		
			
			<td class="action-links">
				<a href="#" class="edit" id="link_view" value="' . $id . '" sub_category_name=" ' . $sub_category_name . '">
					<i class="fa-folder-open-o"></i>
					View
				</a>
	
				<a href="#" class="delete" id="link_delete" value="' . $id . '" sub_category_name=" ' . $sub_category_name . '">
					<i class="linecons-trash"></i>
					Delete
				</a>
			</td>
			
		
		';
		
		
		$str .= "</tr>";	
		
	}


	//CLOSE TBODY
	$str .= "</tbody>";
	
	
	echo $str;
	

} 



if(isset($_POST['pos-admin-load_service_category_name_list-option']) && $_POST['pos-admin-load_service_category_name_list-option'] == true) { 

	$service_id = mysqli_real_escape_string($link,$_POST['service_id']);

	$query = "CALL `SP-admin-load_service_category_name_list_option`('$service_id');";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	$index = 0;
	
	//$str = '<option value="" disabled selected>Select Service..</option>';
	
	$str = '';
	
	while ($row = mysqli_fetch_array($result)) {
	
		$service_category_id = $row['service_category_id'];
		$name = $row['name'];
		
		$str .= '<option value="' . $service_category_id . '">' . $service_category_id . ' - ' . $name . '</option>';
			
	}
	
	
	echo $str;

}


if(isset($_POST['pos-admin-add_service_sub_category']) && $_POST['pos-admin-add_service_sub_category'] == true) {
	
	$resp = array(
	'status' => '',
	'message' => ''
	);
	
	
	$category_id = mysqli_real_escape_string($link,$_POST['category_id']);
	$service_sub_category_code = mysqli_real_escape_string($link,$_POST['service_sub_category_code']);
	$service_sub_category_name = mysqli_real_escape_string($link,$_POST['service_sub_category_name']);
	$price = mysqli_real_escape_string($link,$_POST['price']);
	$unit = mysqli_real_escape_string($link,$_POST['unit']);
	
	$query = "CALL `SP-admin-add_service_sub_category`('$category_id', '$service_sub_category_code', '$service_sub_category_name', '$price', '$unit', '$session_user_id');";

	$result = mysqli_query($link,$query);
	
	if (!$result) {
		
		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);
		
	
	} else {
		
		$resp['status'] = 'success';
	
		
	}
	
	
	echo json_encode($resp);
	

} 


if(isset($_POST['pos-admin-delete_service_sub_category']) && $_POST['pos-admin-delete_service_sub_category'] == true) {
	
	$resp = array(
	'status' => '',
	'message' => ''
	);

	$id = mysqli_real_escape_string($link,$_POST['id']);
	
	$query = "CALL `SP-admin-delete_service_sub_category`('$id');";

	$result = mysqli_query($link,$query);
	
	if (!$result) {
		
		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);
		
	
	} else {
		
		$resp['status'] = 'success';
	
		
	}
	
	
	echo json_encode($resp);
	
	
	
}


if(isset($_POST['pos-admin-load_service_sub_category_details']) && $_POST['pos-admin-load_service_sub_category_details'] == true) {
	
	$resp = array(
	'id' => '',
	'service_id' => '',
	'category_id' => '',
	'service_sub_category_code' => '',
	'service_sub_category_name' => '',
	'price' => '',
	'unit' => '',
	'add_by' => '',
	'add_datetime' => ''
	);

	$id = mysqli_real_escape_string($link,$_POST['id']);
	
	$query = "CALL `SP-admin-view_service_sub_category_details`('$id');";

	$result = mysqli_query($link,$query);
	
	if ($row = mysqli_fetch_array($result)) {
		
		$resp['id'] = $row['service_sub_category_id'];
		$resp['service_id'] = $row['service_id'];
		$resp['category_id'] = $row['service_category_id'];
		$resp['service_sub_category_code'] = $row['code'];
		$resp['service_sub_category_name'] = $row['name'];
		$resp['price'] = $row['price'];
		$resp['unit'] = $row['unit'];
		$resp['add_by'] = $row['add_by'];
		$resp['add_datetime'] = $row['add_datetime'];
		
		
	}
		
	echo json_encode($resp);
	
}




if(isset($_POST['pos-admin-update_service_sub_category']) && $_POST['pos-admin-update_service_sub_category'] == true) {
	
	$resp = array(
	'status' => '',
	'message' => ''
	);
	
	$update_id = mysqli_real_escape_string($link,$_POST['id']);
	$category_id = mysqli_real_escape_string($link,$_POST['category_id']);
	$service_sub_category_code = mysqli_real_escape_string($link,$_POST['service_sub_category_code']);
	$service_sub_category_name = mysqli_real_escape_string($link,$_POST['service_sub_category_name']);
	$price = mysqli_real_escape_string($link,$_POST['price']);
	$unit = mysqli_real_escape_string($link,$_POST['unit']);
	
	
	$query = "CALL `SP-admin-update_service_sub_category`('$update_id', '$category_id', '$service_sub_category_code', '$service_sub_category_name', '$price', '$unit');";

	$result = mysqli_query($link,$query);
	
	if (!$result) {
		
		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);
		
	
	} else {
		
		$resp['status'] = 'success';
	
		
	}
	
	
	echo json_encode($resp);


}



if(isset($_POST['pos-admin-load_service_name_list-option']) && $_POST['pos-admin-load_service_name_list-option'] == true) { 


	$query = "CALL `SP-admin-load_service_name_list_option`();";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));
	
	$index = 0;
	
	$str = '<option value="" disabled selected>Select Service..</option>';
	
	
	while ($row = mysqli_fetch_array($result)) {
	
		$service_id = $row['service_id'];
		$name = $row['name'];
		
		$str .= '<option value="' . $service_id . '">' . $service_id . ' - ' . $name . '</option>';
			
	}
	
	
	echo $str;

}



?>