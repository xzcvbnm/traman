<?php
session_start();

include '../../../../config.php';

$session_user_id = $_SESSION['user_id'];


if(isset($_POST['pos-admin-display_service_category-table']) && $_POST['pos-admin-display_service_category-table'] == true) {


	$str = "";

	$str .= "<thead>";

	$query = "CALL `SP-admin-display_category_service_all`();";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));

	$index = 0;

	while ($row = mysqli_fetch_field($result)) {

		if ($index >= 2) {

			$str .= "<th>";
			$str .= $row->name;
			$str .= "</th>";

		}

		$index++;


	}

	//add Action Header
	$str .= "<th>";
	$str .= "Actions";
	$str .= "</th>";


	//CLOSE THEAD
	$str .= "</thead>";


	//OPEN TBODY
	$str .= "<tbody>";


	$index = 0;



	while($row = mysqli_fetch_array($result)) {

    	/// 0
		$category_id = $row['category_id'];
        /// 1
		$service_main_id = $row['service_main_id'];
		
        /// 2		
		$service_name = $row['Service Name'];
		$category_name = $row['Category Name'];


		$str .= "<tr>";

		$str .= '

			<td class="hidden-xs hidden-sm">
				<span class="email">' . $service_name . '</span>
			</td>

			<td class="hidden-xs hidden-sm">
				<span class="email">' . $category_name . '</span>
			</td>


			<td class="action-links">
				<a href="#link_view" class="edit" id="link_view" category_id = "' . $category_id . '" category_name="' . $category_name . '"service_main_id="' . $service_main_id . '">
					<i class="fa-folder-open-o"></i>
					View
				</a>

				<a href="#link_delete" class="delete" id="link_delete"  category_id = "' . $category_id . '" category_name="' . $category_name . '" service_main_id="' . $service_main_id . '">
					<i class="linecons-trash"></i>
					Delete
				</a>
			</td>


		';


		$str .= "</tr>";



	}



	//CLOSE TBODY
	$str .= "</tbody>";


	echo $str;


}

/////////// LOAD service name drop down option /////////////////////

if(isset($_POST['pos-admin-load_main_service-option']) && $_POST['pos-admin-load_main_service-option'] == true) {


	$query = "CALL `SP-admin-load_main_service_option`();";

		
	$result = mysqli_query($link,$query)or die(mysqli_error($link));
 
	$index = 0;

	$str = '<option selected="true" disabled="disabled">...</option>';


	while ($row = mysqli_fetch_array($result)) {

		$service_main_id = $row['service_main_id'];
		$name = $row['name'];

		$str .= '<option value="' . $service_main_id . '">' . $name . '</option>';

	}


	echo $str;
	



}

///////////// ADD //////////////

if(isset($_POST['pos-admin-add_service_category']) && $_POST['pos-admin-add_service_category'] == true) {

	$resp = array(
	'status' => '',
	'message' => ''
	);

	$service_main_id = mysqli_real_escape_string($link,$_POST['service_main_id']);
	$category_name = mysqli_real_escape_string($link,$_POST['category_name']);
	


	$query = "CALL `SP-admin-add_service_category` ('$service_main_id', '$category_name')";

	$result = mysqli_query($link,$query);

	if (!$result) {

		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);


	} else {

		$resp['status'] = 'success';


	}


	echo json_encode($resp);



}



///////////// DELETE /////////////////


if(isset($_POST['pos-admin-delete_service_category']) && $_POST['pos-admin-delete_service_category'] == true) {

	$resp = array(
	'status' => '',
	'message' => ''
	);

	$category_id = mysqli_real_escape_string($link,$_POST['category_id']);

	$query = "CALL `SP-admin-delete_service_category`('$category_id')";

	$result = mysqli_query($link,$query);

	if (!$result) {

		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);


	} else {

		$resp['status'] = 'success';


	}


	echo json_encode($resp);



}


//////////////////// LOAD service category VIEW DETAILS	 //////////////

if(isset($_POST['pos-admin-load_service_category']) && $_POST['pos-admin-load_service_category'] == true) {

	
	$resp = array(
	'service_main_id' => '',
	'category_id' => '',
	'name' => ''
	);
	
	
	$id = mysqli_real_escape_string($link,$_POST['id']);
	
	$query = "CALL `SP-admin-load_service_category`('$id');";

	$result = mysqli_query($link,$query)or die(mysqli_error($link));

	$index = 0;


	while ($row = mysqli_fetch_array($result)) {

		$service_main_id = $row['service_main_id'];
	 	$category_id = $row['category_id'];
		$name = $row['name'];
		
		
		$resp['service_main_id'] = $service_main_id;
		$resp['category_id'] = $category_id;
		$resp['name'] = $name;
		


	}


	echo json_encode($resp);

}


/////////////////////// UPDATE CATEGORY ////////////////

if(isset($_POST['pos-admin-update_service_category']) && $_POST['pos-admin-update_service_category'] == true) {

	$resp = array(
	'status' => '',
	'message' => ''
	);

	$category_id = mysqli_real_escape_string($link,$_POST['category_id']);
	$service_main_id = mysqli_real_escape_string($link,$_POST['service_main_id']);
	$name = mysqli_real_escape_string($link,$_POST['name']);

	$query = "CALL `SP-admin-update_service_category`('$category_id', '$service_main_id', '$name')";

	$result = mysqli_query($link,$query);

	if (!$result) {

		$resp['status'] = 'failed';
    	$resp['message'] = mysqli_error($link);


	} else {

		$resp['status'] = 'success';


	}


	echo json_encode($resp);



}




?>
