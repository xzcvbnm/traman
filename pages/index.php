<?php
session_start();
include '../config.php';



//pages menu setup
if(!isset($_SESSION['user_id'])) {


	die();


}


?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Xenon Boostrap Admin Panel" />
	<meta name="author" content="" />
    
    

	<title></title>

	<link rel="icon" type="image/png" href="favicon.ico">

	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arimo:400,700,400italic">
	<link rel="stylesheet" href="../assets/css/fonts/linecons/css/linecons.css">
	<link rel="stylesheet" href="../assets/css/fonts/fontawesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="../assets/css/bootstrap.css">
	<link rel="stylesheet" href="../assets/css/xenon-core.css">
	<link rel="stylesheet" href="../assets/css/xenon-forms.css">
	<link rel="stylesheet" href="../assets/css/xenon-components.css">
	<link rel="stylesheet" href="../assets/css/xenon-skins.css">
	<link rel="stylesheet" href="../assets/css/custom.css">

	<script src="../assets/js/jquery-1.11.1.min.js"></script>

    <script src="../pages/_common/js/JSCommonFunction.js"></script>
    
    <script src="../pages/_common/js/sendsms.js"></script>
    


	<style>

	@media print {
		html, body {
			height: auto;    
			margin-top: -8mm;
		}
	}

	a#a-load_more {
		text-align: center;
	}
	a#a-load_more center img {
		float: none;
		margin: auto;
	}

	#chat .chat-group#sms-inbox_unknown {
		padding-bottom: 70px;
	}
	button#btn-load_more {
		width: 100%;
		padding: 5px 0;
		color: #000;
		border: none;
		background: #FFF;
	}
	.dropdown-backdrop {
		display: none !important;
	}
	div#div-overlay p {
		text-align: center;
		background: #45a1ff;
		color: #FFF;
		padding: 10px;
		font-size: 18px;
		top: 0;
		position: absolute;
		width: 100%;
	}
	div#div-overlay {
		position: fixed;
		z-index: 1001;
		background: rgba(255, 255, 255, 0.5);
		width: 100%;
		height: 100%;
		top: 0;
		display:none;
	}
	div#div-loader img {
		position: absolute;
		top: 50%;
		transform: translateY(-50%);
		left: 0;
		right: 0;
		margin:auto;
	}
	</style>

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

    <script type="text/javascript">

		/*
		sessionStorage.name = resp.name;
		sessionStorage.role = resp.role;

		sessionStorage.main_menu_list = resp.main_menu_list;
		sessionStorage.user_dropdown_menu = resp.user_dropdown_menu;
		*/



		<!-- DOCUMENT READY FUNCTION -->
		//$(document).ready(function()
		jQuery(document).ready(function($)
		//$(window).load(function()
		{
			console.log("Clients Count 1: " + localStorage.clients_count);
			count_clients();

			//SET ASSIGNED MENU LIST ON THIS PAGE
			$('#main-menu').append(sessionStorage.main_menu_list);

			//SET ASSIGNED USER DROPDOWN MENU
			$('#user_drop-down-menu').append(sessionStorage.user_dropdown_menu);

			//ASSIGN NAME
			$('#user_name-top').html(sessionStorage.name);
			$('#user_name-right').html(sessionStorage.name);

			//ASSIGN DISPLAY ROLE
			$('#user_role-top').html(sessionStorage.role);
			
			//DECLARATION
			var conversation_phone = '';
			var bol_sms_connected = true;
			
			var recall_interval;
			
			var sms_unread_count = 0;
			var sms_unread_count_new = 0;
			
			
			//maintenance variables
			var countdownInterval;
			var maintenance = 0;
			
			
			
			//============================FUNCTIONS========================================================================
			//=============================================================================================================
			<!-- MENU LIST NAVIGATION -->

			$(".menu-list").click(function () {
				
				show_loader('right-content');

				var linkClicked = $(this).attr('href');

				document.location.hash = linkClicked;
				var pageRoot = linkClicked.replace('#page-', '');
				
				var nav = sessionStorage.user_type + '/' + pageRoot + '/';
				
				show_loading_bar({
					pct: 75,
					finish: function(pct)
					{

						$( "#right-content" ).load(nav, function() {

						  show_loading_bar(100);

						});


					}
				});
				

				
				return false;
				
				$.ajax({
				url: "load.php",
				method: 'POST',
				dataType: 'HTML', //html or json
				data: {
					"page-load": true,
					"page": pageRoot,
				},
					success: function(resp)
					{
						
						alert(resp);
						
						show_loading_bar({
							pct: 75,
							finish: function(pct)
							{

								$( "#right-content" ).load(resp, function() {

								  show_loading_bar(100);

								});


							}
						});

					}

				});

			});

			<!-- /MENU LIST NAVIGATION -->
							
			
			function test() {
				
				//DISABLE CHAT SMS
				//$('#sms-unread_count').html('Chat sms Disabled!');
				//$('#sms-inbox_unknown').html('<center>Chat sms Disabled!</center>');
				//return false;
				
				//console.log('TEST FUNCTION RUN');				
				
				setTimeout(function () {
					$.ajax({
						url: "_common/ajax/chatsms.php",
						type: "POST",
						data: {
							"sms-display_unread_count": true,
						},
						dataType: 'JSON',  
						success: function (resp) {
							
							sms_unread_count_new = Number(resp.sms_count);
							
							//DISPLAY new message count
							if (sms_unread_count_new != 0) {
								
								$('#sms-unread_count').html(sms_unread_count_new);
							
							}
							
							
														
							if (resp == "Chat sms disconnected!") {
								
								bol_sms_connected = false;
								
								//$('#sms-inbox_unknown').html('');
								//$('#sms-inbox_known').html('');
								
								return false;
							
							}
						
						
							
							if(sms_unread_count_new > sms_unread_count) {
								
								//sound SMS notification
								$.playSound('../pages/_common/other/soundnotif');
								
								
								//DISPLAY new message count
								$('#sms-unread_count').html(sms_unread_count_new);
								
								//PARSE phone array data
								var arr = JSON.parse(resp.sms_phones);
								
								//LOOP to each phone numbers array 
								jQuery.each( arr, function( i, val ) {
									
									
									//console.log(val);
									
									//remove phone
									$('#link_sms_view_conversation[phone="' + val + '"]').remove();
								});
								
								//append new message to top
								$('#sms-inbox_unknown').prepend(resp.sms_data);
								
								test();
															
							}
							
							sms_unread_count = sms_unread_count_new;
							
							
							 
						},
						//complete: test,
						error: function (resp) {
							
							$('#sms-unread_count').html('Chat sms Disconnected!');
							$('#sms-inbox_unknown').html('<center>Click Refresh button to reconnect!</center>');
							//alert('ERROR');
							
						
						},
						timeout: 1000 * 100 // sets timeout to 10 seconds
					});
				}, 1000 * 120);
			}
			
			
			//FILE SYNC FUNCTION
			function helper() {
				document.getElementById('loadarea').src = '../winscp/sync-file.php';
			}
			function kill() {
				document.getElementById('loadarea').src = '';
			}
			
			
			function check_session() {
				
				
				$.ajax({
					url: "_common/ajax/maintenance.php",
					method: 'POST',
					dataType: 'HTML', //html or json
					data: {
						"maintenance-session_check": true,
					},
						success: function(resp)
						{
							
						
							if (resp == "Session Expired!") {
								
								alert(resp);
								
								window.location.href = '../logout.php';	
								
							}
									
							
							
							
						}
		
					});
				
				
			}
			
			
			function check_maintenance() {
				
				
				if (maintenance == 0) {
					
					$.ajax({
					url: "_common/ajax/maintenance.php",
					method: 'POST',
					dataType: 'HTML', //html or json
					data: {
						"maintenance-store_check": true,
					},
						success: function(resp)
						{
										
							maintenance = Number(resp);
							
							if (maintenance == 1) {
								
							
								$('#maintenance-row').show();
								
								
								
								
							}
							
						}
		
					});
					
					
				}
				
				
			}
			
			
			function create_name_file () {
				
				$.ajax({
				url: "_common/ajax/admin/create_name_file.php",
				method: 'POST',
				dataType: 'HTML', //html or json
				data: {
					"test": true,		
				},
					success: function(resp)
					{									
					
						//init_table(resp,'tbl_item_list');
					
					}
				
				});
				
				
			}
			
			
			
			
			
			function load_recipient() {
				
				//return false;

				show_loader('tbl_contact');
	
				$.ajax({
				url: "_common/ajax/contact.php",
				method: 'POST',
				dataType: 'HTML', //html or json
				data: {
					"sms-display_contact": true,
				},
					success: function(resp)
					{
					
					
						//$('#tbl_contact').html(resp);
						
						init_table(resp,'tbl_contact');

						
						// Replace checkboxes when they appear
						var $state = $("#tbl_contact thead input[type='checkbox']");
						
						$("#tbl_contact").on('draw.dt', function()
						{
							cbr_replace();
							
							$state.trigger('change');
						});
						
						// Script to select all checkboxes
						$state.on('change', function(ev)
						{
							var $chcks = $("#tbl_contact tbody input[type='checkbox']");
							
							if($state.is(':checked'))
							{
								$chcks.prop('checked', true).trigger('change');
							}
							else
							{
								$chcks.prop('checked', false).trigger('change');
							}
						});
						
	
					}
	
				});
	
	
			}
						
			
			
			
			
			//LOAD UNKNOWN NUMBER INBOX
			function load_unknown_inbox(refresh = false) {
				
				//DISABLE CHAT SMS
				//$('#sms-unread_count').html('Chat sms Disabled!');
				//$('#sms-inbox_unknown').html('<center>Chat sms Disabled!</center>');
				//return false;


				
				/*
				$.ajax({
					url: "_common/ajax/chatsms.php",
					method: 'GET',
					dataType: 'HTML', //html or json
					data: {
					"sms-display_inbox_unknown": true,
					},
					xhrFields: {
						onprogress: function(e) {
							
							//alert(e);
							//alert($.trim(e.target.responseText));
							
							var temp1 = $.trim(e.target.responseText);
							
							//$('#sms-inbox_unknown').html(e.target.responseText);	
							
							//var count1 = (temp1.match(/-/g) || []).length;
							
							//$('#btn-export_csv_text').html("Processing: " + count1 + " row/s");
							
							
							//console.log(e);
							//console.log(e.target.responseText);
							//$("body").html(e.target.responseText);
							
							
							
						}
					},
					success: function(text) {
							//console.log(text);
							
							var fileName = text;
							
							var filterStr = fileName.replace(/\-/g, '');
							
							
							$('#sms-inbox_unknown').html(e.target.responseText);	
							
							
							//window.open('admin/setup-client/' + filterStr, '' );
							
							//window.location.href = 'admin/setup-client/' + filterStr;
							
							
							//$('#btn-export_csv_text').html("CSV Export");
							
							
							//console.log(filterStr);
							
							
							
							//$("body").html(text+"<h1>done!</h1>")
					}
				});
				*/

				var limit_start = $('a#link_sms_view_conversation').length;
				if(refresh == true){
					limit_start = 0;
				}
				if(limit_start ==0) {
					show_loader('sms-inbox_unknown');
				}else{
					show_loader('a-load_more');
				}

				$.ajax({
				url: "_common/ajax/chatsms.php",
				method: 'POST',
				dataType: 'HTML', //html or json
				data: {
					"sms-display_inbox_unknown": true,
					"limit_start": limit_start
				},
					success: function(resp)
					{
	
											
						if (resp == "Chat sms disconnected!") {
							
							$('#sms-inbox_unknown').html('');
							
							
						} else {

							if(limit_start ==0){
								$('#sms-inbox_unknown').html(resp);
							}else{
								$('#a-load_more').remove();
								$(resp).appendTo('#sms-inbox_unknown');
							}

							//CALL TEST FUNCTION
							test();
							
							
						}
	
					},
					error: function () {
							
							$('#sms-unread_count').html('Chat sms Timeout!');	
							$('#sms-inbox_unknown').html('<center>Click Refresh button to reconnect!</center>');				
							
						
					},
					timeout: 1000 * 100 // sets timeout to 10 seconds

				});
	
	
			}
			
			
			
			// LOAD CONVESATION DATA
			function load_conversation_data(phone) {
				
				show_loader('sms-conversation_box');
	
				$.ajax({
				url: "_common/ajax/chatsms.php",
				method: 'POST',
				dataType: 'HTML', //html or json
				data: {
					"sms-display_conversation_data": true,
					phone: phone
				},
					success: function(resp)
					{
						
						
						$('#sms-conversation_box').html(resp);
						
						
						$("#sms-conversation_box").scrollTop($("#sms-conversation_box")[0].scrollHeight);
	
					}
	
				});
				
				
			}
			
			
			
			//CHECK UNREAD MESSAGE COUNT
			function check_unread_count () {
				
				$.ajax({
				url: "_common/ajax/chatsms.php",
				method: 'POST',
				dataType: 'HTML', //html or json
				data: {
					"sms-display_unread_count": true,
				},
					success: function(resp)
					{
						
						
						
						$('#sms-unread_count').html(resp);
						
						
						if (resp == "Chat sms disconnected!") {
							
							bol_sms_connected = false;
							
							$('#sms-inbox_unknown').html('');
							$('#sms-inbox_known').html('');
							
							clearInterval(recall_interval);	
							
							return false;
							
						}
						
						
						//sound SMS notification
						sms_unread_count_new = Number(resp);
						
						if(sms_unread_count_new > sms_unread_count) {
							
							$.playSound('../pages/_common/other/soundnotif');
							//refresh inbox
							//load_known_inbox();
							load_unknown_inbox();
														
						}
						
						sms_unread_count = sms_unread_count_new;
						
						
						
	
					}
	
				});
				
				
			}
			
			
			function sendsms1(number, message, callback) {
			
					var sender = sessionStorage.name;


					toastr.warning("Sending SMS message, please wait!", "SMS NOTIFICATION!", opts);
					
					$.ajax({
					url: "_common/ajax/chatsms.php",
					method: 'POST',
					dataType: 'JSON', //html or json
					data: {
						"sendsms": true,
						number: number,
						message: message,
						sender: sender
					},
						success: function(resp) {



							toastr.info("SMS Message Sent", "SMS NOTIFICATION!", opts);

							return callback(resp);
							
							
						
						},
                        error: function (resp)  {

					    toastr.error("Error Sending SMS.", "SMS NOTIFICATION", opts);


                        }
					
					});
									
			}
				
			
			//=============================================================================================================
			//=============================================================================================================
			
			<!-- MAINTENANCE LINK SYSTEM UPDATE CLICK-->
			$("#maintenance-link_update").click(function () {

				$.ajax({
					url: "_common/ajax/maintenance.php",
					method: 'POST',
					dataType: 'HTML', //html or json
					data: {
						"maintenance-set_maintenance_value": true,
					},
						success: function(resp)
						{
							
							$('#modal-file_sync').modal('show', {backdrop: 'static'});
							helper();
							
							
						}
		
					});


				
			})
			
			
			
			
			<!-- SEND BUTTON CLICK-->
			$("#btn-sms_send").click(function () {
			
				var number = $('#txt-sms_recipient').val();
				var message = $('#txt-sms_message').val();
				
				sendsms1(number,message, function (resp) {



                    document.getElementById("txt-sms_recipient").value = "";
                    document.getElementById("txt-sms_message").value = "";


                });
				

				
			});	
			
			
			
			<!-- SEND BUTTON CLICK (CONVERSATION BOX)-->
			$("#btn-sms_send_convesationbox").click(function () {
			
				var number = conversation_phone;
				var message = $('#txt-sms_message_convesationbox').val();
				
				sendsms1(number,message, function (resp) {

                    document.getElementById("txt-sms_message_convesationbox").value = "";
                    load_conversation_data(number);

                });
				

				
			});	
			
			
			
			<!-- CREATE SMS CLICK-->
			$("#sms-create").click(function () {
				
				
				$('#modal-sms_create').modal('show', {backdrop: 'static'});
				
			});
			
			
			<!-- BROWSE RECIPIENT CLICK-->
			$("#btn-sms_browse_recipient").click(function () {
				
				
				$('#modal-client_list').modal('show', {backdrop: 'static'});
				
			});
			
			
			
			<!-- BUTTON SELECT RECIPIENT CLICK-->
			$("#btn-select_recipient").click(function () {
				
				//alert('BUTTON CLICK');
				
				var recipient = [];
				$.each($("input[name='recipient']:checked"), function(){            
					recipient.push($(this).val());
				});
				//alert("My favourite sports are: " + recipient.join(", "));
				
				document.getElementById("txt-sms_recipient").value = document.getElementById("txt-sms_recipient").value + "," + recipient.join(",");
				
				$('#modal-sms_create').modal('show', {backdrop: 'static'});
				
			});
			
			
			<!-- LINK SELECT CONTACT NUMBER -->
			$("#tbl_contact").on("click", "#link_select", function() {
	
	
				var contact_num = $(this).attr('contact_num');

				document.getElementById("txt-sms_recipient").value = document.getElementById("txt-sms_recipient").value + "," + contact_num;
	
	
				$('#modal-sms_create').modal('show', {backdrop: 'static'});
				
				
	
			});
			
			
			
			<!-- INBOX LINK CLICK -->
			$("#inbox").on("click", "#link_sms_view_conversation", function() { 
			
			
				var contact = $(this).attr('contact');
				var phone = $(this).attr('phone');
				
				
				//remove on-line class (new message icon)
				$('#link_sms_view_conversation[phone="' + phone + '"] > span').removeClass('is-online');
				
			
				$('#sms-display_name').html(contact);
				
				conversation_phone = phone;
				
				load_conversation_data(phone);
				
				$('#sms-chat_conversation').addClass('is-open');				
			
			});
			
			
			//close convo box
			$(".conversation-close").on('click', function(ev)
			{
				
				$('#sms-chat_conversation').removeClass('is-open');
				
			});
			
			
			
			
			<!-- BROWSE RECIPIENT CLICK-->
			$("#sms_link_inbox_refresh").click(function () {
				
				
				$('#sms-unread_count').html('');
				
				//load_known_inbox();
				load_unknown_inbox(true);
				
				bol_sms_connected = true;
				//recall();
				
				
			});

			$("div#sms-inbox_unknown").on("click", "#btn-load_more", function() {
				console.log('more');
				load_unknown_inbox();
			});
			
			
			<!-- CONVERSATION LINK DELETE CLICK -->
			$("#sms-chat_conversation").on("click", "#sms-link_delete", function() { 
			
				var id = $(this).attr('value');
				
				
				if (confirm('Are you sure you want to delete selected message?')) {
					
					$.ajax({
					url: "_common/ajax/chatsms.php",
					method: 'POST',
					dataType: 'JSON', //html or json
					data: {
						"sms-delete_sms_data": true,
						id: id
					},
						success: function(resp)
						{
							
							
							if (resp.status === "success") {
							
								toastr.info("Selected sms data has been removed", "SMS DATA DELETED!", opts);
								$('a#sms-link_delete[value="' + id + '"]').parents('li').remove();
								
								//load_conversation_data(conversation_phone);
								
							} else {
								
								toastr.error(resp.message, "ERROR!", opts);
								
							}
							
							
							
							
		
						}
		
					});
					
					
					
				}
				
								
				
			
			});
			
			
			

			
			
			//CALL CREATE FILE NAME
			//create_name_file();


			load_recipient();
			
			
			//load_known_inbox();
			load_unknown_inbox();
			
			//recall();
			
			
			$('#maintenance-row').hide();
			
			



		});
		<!-- /DOCUMENT READY FUNCTION -->


	</script>


</head>
<body class="page-body">
	<div id="div-overlay">
		<div id="div-loader"></div>
		<p>Syncing data from central database, please wait.</p>
	</div>

	<div class="settings-pane">

		<a href="#" data-toggle="settings-pane" data-animate="true">
			&times;
		</a>

		<div class="settings-pane-inner">

			<div class="row">

				<div class="col-md-4">

					<div class="user-info">

						<div class="user-image">
							<a href="#page-profile">
								<img src="../assets/images/user-2.png" class="img-responsive img-circle" />
							</a>
						</div>

						<div class="user-details">

							<h3>
								<span id = "user_name-top">Full Name</span>
							</h3>

							<p class="user-title"> <span id="user_role-top">User Role</span> </p>

							<div class="user-links" class="">
								<a href="#page-profile" class="btn btn-primary menu-list">Profile</a>
								<a href="../logout.php" class="btn btn-danger">Logout</a>
							</div>

						</div>

					</div>

				</div>

				<div class="col-md-8 link-blocks-env">

					<div class="links-block right-sep">

                    	<div id="user_links_block1">

                        </div>

    				</div>

					<div class="links-block left-sep">

                        <div id="user_links_block2">

                        </div>

					</div>

				</div>

			</div>

		</div>

	</div>
	
    <!--
	<nav class="navbar horizontal-menu  navbar-minimal">
    -->
    <nav class="navbar horizontal-menu navbar-fixed-top navbar-minimal">
    <!-- set fixed position by adding class "navbar-fixed-top" -->

		<div class="navbar-inner">

			<!-- Navbar Brand -->
			<div class="navbar-brand">
				<a href="dashboard-1.html" class="logo">
					<img src="../assets/images/logo-white-bg@2x.png" width="80" alt="" class="hidden-xs" />
					<img src="../assets/images/logo@2x.png" width="80" alt="" class="visible-xs" />
				</a>
				<a href="#" data-toggle="settings-pane" data-animate="true">
					<i class="linecons-cog"></i>
				</a>
			</div>

			<!-- Mobile Toggles Links -->
			<div class="nav navbar-mobile">

				<!-- This will toggle the mobile menu and will be visible only on mobile devices -->
				<div class="mobile-menu-toggle">
					<!-- This will open the popup with user profile settings, you can use for any purpose, just be creative -->
					<a href="#" data-toggle="settings-pane" data-animate="true">
						<i class="linecons-cog"></i>
					</a>

					<!-- data-toggle="mobile-menu-horizontal" will show horizontal menu links only -->
					<!-- data-toggle="mobile-menu" will show sidebar menu links only -->
					<!-- data-toggle="mobile-menu-both" will show sidebar and horizontal menu links -->
					<a href="#" data-toggle="mobile-menu-horizontal">
						<i class="fa-bars"></i>
					</a>
				</div>

			</div>

			<div class="navbar-mobile-clear"></div>

			<!-- main menu -->

			<ul class="navbar-nav click-to-expand" id="main-menu">



			</ul>


			<!-- notifications and other links -->

			<ul class="nav nav-userinfo navbar-right">
				
				<li class="dropdown user-profile">
					<a href="#" data-toggle="dropdown">
						<img src="../assets/images/user-1.png" alt="user-image" class="img-circle img-inline userpic-32" width="28" />
						<span>
                            <b id="user_name-right"></b>
                            <i class="fa-angle-down"></i>
                        </span>
					</a>


					<ul class="dropdown-menu user-profile-menu list-unstyled" id="user_drop-down-menu">


					</ul>

				</li>
                
                <li>
                    <a href="#" data-toggle="chat">
                        <i class="fa-comments-o"></i>
                        <span class="badge badge-green" id="sms-unread_count"></span>
                    </a>
                </li>
                

			</ul>

		</div>

	</nav>

	<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->

		<div class="main-content">

        	<div class="page-title">

				<div class="title-env">
					<h1 class="title" id="page-title">Updates</h1>
					<p class="description" id="page-description">What's new in TRAMAN</p>
				</div>

			</div>
            
            
            <div class="row hidden-print" id="maintenance-row">
								
                <div class="col-md-8" >
                    <div class="alert alert-warning">
                        <strong>Warning!</strong> 
                        Your local TRAMAN is outdated. <a href="#" id="maintenance-link_update">Click here</a> to update your system.
                    </div>
                    
                </div>
                
                <!--
                <div class="col-md-6">
                    <div class="alert alert-info" id="maintenance-alert-info">
                         <a href="#" id="maintenance-link-start">Click here!</a> to start the system update.
                    </div>
                </div>
                -->
                
            </div>



            <div id="right-content">

                <div class="panel panel-headerless">
                    <div class="panel-body">

                        <ul class="highlights list-unstyled">
                            <li class="version">
                                <h4>UPDATES</h4>
                            </li>
                            <li>
                                <a href="#" class="entry-img">
                                    <img src="../assets/update-images/icons8-Countdown-80.png" />
                                </a>

                                <div class="entry-description">
                                    <h3>Fast Pages and Data Loads</h3>
                                    <p>We hate waiting, so we've optimized all the datas and pages</p>
                                </div>
                            </li>

                            <li>
                                <a href="#" class="entry-img">
                                    <img src="../assets/update-images/icons8-Widescreen-80.png" />
                                </a>

                                <div class="entry-description">
                                    <h3>Wider Working Place</h3>
                                    <p>Changing Views and navigations to maximized working place </p>
                                </div>
                            </li>

                            <li>
                                <a href="#" class="entry-img">
                                    <img src="../assets/update-images/icons8-Streaming-80.png" />
                                </a>

                                <div class="entry-description">
                                    <h3>Load Indicator</h3>
                                    <p>Show a "Loading Indicator" on every page navigation and data table load</p>
                                </div>
                            </li>

                            <li>
                                <a href="#" class="entry-img">
                                    <img src="../assets/update-images/icons8-Maintenance-80.png" />
                                </a>

                                <div class="entry-description">
                                    <h3>Bug Fixes</h3>
                                    <p>Minor Bug Fixed on Input and Output</p>
                                </div>
                            </li>

                        </ul>

                    </div>
                </div>



            </div>
            
            
            <!-- Main Footer -->
			<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
			<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
			<!-- Or class "fixed" to  always fix the footer to the end of page -->
			<footer class="main-footer sticky footer-type-1 hidden-print">
				
				<div class="footer-inner">
				
					<!-- Add your copyright text here -->
					<div class="footer-text">
						&copy; 2015 
						<strong>TRAMAN | </strong> 
						theme by <a href="http://laborator.co" target="_blank"><strong>Laborator</strong></a> | System Developed by <a href="http://http://www.naotech.com.ph/" target="_blank"> <strong>Naotech Incorporated</strong></a>
					</div>
					
					
					<!-- Go to Top Link, just add rel="go-top" to any link to add this functionality -->
					<div class="go-up">
					
						<a href="#" rel="go-top">
							<i class="fa-angle-up"></i>
						</a>
						
					</div>
					
				</div>
				
			</footer>
		</div>
	
			
		<div id="chat" class="fixed hidden-print"><!-- start: Chat Section -->
			
			<div class="chat-inner">
			
				
				<h2 class="chat-header">
					<a  href="#" class="chat-close" data-toggle="chat">
						<i class="fa-plus-circle rotate-45deg"></i>
					</a>
					
					SMS Messages
                    
                    
				</h2>
                
                <div id="inbox">
                
                <div class="chat-group">
                	
                    <!--
                    <a href="#" id="sms-create">
                    <strong>
                    <i class="fa linecons-pencil"></i>
                    Create SMS
                    </strong>
                    </a>
                    -->
                
                
					<a href="#" id="sms_link_inbox_refresh">
                    <strong>
                    <i class="fa fa-refresh"></i>
                    Refresh
                    </strong>
                    </a>
                    
                   
                    
                   
                    
				</div>
                
               
				<div class="chat-group" id="sms-inbox_known">
                	<!--
					<strong>New</strong>
                    -->
                                      
                                    
				</div>
                
                
                <div class="chat-group" id="sms-inbox_unknown">
					<!--
                    <strong>Done</strong>
                    -->
					
                    
				</div>
                
                </div>
				
				
				
			
			</div>
			
			<!-- conversation template -->
			<div class="chat-conversation" id="sms-chat_conversation">
				
				<div class="conversation-header">
					<a href="#" class="conversation-close">
						&times;
					</a>
					<span class="display-name" id="sms-display_name"></span> 
					
				</div>
				
				<ul class="conversation-body" id="sms-conversation_box">	
					
                    
                    
				</ul>
				
                <div class="input-group">
                    <input type="text" class="form-control no-right-border form-focus-secondary" id="txt-sms_message_convesationbox">

                    <span class="input-group-btn">
                        <button class="btn btn-secondary" type="button" id="btn-sms_send_convesationbox" value="">
                        <i class="linecons-paper-plane"></i>
                        
                        Send
                        
                        </button>
                    </span>
                </div>
				
					
				
				
			</div>
			
		<!-- end: Chat Section -->
		</div>
	
	</div>
    
    
    <div class="footer-sticked-chat hidden-print" id="sms-create">
		
		<ul class="chat-conversations list-unstyled">
	
			<li id="sample-chat-window">
				<!-- User Info Link -->
				<a href="#"  class="chat-user">
					<span class="user-status is-online"></span>
					<b>CREATE SMS</b>
				</a>
	
				<span class="badge badge-red">Click me!</span>
			</li>
	
		</ul>

	
	<!-- End: Footer Sticked Chat -->
	</div>
    
    
    
    
    <!-- MODAL CREATE SMS-->
	<div class="modal fade" id="modal-sms_create" data-backdrop="static">
		<div class="modal-dialog" style="width: 50%;">
			<div class="modal-content">

				<div class="modal-header">
					<h4 class="modal-title">Send new SMS</h4>
                    <input type="text" id="txt-view-id" hidden>

				</div>

				<div class="modal-body">

					<div class="row">

						<div class="col-md-12">

							<div class="form-group">
								<label for="field-1" class="control-label">Recipient</label>
								
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <button class="btn btn-info" type="button" id="btn-sms_browse_recipient" data-dismiss="modal">+</button>
                                    </span>
    
                                    <input type="text" class="form-control no-left-border form-focus-info" placeholder="Enter cellphone# here.." id="txt-sms_recipient">
                                </div>

								
							</div>

						</div>

					</div>
                    
                    
                    <div class="row">

						<div class="col-md-12">

							<div class="form-group">
								<label for="field-1" class="control-label">Message</label>

								<textarea class="form-control autogrow" cols="5" id="txt-sms_message" placeholder="Messages.." style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 200px;"></textarea>
							</div>

						</div>

					</div>



				</div>

				<div class="modal-footer">

                	<button type="button" class="btn btn-secondary" id="btn-sms_send" data-dismiss="modal">
                    
                     <i class="fa-send-o"></i>
                     <span>Send</span>
                    
                    </button>
					<button type="button" class="btn btn-danger" id="btn_close_modal" data-dismiss="modal">Close</button>

				</div>
			</div>
		</div>
	</div>
    
    
    
    <!-- MODALS -->
    <div class="modal fade" id="modal-client_list" data-backdrop="static">
		<div class="modal-dialog" style="width: 70%;">
			<div class="modal-content">

				<div class="modal-header">

                	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="btn-hidden-close_modal"></button>

					<h4 class="modal-title">Select Client</h4>

				</div>

				<div class="modal-body">

					<div class="row">

                        <div class="col-md-12">

							<table class="table table-hover members-table middle-align" id="tbl_contact">


                    		</table>


							</div>

						</div>

					</div>

                    <div class="modal-footer">
                    
                    	<button type="button" class="btn btn-info" id="btn-select_recipient" data-dismiss="modal">Select Recipient</button>
                    
						<button type="button" class="btn btn-danger" id="btn_close_modal" data-dismiss="modal">Close</button>
                        
					</div>

				</div>

			</div>
		</div>
	</div>
    
    
    <div class="modal fade" id="modal-file_sync" data-backdrop="static">
		<div class="modal-dialog" style="width: 70%;">
			<div class="modal-content">

				<div class="modal-header">

                	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="btn-hidden-close_modal"></button>

					<h4 class="modal-title">System Update</h4>

				</div>

				<div class="modal-body">

					<div class="row">

                        <div class="col-md-12">

							<iframe id="loadarea" style=" height: 300px; width: 900px;"></iframe>

							</div>

						</div>

					</div>

                    <div class="modal-footer">
                    	<!--
                    	<button type="button" class="btn btn-info" onclick="helper()">UPDATE START</button>
                         -->                
					</div>

				</div>

			</div>
		</div>
	</div>
	




	<!-- Bottom Scripts -->
	<script src="../assets/js/bootstrap.min.js"></script>
	<script src="../assets/js/TweenMax.min.js"></script>
	<script src="../assets/js/resizeable.js"></script>
	<script src="../assets/js/joinable.js"></script>
	<script src="../assets/js/xenon-api.js"></script>
	<script src="../assets/js/xenon-toggles.js"></script>
	<script src="../assets/js/moment.min.js"></script>



	<!-- TABLE SCRIPTS -->
    <script src="../assets/js/datatables/js/jquery.dataTables.min.js"></script>
	<script src="../assets/js/datatables/dataTables.bootstrap.js"></script>
	<script src="../assets/js/datatables/yadcf/jquery.dataTables.yadcf.js"></script>
	<script src="../assets/js/datatables/tabletools/dataTables.tableTools.min.js"></script>
    <!-- TABLE STYLE -->
    <link rel="stylesheet" href="../assets/js/datatables/dataTables.bootstrap.css">


    <!-- TO A STRING SCRIPT -->
    <script src="../assets/js/toastr/toastr.min.js"></script>

    <!-- INPUT MASK SCRIPT -->
    <script src="../assets/js/inputmask/jquery.inputmask.bundle.js"></script>

    <!-- Imported styles on this page -->
	<link rel="stylesheet" href="../assets/js/fullcalendar/fullcalendar.min.css">
    <script src="../assets/js/fullcalendar/fullcalendar.min.js"></script>
	<script src="../assets/js/jquery-ui/jquery-ui.min.js"></script>

    <!-- OPTION SCRIPT -->
    <link rel="stylesheet" href="../assets/js/select2/select2.css">
	<link rel="stylesheet" href="../assets/js/select2/select2-bootstrap.css">
    <script src="../assets/js/select2/select2.min.js"></script>
    <script src="../assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>

    <!-- PICKER SCRIPT -->
    <script src="../assets/js/daterangepicker/daterangepicker.js"></script>
	<script src="../assets/js/datepicker/bootstrap-datepicker.js"></script>
	<script src="../assets/js/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="../assets/js/inputmask/jquery.inputmask.bundle.js"></script>


    <!-- CHART AND WIDGET SCRIPT -->
    <script src="../assets/js/xenon-widgets.js"></script>
	<script src="../assets/js/devexpress-web-14.1/js/globalize.min.js"></script>
	<script src="../assets/js/devexpress-web-14.1/js/dx.chartjs.js"></script>

    <!-- TYPE HEAD SCRIPT -->
    <script src="../assets/js/typeahead.bundle.js"></script>
    
    <!-- BARCODE  -->  
    <script  src="../assets/js/barcode/jquery-barcode.js"></script>

    <!--QRCODE -->
    <script  src="../assets/js/qrcode/jquery.qrcode.min.js"></script>
      

	<!-- JavaScripts initializations and stuff -->
	<script src="../assets/js/xenon-custom.js"></script>
    
    
    <!-- Validation Script -->
	<script src="../assets/js/jquery-validate/jquery.validate.min.js"></script>
    
    <!-- ICHECK -->
	<script src="../assets/js/icheck/icheck.min.js"></script>

	<!-- PLAY SOUND -->
    <script src="../assets/js/jquery-play-sound-master/jquery.playSound.js"></script>
    
    
    <!-- SMS Chat App STYLES AND JS 
    <link rel="stylesheet" href="../assets/SMSChatApp/css/reset.css">
	<link rel="stylesheet" href="../assets/SMSChatApp/css/style.css">
    
    <!--
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
  
	<script src="../assets/SMSChatApp/js/index.js"></script>
    -->







</body>
</html>
