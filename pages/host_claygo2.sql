-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 05, 2016 at 10:21 AM
-- Server version: 5.6.24
-- PHP Version: 5.5.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `host_claygo2`
--

DELIMITER $$
--
-- Procedures
--
DROP PROCEDURE IF EXISTS `SP-admin-add_client_credit`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-add_client_credit`(

IN id VARCHAR(255),
IN amount DECIMAL(10,2),
IN user_id VARCHAR(255)


)
BEGIN 

#GENERATE RANDOM UNIQUE ID
SELECT CONCAT(NOW(), SUBSTRING(MD5(RAND()) FROM 1 FOR 10), UNIX_TIMESTAMP()) INTO @random;


INSERT INTO pos_client_credit (credit_id, client_id, amount, user_id)
VALUES (@random, id, amount, user_id);

END$$

DROP PROCEDURE IF EXISTS `SP-admin-add_item`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-add_item`(

IN item_name VARCHAR(255)

)
BEGIN 

INSERT INTO pos_item (item_name) 
VALUES (item_name);

END$$

DROP PROCEDURE IF EXISTS `SP-admin-add_main_service`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-add_main_service`( 

IN service_name VARCHAR(255)

)
BEGIN 

INSERT INTO pos_main_service (name) VALUES (service_name);


END$$

DROP PROCEDURE IF EXISTS `SP-admin-add_service`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-add_service`( 

IN service_main_id VARCHAR(100),
IN name VARCHAR(50),
IN record_datetime Varchar(50),
IN status varchar (50)

)
BEGIN 


INSERT INTO pos_main_service (service_main_id, name,record_datetime, status) 
VALUES (service_main_id, name, record_datetime, 'active');

END$$

DROP PROCEDURE IF EXISTS `SP-admin-add_service_category`$$
CREATE DEFINER=`teejay.pimentel`@`%` PROCEDURE `SP-admin-add_service_category`(

IN service_main_id VARCHAR(255),
IN name VARCHAR(255)

)
BEGIN

INSERT INTO pos_category_service (service_main_id, name) 
VALUES (service_main_id, name);

END$$

DROP PROCEDURE IF EXISTS `SP-admin-add_service_price`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-add_service_price`( 

IN category_id VARCHAR(255),
IN code VARCHAR(50),
IN name VARCHAR(50),
IN price VARCHAR(50),
IN unit VARCHAR(50),
IN user_id VARCHAR(50)

)
BEGIN 


INSERT INTO pos_service_and_price (category_id, code, name, price, unit, add_by_user_id) 
VALUES (category_id, code, name, price, unit, user_id);

END$$

DROP PROCEDURE IF EXISTS `SP-admin-add_special`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-add_special`(



IN name  VARCHAR(100),
IN value  VARCHAR(100),
IN special_type  VARCHAR(100),
IN value_type  VARCHAR(100),
IN user_id  VARCHAR(100)

)
BEGIN 

INSERT INTO pos_special (name, value, special_type, value_type, add_by_user_id) 
VALUES (name, value, special_type, value_type, user_id);

END$$

DROP PROCEDURE IF EXISTS `SP-admin-add_store`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-add_store`(


IN store_code  VARCHAR(100),
IN store_name VARCHAR(100),
IN address VARCHAR(100),
IN address_city VARCHAR(100),
IN num_tel VARCHAR(100),
IN num_cell VARCHAR(100),
IN user_id VARCHAR(100),
IN surcharge_store VARCHAR(100),
IN surcharge_pickup VARCHAR(100)

)
BEGIN 


INSERT INTO pos_store (store_code, store_name, address, address_city, num_tel, num_cell, add_by_user_id, surcharge_store, surcharge_pickup) 
VALUES (store_code, store_name, address, address_city, num_tel, num_cell, user_id, surcharge_store, surcharge_pickup);

END$$

DROP PROCEDURE IF EXISTS `SP-admin-add_user_admin`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-add_user_admin`(

IN username  VARCHAR(100),
IN password VARCHAR(100),
IN first_name VARCHAR(100),
IN middle_name VARCHAR(100),
IN last_name VARCHAR(100)


)
BEGIN 


INSERT INTO pos_user (store_id, username, password, first_name, middle_name, last_name, user_type) 
VALUES ('0', username, password, first_name, middle_name, last_name, 'ADMIN')

;




END$$

DROP PROCEDURE IF EXISTS `SP-admin-add_user_store`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-add_user_store`(

IN username  VARCHAR(100),
IN password VARCHAR(100),

IN first_name VARCHAR(100),
IN middle_name VARCHAR(100),
IN last_name VARCHAR(100),
IN store_id VARCHAR(100)


)
BEGIN 

INSERT INTO pos_user (store_id, username, password, first_name, middle_name, last_name, user_type) 
VALUES (store_id, username, password, first_name, middle_name, last_name, 'STORE');

END$$

DROP PROCEDURE IF EXISTS `SP-admin-approve_void_transaction`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-approve_void_transaction`(

IN id VARCHAR(100),
IN user_id VARCHAR(100),
IN status VARCHAR(100)

)
BEGIN 

UPDATE pos_trans_master 
SET status = status 
WHERE trans_master_id = id; 


INSERT INTO pos_trans_trail (trans_master_id, status, user_store_id, user_admin_id) 

VALUES (id, status, 'NA', user_id);

END$$

DROP PROCEDURE IF EXISTS `SP-admin-daily_report`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-daily_report`(

IN datefrom date,
IN dateto date



)
BEGIN


SET SESSION group_concat_max_len = 1000000;
SET @header = NULL;
SET @query = NULL;


SELECT 
GROUP_CONCAT(DISTINCT
CONCAT('SUM(CASE WHEN pos_trans_detail.service_id =\'', pos_trans_detail.service_id, '\' THEN TRIM(TRAILING \'.\' FROM TRIM(TRAILING \'0\' from pos_trans_detail.unit_count)) ELSE 0 END) AS \'', code, '\'')
) INTO @header
FROM pos_service_and_price 

INNER JOIN pos_trans_detail 
ON pos_service_and_price.service_id = pos_trans_detail.service_id

WHERE code != ''
;


SET @query = CONCAT(
'
SELECT 
pos_trans_master.trans_master_id as \'ID\',
CONCAT(pos_client.first_name, \' \', pos_client.middle_name, \' \', pos_client.last_name) AS \'Customer\',
pos_store.store_name AS \'Location\',
pos_trans_master.record_datetime AS \'Date Recieve\',

pos_trans_master.due_date AS \'Date Due\',

pos_trans_master.total AS \'Payment\',


CASE WHEN
pos_trans_master.status = \'complete - paid\' 
THEN \'paid\' ELSE \'unpaid\' 
END AS \'Paid/Unpaid\',



', @header, '




FROM pos_trans_master 

LEFT JOIN pos_store 
ON pos_trans_master.store_id = pos_store.store_id 


LEFT JOIN pos_client 
ON pos_trans_master.client_id = pos_client.client_id 

LEFT JOIN pos_trans_detail 
ON pos_trans_master.trans_master_id = pos_trans_detail.trans_master_id 

LEFT JOIN pos_service_and_price 
ON pos_trans_detail.service_id = pos_service_and_price.service_id

WHERE 
(pos_trans_master.record_date BETWEEN \'',  datefrom, '\' AND \'', dateto, '\') 
AND pos_trans_master.status NOT LIKE \'%cancelled%\' 
AND pos_trans_master.status NOT LIKE \'%void%\'


GROUP BY pos_trans_master.trans_master_id

'


); 


PREPARE stmt FROM @query;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

#SELECT @query;



END$$

DROP PROCEDURE IF EXISTS `SP-admin-dashboard-display_customer_sales_analytics`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-dashboard-display_customer_sales_analytics`(

IN monthNow VARCHAR(10),
IN yearNow VARCHAR(10)


)
BEGIN 

SELECT 


SUM(CASE WHEN MONTH(pos_client.add_datetime) = monthNow AND YEAR(pos_client.add_datetime) = yearNow THEN 1 ELSE 0 END) AS 'client_count_new',
SUM(CASE WHEN MONTH(pos_client.add_datetime) != monthNow AND YEAR(pos_client.add_datetime) != yearNow THEN 1 ELSE 0 END) AS 'client_count_existing',
COUNT(pos_client.client_id) AS 'client_count_total',


SUM(
CASE 
WHEN 
(MONTH(pos_client.add_datetime) = monthNow AND YEAR(pos_client.add_datetime) = yearNow)
AND pos_trans_master.status = 'complete - paid'
THEN pos_trans_master.total
ELSE 0 
END
) AS 'client_paid_sales_new',


SUM(
CASE 
WHEN 
(MONTH(pos_client.add_datetime) != monthNow AND YEAR(pos_client.add_datetime) != yearNow)
AND pos_trans_master.status = 'complete - paid'
THEN pos_trans_master.total
ELSE 0 
END
) AS 'client_paid_sales_existing',

SUM(
CASE 
WHEN 
pos_trans_master.status = 'complete - paid'
THEN pos_trans_master.total
ELSE 0 
END
) AS 'client_paid_sales_total',

###########
SUM(
CASE 
WHEN 
(MONTH(pos_client.add_datetime) = monthNow AND YEAR(pos_client.add_datetime) = yearNow)
AND pos_trans_master.status = 'complete - unpaid'
THEN pos_trans_master.total

ELSE 0 
END
) AS 'client_unpaid_sales_new',



SUM(
CASE 
WHEN 
(MONTH(pos_client.add_datetime) != monthNow AND YEAR(pos_client.add_datetime) != yearNow)
AND pos_trans_master.status = 'complete - unpaid'
THEN pos_trans_master.total
ELSE 0 
END
) AS 'client_unpaid_sales_existing',


SUM(
CASE 
WHEN 
pos_trans_master.status = 'complete - unpaid'
THEN pos_trans_master.total
ELSE 0 
END
) AS 'client_unpaid_sales_total'

FROM pos_client 

LEFT JOIN pos_trans_master 
ON pos_client.client_id = pos_trans_master.client_id;


END$$

DROP PROCEDURE IF EXISTS `SP-admin-dashboard-display_month_daily_sales_count`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-dashboard-display_month_daily_sales_count`(

IN datefrom date,
IN dateto date

)
BEGIN 


SELECT 
DATE_FORMAT(pos_trans_master.record_date,'%b-%d') AS 'day',
SUM(pos_trans_master.total) AS 'total',
SUM(CASE WHEN pos_trans_master.status = 'complete - paid' THEN pos_trans_master.total ELSE 0 END) AS 'total_paid',
SUM(CASE WHEN pos_trans_master.status = 'complete - unpaid' THEN pos_trans_master.total ELSE 0 END) AS 'total_unpaid'

FROM pos_trans_master 

WHERE status LIKE '%complete%' 
AND (pos_trans_master.record_date BETWEEN datefrom AND dateto)
GROUP BY DATE_FORMAT(pos_trans_master.record_date,'%d');

END$$

DROP PROCEDURE IF EXISTS `SP-admin-dashboard-display_month_sales_count`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-dashboard-display_month_sales_count`(

IN datefrom date,
IN dateto date

)
BEGIN 

SELECT 
SUM(pos_trans_master.total) AS 'total',
SUM(CASE WHEN pos_trans_master.status = 'complete - paid' THEN pos_trans_master.total END) AS 'total_paid',
SUM(CASE WHEN pos_trans_master.status = 'complete - unpaid' THEN pos_trans_master.total END) AS 'total_unpaid'

FROM pos_trans_master 

WHERE pos_trans_master.status LIKE '%complete%'  
AND (pos_trans_master.record_date BETWEEN datefrom AND dateto);

END$$

DROP PROCEDURE IF EXISTS `SP-admin-dashboard-display_store_contribution`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-dashboard-display_store_contribution`(

IN datefrom date,
IN dateto date


)
BEGIN

SELECT 
pos_store.store_code,
SUM(pos_trans_master.total) AS 'total',
SUM(CASE WHEN pos_trans_master.status = 'complete - paid' THEN pos_trans_master.total END) AS 'total_paid',
SUM(CASE WHEN pos_trans_master.status = 'complete - unpaid' THEN pos_trans_master.total 
END) AS 'total_unpaid'

FROM pos_trans_master 

LEFT JOIN pos_store 
ON pos_trans_master.store_id = pos_store.store_id

WHERE pos_trans_master.status LIKE '%complete%' 
AND (pos_trans_master.record_date BETWEEN datefrom AND dateto)
GROUP BY pos_store.store_id;


END$$

DROP PROCEDURE IF EXISTS `SP-admin-dashboard-hourly_sales`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-dashboard-hourly_sales`(

IN datefrom date,
IN dateto date

)
BEGIN 

SELECT 
DATE_FORMAT(record_datetime, '%l%p') AS 'hour_time',
SUM(pos_trans_master.total) AS 'total',

SUM(CASE WHEN pos_trans_master.status = 'complete - paid' THEN pos_trans_master.total END) AS 'total_paid',
SUM(CASE WHEN pos_trans_master.status = 'complete - unpaid' THEN pos_trans_master.total END) AS 'total_unpaid'
FROM pos_trans_master 

WHERE pos_trans_master.record_date = dateto

GROUP BY DATE_FORMAT(record_datetime, '%l%p');



END$$

DROP PROCEDURE IF EXISTS `SP-admin-delete_item`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-delete_item`(


IN id VARCHAR(50)

)
BEGIN


DELETE FROM pos_item 
WHERE item_id = id;

END$$

DROP PROCEDURE IF EXISTS `SP-admin-delete_main_service`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-delete_main_service`(

IN id VARCHAR(255)

)
BEGIN 


UPDATE pos_main_service 
SET status = 'inactive' 
WHERE service_main_id = id;




END$$

DROP PROCEDURE IF EXISTS `SP-admin-delete_service_category`$$
CREATE DEFINER=`teejay.pimentel`@`%` PROCEDURE `SP-admin-delete_service_category`(

IN id VARCHAR(255)

)
BEGIN


UPDATE pos_category_service 
SET status = 'inactive' 
WHERE category_id = id;

END$$

DROP PROCEDURE IF EXISTS `SP-admin-delete_service_price`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-delete_service_price`(

IN id VARCHAR(255)

)
BEGIN 

UPDATE pos_service_and_price 
SET status = 'inactive' 
WHERE service_id = id;

END$$

DROP PROCEDURE IF EXISTS `SP-admin-delete_special`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-delete_special`(

IN id  VARCHAR(100)

)
BEGIN

UPDATE pos_special 
SET status = 'inactive' 
WHERE special_id = id;


END$$

DROP PROCEDURE IF EXISTS `SP-admin-delete_store`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-delete_store`(

IN id  VARCHAR(100),
IN user_id  VARCHAR(100)

)
BEGIN 


UPDATE pos_store 
SET status = 'inactive', 
status_change_by_user_id = user_id,
status_change_datetime = CURRENT_TIMESTAMP 
WHERE store_id = id;

END$$

DROP PROCEDURE IF EXISTS `SP-admin-delete_user_admin`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-delete_user_admin`(




IN id  VARCHAR(100)

)
BEGIN 


UPDATE pos_user 
SET status = 'inactive',
status_change_datetime = CURRENT_TIMESTAMP 
WHERE user_id = id;

END$$

DROP PROCEDURE IF EXISTS `SP-admin-delete_user_store`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-delete_user_store`(

IN id  VARCHAR(100)

)
BEGIN 


UPDATE pos_user 
SET status = 'inactive',
status_change_datetime = CURRENT_TIMESTAMP 
WHERE user_id = id;

END$$

DROP PROCEDURE IF EXISTS `SP-admin-display_category_service_all`$$
CREATE DEFINER=`teejay.pimentel`@`%` PROCEDURE `SP-admin-display_category_service_all`()
BEGIN

SELECT 
pos_category_service.category_id,
pos_main_service.service_main_id,
pos_main_service.name AS 'Service Name',
pos_category_service.name AS 'Category Name'



FROM pos_category_service

LEFT JOIN pos_main_service 
ON pos_category_service.service_main_id = pos_main_service.service_main_id

WHERE pos_category_service.status = 'active';

END$$

DROP PROCEDURE IF EXISTS `SP-admin-display_client_all`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-display_client_all`()
BEGIN 

SELECT 


pos_client.client_id,
pos_client.contact_num1 AS 'Contact No#',
CONCAT(pos_client.first_name, ' ', pos_client.last_name) AS 'Customer Name',
pos_client.email AS 'Email',



CASE 
WHEN pos_client_credit.amount IS NULL THEN 0 
ELSE pos_client_credit.amount
END
AS 'Credit Balance',


pos_store.store_name AS 'Branch'

FROM pos_client 

LEFT JOIN 
(


SELECT 
pos_client_credit.client_id,
SUM(pos_client_credit.amount) AS 'Amount'
FROM pos_client_credit 

GROUP BY pos_client_credit.client_id

) AS pos_client_credit 

ON  pos_client.client_id = pos_client_credit.client_id


LEFT JOIN pos_user 
ON pos_client.add_by_user_id = pos_user.user_id 

LEFT JOIN pos_store 
ON pos_client.store_id = pos_store.store_id


WHERE pos_client.status = 'active';


END$$

DROP PROCEDURE IF EXISTS `SP-admin-display_client_credit_memo_history`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-display_client_credit_memo_history`(

IN id VARCHAR(255)

)
BEGIN 

SELECT 
amount AS 'Amount',
record_datetime AS 'Record Datetime'
FROM pos_client_credit 
WHERE client_id = id;

END$$

DROP PROCEDURE IF EXISTS `SP-admin-display_discount_all`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-display_discount_all`()
BEGIN 

SELECT 
special_id,
name AS 'Name',
value AS 'Value',
value_type AS 'Value Type' 
FROM pos_special 
WHERE special_type = 'discount' 
AND status = 'active';


END$$

DROP PROCEDURE IF EXISTS `SP-admin-display_item_all`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-display_item_all`()
BEGIN 

SELECT 
item_id AS 'ID',
item_name AS 'Item Name' 
FROM pos_item;

END$$

DROP PROCEDURE IF EXISTS `SP-admin-display_main_category_service_all`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-display_main_category_service_all`()
BEGIN 

SELECT 
service_main_id AS 'Service ID', 
name AS 'Service Name' 
FROM pos_main_service 
WHERE status = 'active'

;


END$$

DROP PROCEDURE IF EXISTS `SP-admin-display_main_service_all`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-display_main_service_all`()
BEGIN 

SELECT 
service_main_id AS 'Service ID', 
name AS 'Service Name' 
FROM pos_main_service 
WHERE status = 'active'

;


END$$

DROP PROCEDURE IF EXISTS `SP-admin-display_markup_all`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-display_markup_all`()
BEGIN 


SELECT 
special_id,
name AS 'Name',

value AS 'Value',
value_type AS 'Value Type' 
FROM pos_special 
WHERE special_type = 'MARK-UP' 
AND status = 'active';

END$$

DROP PROCEDURE IF EXISTS `SP-admin-display_paid_transaction_all`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-display_paid_transaction_all`(

IN start_date date,
IN end_date date

)
BEGIN 

SELECT 


pos_trans_master.trans_master_id,
pos_trans_master.central_code AS 'Central Code',
pos_trans_master.local_code AS 'Local Code',

pos_store.store_name AS 'Store',
CONCAT(pos_user.first_name, ' ', pos_user.last_name) AS 'Cashier',
CONCAT(pos_client.first_name, ' ', pos_client.last_name) AS 'Client',
DATE_FORMAT(pos_trans_master.record_datetime, '%Y-%m-%d') AS 'Date' 
FROM pos_trans_master 

LEFT JOIN pos_client 

ON pos_trans_master.client_id = pos_client.client_id 

LEFT JOIN pos_store 
ON pos_trans_master.store_id = pos_store.store_id 

LEFT JOIN pos_user 
ON pos_trans_master.user_id = pos_user.user_id

WHERE pos_trans_master.status = 'complete - paid'
AND (DATE_FORMAT(pos_trans_master.record_datetime, '%Y-%m-%d') BETWEEN start_date AND end_date)

;

END$$

DROP PROCEDURE IF EXISTS `SP-admin-display_pending_void_transaction_all`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-display_pending_void_transaction_all`()
BEGIN 

SELECT 
pos_trans_master.trans_master_id AS 'ID',

pos_store.store_name AS 'Store',
CONCAT(pos_user.first_name, ' ', pos_user.last_name) AS 'Cashier',

CONCAT(pos_client.first_name, ' ', pos_client.last_name) AS 'Client',
DATE_FORMAT(pos_trans_master.record_datetime, '%Y-%m-%d') AS 'Date',
pos_trans_master.status AS 'Status'

FROM pos_trans_master 

LEFT JOIN pos_client 
ON pos_trans_master.client_id = pos_client.client_id 

LEFT JOIN pos_store 
ON pos_trans_master.store_id = pos_store.store_id 

LEFT JOIN pos_user 
ON pos_trans_master.user_id = pos_user.user_id

WHERE pos_trans_master.status LIKE '%pending void%';

END$$

DROP PROCEDURE IF EXISTS `SP-admin-display_service_price_all`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-display_service_price_all`()
BEGIN 

SELECT  

pos_main_service.service_main_id,
pos_category_service.category_id,
pos_service_and_price.service_id,

pos_main_service.name AS 'Service Name',
pos_category_service.name AS 'Category',
pos_service_and_price.code AS 'Code',
pos_service_and_price.name AS 'Name',
CONCAT(pos_service_and_price.price, '/', pos_service_and_price.unit) AS 'Unit Price'


FROM pos_service_and_price 

LEFT JOIN pos_category_service 
ON pos_service_and_price.category_id = pos_category_service.category_id 

LEFT JOIN pos_main_service 
ON pos_category_service.service_main_id = pos_main_service.service_main_id 

WHERE pos_service_and_price.status = 'active'
;
END$$

DROP PROCEDURE IF EXISTS `SP-admin-display_store_all`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-display_store_all`()
BEGIN

SELECT 
store_id,
address_city,
store_code AS 'Store Code',
store_name  AS 'Store Name',
surcharge_ref AS 'Surcharge Reference'
FROM pos_store 
WHERE store_id != 0 
AND status != 'inactive';

END$$

DROP PROCEDURE IF EXISTS `SP-admin-display_unpaid_transaction_all`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-display_unpaid_transaction_all`(

IN start_date date,

IN end_date date

)
BEGIN  


SELECT 
pos_trans_master.trans_master_id,
pos_trans_master.central_code AS 'Central Code',
pos_trans_master.local_code AS 'Local Code',

pos_store.store_name AS 'Store',
CONCAT(pos_user.first_name, ' ', pos_user.last_name) AS 'Cashier',
CONCAT(pos_client.first_name, ' ', pos_client.last_name) AS 'Client',
DATE_FORMAT(pos_trans_master.record_datetime, '%Y-%m-%d') AS 'Date' 
FROM pos_trans_master 

cross join (select @rownum := 0) r

LEFT JOIN pos_client 
ON pos_trans_master.client_id = pos_client.client_id 

LEFT JOIN pos_store 
ON pos_trans_master.store_id = pos_store.store_id 

LEFT JOIN pos_user 
ON pos_trans_master.user_id = pos_user.user_id

WHERE pos_trans_master.status = 'complete - unpaid'
AND (DATE_FORMAT(pos_trans_master.record_datetime, '%Y-%m-%d') BETWEEN start_date AND end_date)

;



END$$

DROP PROCEDURE IF EXISTS `SP-admin-display_user_admin_all`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-display_user_admin_all`(

IN id  VARCHAR(100)

)
BEGIN 

SELECT 
user_id AS 'User ID',
CONCAT(first_name, ' ', last_name) AS 'Name'



FROM pos_user  
WHERE user_id != 0 
AND user_type = 'ADMIN' 
AND status = 'active' 
and user_id != id;


END$$

DROP PROCEDURE IF EXISTS `SP-admin-display_user_store_all`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-display_user_store_all`()
BEGIN 



SELECT 
pos_user.user_id,
pos_store.store_name,
CONCAT(first_name, ' ', last_name) AS 'Name & Store designation',
pos_user.username AS 'Username'



FROM pos_user 
LEFT JOIN pos_store 

ON pos_user.store_id = pos_store.store_id

WHERE user_type = 'STORE' 
AND pos_user.status = 'active';




END$$

DROP PROCEDURE IF EXISTS `SP-admin-load_main_service`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-load_main_service`(


IN service_id VARCHAR(255)

)
BEGIN


SELECT 
service_main_id,
name 
FROM pos_main_service
WHERE service_main_id = service_id;


END$$

DROP PROCEDURE IF EXISTS `SP-admin-load_main_service_option`$$
CREATE DEFINER=`teejay.pimentel`@`%` PROCEDURE `SP-admin-load_main_service_option`()
BEGIN 

SELECT 
service_main_id,
name 
FROM pos_main_service

where status = 'active';

END$$

DROP PROCEDURE IF EXISTS `SP-admin-load_service_category`$$
CREATE DEFINER=`teejay.pimentel`@`%` PROCEDURE `SP-admin-load_service_category`(

IN id VARCHAR(255)

)
BEGIN

SELECT 
category_id,
service_main_id,
name
FROM pos_category_service 
WHERE category_id = id;

END$$

DROP PROCEDURE IF EXISTS `SP-admin-load_service_price_details`$$
CREATE DEFINER=`teejay.pimentel`@`%` PROCEDURE `SP-admin-load_service_price_details`(

IN id VARCHAR (255)

)
BEGIN

SELECT 
service_main_id,
name 
FROM pos_main_service
WHERE service_main_id = id;


END$$

DROP PROCEDURE IF EXISTS `SP-admin-load_store_list_option`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-load_store_list_option`()
BEGIN


SELECT 
store_id,
store_code,
store_name 
FROM pos_store
WHERE store_id != 0 
AND status = 'active';


END$$

DROP PROCEDURE IF EXISTS `SP-admin-update_main_service`$$
CREATE DEFINER=`teejay.pimentel`@`%` PROCEDURE `SP-admin-update_main_service`(

IN id VARCHAR(255),
IN service_name VARCHAR(255)

)
BEGIN


UPDATE pos_main_service 
SET status = 'inactive' 
WHERE service_main_id = id; 


INSERT INTO pos_main_service (name) VALUES (service_name);






END$$

DROP PROCEDURE IF EXISTS `SP-admin-update_profile`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-update_profile`(

IN id  VARCHAR(100),
IN username  VARCHAR(100),
IN first_name  VARCHAR(100),
IN middle_name  VARCHAR(100),
IN last_name  VARCHAR(100)

)
BEGIN 

UPDATE pos_user 
SET username = username,
first_name = first_name,
middle_name = middle_name,
last_name = last_name 
WHERE user_id = id;

END$$

DROP PROCEDURE IF EXISTS `SP-admin-update_service_category`$$
CREATE DEFINER=`teejay.pimentel`@`%` PROCEDURE `SP-admin-update_service_category`(

IN id VARCHAR(255),
IN service_main_id VARCHAR(255),
IN name VARCHAR(255)

)
BEGIN 

UPDATE pos_category_service 
SET status = 'inactive' 
WHERE category_id = id;

INSERT INTO pos_category_service (service_main_id, name) 
VALUES (service_main_id, name);

END$$

DROP PROCEDURE IF EXISTS `SP-admin-update_service_price`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-update_service_price`(

IN category_id VARCHAR(255),
IN update_id VARCHAR(255),
IN code VARCHAR(255),
IN name VARCHAR(255),
IN price VARCHAR(255),
IN unit VARCHAR(255)

)
BEGIN 

UPDATE pos_service_and_price
SET status = 'inactive'
WHERE service_id = update_id;

INSERT INTO pos_service_and_price (category_id, code, name, price, unit) 
VALUES (category_id, code, name, price, unit);



END$$

DROP PROCEDURE IF EXISTS `SP-admin-update_special`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-update_special`(

IN id  VARCHAR(100),
IN name  VARCHAR(100),
IN value  VARCHAR(100),
IN special_type  VARCHAR(100),
IN value_type  VARCHAR(100),
IN user_id VARCHAR(100)


)
BEGIN 

UPDATE pos_special 

SET status = 'inactive' 
WHERE special_id = id; 

INSERT INTO pos_special (name, value, special_type, value_type, add_by_user_id) 
VALUES (name, value, special_type, value_type, user_id);

END$$

DROP PROCEDURE IF EXISTS `SP-admin-update_store`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-update_store`(

IN id  VARCHAR(100),
IN store_code  VARCHAR(100),
IN store_name VARCHAR(100),
IN address VARCHAR(100),
IN address_city VARCHAR(100),
IN num_tel VARCHAR(100),
IN num_cell VARCHAR(100),
IN surcharge_store VARCHAR(100),
IN surcharge_pickup VARCHAR(100)

)
BEGIN 


UPDATE pos_store 
SET store_code = store_code, 
store_name = store_name,
address = address,
address_city = address_city,
num_tel = num_tel,
num_cell = num_cell ,
surcharge_store = surcharge_store,
surcharge_pickup = surcharge_pickup

WHERE store_id = id;



END$$

DROP PROCEDURE IF EXISTS `SP-admin-update_user_admin`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-update_user_admin`(

IN id  VARCHAR(100),
IN username  VARCHAR(100),
IN first_name VARCHAR(100),
IN middle_name VARCHAR(100),
IN last_name VARCHAR(100)

)
BEGIN 

UPDATE pos_user 
SET username = username,
first_name = first_name,
middle_name = middle_name,
last_name = last_name 
WHERE user_id = id;


END$$

DROP PROCEDURE IF EXISTS `SP-admin-update_user_admin_password`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-update_user_admin_password`(

IN id  VARCHAR(100),
IN new_pw  VARCHAR(100)

)
BEGIN 

UPDATE pos_user 
SET password = new_pw,

require_new_pw = 'YES'
WHERE user_id = id;


END$$

DROP PROCEDURE IF EXISTS `SP-admin-update_user_store`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-update_user_store`(

IN id  VARCHAR(100),
IN username  VARCHAR(100),
IN first_name VARCHAR(100),
IN middle_name VARCHAR(100),
IN last_name VARCHAR(100),
IN store_id VARCHAR(100)


)
BEGIN 

UPDATE pos_user 
SET 
store_id = store_id,
username = username,
first_name = first_name,
middle_name = middle_name,
last_name = last_name

WHERE user_id = id;



END$$

DROP PROCEDURE IF EXISTS `SP-admin-update_user_store_password`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-update_user_store_password`(

IN id  VARCHAR(100),
IN new_pw  VARCHAR(100)

)
BEGIN


UPDATE pos_user 
SET password = new_pw,

require_new_pw = 'YES'
WHERE user_id = id;


END$$

DROP PROCEDURE IF EXISTS `SP-admin-view_profile`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-view_profile`(

IN id  VARCHAR(100)


)
BEGIN 


SELECT 
pos_user.username,
pos_user.first_name,
pos_user.middle_name,
pos_user.last_name,
pos_store.store_name 
FROM pos_user 


LEFT JOIN pos_store 

ON pos_user.store_id = pos_store.store_id 
WHERE pos_user.user_id = id;



END$$

DROP PROCEDURE IF EXISTS `SP-admin-view_service_and_price_details`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-view_service_and_price_details`( 

IN id VARCHAR(255)

)
BEGIN 

SELECT 
pos_service_and_price.category_id,
pos_service_and_price.service_id,
pos_service_and_price.code,

pos_service_and_price.name,
pos_service_and_price.price,
pos_service_and_price.unit,
CONCAT(pos_user.first_name, ' ', pos_user.last_name) AS 'add_by', 
pos_service_and_price.add_datetime 
FROM pos_service_and_price 

LEFT JOIN pos_user 
ON pos_service_and_price.add_by_user_id = pos_user.user_id

WHERE pos_service_and_price.service_id = id;

END$$

DROP PROCEDURE IF EXISTS `SP-admin-view_special_details`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-view_special_details`(

IN id  VARCHAR(100)

)
BEGIN 

SELECT 
pos_special.special_id,
pos_special.name,
pos_special.value,
pos_special.special_type,
pos_special.value_type,
CONCAT(pos_user.first_name, ' ', pos_user.last_name) AS 'add_by',
pos_special.add_datetime 

FROM pos_special 

LEFT JOIN pos_user 
ON pos_special.add_by_user_id = pos_user.user_id  


WHERE pos_special.special_id = id;

END$$

DROP PROCEDURE IF EXISTS `SP-admin-view_store_details`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-view_store_details`(

IN id  VARCHAR(100)

)
BEGIN 


SELECT 

pos_store.store_id,
pos_store.store_code,
pos_store.store_name,
pos_store.address,
pos_store.address_city,
pos_store.num_tel,
pos_store.num_cell,
CONCAT(pos_user.first_name, ' ', pos_user.last_name) AS 'add_by',
pos_store.add_datetime,
pos_store.surcharge_store,
pos_store.surcharge_pickup

FROM pos_store

LEFT JOIN pos_user 
ON pos_store.add_by_user_id = pos_user.user_id 

WHERE pos_store.store_id = id;




END$$

DROP PROCEDURE IF EXISTS `SP-admin-view_user_admin_details`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-view_user_admin_details`(

IN id  VARCHAR(100)

)
BEGIN

SELECT 
user_id,
username,
first_name,
middle_name,
last_name 
FROM pos_user 
WHERE user_id = id;


END$$

DROP PROCEDURE IF EXISTS `SP-admin-view_user_store_details`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-admin-view_user_store_details`(

IN id  VARCHAR(100)

)
BEGIN 

SELECT 
user_id,
store_id,
username,
first_name,
middle_name,
last_name 
FROM pos_user 
WHERE user_id = id;

END$$

DROP PROCEDURE IF EXISTS `SP-all-display_claim_detail`$$
CREATE DEFINER=`teejay.pimentel`@`%` PROCEDURE `SP-all-display_claim_detail`(

IN id VARCHAR(255)

)
BEGIN 

SELECT 
pos_store.store_code,
pos_store.store_name,
pos_trans_master.local_code,
CONCAT(pos_client.first_name, ' ', pos_client.last_name) AS 'client_name',
pos_client.contact_num1 AS 'client_contact',
pos_client.network1 AS 'client_network',
pos_trans_master.claim_by,
#pos_trans_master.claim_datetime
DATE_FORMAT(pos_trans_master.claim_datetime,'%W %b %d, %Y %h:%i %p') AS 'claim_datetime'


FROM pos_trans_master

LEFT JOIN pos_store 
ON pos_trans_master.store_id = pos_store.store_id

LEFT JOIN pos_client
ON pos_trans_master.client_id = pos_client.client_id

WHERE pos_trans_master.trans_master_id = id;

END$$

DROP PROCEDURE IF EXISTS `SP-load_category_service_option`$$
CREATE DEFINER=`teejay.pimentel`@`%` PROCEDURE `SP-load_category_service_option`(

IN main_id VARCHAR(255)

)
BEGIN 

SELECT 
category_id,
name 
FROM pos_category_service 
WHERE status = 'active' 
AND service_main_id = main_id;

END$$

DROP PROCEDURE IF EXISTS `SP-store-add_client`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-add_client`(


IN first_name  VARCHAR(100),
IN middle_name  VARCHAR(100),
IN last_name  VARCHAR(100),
IN contact_num1  VARCHAR(100),
IN contact_num2  VARCHAR(100),
IN email  VARCHAR(100),
IN address  VARCHAR(100),
IN address_city  VARCHAR(100),
IN user_id  VARCHAR(100),
IN client_type  VARCHAR(100),
IN company_name  VARCHAR(100),
IN fax  VARCHAR(100),

IN retail_type  VARCHAR(100),
IN network1  VARCHAR(100),
IN network2  VARCHAR(100),
IN birth_date  date,
IN postal_code  VARCHAR(100),
IN branch VARCHAR(255)

)
BEGIN 

#GENERATE RANDOM UNIQUE ID
SELECT CONCAT(NOW(), SUBSTRING(MD5(RAND()) FROM 1 FOR 10), UNIX_TIMESTAMP()) INTO @random;


INSERT INTO pos_client (client_id, first_name, middle_name, last_name, contact_num1, contact_num2, email, address, address_city, add_by_user_id, client_type, company_name, fax, retail_type, network1, network2, birth_date, postal_code, store_id) 
VALUES (@random, first_name, middle_name, last_name, contact_num1, contact_num2, email, address, address_city, user_id, client_type, company_name, fax, retail_type, network1, network2, birth_date, postal_code, branch);

END$$

DROP PROCEDURE IF EXISTS `SP-store-add_special_due_date`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-add_special_due_date`(

IN trans_id VARCHAR(255),
IN due_date_type VARCHAR(255)

)
BEGIN


DELETE FROM pos_trans_special 
WHERE trans_master_id = trans_id 
AND  special_id = 3;

IF (due_date_type = 'rush') THEN


SELECT CONCAT(UNIX_TIMESTAMP(), SUBSTRING(MD5(RAND()) FROM 1 FOR 10)) INTO @random;


#special id = 3
INSERT INTO pos_trans_special (trans_special_id, trans_master_id, special_id)
VALUES (@random, trans_id, 3);

END IF;



END$$

DROP PROCEDURE IF EXISTS `SP-store-check_client_exist`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-check_client_exist`(

IN fname VARCHAR(255),
IN mname VARCHAR(255),
IN lname VARCHAR(255)

)
BEGIN 

SELECT * FROM pos_client 
WHERE first_name LIKE CONCAT('%', fname, '%') 
AND middle_name LIKE CONCAT('%', mname, '%') 
AND last_name LIKE CONCAT('%', lname, '%');


END$$

DROP PROCEDURE IF EXISTS `SP-store-daily_report`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-daily_report`(

IN datefrom date,
IN dateto date,
IN id VARCHAR(10)

)
BEGIN 


SET SESSION group_concat_max_len = 1000000;

SET @header = NULL;
SET @query = NULL;

SELECT 
GROUP_CONCAT(DISTINCT
CONCAT('SUM(CASE WHEN pos_trans_detail.service_id =\'', pos_trans_detail.service_id, '\' THEN TRIM(TRAILING \'.\' FROM TRIM(TRAILING \'0\' from pos_trans_detail.unit_count)) ELSE 0 END) AS \'', code, '\'')
) INTO @header
FROM pos_service_and_price 

INNER JOIN pos_trans_detail 
ON pos_service_and_price.service_id = pos_trans_detail.service_id

WHERE code != ''
;


SET @query = CONCAT(
'
SELECT 
pos_trans_master.trans_master_id as \'ID\',
CONCAT(pos_client.first_name, \' \', pos_client.middle_name, \' \', pos_client.last_name) AS \'Customer\',
pos_store.store_name AS \'Location\',
pos_trans_master.record_datetime AS \'Date Recieve\',

pos_trans_master.due_date AS \'Date Due\',

pos_trans_master.total AS \'Payment\',


CASE WHEN
pos_trans_master.status = \'complete - paid\' 
THEN \'paid\' ELSE \'unpaid\' 
END AS \'Paid/Unpaid\',



', @header, '




FROM pos_trans_master 

LEFT JOIN pos_store 
ON pos_trans_master.store_id = pos_store.store_id 

LEFT JOIN pos_client 
ON pos_trans_master.client_id = pos_client.client_id 

LEFT JOIN pos_trans_detail 
ON pos_trans_master.trans_master_id = pos_trans_detail.trans_master_id 

LEFT JOIN pos_service_and_price 
ON pos_trans_detail.service_id = pos_service_and_price.service_id

WHERE 
(pos_trans_master.record_date BETWEEN \'',  datefrom, '\' AND \'', dateto, '\') 
AND pos_trans_master.status NOT LIKE \'%cancelled%\' 
AND pos_trans_master.status NOT LIKE \'%void%\' 
AND pos_trans_master.user_id = ', id, '


GROUP BY pos_trans_master.trans_master_id

'


); 


PREPARE stmt FROM @query;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

END$$

DROP PROCEDURE IF EXISTS `SP-store-delete_client`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-delete_client`(

IN id VARCHAR(255)

)
BEGIN 

UPDATE pos_client 
SET status = 'inactive' 

WHERE client_id = id;

END$$

DROP PROCEDURE IF EXISTS `SP-store-display_client_all`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-display_client_all`(



)
BEGIN 



SELECT 
	client_id,
	pos_client.address_city,
	CONCAT(first_name, ' ', last_name) AS 'Customer Name',
	email AS 'Email',
	contact_num1 AS 'Contact No#',
    pos_store.store_name AS 'Branch'
	FROM pos_client  
    
    LEFT JOIN pos_store 
    ON pos_client.store_id = pos_store.store_id
    
    
	WHERE pos_client.status = 'active';


END$$

DROP PROCEDURE IF EXISTS `SP-store-display_paid_transaction_all`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-display_paid_transaction_all`(

IN user_id VARCHAR(100),
IN start_date date,
IN end_date date,
IN store VARCHAR(255)


)
BEGIN 

SELECT 
pos_trans_master.trans_master_id AS 'Transaction ID',
pos_trans_master.local_code AS 'ID',
CONCAT(pos_client.first_name, ' ', pos_client.last_name) AS 'Client Name',
DATE_FORMAT(pos_trans_master.record_datetime, '%Y-%m-%d') AS 'Date' 
FROM pos_trans_master 

LEFT JOIN pos_client 
ON pos_trans_master.client_id = pos_client.client_id 


WHERE pos_trans_master.status = 'complete - paid'
AND (DATE_FORMAT(pos_trans_master.record_datetime, '%Y-%m-%d') BETWEEN start_date AND end_date)
AND pos_trans_master.store_id = store

;

END$$

DROP PROCEDURE IF EXISTS `SP-store-display_pending_void_transaction_all`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-display_pending_void_transaction_all`(

IN user_id VARCHAR(100),
IN start_date date,
IN end_date date

)
BEGIN 

SELECT 
pos_trans_master.trans_master_id AS 'Transaction ID',
CONCAT(pos_client.first_name, ' ', pos_client.last_name) AS 'Client Name',
DATE_FORMAT(pos_trans_master.record_datetime, '%Y-%m-%d') AS 'Date' 

FROM pos_trans_master 

LEFT JOIN pos_client 
ON pos_trans_master.client_id = pos_client.client_id 

WHERE pos_trans_master.status LIKE '%pending void%'
AND (DATE_FORMAT(pos_trans_master.record_datetime, '%Y-%m-%d') BETWEEN start_date AND end_date)

;


END$$

DROP PROCEDURE IF EXISTS `SP-store-display_suspend_transaction_all`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-display_suspend_transaction_all`(

IN user_id VARCHAR(100),
IN start_date date,
IN end_date date,
IN store VARCHAR(255)


)
BEGIN 

SELECT 
pos_trans_master.trans_master_id AS 'Transaction ID',

CONCAT(pos_client.first_name, ' ', pos_client.last_name) AS 'Client Name',
DATE_FORMAT(pos_trans_master.record_datetime, '%Y-%m-%d') AS 'Date' 
FROM pos_trans_master 

LEFT JOIN pos_client 
ON pos_trans_master.client_id = pos_client.client_id 

WHERE pos_trans_master.status = 'suspend' 
AND (DATE_FORMAT(pos_trans_master.record_datetime, '%Y-%m-%d') BETWEEN start_date AND end_date)
AND pos_trans_master.store_id = store
;

END$$

DROP PROCEDURE IF EXISTS `SP-store-display_unpaid_transaction_all`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-display_unpaid_transaction_all`(

IN user_id VARCHAR(100),
IN start_date date,
IN end_date date,
IN store VARCHAR(255)

)
BEGIN

SELECT 
pos_trans_master.trans_master_id AS 'Transaction ID',
pos_trans_master.local_code AS 'ID',
CONCAT(pos_client.first_name, ' ', pos_client.last_name) AS 'Client Name',
DATE_FORMAT(pos_trans_master.record_datetime, '%Y-%m-%d') AS 'Date' 
FROM pos_trans_master 

LEFT JOIN pos_client 
ON pos_trans_master.client_id = pos_client.client_id 

WHERE pos_trans_master.status = 'complete - unpaid' 
#AND (DATE_FORMAT(pos_trans_master.record_datetime, '%Y-%m-%d') BETWEEN start_date AND end_date)
AND pos_trans_master.store_id = store
;

END$$

DROP PROCEDURE IF EXISTS `SP-store-load_main_service_option`$$
CREATE DEFINER=`teejay.pimentel`@`%` PROCEDURE `SP-store-load_main_service_option`()
BEGIN

SELECT
service_main_id,
name 
FROM pos_main_service 
WHERE status = 'active';

END$$

DROP PROCEDURE IF EXISTS `SP-store-load_service_category_id`$$
CREATE DEFINER=`teejay.pimentel`@`%` PROCEDURE `SP-store-load_service_category_id`(

IN service_id VARCHAR(255)

)
BEGIN

SELECT  
category_id,
name
FROM pos_category_service 

WHERE service_main_id = service_id;

END$$

DROP PROCEDURE IF EXISTS `SP-store-load_service_name_list_option`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-load_service_name_list_option`(

IN id VARCHAR(255)

)
BEGIN 

SELECT 

service_id,
code,
name,
price,
unit
FROM pos_service_and_price
WHERE status = 'active' 
AND category_id = id;


END$$

DROP PROCEDURE IF EXISTS `SP-store-load_special_name_list_option`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-load_special_name_list_option`(

IN special_type VARCHAR(100)

)
BEGIN

SELECT 
special_id,
name
FROM pos_special 
WHERE status = 'active' 

AND pos_special.special_type = special_type;


END$$

DROP PROCEDURE IF EXISTS `SP-store-load_store_option`$$
CREATE DEFINER=`teejay.pimentel`@`%` PROCEDURE `SP-store-load_store_option`()
BEGIN 

SELECT  

store_id,
store_code,
store_name

FROM pos_store
WHERE store_id != '0';

END$$

DROP PROCEDURE IF EXISTS `SP-store-transaction-add_special_detail`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-transaction-add_special_detail`(

IN unique_id VARCHAR(255),
IN id  VARCHAR(100),
IN special_id  VARCHAR(100)

)
BEGIN 


INSERT INTO pos_trans_special(trans_special_id, trans_master_id, special_id) 
VALUES (unique_id, id, special_id);

END$$

DROP PROCEDURE IF EXISTS `SP-store-transaction-add_transaction_detail`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-transaction-add_transaction_detail`( 


IN unique_id VARCHAR(255),
IN id  VARCHAR(255),

IN service  VARCHAR(100),
IN unit VARCHAR(100),
IN price VARCHAR(100),
IN unit_count  VARCHAR(100),
IN unit_price  VARCHAR(100)



)
BEGIN 


INSERT INTO pos_trans_detail (trans_detail_id, trans_master_id, service_id, unit, price, unit_count, unit_price) 
VALUES (unique_id, id, service, unit, price, unit_count, unit_price);

END$$

DROP PROCEDURE IF EXISTS `SP-store-transaction-add_transaction_item`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-transaction-add_transaction_item`(

IN id  VARCHAR(100),
IN item_name  VARCHAR(100),
IN qty VARCHAR(100),
IN unique_id VARCHAR(100)


)
BEGIN 

INSERT INTO pos_trans_item (trans_item_id, trans_master_id, item_name, qty)
VALUES (unique_id, id, item_name, qty);


END$$

DROP PROCEDURE IF EXISTS `SP-store-transaction-create_master_table`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-transaction-create_master_table`(

IN client_id  VARCHAR(100),
IN store_id VARCHAR(100),
IN user_id  VARCHAR(100)

)
BEGIN 



INSERT INTO pos_trans_master (client_id, record_date, store_id, user_id) 
VALUES (client_id, store_id, user_id);


SELECT 


pos_trans_master.trans_master_id,
pos_store.store_name,
CONCAT(pos_store.address, ' ', pos_store.address_city) AS 'store_address',
CONCAT(pos_store.num_cell, ' | ', pos_store.num_tel) AS 'store_contact',
CONCAT(pos_client.first_name, ' ', pos_client.middle_name, ' ', pos_client.last_name) AS 'client_name',

CONCAT(pos_client.address, ' ', pos_client.address_city) AS 'client_address', 
pos_client.contact_num1 AS 'client_contact',
pos_trans_master.record_datetime 
FROM pos_trans_master 

LEFT JOIN pos_client
ON pos_trans_master.client_id = pos_client.client_id 

LEFT JOIN pos_store 
ON pos_trans_master.store_id = pos_store.store_id;


END$$

DROP PROCEDURE IF EXISTS `SP-store-transaction-delete_transaction_detail`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-transaction-delete_transaction_detail`(

IN id  VARCHAR(100)

)
BEGIN 


UPDATE pos_trans_detail 
SET line_void = 'YES' 
WHERE trans_detail_id = id;


#DELETE FROM pos_trans_detail WHERE trans_detail_id = id;


#DELETE FROM pos_trans_item WHERE trans_detail_id = id;


END$$

DROP PROCEDURE IF EXISTS `SP-store-transaction-delete_transaction_item`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-transaction-delete_transaction_item`(


IN id  VARCHAR(100)

)
BEGIN 


UPDATE pos_trans_item 
SET item_void = 'YES' 

WHERE trans_item_id = id;


END$$

DROP PROCEDURE IF EXISTS `SP-store-transaction-delete_transaction_special`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-transaction-delete_transaction_special`(

IN id  VARCHAR(100)

)
BEGIN 

UPDATE pos_trans_special 
SET special_void = 'YES' 
WHERE trans_special_id = id;

END$$

DROP PROCEDURE IF EXISTS `SP-store-transaction-dispay_trans_item`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-transaction-dispay_trans_item`( 


IN id VARCHAR(255)

)
BEGIN 

SELECT 
trans_item_id,
item_name AS 'Item Name',
qty AS 'Qty' 
FROM pos_trans_item 
WHERE trans_master_id = id 
AND item_void = 'NO';


END$$

DROP PROCEDURE IF EXISTS `SP-store-transaction-finalize_transaction`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-transaction-finalize_transaction`(

IN id VARCHAR(255),
IN user_store_id VARCHAR(100),
IN status VARCHAR(100),
IN due_date DATE,
IN total_grand DECIMAL(10,2),
IN new_id VARCHAR(255),
IN local_code VARCHAR(255),
IN user_id VARCHAR(255)


)
BEGIN 


UPDATE pos_trans_master 
SET status = status,

due_date = due_date,
total = total_grand,
trans_master_id = new_id,
local_code = local_code
WHERE trans_master_id = id; 


##CREATE PRIMARY ID BASE ON LAST INSERT ID OF POS TRANS INC REF
INSERT INTO pos_trans_inc_ref (datetime) VALUES (NOW());
SET @last_id = LAST_INSERT_ID();
SELECT SUBSTRING(MD5(RAND()) FROM 1 FOR 10) INTO @random;

INSERT INTO pos_trans_trail (trans_trail_id , trans_master_id, status, user_store_id, user_admin_id, user_staff_id) 
VALUES (CONCAT(@random, @last_id,id), id, status, user_store_id, 'NA', user_id); 


UPDATE pos_trans_detail 
SET trans_master_id = new_id 
WHERE trans_master_id = id;

UPDATE pos_trans_item 
SET trans_master_id = new_id 
WHERE trans_master_id = id;

UPDATE pos_trans_special 
SET trans_master_id = new_id 
WHERE trans_master_id =id; 

UPDATE pos_trans_trail 
SET trans_master_id = new_id 
WHERE trans_master_id = id;








END$$

DROP PROCEDURE IF EXISTS `SP-store-transaction-finalize_transaction_paid`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-transaction-finalize_transaction_paid`(

IN id VARCHAR(255),
IN user_store_id VARCHAR(100),
IN status VARCHAR(100),
IN due_date DATE,
IN total_grand DECIMAL(10,2),
IN claim_by VARCHAR(100),
IN payment_type VARCHAR(100),
IN user_id VARCHAR(255)

)
BEGIN 



UPDATE pos_trans_master 
SET status = status,
due_date = due_date,
total = total_grand,
claim_by = claim_by,
claim_datetime = NOW(),
payment_type = payment_type

WHERE trans_master_id = id; 


##CREATE PRIMARY ID BASE ON LAST INSERT ID OF POS TRANS INC REF
INSERT INTO pos_trans_inc_ref (datetime) VALUES (NOW());
SET @last_id = LAST_INSERT_ID();
SELECT SUBSTRING(MD5(RAND()) FROM 1 FOR 10) INTO @random;

INSERT INTO pos_trans_trail (trans_trail_id, trans_master_id, status, user_store_id, user_admin_id, user_staff_id) 
VALUES (CONCAT(@random, @last_id,id), id, status, user_store_id, 'NA', user_id);



IF (payment_type = 'CREDIT MEMO') THEN
  
SELECT client_id 
INTO @client_id 
FROM pos_trans_master 
WHERE trans_master_id = id; 


INSERT INTO pos_client_credit (credit_id ,client_id, amount, trans_master_id, user_id)
VALUES (CONCAT(NOW(), @random, UNIX_TIMESTAMP()), @client_id, abs(total_grand) * -1, id, user_store_id);
  

END IF;






END$$

DROP PROCEDURE IF EXISTS `SP-store-transaction-suspend_transaction`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-transaction-suspend_transaction`(

IN id VARCHAR(100),
IN user_store_id VARCHAR(100),
IN status VARCHAR(100),

IN due date

)
BEGIN




UPDATE pos_trans_master 
SET status = status,
due_date = due
WHERE trans_master_id = id; 

#GENERATE RANDOM UNIQUE ID
SELECT CONCAT(NOW(), SUBSTRING(MD5(RAND()) FROM 1 FOR 10), UNIX_TIMESTAMP()) INTO @random;

INSERT INTO pos_trans_trail (trans_trail_id, trans_master_id, status, user_store_id, user_admin_id) 
VALUES (@random, id, status, user_store_id, 'NA');


END$$

DROP PROCEDURE IF EXISTS `SP-store-transaction-void_transaction`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-transaction-void_transaction`(

IN id VARCHAR(100),
IN user_store_id VARCHAR(100),
IN status VARCHAR(100)

)
BEGIN 

#GENERATE RANDOM UNIQUE ID
SELECT CONCAT(NOW(), SUBSTRING(MD5(RAND()) FROM 1 FOR 10), UNIX_TIMESTAMP()) INTO @random;

INSERT INTO pos_trans_trail (trans_trail_id, trans_master_id, status, user_store_id, user_admin_id) 
VALUES (@random ,id, status, user_store_id, 'NA');

UPDATE pos_trans_master 
SET status = status 
WHERE trans_master_id = id; 



END$$

DROP PROCEDURE IF EXISTS `SP-store-update_client`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-update_client`( 

IN id  VARCHAR(100),
IN first_name  VARCHAR(100),
IN middle_name  VARCHAR(100),
IN last_name  VARCHAR(100),
IN contact_num1  VARCHAR(100),
IN contact_num2  VARCHAR(100),
IN email  VARCHAR(100),
IN address  VARCHAR(100),
IN address_city  VARCHAR(100),

IN client_type  VARCHAR(100),
IN company_name  VARCHAR(100),
IN fax  VARCHAR(100),

IN retail_type VARCHAR(100),
IN network1 VARCHAR(100),
IN network2 VARCHAR(100),
IN birth_date DATE,
IN postal_code VARCHAR(100),
IN branch VARCHAR(255)

)
BEGIN 

UPDATE pos_client 
SET first_name = first_name, 
middle_name = middle_name,
last_name = last_name,
contact_num1 = contact_num1,
contact_num2 = contact_num2, 
email = email,
address = address,
address_city = address_city,
client_type = client_type,
company_name = company_name,
fax = fax,
retail_type = retail_type, 
network1 = network1, 
network2 = network2,
birth_date = birth_date,
postal_code = postal_code,
store_id = branch
WHERE client_id = id;

END$$

DROP PROCEDURE IF EXISTS `SP-store-update_profile`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-update_profile`(

IN id  VARCHAR(100),

IN first_name  VARCHAR(100),
IN middle_name  VARCHAR(100),

IN last_name  VARCHAR(100)

)
BEGIN 

UPDATE pos_user 
SET first_name = first_name,
middle_name = middle_name,
last_name = last_name 
WHERE user_id = id;

END$$

DROP PROCEDURE IF EXISTS `SP-store-view_client_details`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-view_client_details`(

IN id  VARCHAR(100)

)
BEGIN 

SELECT 
pos_client.client_id,
pos_client.first_name,
pos_client.middle_name,
pos_client.last_name,
pos_client.contact_num1,
pos_client.contact_num2,
pos_client.email,
pos_client.address,
pos_client.address_city,
CONCAT(pos_user.first_name, ' ', pos_user.last_name) AS 'add_by',
pos_client.add_datetime,

pos_client.client_type,
pos_client.company_name,
pos_client.fax,
pos_client.network1,
pos_client.network2,
pos_client.birth_date,
pos_client.postal_code,
pos_client.retail_type,
pos_client.store_id

FROM pos_client 
LEFT JOIN pos_user 
ON pos_client.add_by_user_id = pos_user.user_id 

WHERE pos_client.client_id = id

;

END$$

DROP PROCEDURE IF EXISTS `SP-store-view_profile`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-store-view_profile`(

IN id  VARCHAR(100)


)
BEGIN 

SELECT 
pos_user.username,
pos_user.first_name,
pos_user.middle_name,
pos_user.last_name,
pos_store.store_name 
FROM pos_user 

LEFT JOIN pos_store 
ON pos_user.store_id = pos_store.store_id 
WHERE pos_user.user_id = id;

END$$

DROP PROCEDURE IF EXISTS `SP-test`$$
CREATE DEFINER=`teejay.pimentel`@`%` PROCEDURE `SP-test`(

IN user VARCHAR(255),
IN password VARCHAR(255)

)
BEGIN

SELECT username, first_name FROM pos_user where username=user AND password=password;

END$$

DROP PROCEDURE IF EXISTS `SP-user_change_password`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-user_change_password`(

IN id VARCHAR(100),
IN given_password VARCHAR(100)

)
BEGIN 

UPDATE pos_user 
SET `password` = given_password,
`require_new_pw` = 'NO'
WHERE `user_id` = id;


END$$

DROP PROCEDURE IF EXISTS `SP-user_check_new`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-user_check_new`(

IN id VARCHAR(100)

)
BEGIN 


SELECT 
user_id,
require_new_pw 
FROM pos_user 
WHERE require_new_pw = 'YES' 
AND user_id = id;



END$$

DROP PROCEDURE IF EXISTS `SP-user_check_password`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-user_check_password`(


IN id VARCHAR(100),
IN given_password VARCHAR(100)

)
BEGIN 

SELECT * FROM pos_user 
WHERE user_id = id 
AND password = given_password;

END$$

DROP PROCEDURE IF EXISTS `SP-user_login`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP-user_login`(

IN given_username  VARCHAR(100),
IN given_password VARCHAR(100)

)
BEGIN 


SELECT 
pos_store.store_code,
pos_user.user_id,
pos_store.store_id,
CONCAT(pos_user.first_name, ' ', pos_user.last_name) AS 'name',
pos_user.user_type,
pos_store.store_name,

pos_user.status,
pos_store.surcharge_store,
pos_store.surcharge_pickup


FROM pos_user 

LEFT JOIN pos_store 
ON pos_user.store_id = pos_store.store_id 

WHERE pos_user.username = given_username 
AND pos_user.password = given_password

;

END$$

DELIMITER ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
