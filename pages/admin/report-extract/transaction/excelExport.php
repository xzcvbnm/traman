<?php
session_start();
//ob_start();


/*

Generating Excel File..
<!-- Progress bar holder -->
<div id="progress" style="width:500px;border:1px solid #ccc;"></div>
<!-- Progress information -->
<div id="information" style="width"></div>

*/


include '../../../../config.php';
include '../../../../assets/php/PHPExcel/Classes/PHPExcel.php';

//set memory limit
ini_set('memory_limit', '2048M');

$session_store_id = $_SESSION['store_id'];

$date_from = date('Y') . '-' . date('m') . '-' . '01';
$date_to = date('Y-m-d');



if(isset($_POST['pos-admin-display_transaction_report-table']) && $_POST['pos-admin-display_transaction_report-table'] == true) {

    if (isset($_POST['start_date']) && isset($_POST['end_date'])) {

        $start_date = $_POST['start_date'] . ' 01:00:00';
        $end_date = $_POST['end_date'] . ' 23:59:59';

        $query = "CALL `SP-admin-report_transaction`('$start_date', '$end_date', '$session_store_id');";

    } else {

        $query = "CALL `SP-admin-report_transaction`('$date_from', '$date_to', '$session_store_id');";

    }

//random string
    $rnd_string = md5(uniqid(rand(), true));


//Excel Header
    $headings1 = array();


//measure time of execution
    $time_start = microtime(true);

    if (($result = mysqli_query($link,$query) or die(mysqli_error())

    )) {

        // Create a new PHPExcel object
        $objPHPExcel = new PHPExcel();

        //SET TITLE OF WORKSHEET
        $objPHPExcel->getActiveSheet()->setTitle('Data');

        $rowNumber = 1;
        $col = 'A';


        $row_count = mysqli_num_rows($result);
        $j = 0;

        //GET THE QUERY TABLE HEADER TITLE AND SET TO EXCEL
        while($row = mysqli_fetch_field($result)) {

            $headings[] =  $row->name;

        }



        foreach($headings as $heading) {
            $objPHPExcel->getActiveSheet()->setCellValue($col.$rowNumber,$heading);


            //autosize column witdth
            $objPHPExcel->getActiveSheet()
                ->getColumnDimension($col)
                ->setAutoSize(true);


            //set cell color
            //cellColor('A'.$rowNumber.':'.$col.$rowNumber, 'F28A8C');
            $col++;
        }

        // Loop through the result set
        $rowNumber = 2;
        while ($row = mysqli_fetch_row($result)) {
            $col = 'A';
            foreach($row as $cell) {
                $objPHPExcel->getActiveSheet()->setCellValueExplicit($col.$rowNumber,$cell);
                $col++;
            }
            $rowNumber++;



            //==================================================================================
            // Sleep one second so we can see the delay
            //sleep(1);


            $j++;
            $percent = intval($j/$row_count * 100)."%";

            //echo $j . "</br>";;

            echo '-';

            // Javascript for updating the progress bar and information
            /*
            echo '<script language="javascript">
            document.getElementById("progress").innerHTML="<div style=\"width:'.$percent.';background-color:#ddd;\">&nbsp;</div>";
            document.getElementById("information").innerHTML="' . number_format($j) . ' of ' . number_format($row_count) . ' row(s) processed . <br>' . $percent . ' completed.";
            </script>';
            */
            flush();
            ob_flush();

            ob_clean();


            //==================================================================================



        }


        // Set autofilter range
        $objPHPExcel->getActiveSheet()->setAutoFilter($objPHPExcel->getActiveSheet()->calculateWorksheetDimension());

        // Set active filters
        $autoFilter = $objPHPExcel->getActiveSheet()->getAutoFilter();

        // Freeze pane so that the heading line won't scroll
        $objPHPExcel->getActiveSheet()->freezePane('A2');

        //================= END OF WORKSHEET 1==============================


        //set active sheet to sheet1(first sheet)
        $objPHPExcel->setActiveSheetIndex(0);


        // Save as an Excel BIFF (xls) file

        /*
        FILE TYPE

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');  ==> EXCEL
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');  ==> CSV
        */
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');

        //header('Content-Type: application/vnd.ms-excel');
        //header('Content-Disposition: attachment;filename="rawdata_activations.xls"');
        //header('Cache-Control: max-age=0');

        //$objWriter->save('php://output');

        /*
        SAVE THE EXCEL FILE LOCATION and FILE NAME
        example:
        @location - data_excel/outright_price_bracket/
        @file name - opb_20150604.xlsx

        NOTE: change file extension accoring to selected file type

        */
        $objWriter->save($rnd_string . '.csv');

        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start)/60;

        echo $rnd_string . '.csv';

        //Display to browser that the process completed
        /*
        echo '<script language="javascript">

        document.getElementById("information").innerHTML="Process completed. <br> Execution Time: ' . number_format($execution_time,2) . ' mins."


        </script>';
        */
        //ob_end_flush();


        //SET THE NEXT RUN
        //echo '<meta http-equiv="Refresh" content="5; url="' . $rnd_string . '.csv">';

        /*
        echo "<br>DOWNLOAD THE FILE HERE --> <a href='$rnd_string.csv'>LINK</a>";
        */




        exit;
    }
//echo 'a problem has occurred... no data retrieved from the database! retrying';

//RETRY HEADER
//SHOUD BE THE NAME OF php file
//echo '<meta http-equiv="Refresh" content="5; url=mail_outright_price_point.php">';

}


?>