<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Xenon Boostrap Admin Panel" />
	<meta name="author" content="" />
	
	<title>Logging out ...</title>

	<link rel="icon" type="image/png" href="pages/favicon.ico">

	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arimo:400,700,400italic">
	<link rel="stylesheet" href="assets/css/fonts/linecons/css/linecons.css">
	<link rel="stylesheet" href="assets/css/fonts/fontawesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/bootstrap.css">
	<link rel="stylesheet" href="assets/css/xenon-core.css">
	<link rel="stylesheet" href="assets/css/xenon-forms.css">
	<link rel="stylesheet" href="assets/css/xenon-components.css">
	<link rel="stylesheet" href="assets/css/xenon-skins.css">
	<link rel="stylesheet" href="assets/css/custom.css">

	<script src="assets/js/jquery-1.11.1.min.js"></script>
    
    
    <script type="text/javascript">
	
		
	<!-- DOCUMENT READY FUNCTION -->
	$(document).ready(function()
	//jQuery(document).ready(function($)
	{
		
		//CLEAR ALL SESSION STORAGE
		sessionStorage.clear();
		
		
	});
	</script>
    

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
	
</head>
<body class="page-body page-error-env">

	<div class="page-error centered">
		
		<div class="error-symbol">
			<i class="fa-warning"></i>
		</div>
		
		<h2>
			Logging Out ...
			<small>Please wait</small>
		</h2>
		
		<p>If the page doesn't redirect you, click ---> <a href="index.html">here</a></b></p>
		<!--
        <p>for any clarrifications/questions, you may contact MIS Team or Finance Team</p>
		-->
	</div>
	
	



	<!-- Bottom Scripts -->
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/TweenMax.min.js"></script>
	<script src="assets/js/resizeable.js"></script>
	<script src="assets/js/joinable.js"></script>
	<script src="assets/js/xenon-api.js"></script>
	<script src="assets/js/xenon-toggles.js"></script>


	<!-- JavaScripts initializations and stuff -->
	<script src="assets/js/xenon-custom.js"></script>

</body>
</html>




<?php

error_reporting(0);


session_start();



session_destroy();
session_unset();
   
   
echo '<meta http-equiv="Refresh" content="3; url=index.html">';




?>