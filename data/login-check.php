<?php
session_start();
include '../config.php';


$store_menu_list = '

		 <!--
		 <li>
			<a href="#">
				<i class="fa-dashboard"></i>
				<span class="title">Dashboard</span>
			</a>
			
			<ul>
			
				<li>
					<a href="#page-dashboard-laundry-due" class="menu-list">
						<span class="title">Laundry Due</span>
					</a>
				</li>
				
				
			
				<li>
					<a href="#" class="menu-list">
						<span class="title">Collection</span>
					</a>
				</li>
				
				<li>
					<a href="#" class="menu-list">
						<span class="title">Transaction</span>
					</a>
				</li>
				
				
				
			</ul>
			  
		 </li>
		 
		 -->
		 
		 

		<li>
			<a href="#">
				<i class="fa-check-square-o"></i>
				<span class="title">POS</span>
			</a>
			
			<ul>
			
				<!--<li>
					<a href="#page-pos-start-transaction" class="menu-list">
						<span class="title">Start Transaction</span>
					</a>
				</li>-->
				
				<!--<li>
					<a href="#page-pos-start-transaction-pickup" class="menu-list">
						<span class="title">Start Pick-up Transaction</span>
					</a>
				</li>-->
				
				
				<li>
					<a href="#page-pos-suspend-transaction" class="menu-list">
						<span class="title">Suspend Transaction</span>
					</a>
				</li>
				
			
				<li>
					<a href="#page-pos-pending-void-transaction" class="menu-list">
						<span class="title">Pending Void Transaction</span>
					</a>
				</li>
				
				<li>
					<a href="#page-pos-unpaid-transaction" class="menu-list">
						<span class="title">Unpaid Transaction</span>
					</a>
				</li>
				
				<li>
					<a href="#page-pos-paid-transaction" class="menu-list">
						<span class="title">Paid Transaction</span>
					</a>
				</li>
				
				
				
			</ul>
			  
		 </li>		 
		 
		 <li>
			<a href="#">
				<i class="fa-bookmark"></i>
				<span class="title">Customer</span>
			</a>
			
			<ul>
				<li>
					<a href="#page-customer-list" class="menu-list">
						<span class="title">List</span>
					</a>
				</li>
		
			</ul>
			  
		 </li>
		 
		 
		 <li>
			<a href="#">
				<i class="fa-file"></i>
				<span class="title">Reports</span>
			</a>
			
			<ul>
			
				<li>
					<a href="#page-report-collection" class="menu-list">
						<span class="title">Collection</span>
					</a>
				</li>
				
				<li>
					<a href="#page-report-transaction" class="menu-list">
						<span class="title">Transaction</span>
					</a>
				</li>
				
				<li>
					<a href="#page-report-laundry-due" class="menu-list">
						<span class="title">Laundry Dues</span>
					</a>
				</li>
				
				<li>
					<a href="#page-report-sales" class="menu-list">
						<span class="title">Sales</span>
					</a>
				</li>
	
			 </ul>		
			
		 </li>
		 
		


';



//menu_list
$admin_menu_list = '

		<li>
			<a href="#">
				<i class="fa-dashboard"></i>
				<span class="title">Dashboard</span>
			</a>
			
			<ul>
			
				<li>
					<a href="#page-dashboard" class="menu-list">
						<span class="title">Sales</span>
					</a>
				</li>
				
				<!--
				<li>
					<a href="#page-dashboard-production" class="menu-list">
						<span class="title">Productions</span>
					</a>
				</li>
				-->
							
				<li>
					<a href="#page-dashboard-sms" class="menu-list">
						<span class="title">SMS</span>
					</a>
				</li>
				
			</ul>
			  
		 </li>
		 
		 <li>
			<a href="#">
				<i class="fa-check-square-o"></i>
				<span class="title">POS</span>
			</a>
			
			<ul>
			
				<li>
					<a href="#page-pos-void-request" class="menu-list">
						<span class="title">Void Request</span>
					</a>
				</li>
				
				<li>
					<a href="#page-pos-paid-transaction" class="menu-list">
						<span class="title">Paid Transaction</span>
					</a>
				</li>
				
				<li>
					<a href="#page-pos-unpaid-transaction" class="menu-list">
						<span class="title">Unpaid Transaction</span>
					</a>
				</li>
				
			</ul>
			  
		 </li>
		 
		 
		 <li>
		 	<a href="#page-store-list" class="menu-list">
				<i class="fa-home"></i>
				<span class="title">Store</span>
			</a>
		 </li>
		 
		 
		 <li>
			<a href="#">
				<i class="fa-cogs"></i>
				<span class="title">Setup</span>
			</a>
			<ul>
				
				
				
				<li>
					<a href="#page-setup-main-service" class="menu-list">
						<i class="entypo-flow-line"></i>
						<span class="title">Main Services</span>
					</a>
				</li>
				
				<li>
					<a href="#page-setup-main-service-category" class="menu-list">
						<i class="entypo-flow-line"></i>
						<span class="title">Services Category</span>
					</a>
				</li>
				
				<li>
					<a href="#page-setup-service-and-price" class="menu-list">
						<i class="entypo-flow-line"></i>
						<span class="title">Service Sub-Category</span>
					</a>
				</li>
				
				
				<li>
					<a href="#page-setup-item" class="menu-list">
						<i class="entypo-flow-line"></i>
						<span class="title">Item</span>
					</a>
				</li>
				
				<li>
					<a href="#page-setup-discount-and-markup" class="menu-list">
						<i class="entypo-flow-line"></i>
						<span class="title">Discount and Mark-up</span>
					</a>
				</li>
				
				<li>
					<a href="#page-setup-client" class="menu-list">
						<i class="entypo-flow-line"></i>
						<span class="title">Client</span>
					</a>
				</li>
				
			</ul>
		</li>
		 
		 
				 
		 <li>
			<a href="#">
				<i class="fa-users"></i>
				<span class="title">Users</span>
			</a>
			
			<ul>
				<li>
					<a href="#page-user-admin" class="menu-list">
						<span class="title">Admin</span>
					</a>
				</li>
				
				<li>
					<a href="#page-user-store" class="menu-list">
						<span class="title">Store</span>
					</a>
				</li>
		
			 </ul>
			  
		 </li>

		 <li>
			<a href="#">
				<i class="fa-file"></i>
				<span class="title">Reports</span>
			</a>
			
			<ul>
			    <li>
					<a href="#page-report-collection" class="menu-list">
						<span class="title">Collection</span>
					</a>
				</li>
				
                <li>
					<a href="#page-report-transaction" class="menu-list">
						<span class="title">Transaction</span>
					</a>
				</li>
				
				<li>
					<a href="#page-report-laundry-due" class="menu-list">
						<span class="title">Laundry Dues</span>
					</a>
				</li>
				
				<li>
					<a href="#page-report-sales" class="menu-list">
						<span class="title">Sales</span>
					</a>
				</li>
				
				
				
	
			 </ul>		
			
		 </li>
		 
		 
		 <li>
		 	<a href="../smschatapp" target="_blank">
				<i class="fa-mobile-phone"></i>
				<span class="title">SMS Chat App</span>
			</a>
		 </li>
		 
		
		 

';



$user_dropdown_menu = '

			<li>
			<a href="#page-profile" class="menu-list">
				<i class="fa-user"></i>
				Profile
			</a>
			</li>
			
			<li>
				<a href="#help">
					<i class="fa-info"></i>
					Help
				</a> 
				
			</li>
			
			<li class="last">
				<a href="../logout.php">
					<i class="fa-lock"></i>
					Logout
				</a>
			</li>

'; 






$resp = array(
'accessGranted' => '',
'name' => '',
'role' => '',
'main_menu_list' => '',
'user_dropdown_menu' => '',
'error_message' => ''
); // For ajax response





if(isset($_POST['do_login'])) {
	
	
		$given_username = $_POST['username'];
		$given_password = $_POST['passwd'];
		
		
		$query = "CALL `SP-user_login`('$given_username', '$given_password')";
		$result = mysqli_query($link,$query);
		
		if($row = mysqli_fetch_array($result)) {
			
			
			$status = $row['status'];
			
			if ($status == 'active') {
				
				$_SESSION['user_id'] = $row['user_id'];
				$_SESSION['store_id'] = $row['store_id'];
				$_SESSION['user_type'] = $row['user_type'];
				$_SESSION['account_name'] = $row['name'];
				$_SESSION['store_name'] = $row['store_name'];
				
				$_SESSION['store_code'] = $row['store_code'];
				$_SESSION['surcharge_store'] = $row['surcharge_store'];
				$_SESSION['surcharge_pickup'] = $row['surcharge_pickup'];
				
				$resp['accessGranted'] = true;
				
				$resp['account_name'] = $row['name'];
				$resp['user_type'] = $row['user_type'];
				$resp['store_name'] = $row['store_name'];
			
			
				$user_type = $row['user_type'];
			
				if ($user_type == "ADMIN") {
					
					$resp['main_menu_list'] = $admin_menu_list;
					$resp['user_dropdown_menu'] = $user_dropdown_menu;
					
					
				} elseif ($user_type == "STORE") {
					
					$resp['main_menu_list'] = $store_menu_list;
					$resp['user_dropdown_menu'] = $user_dropdown_menu;
					
					
				}
				
				
				
				
				
			} else {
				
				$resp['error_message'] = "Account has been set to inactive. Please Contact your administrator";
				
				
			}
			
		
			
		} else {
			
			
			$resp['error_message'] = "Invalid Username or Passowrd";
			
			
		}
	
	
	echo json_encode($resp);
	
	
	
}






?>