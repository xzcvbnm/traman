<?php

include 'pages/_common/ajax/central_config.php';


/*
This demo shows how to handle requests from SMS Gateway with full UTF-8 support and send reply SMS back
*/

// Get cURL resource
$curl = curl_init();
// Set some options - we are passing in a useragent too here
curl_setopt_array($curl, array(
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_URL => 'https://semysms.net/api/3/inbox_sms.php?token=d07a3d6f13d922029241f18fc43f65c7&device=69417',
));
// Send the request & save response to $resp
$resp = curl_exec($curl);
// Close request to clear up some resources

$json_data = json_decode($resp);

$data = $json_data->data;
$arrayLength = count($data);


for ($i = 0; $i < $arrayLength; $i++) {


    //echo json_encode($data[$i]);

    $id = mysqli_real_escape_string($link, $data[$i]->id);
    $phone = mysqli_real_escape_string($link, $data[$i]->phone);
    $date = mysqli_real_escape_string($link, $data[$i]->date);
    $msg = mysqli_real_escape_string($link, $data[$i]->msg);

    $query = "INSERT INTO sms_data_listen (id, phone, text, record_datetime)
VALUES ('$id', '$phone', '$msg', NOW())";


    mysqli_query($link, $query);


    /*

    foreach($data[$i] as $value) {
        echo $value;
    }
    */

}


curl_close($curl);


/*

 //setup PHP UTF-8 stuff
setlocale(LC_CTYPE, 'en_US.UTF-8');
mb_internal_encoding("UTF-8");
mb_http_output('UTF-8');


//read parameters from HTTP Get URL
$phone = $_GET["phone"];
$smscenter = $_GET["smscenter"];
$text_utf8 = rawurldecode($_GET["text"]);

//if parameters are not present in HTTP url, they can be also present in HTTP header
$headers = getallheaders();
if (empty($phone)) {
        $phone = $headers["phone"];
}
if (empty($smscenter)) {
        $smscenter = $headers["smscenter"];
}
if (empty($text_utf8)) {
        $text_utf8 = rawurldecode($headers["text"]);
}



$c_phone = mysqli_real_escape_string($link,$phone);
$c_smscenter = mysqli_real_escape_string($link,$smscenter);
$c_text_utf8 = mysqli_real_escape_string($link,$text_utf8);


$random = md5(uniqid(rand(), true));

$query = " CALL `SP-sms-save_receive`('$c_phone', '%$c_smscenter', '$c_text_utf8');";


$result = mysqli_query($link,$query)or die(mysqli_error($link));

*/


/*
//create reply SMS
$reply_utf8 = mb_strtoupper($text_utf8); // mare reply message uppercased input message

//write reply to HTTP header
$reply_header = rawurlencode($reply_utf8);
header('Content-Type: text/html; charset=utf-8');
header("text: $reply_header"); //if you don't want reply sms, comment out this this line
*/


// Debug outputs:
//echo "phone = $phone\n";
//echo "smscenter = $smscenter\n";
//echo "text_utf8 = $text_utf8\n";
//echo "reply_utf8 = $reply_utf8\n";
?>